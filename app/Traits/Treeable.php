<?php


namespace App\Traits;


trait Treeable
{
    public static function getTree()
    {
        $all = static::query()->orderBy('order', 'asc')->get();
        $parentMap = [];
        foreach ($all as $e) {
            if (isset($e->displayName)) {
                $e->name = $e->displayName;
            }
            $parentMap[$e->parentId] [] = $e;
        }

        $tree = [];
        foreach ($all as $e) {
            $e->children = $parentMap[$e->id] ?? [];
        }

        foreach ($all as $e) {
            if ($e->parentId == 0) {
                $tree[] = $e;
            }
        }

        return $tree;
    }
}
