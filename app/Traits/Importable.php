<?php

namespace App\Traits;

use App\Utils\ExcelHelper;
use PhpOffice\PhpSpreadsheet\IOFactory;

trait Importable
{
    public function importXlsx(string $inputFileName)
    {
        return ExcelHelper::convertToArray($inputFileName);
    }
}
