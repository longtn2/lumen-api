<?php

namespace App\Traits;

use App\Utils\ExcelHelper;
use Illuminate\Support\Collection;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

trait Exportable
{
    protected function exportHTML($entries)
    {
        $view = view('exports.index', [
            'toExports' => $this->toExports,
            'entries' => $entries,
        ])->toHtml();
        echo $view;

        die;
    }

    /**
     * @param array| Collection $entries
     *
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    protected function exportXlsx($entries)
    {
        ExcelHelper::export($this->toExports, $entries);
    }

}
