<?php

use OmiCare\Utils\Cache;
use OmiCare\Utils\DB;
const DISCOUNT_TYPE_VND = 1;
const DISCOUNT_TYPE_PERCENT = 2;


function email_image_url($path)
{
    return 'https://www.omipharma.vn/'.ltrim($path, '/');
}

function isIpVN($ip = null)
{
    if (!$ip) {
        $ip = request()->ip();
    }

    $e = DB::selectOne('SELECT * FROM ipinfo WHERE
to_int > INET_ATON(?)
AND from_int < INET_ATON(?)', [$ip, $ip]);

    return (bool) $e;
}

function image_url($path): string
{
    if (substr($path, 0, 8) === 'https://' || substr($path, 0, 7) === 'http://') {
        return $path;
    }

    return config('app')['url'].$path;
}

function thumb_url($path): string
{
    if (substr($path, 0, 8) === 'https://' || substr($path, 0, 7) === 'http://') {
        return $path;
    }

    return config('app')['image_url'].$path;
}

function get_thumb_product($images): string
{
    if (!empty($images)) {
        if (is_string($images)) {
            $images = json_decode($images);
        }

        if (is_array($images) && count($images)) {
            $thumb = $images[0]['url'] ?? null;

            if ($thumb) {
                return thumb_url($thumb);
            }
        }
    }

    return '';
}

function randomNumber($length)
{
    $result = '';

    for ($i = 0; $i < $length; ++$i) {
        $result .= mt_rand(0, 9);
    }

    return $result;
}

function isTrustedIP(): bool
{
    $ip = request()->ip();

    if ($ip === '127.0.0.1' || $ip === '::1') {
        return true;
    }

    $rKey = 'common.trusted_ip';
    $trustedIPs = Cache::get($rKey);

    if ($trustedIPs) {
        return isset($trustedIPs[$ip]);
    }

    $trustedIPs = [];
    $IPs = DB::select('SELECT * FROM trusted_ips');

    foreach ($IPs as $i) {
        $trustedIPs[$i->ip] = true;
    }

    Cache::set($rKey, $trustedIPs);

    return isset($trustedIPs[$ip]);
}

function getDayName($time): string
{
    $dayNames = [
        'Mon' => 'Thứ 2',
        'Tue' => 'Thứ 3',
        'Wed' => 'Thứ 4',
        'Thu' => 'Thứ 5',
        'Fri' => 'Thứ 6',
        'Sat' => 'Thứ 7',
        'Sun' => 'CN',
    ];

    return $dayNames[date('D', $time)] ?? '';
}

function str_remove_accent(string $str, bool $lower = false): string
{
    $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
    $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
    $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
    $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
    $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
    $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
    $str = preg_replace('/(đ)/', 'd', $str);
    $str = preg_replace('/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/', 'A', $str);
    $str = preg_replace('/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/', 'E', $str);
    $str = preg_replace('/(Ì|Í|Ị|Ỉ|Ĩ)/', 'I', $str);
    $str = preg_replace('/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/', 'O', $str);
    $str = preg_replace('/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/', 'U', $str);
    $str = preg_replace('/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/', 'Y', $str);
    $str = preg_replace('/(Đ)/', 'D', $str);
    //$str = str_replace(" ", "-", str_replace("&*#39;","",$str));
    return $lower ? strtolower($str) : $str;
}

function calculateDiscountValue($price, $discountValue = 0, $discountType = 'value')
{
    if ($discountValue > 0) {
        if ($discountType === 'value') {
            if ($discountValue < 1000) {
                throw new \Exception('Giá trị tiền khuyến mại phải >= 1000đ');
            }

            $price -= $discountValue;
        } elseif ($discountType === '%') {
            if ($discountValue <= 0 || $discountValue > 100) {
                throw new \Exception('% Khuyến mại phải từ 1 đến 100');
            }

            $percent = $discountValue / 100;
            $price = $price - ceil($price * $percent);
        }
    }

    return max(0, $price);
}

function excelColumnFromIndex($num)
{
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);

    if ($num2 > 0) {
        return excelColumnFromIndex($num2 - 1).$letter;
    }

    return $letter;
}

function getPriceByPercent($price, $percentValue): int {
    if ($percentValue > 100 || $percentValue < 0) {
        $percentValue = 0;
    }

    $percent = $percentValue/100;

    return intval($price - $price * $percent);
}

function appDateFormat($input)
{
    return date('d/m/Y', strtotime($input));
}

function appDateTimeFormat($input)
{
    return date('d/m/Y H:i', strtotime($input));
}

function currentBranch(): \App\Models\Branch
{
    $branchId = request()->currentBranchId();
    $branch = \App\Models\Branch::find($branchId);

    if (!$branch) {
        throw new \LogicException("Vui lòng chọn chi nhánh");
    }

    return $branch;
}

function getClassName($object): string
{
    $tmp = explode('\\', get_class($object));

    return last($tmp);
}

function isValidEmail($var): bool
{
    if (!is_string($var)) {
        return false;
    }

    return filter_var($var, FILTER_VALIDATE_EMAIL);
}
