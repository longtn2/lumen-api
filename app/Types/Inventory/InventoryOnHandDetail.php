<?php

namespace App\Types\Inventory;

/**
 * @property int $current
 * @property int $others
 * @property int $all
 */
class InventoryOnHandDetail implements \JsonSerializable
{
    public $current = 0;
    public $others = 0;
    public $all = 0;


    public function jsonSerialize()
    {
        return [
            'current' => $this->current,
            'others' => $this->others,
            'all' => $this->all
        ];
    }
}
