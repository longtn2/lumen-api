<?php

namespace App\Types;
/**
 * @property int $quantity
 * @property DiscountType $discount;
 */
class QuantityDiscountType
{
    public $quantity;
    public $discount;

    public function __construct($quantity, $discount)
    {
        $this->quantity = (int) $quantity;
        $this->discount = new DiscountType($discount['discountValue'], $discount['discountType']);
    }

}
