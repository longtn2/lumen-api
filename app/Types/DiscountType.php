<?php

namespace App\Types;

/**
 * @property $discountValue int
 * @property $discountType  int
 */
class DiscountType
{
    public $discountValue;
    public $discountType;

    public function __construct($discountValue, $discountType = DISCOUNT_TYPE_VND)
    {
        $this->discountValue = $discountValue;
        $this->discountType = (int) $discountType;
    }

    public function getDiscountExplain(): string
    {

        if ($this->discountType === DISCOUNT_TYPE_VND) {
            $explain = 'Giảm giá ' . number_format($this->discountValue) . ' VNĐ';
        } else if ($this->discountType === DISCOUNT_TYPE_PERCENT) {
            $explain = 'Giảm giá ' . $this->discountValue . '%';
        }

        return $explain;
    }

    public function getDiscountAmount($basePrice): int
    {
        $discount = 0;

        if ($this->discountType === DISCOUNT_TYPE_VND) {
            $discount = $this->discountValue;
        } else if ($this->discountType === DISCOUNT_TYPE_PERCENT) {
            $percent = $this->discountValue/100;
            $discount = (int) ($basePrice * $percent);
        }

        return $discount;
    }
}
