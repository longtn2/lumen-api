<?php

namespace App\Types;

/**
 * @property boolean $isRoot
 * @property boolean $isAdmin
 * @property array   $permissions
 */
class PermissionInfo
{
    public $isRoot = false;
    public $isAdmin = false;
    public $permissions = [];
}
