<?php

namespace App\Types\Social;

class SocialUser
{
    public $id;
    public $email;
    public $name;
    public $avatar;
    public $provider;

    public function __construct(string $provider, array $data)
    {
        $this->provider = $provider;
        $this->id = $data['id'];
        $this->email = $data['email'];
        $this->name = $data['name'];
        $this->avatar = $data['avatar'];
    }
}
