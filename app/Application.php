<?php

namespace App;

use App\Services\Permission\AppPermission;
use OmiCare\Http\Request;
use OmiCare\Http\Response;

class Application extends \OmiCare\Application
{

    protected function getWhiteListClasses(): array
    {
        return config('whitelist');
    }

    protected function register()
    {
        $this->setPermissionValidation(new AppPermission());
        parent::register();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }

    protected function handleException(\Throwable $e, Request $request): Response
    {
        return parent::handleException($e, $request);
    }
}
