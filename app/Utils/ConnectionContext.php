<?php

namespace App\Utils;

class ConnectionContext
{
    private static $connection;

    public static function getConnection(): string
    {
        if (static::$connection) {
            return static::$connection;
        }

        if (php_sapi_name() !== 'cli') {
            static::$connection = request()->get('_connection', 'default');
        } else {
            static::$connection = 'default';
        }

        return static::$connection;
    }

    public static function run(string $connectionName, $callback)
    {
        $original = static::$connection;
        static::$connection = $connectionName;
        $ret = $callback();
        static::$connection = $original;

        return $ret;
    }
}
