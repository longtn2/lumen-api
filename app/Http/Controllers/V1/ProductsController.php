<?php

namespace App\Http\Controllers\V1;


use App\Models\Branch;
use App\Models\Category;
use App\Models\History;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderSupplierDetail;
use App\Models\Product;
use App\Http\Controllers\AuthenticatedController;
use App\Models\ProductBatch;
use App\Models\ProductBranchMeta;
use App\Models\ProductCombo;
use App\Models\ProductUnit;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use App\Models\ReturnDetail;
use App\Models\ReturnOrder;
use App\Models\Returns;
use App\Models\TransferDetail;
use App\Models\Transfer;
use App\Models\Unit;
use App\Utils\ExcelHelper;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;
use App\Models\SaleChannel;
use App\Models\SyncSaleChannels;
use App\Models\OmiSyncLink;
use phpDocumentor\Reflection\Types\True_;
use OmiCare\Utils\Cache;

class ProductsController extends AuthenticatedController
{

    public function redisSet(Request $req){
        $redis = app('redis')->set('test.redis','1111111111111');
        dd($redis);
    }

    public function redisGet(Request $req){
        $redis = app('redis')->get('test.redis');
        dd($redis);
    }

    public function redisPublish(Request $req){
        // dd(1);
        app('redis')->publish('LumenRedis','Testing, lumen redis');
        // dd($redis);
    }

    public function redisSub(Request $req){
        $redis = app('redis')->subscribe('LumenRedis',function ($mess){
            dd($mess);
        });
        dd($redis);
    }

    public function redisPubSub(Request $req){
        $config = config('redis');
        dump($config);
        $redis = app('redis');
        dump($redis);
        $ps = $redis->pubSub("channels");
        dd($ps);
    }
    /**
    * @uri /v1/products/show
    * @return array
    */
    public function show(Request $req)
    {
        // dd(1);
        $id = $req->id;
        $entry = Product::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        if (!is_array($entry->images)) {
            $entry->images = [];
        }

        $productUnits = ProductUnit::where('productId', $entry->id)
            ->orderBy('conversionValue', 'asc')
            ->get();

        /**
         * @var Inventory $inventory
         */
        $inventories = Inventory::query()
            ->selectRaw('id, onHand, branchId, productUnitId,conversionValue, SUM(onHand*conversionValue) as quantity')
            ->with(['branch:id,name'])
            ->where('productId', $entry->id)
            ->groupBy('branchId')
            ->get();

        $actualInventory = Inventory::query()
            ->where('productId', $id)
            ->where('refTable', '!=', Inventory::REF_CODE_ORDER_DETAIL)
            ->selectRaw('id, onHand, branchId, productId, SUM(onHand*conversionValue) as quantity')
            ->groupBy('branchId')
            ->pluck('quantity', 'branchId');

        $inventoryWithBatches = Inventory::query()->where('productId', $entry->id)
            ->with('productBatch')
            ->paginate(2, ['*'], 'page', $req->getInt('page'));

        $productBranchMeta = ProductBranchMeta::query()
            ->where('productId', $entry->id)
            ->where('branchId', $req->currentBranchId())
            ->first();

        if (!$productBranchMeta) {
            $productBranchMeta = new ProductBranchMeta();
            $productBranchMeta->quantityThreshold = 0;
            $productBranchMeta->productId = $entry->id;
            $productBranchMeta->branchId = $req->currentBranchId();
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                    'productUnits'   => $productUnits,
                    'inventories' => $inventories,
                    'actualInv' => $actualInventory,
                    'inventoryWithBatches' => [
                        'data' => $inventoryWithBatches->items(),
                        'paginate' => [
                            'currentPage' => $inventoryWithBatches->currentPage(),
                            'lastPage' => $inventoryWithBatches->lastPage()
                        ]
                    ],
                    'comboProducts' => ProductCombo::getComboProduct($entry->id),
                    'fields' => $this->getDisplayFields(),
                    'productBranchMeta' => $productBranchMeta,

                ]
        ];
    }

    public function showUnitsAction(Request $req)
    {
        $id = $req->product_id;
        $productUnits = ProductUnit::where('productId', $id)
            ->orderBy('conversionValue', 'asc')
            ->get();

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => $productUnits
        ];
    }

    public function sendQuantityAction($id, $branchID){
        $total = 0;
        $tranferID = [];
        $tranferDetails = TransferDetail::where('productId', $id)->get();
        foreach ($tranferDetails as $value) {
            // $total = $total + $value -> sendQuantity;
            $tranferID [] = $value -> transferId;
        }

        $transfer = Transfer::where('fromBranchId', $branchID)->get();
        foreach ($transfer as $item) {
            if(isset($item ->id) && in_array($item -> id, $tranferID )) {
               $total = $total + $item -> totalSendQuantity;
            }
        }

       return $total;
    }
    public function takeQuantityAction($productId, $currentBranchID){
        $transfers = Transfer::where('toBranchId', $currentBranchID)->where('status', 2)->get();
        return $this->getQuantityAction($transfers, $productId);
    }
    public function getQuantityAction($transfers, $productId){
        $total = 0;
        foreach ($transfers as $transfer){
            $transferDetail = TransferDetail::where('productId', $productId)->where('transferId', $transfer->id)->get();
            foreach ($transferDetail as $value) {
                $total = $total + $value -> sendQuantity;
            }
        }
        return $total;
    }
    /**
    * @uri /v1/products/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        //      return ['code' => 405, 'message' => 'Method not allow'];
            if (isset($req->multi)){
                $entry = $this->removeManyProduct($req->data, $req->getInt('page'));
            }else{
                $id = $req->id;
                $entry = Product::find($id);
                if (!$entry) {
                    return [
                        'code' => 404,
                        'message' => 'Entry not found'
                    ];
                }
                $entry->delete();
            }
            return [
                'code' => 200,
                'entry' => $entry['entry'],
                'message' => 'Đã xóa',
                'removed'=>$entry['removed'],
                'paginate' => [
                    'currentPage' => $entry['entry']->currentPage(),
                    'lastPage' => $entry['entry']->lastPage(),
                ]
            ];
    }
    public function removeManyProduct($data, $page){
        $productCode = [];
        $removed = [];
        foreach ($data as $prod){
            $countPurchaseOrderDetail = PurchaseOrderDetail::where('productCode', $prod['code'])->count();
            $countInvoicesDetail = InvoiceDetail::where('productCode', $prod['code'])->count();
            $countOrderDetail = OrderDetail::where('productCode', $prod['code'])->count();
            $countReturnDetail = ReturnDetail::where('productCode', $prod['code'])->count();
            $countTransferDetail = TransferDetail::where('productCode', $prod['code'])->count();
            $countInventories = Inventory::where('productId', $prod['id'])->count();
            $countOrderSupplierDetail = OrderSupplierDetail::where('productId', $prod['id'])->count();
            $countProductBatch = ProductBatch::where('productId', $prod['id'])->count();
            $countProductBranchMeta = ProductBranchMeta::where('productId', $prod['id'])->count();
            $countProductCombo = ProductCombo::where('productId', $prod['id'])->count();
            if($countPurchaseOrderDetail > 0 || $countInvoicesDetail > 0 || $countOrderDetail > 0 || $countReturnDetail > 0 || $countTransferDetail > 0 || $countInventories > 0 || $countOrderSupplierDetail > 0 || $countProductBatch > 0 || $countProductBranchMeta > 0 || $countProductCombo > 0){
                $productCode[] = [ $prod['code'] ];
            }else{
                $removeProduct = Product::where('code', $prod['code'])->delete();
                array_push($removed, $prod['code']);
            }
        }
        $entry = Product::query()->whereIn('code', $productCode)->paginate(10, ['*'], 'page', $page);
        return [
          'entry'=>$entry,
          'removed'=>$removed,
        ];
    }


    public function saveProductBranchMetaAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return [
                'code' => 405
            ];
        }
        $productId = $request->get('productId');

        if(isset($request->multi) && $request->multi == true){
            foreach ($productId as $item) {
                $data = $request->get('entry');
                $branch= currentBranch();
                $product = Product::find($item);
                if (!$product) {
                    return [
                        'code' => 404,
                        'message' => 'Không tìm thấy sản phẩm'
                    ];
                }
                $productBranchMeta = ProductBranchMeta::query()
                    ->where('productId', $item)
                    ->where('branchId', $branch->id)
                    ->first();

                if (!$productBranchMeta) {
                    $productBranchMeta = new ProductBranchMeta();
                    $productBranchMeta->branchId = $branch->id;
                    $productBranchMeta->productId = $item;
                }

                $productBranchMeta->fill($data);
                $productBranchMeta->save();
            }
        }
        else {
            $data = $request->get('entry');
            $branch= currentBranch();
            $product = Product::find($productId);
            if (!$product) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy sản phẩm'
                ];
            }
            $productBranchMeta = ProductBranchMeta::query()
                ->where('productId', $productId)
                ->where('branchId', $branch->id)
                ->first();

            if (!$productBranchMeta) {
                $productBranchMeta = new ProductBranchMeta();
                $productBranchMeta->branchId = $branch->id;
                $productBranchMeta->productId = $productId;
            }

            $productBranchMeta->fill($data);
            $productBranchMeta->save();

        }


        return [
            'code' => 200,
            'message' => 'Đã lưu'
        ];
    }

    public function saveComboProductsAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return [
                'code' => 405
            ];
        }
        DB::beginTransaction();
        $products = $request->get('products');
        $parentProductId = $request->get('parentProductId');
        if (isset($request->multi)){
            foreach ($parentProductId as $item){
                ProductCombo::where('parentProductId', $item)->delete();
                $this->saveProduct($item, $products);
            }
        }else{
            $this->saveProduct($parentProductId, $products);
        }
        DB::commit();
        return  [
            'code'  => 200,
            'message' => 'Đã lưu'
        ];
    }

    public function saveProduct($parentProductId, $products){
        $bulkData = [];
        $now =  date('Y-m-d H:i:s');
        foreach ($products as $product) {
            if(!isset($product['parentProductId'])) {
                $bulkData[] = [
                    'quantity' => $product['quantity'],
                    'parentProductId' => $parentProductId,
                    'productId' => $product['id'],
                    'productUnitId' => $product['productUnitId'],
                    'createdAt' => $now,
                    'updatedAt' => $now
                ];
            }else{
                ProductCombo::query()->where('id', $product['id'])->update([
                    'quantity' => $product['quantity'],
                ]);
            }
        }
        ProductCombo::query()->insert($bulkData);
    }

    /**
    * @uri /v1/products/save
    * @return array
    */

    public function saveMultipleAction(Request $req) {
        $id = [];
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        $uniqRule = ['products'];
        if (isset($data['id'])) {
            $uniqRule = ['products', 'id', $data['id']];
        }
        V::make($data)
            //->rule(V::required, 'code')->message('Vui lòng nhập mã hàng hóa')
            ->rule(V::unique, 'code', $uniqRule)->message('Vui lòng chọn mã khác')
            ->rule(V::min, 'basePrice', 0)->message('Giá bán phải >= 0')
            ->rule(V::min, 'capitalPrice', 0)->message('Giá vốn phải >= 0')
            //->rule(V::arrayLengthBetween, 'images', 1, 3)->message('Vui lòng chọn ảnh')
            ->validOrFail();


        /**
        * @var Product $entry
        */

        $id = $data['id'];

        foreach ($id as $key => $item) {
            $data['id'] = $item;
            $this->updateAction($req, $item, $data);
        }
        return [
            'code' => 200,
            'message' => 'Đã lưu'
        ];

    }

    public function updateAction(Request $req, $id, $data){

        $productUnits = $req->get('productUnits');
        $redirect = true;
        if (isset($id)) {
            $entry = Product::find($id);
            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Product();
            $redirect = true;
        }

        $entry->fill($data);

        DB::beginTransaction();
        if (!isset($data['id'])) {
            $entry->saveHistory(History::ACTION_CREATED);
        } else {
            $entry->saveHistory(History::ACTION_UPDATED);
        }
        $entry->save();

        if (!$entry->code) {
            $entry->code = 'SP' . $entry->id;
            $entry->save();
        }

        //Kiểm tra có thêm đơn vị cho hàng hoá mới cho xoá và thêm mới
        if(isset($productUnits) && $productUnits!=null){
            $productAttribute = ProductUnit::where('productId', $id) -> delete();
            foreach ($productUnits as $pu) {
                if ($pu['deleted']) {
                    if (isset($pu['id'])) {
                        ProductUnit::where('id', $pu['id'])->delete();
                    }
                }
                else {
                    if (isset($pu['id'])) {
                        $productAttribute = ProductUnit::find($pu['id']);
                    }
                    else {
                        $productAttribute = new ProductUnit();
                        $productAttribute->productId = $entry->id;
                        $productAttribute->unitId = $pu['unitId'];
                    }
                    $productAttribute->productName = $entry->name;
                    $productAttribute->fill($pu);
                    $productAttribute->unitName = Unit::getUnitName($pu['unitId']);
                    $productAttribute->save();
                }
            }
        }
        DB::commit();
        $productUnits = ProductUnit::where('productId', $entry->id)
            ->orderBy('conversionValue', 'asc')
            ->get();
        return $redirect;
    }
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        $productUnits = $req->get('productUnits');
        $uniqRule = ['products'];
        if (isset($data['id'])) {
            $uniqRule = ['products', 'id', $data['id'] ];
        }
        V::make($data)
            ->rule(V::required, 'code')->message('Vui lòng nhập mã hàng hóa')
            ->rule(V::unique, 'code', $uniqRule)->message('Vui lòng chọn mã khác')
            ->rule(V::required, 'name')->message('Vui lòng nhập tên hàng hóa')
            ->rule(V::required, 'type')->message('Đây là trường bắt buộc. Vui lòng kiểm tra lại')
            ->rule(V::min, 'basePrice', 0)->message('Giá bán phải >= 0')
            ->rule(V::min, 'capitalPrice', 0)->message('Giá vốn phải >= 0')
            //->rule(V::arrayLengthBetween, 'images', 1, 3)->message('Vui lòng chọn ảnh')
            ->validOrFail();


        /**
        * @var Product $entry
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Product::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Product();
        }

        $entry->fill($data);

        DB::beginTransaction();
        if (!isset($data['id'])) {
            $entry->saveHistory(History::ACTION_CREATED);
        } else {
            $entry->saveHistory(History::ACTION_UPDATED);
        }
        $entry->save();

        if (!$entry->code) {
            $entry->code = 'SP' . $entry->id;
            $entry->save();
        }

        foreach ($productUnits as $pu) {

            if ($pu['deleted']) {
                if (isset($pu['id'])) {
                    ProductUnit::where('id', $pu['id'])->delete();
                }
            } else {

                if (isset($pu['id'])) {
                    $productAttribute = ProductUnit::find($pu['id']);
                } else {
                    $productAttribute = new ProductUnit();
                    $productAttribute->productId = $entry->id;
                    $productAttribute->unitId = $pu['unitId'];
                }

                $productAttribute->productName = $entry->name;
                $productAttribute->fill($pu);
                $productAttribute->unitName = Unit::getUnitName($pu['unitId']);
                $productAttribute->save();
            }
        }

        DB::commit();

        $productUnits = ProductUnit::where('productId', $entry->id)
            ->orderBy('conversionValue', 'asc')
            ->get();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/products/index' : false,
            'data' => [
                'entry' => $entry->toArray(),
                'notFilled' => $entry->notFilledFields,
                'productUnits' => $productUnits
            ]
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/products/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $branchID = $req -> branchID;
        $fields = $this->getDisplayFields();
        $fieldSelects = [];
        $fieldDisplays = [];
        //Show/Hide collum title table
        //----------------------------------------------
            foreach ($fields as $field) {
                if (!empty($field['isColumn']) && !empty($field['field'])) {
                    $fieldSelects[] = $field['field'];
                }

                if (!empty($field['relatedField'])) {
                    if (is_array($field['relatedField'])) {
                        $fieldSelects = array_merge($fieldSelects, $field['relatedField']);
                    } else {
                        $fieldSelects[] = $field['relatedField'];
                    }
                }

                if (empty($field['hide'])) {
                    $field['selected'] = empty($field['selected']) ? false : true;
                    $fieldDisplays[] = $field;
                }
            }
        //----------------------------------------------

        $categories = $req->get('categories');
        $suppliers = $req->get('suppliers');
        $inventory = $req->get('inventory');
        $created = $req->get('created_at');
        $currentBranchID = $req->currentBranchId();
        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Product::query()
            ->with('productUnits', 'productBranchMeta')
            ->with(['productBranchMeta' => function($q) use ($currentBranchID){
            $q->where('branchId', $currentBranchID);
        }])
          //  ->where('branchIDS', 'LIKE', '%'.$currentBranchID.'%')
            ->orderBy($sortField, $sortDirection);
        if ($req->keyword) {
            $query->whereSearch(['name', 'code'], $req->keyword);
        }

        if ($categories) {
            $categoryIDs = $categories ? explode(',', $categories) : [];
            $query->whereIn('categoryId', $categoryIDs);
        }

        if ($suppliers) {
            $supplierIds = explode(',', $suppliers);
            $product_ids = PurchaseOrder::query()->whereIn('supplierId', $supplierIds)
                ->selectRaw('purchase_orders.*, pod.productId as podProductId')
                ->join('purchase_order_details as pod', 'pod.purchaseOrderId', '=', 'purchase_orders.id')
                ->groupBy('podProductId')
                ->get()->pluck('podProductId');
            $query->whereIn('id',$product_ids);

        }

        if ($req->type) {
            $query->where('type', $req->type);
        }

        $action = $req->get('_action');
        $query->createdIn($created);

        if ($action == 'excel') {
            $entries = $query->limit(1000)->orderByDesc('id')->get();
        } else {
            $entries = $query->paginate(25, ['*'], 'page', $page);
        }

        $productIds = [];

        foreach ($entries as $entry) {
            $productIds[] = $entry->id;
        }

        $inventoryProductBatchMap = Inventory::getInventoryProductBatchMap($productIds, $currentBranchID);
        $inventories = Inventory::query()->whereIn('productId', $productIds)
            ->selectRaw('id, productId, SUM(onHand*conversionValue) as quantity')
            ->where('branchId', $currentBranchID)
            ->groupBy('productId')
            ->get();
        $inventoryMap = [];

        $actualInventoryMap = Inventory::query()
            ->whereIn('productId', $productIds)
            ->where('refTable', '!=', Inventory::REF_CODE_ORDER_DETAIL)
            ->where('branchId', $currentBranchID)
            ->selectRaw('id, productId, SUM(onHand*conversionValue) as quantity')
            ->groupBy('productId')
            ->pluck('quantity', 'productId');

        foreach ($inventories as $iv) {
            $inventoryMap[$iv->productId] = $iv;
        }

        $orderDetailsSumByProductUnitMap = OrderDetail::query()
            ->selectRaw('SUM(ABS(`i`.`onHand`)*ABS(`i`.`conversionValue`)) as order_details_sum, order_details.productId')
            ->join('orders as o', 'o.id', '=', 'order_details.orderId')
            ->join('inventories as i', 'i.refId', '=', 'order_details.id')
            ->where('i.refTable', Inventory::REF_CODE_ORDER_DETAIL)
            ->whereIn('order_details.productId', $productIds)
            ->where('o.branchId', $currentBranchID)
            ->whereIn('o.status', [Order::STATUS_WAIT, Order::STATUS_CONFIRMED, Order::STATUS_DELIVERY])
            ->groupBy('order_details.productId')
            ->get()
            ->pluck('order_details_sum', 'productId');

        $branches = Branch::get()->pluck('name', 'id');

        foreach ($entries as $entry) {
        // Lấy lô hàng của hàng hoá
            $productBatches = ProductBatch::query()
            ->where('productId', $entry->id)
            ->get();
            if(count($productBatches)){
                foreach ($productBatches as $key => $proBatch) {
                    $proBatch->quantityOrigin = $inventoryProductBatchMap[$proBatch->id] ?? 0;
                }
            }

            if ($entry->productBranchMeta) {
                foreach ($entry->productBranchMeta as $key => $value) {
                    $entry->quantity_min = $value['minimalQuantity'];
                    $entry->quantity_max = $value['maximumQuantity'];
                    $entry->quantity = $value['onShowRoomQuantity'];
                }

            }
            $entry->currentProductUnitId = null;
            if ($entry->productUnits->count() > 0) {
                $entry->currentProductUnitId = $entry->productUnits[0]->id ?? null;
                $entry->basePrice = $entry->productUnits[0]->price ?? null;

            }

            $entry->inventory = $inventoryMap[$entry->id] ?? null;
            if ($entry->inventory) {
                $entry->inventory->quantity = (int)$entry->inventory->quantity;
                $entry->inventory->quantityOriginal = (int)$entry->inventory->quantity;
            }

            $entry->quantityOrder = $orderDetailsSumByProductUnitMap[$entry->id] ?? null;

            if ($entry->quantityOrder && $entry->productUnits->count() > 0) {
                $entry->quantity_ordered = $entry->quantityOrder / $entry->productUnits[0]->conversionValue;
            }
            $entry->branchName = null;
            if($branches && $entry->branchIDs) {
                $branchName = [];
                foreach ($entry->branchIDs as $key => $value) {
                    if(array_key_exists($value, $branches->toArray())) {
                        if($key == 0) {
                            $branchName[] = $branches[$value];
                        } else {
                            $branchName[] = $branches[$value];
                        }
                    }
                }
                $entry->branchName = $branchName;
            }
            $actualInv = isset($actualInventoryMap[$entry->id]) && isset($entry->productUnits[0]) ? $actualInventoryMap[$entry->id] / $entry->productUnits[0]->conversionValue : null;
            $entry->actualInventory = $actualInv;
            $entry -> totalTakeQuantity = $this->takeQuantityAction($entry -> id, $currentBranchID);
            $entry -> totalSendQuantity = $this->sendQuantityAction($entry -> id, $branchID);
            $entry -> productBatches = $productBatches;
            $entry -> product_batches = $productBatches;

        }

        if ($action == 'excel') {

            (new Product())->saveHistory(History::ACTION_EXPORT);
            return [
                'code' => 200,
                'data' => $entries
            ];
        }
        $data =  [
                'fields' => $fields,
                'entries' => $entries->items(),
                'orderDetailsSumByProductUnitMap' => $orderDetailsSumByProductUnitMap,
                'actualInventoryMap' => $actualInventoryMap,
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ];
        return [
            'code' => 200,
            'data' => $data

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
                'isColumn' => true,
                'selected' => false,
                'style' => 'max-width:100px',
                'class' => 'prdID'
            ],
            [
                'field' => 'code',
                'name' => 'Mã hàng',
                'isColumn' => true,
                'selected' => true,
                'style' => 'max-width:100px',

            ],
            [
                'field' => 'name',
                'name' => 'Tên hàng',
                // 'render' => 'truncate40',
                'isColumn' => true,
                'selected' => true,
                'style' => "min-width:200px"
            ],
            [
                'field' => 'categoryName',
                'name' => 'Danh mục',
                'style' => 'min-width:140px',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'activeIngredient',
                'name' => 'Hoạt chất',
                'selected' => true,
                'style' => 'min-width:180px',
                'class' => "activeIngredient",

            ],

            [
                'field' => 'type',
                'name' => 'Tính chất',
                'selected' => true,
                'style' => 'min-width:80px',
            ],
            [
                'field' => 'manufacturer',
                'name' => 'Hãng sản xuất',
                'selected' => true,
                'style' => 'min-width:150px',
                'class' => 'manufacturer'
            ],
            [
                'field' => 'country',
                'name' => 'Nước sản xuất',
                'selected' => true,
                'style' => 'min-width:150px',
                'class' => 'country'
            ],
            [
                'field' => 'unit',
                'name' => 'Quy cách đóng gói',
                'selected' => true,
                'style' => 'min-width:170px',
                'class' => 'prdUnit'
            ],
            [
                'field' => 'description',
                'name' => 'Mô tả',
                'selected' => true,
                'style' => 'min-width:280px',
                'class' => 'description'
            ],
            [
                'field' => 'weight',
                'name' => 'Trọng lượng (gr)',
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'branches',
                'name' => 'Kinh doanh tại kho',
                'selected' => true,

            ],
            [
                'field' => 'takeQuantity',
                'name' => 'SL chưa nhận',
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'basePrice',
                'name' => 'Giá bán',
                'render' => 'number',
                'selected' => true,
                'class' => 'text-right',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'quantity_min',
                'name' => 'Tồn Kho Min',
                'render' => 'number',
                'hideSort' => true,
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'quantity',
                'name' => 'SL Trưng Bày',
                'render' => 'number',
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'inventory.quantity',
                'name' => 'Tồn Kho Hệ thống',
                'render' => 'dot',
                'defaultValue' => '0',
                'hideSort' => true,
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'actualInventory',
                'name' => 'Tồn có thể bán',
                'render' => 'dot',
                'defaultValue' => '0',
                'hideSort' => true,
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'quantity_max',
                'name' => 'Tồn Kho Max',
                'render' => 'number',
                'hideSort' => true,
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'quantity_ordered',
                'name' => 'Khách Hàng Đặt',
                'render' => 'dot',
                'hideSort' => true,
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => 'sendQuantity',
                'name' => 'SL đang chuyển',
                'hideSort' => true,
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',

            ],
            [
                'field' => '',
                'name' => 'Đặt nhà cung cấp',
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
            [
                'field' => '',
                'name' => 'Dự kiến ngày hết',
                'selected' => true,
                'class' => 'text-center',
                'style' => 'min-width:130px',
            ],
        ];

        return $fields;
    }

    public function relatedInvoiceAction(Request $req)
    {
        $productId = $req->get('productId');
        $currentBranchId = $req->currentBranchId();
        list($startTime, $endTime, $span) = $this->getDateInfo($req);
        $product = Product::find($productId);

        if (!$product) {
            return [
                'code' => 3,
                'message' => 'Sản phẩm này không tồn tại'
            ];
        }
        $invoiceItems = InvoiceDetail::query()
            ->select('id', 'invoiceId', 'productId')
            ->where('productId', $product->id)
            ->orderBy('id', 'desc')
            ->limit(50);

        if ($invoiceItems->count() === 0) {
            return [
                'code' => 100,
                'message' => 'Không có đơn hàng nào có chứa sản phẩm này!'
            ];
        }

        $invoiceIDs = $invoiceItems->pluck('invoiceId')->toArray();
        $invoices = Invoice::query()->where('branchId', $currentBranchId)
            ->whereIn('id', $invoiceIDs)
            ->where('createdAt', '>=', $startTime)
            ->where('createdAt', '<=', $endTime)
            ->orderBy('id', 'desc')
            ->get();

        return [
            'code' => 200,
            'invoices' => $invoices
        ];
    }

    private function getDateInfo(Request $request): array
    {
        $created = $request->get('created');
        $startDate = date('Y-m-d', strtotime('-30 days'));
        $endDate = date('Y-m-d');

        if ($created) {
            list($startDate, $endDate) = explode('_', $created);
        }

        $startTime = $startDate.' 00:00:00';
        $endTime = $endDate.' 23:59:59';

        $span = (strtotime($endDate) - strtotime($startDate)) / 86400;

        return [$startTime, $endTime, $span, $startDate, $endDate];
    }
    public function productInvoiceReportAction(Request $req)
    {
        $currentBranchId = $req->currentBranchId();

        $branchIds = [];

        if ($req->branch_ids) {
            $branchIds = explode(',', $req->branch_ids);
        } else {
            $branchIds[] = $currentBranchId;
        }

        $query = Invoice::query()->whereIn('branchId', $branchIds)
            ->orderBy('id', 'desc')
            ->createdIn($req->date)
            ->get();

        $invoiceIds = $query->pluck('id');

        $items = InvoiceDetail::query()
            ->selectRaw('*,SUM(quantity*price) as originalPrice, SUM(quantity*subTotal) as subPrice, SUM(quantity) as qty')
            ->whereIn('invoiceId', $invoiceIds)
            ->with(['productUnit:id,unitName', 'product:id,code,name'])
            ->groupBy('productUnitId')
            ->get();

        $returnIds = Returns::query()->whereIn('branchId', $branchIds)
            ->whereIn('invoiceId', $invoiceIds)
            ->get()->pluck('id');
        $returnDetails = ReturnDetail::query()->whereIn('returnId', $returnIds)
            ->selectRaw('*,SUM(quantity*price) as returnTotal,SUM(quantity) as returnQty')
            ->groupBy('productUnitId')
            ->get();

        $returnMapPrice = $returnDetails->pluck('returnTotal', 'productUnitId');
        $returnMapQty = $returnDetails->pluck('returnQty', 'productUnitId');

        foreach ($items as $i) {
            $i->returnQty = $returnMapQty[$i->productUnitId] ?? 0;
            $i->returnPrice = $returnMapPrice[$i->productUnitId] ?? 0;
        }


        return [
            'code' => 200,
            'data' => $items
        ];
    }

    public function importExcelAction(Request $request)
    {
        if ($_FILES['file']) {
            ini_set('memory_limit', '500M');
            set_time_limit(300);
            $filename = $_FILES['file']['tmp_name'];
            $clientExtension = $_FILES['file']['type'];
            $allowed = [
                'xls', 'xlsx'
            ];
            $info = pathinfo($_FILES['file']['name']);
            $extension = strtolower($info['extension']);
            if (!in_array($extension, $allowed)) {
                return [
                    'code' => 3,
                    'message' => 'file: ' . $extension . ' Không đúng định dạng'
                ];
            }
            $rows = ExcelHelper::importXlsx($filename, $clientExtension);
            foreach ($rows as $row) {
                if (!empty($row[0]) && !empty($row[1]) && !empty($row[2]) && !empty($row[11]) && !empty($row[15])) {
                    $product = Product::where('code', $row[0])->first();
                    $Category = Category::where('name', trim($row[3]))->first();
                    $arr = [];
                    $dataBranchs = explode('; ', trim($row[10]));
                    $BranchIDs = Branch::whereIn('name', $dataBranchs)->get();
                    foreach ($BranchIDs as $BranchID) {
                        array_push($arr, $BranchID->id);
                    }
                    $dataBranchIDs = implode(',', $arr);
                    $UnitId = Unit::where('name', trim($row[15]))->first();
                    if (!$UnitId) {
                        return $this->errorMessage();
                    }
                    if (isset($Category)) {
                        $CategoryName = $Category->name;
                    } else {
                        if ($row[3] != null) {
                            return $this->errorMessage();
                        } else {
                            $CategoryName = null;
                        }
                    }
                    $nature = ucfirst(mb_strtolower(trim($row[2]), 'UTF-8'));
                    $allowed = [
                        'Hàng hóa', 'Dịch vụ', 'Combo'
                    ];
                    if (!in_array($nature, $allowed)) {
                        return $this->errorMessage();
                    }
                    switch ($nature) {
                        case  'Hàng hóa':
                            $type = 1;
                            break;
                        case 'Dịch vụ':
                            $type = 2;
                            break;
                        case 'Combo':
                            $type = 3;
                            break;
                        default:
                            break;
                    }
                    $productData = [
                        'code' => trim($row[0]),
                        'name' => trim($row[1]),
                        'type' => $type,
                        'categoryName' => $CategoryName,
                        'activeIngredient' => trim($row[4]),
                        'manufacturer' => trim($row[5]),
                        'country' => trim($row[6]),
                        'unit' => trim($row[7]),
                        'description' => trim($row[8]),
                        'weight' => trim($row[9]),
                    ];
                    if (trim($row[13]) < 0 || trim($row[14]) < 0 || trim($row[13]) == '' || trim($row[14]) == '') {
                        $Quantity = 0;

                    }
                    $ProductBranchMeta = [
                        'branchId' => @$BranchID->id,
                        'onShowRoomQuantity' => trim($row[12]),
                        'minimalQuantity' => $Quantity,
                        'maximumQuantity' => $Quantity,
                    ];
                    $ProductUnits = [
                        'unitId' => @$UnitId->id,
                        'unitName' => @$UnitId->name,
                        'price' => trim($row[11]),
                        'conversionValue' => 1,
                    ];
                    DB::beginTransaction();
                    try {
                        if ($product != []) {
                            return [
                                'code' => 400,
                                'message' => 'Mã hàng ' . $product->code . ' đã tồn tại',
                            ];
                        } else {
                            $Productid = Product::create($productData);
                            $idProduct = Product::where('id', $Productid->id)->first();
                            Product::where('id', $idProduct->id)->update([
                                'categoryId' => @$Category->id,
                                'branchIDs' => @$dataBranchIDs
                            ]);
                            $ProductBranchMeta = array_merge($ProductBranchMeta, [
                                'productId' => $idProduct->id,
                                'productName' => $idProduct->name,
                            ]);
                            ProductBranchMeta::create($ProductBranchMeta);
                            $ProductUnits = array_merge($ProductUnits, ['productId' => $idProduct->id]);
                            ProductUnit::create($ProductUnits);
                        }
                        DB::commit();
                    } catch (Exception $e) {
                        DB::rollBack();
                        return [
                            'code' => 400,
                            'message' => $e->getMessage(),
                        ];
                    }
                } else {
                    return $this->errorMessage();
                }
            }
            return [
                'code' => 200,
                'message' => 'import file thành công',
                'redirect' => '/products/index',
                'count' => count($rows)
            ];
        }
    }
    private function errorMessage()
    {
        return [
            'code' => 400,
            'message' => 'Vui lòng kiểm tra lại dữ liệu',
//            'redirect' => '/products/index',
        ];
    }
    public function savePrdSyncAction(Request $req)
    {
        $data = $req->all();
        if(isset($req->sale_channel_name)){
            $status = 0;
            $message = null;
            $quantity = $data['quantity'];
            $basePrice = $data['basePrice'];
            $minPrice = 1000;
            // kiểm tra số lượng sản phẩm trên sàn đã được đặt
            $occupyQuantity = $req->occupyQuantity;
            $saleChannelID = SaleChannel::select('id')->where('name', "LIKE", "%" . $req->sale_channel_name . "%")->first();
            $getInfoSyncShop = SyncSaleChannels::query()->where('identity_key', $req->shop_id)->first();
            // kiểm tra có đồng bộ số lượng bán và giá bán hay không?
            if($getInfoSyncShop['sync_onhand'] === 1 && $getInfoSyncShop['sync_price'] !== 1 && isset($occupyQuantity) ){
                if($quantity < $occupyQuantity){
                    $status = 1;
                    $message = 'Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được đặt trên '. $req->sale_channel_name;
                }
            }else if($getInfoSyncShop['sync_onhand'] !== 1 && $getInfoSyncShop['sync_price'] === 1){
                if($basePrice < $minPrice){
                    $status = 1;
                    $message = 'Giá bán không được thấp hơn ' . $minPrice;
                }
            }else if($getInfoSyncShop['sync_onhand'] === 1 && $getInfoSyncShop['sync_price'] === 1){
                if($quantity < $occupyQuantity){
                    $status = 1;
                    $message = 'Tồn kho của sản phẩm đang nhỏ hơn tổng số lượng đã được đặt trên '. $req->sale_channel_name;
                }else if($basePrice < $minPrice){
                    $status = 1;
                    $message = 'Giá bán không được thấp hơn ' . $minPrice;
                }
            }
            $check = OmiSyncLink::where('sync_prd_id', $req->sync_prd_id)->whereNull('deletedAt')->count();
            if($check <= 0){
                $syncPrd = new OmiSyncLink();
                $syncPrd->fill($data);
                $syncPrd->sale_channel_id = $saleChannelID['id'];
                $syncPrd->sale_channel_name = $req->sale_channel_name;
                $syncPrd->shop_name = $req->shop_name;
                $syncPrd->shop_id = (int)$req->shop_id;
                $syncPrd->status = $status;
                $syncPrd->message = $message;
                $today = new \DateTime();
                $syncPrd->deletedAt = null;
                $syncPrd->createdAt = $today;
                $syncPrd->updatedAt = $today;
                $syncPrd->save();
                return [
                    'code' => 200,
                    'message' => 'Liên kết hàng thành công',
                    'sync' => $syncPrd,
                ];
            }else{
                return [
                    'code' => 400,
                    'message' => 'Hàng hóa đã được liên kết',
                ];
            }
        }
    }
    public function getPrdSyncAction(Request $req)
    {
        // $entries = OmiSyncLink::where('sale_channel_name', $req->sale_channel_name)->whereNull('deletedAt')->get();
        // if(!empty($entries)){
        //     return [
        //         'code' => 200,
        //         'entry' => $entries,
        //     ];
        // }
        $entries = OmiSyncLink::where('sale_channel_name', $req->sale_channel_name)->whereNull('deletedAt')->get();
        $data = [];
        if(!empty($entries)){
            foreach ($entries as $entry) {
                $getOmiPrd = Product::query()->where('id', $entry->omi_prd_id)->get();
                array_push($data, [
                    'sync_prd_id' => $entry->sync_prd_id,
                    'status' => $entry->status,
                    'message' => $entry->message,
                    'omi_prd' => $getOmiPrd,
                ]);
            }
            return [
                'code' => 200,
                'data' => $data,
                'entry' => $entries,
            ];
        }
    }
    public function getProdSyncErrorAction(Request $req)
    {
        $saleChannelID = SaleChannel::select('id')->where('name', "LIKE", "%" . $req->sale_channel_name . "%")->first();
        $getShop = SyncSaleChannels::query()
        ->with('getProductSync')
        ->whereNull('deleted_at')->where('sale_channel_id', $saleChannelID['id'])->get();
        $getProductID = OmiSyncLink::query()->where('sale_channel_name', "LIKE", "%" . $req->sale_channel_name . "%")->where('status', 1)->whereNull('deletedAt')->get();
        $entries = [];
        if($getProductID){
            foreach ($getShop as $shop) {
                foreach ($getProductID as $product) {
                    if($shop->identity_key === $product->shop_id){
                        $entry = Product::query()->where('id', $product->omi_prd_id)->first();
                        array_push($entries, [
                            'shop_id' => $product->shop_id,
                            'message' => $product->message,
                            'name' => $entry['name'],
                            'id' => $entry['id'],
                            'basePrice' => $entry['basePrice'],
                            'quantity' => $entry['quantity'],
                        ]);
                    }
                }
            }
        }
        return [
            'code' => 200,
            'shop' => $getShop,
            'product' => $entries,
        ];
    }
    public function removePrdSyncAction(Request $req)
    {
        $entry = OmiSyncLink::where('sync_prd_id', $req->id)->whereNull('deletedAt')->count();
        if($entry <= 0){
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }else{
            $remove = OmiSyncLink::where('sync_prd_id', $req->id)->delete();
            return [
                'code' => 200,
                'message' => 'Hủy liên kết thành công',
            ];
        }
    }

}
