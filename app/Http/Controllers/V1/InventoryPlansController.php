<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Branch;
use App\Models\Inventory;
use App\Models\InventoryPlan;
use App\Models\OrderSupplier;
use App\Models\OrderSupplierDetail;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;

class InventoryPlansController extends AuthenticatedController
{
    public function orderSuppliersPlanAction(Request $request)
    {
        $limit = $request->getInt('limit', 50);
        $productIDs = $request->get('productIDs');
        $keyword = $request->getTrimmed('keyword');

        if ($limit > 1000) {
            $limit = 1000;
        }

        $currentPlantId = $request->getInt('currentPlantId');
        $plan = null;

        if ($currentPlantId) {
            $plan = InventoryPlan::find($currentPlantId);
            if (!$plan) {
                return [
                    'code' => 404
                ];
            }
        }


        $branches = Branch::query()
            ->where('isDrugStore', 1)
            ->where('status', 1)
            ->orderBy('order', 'asc')
            ->get();

        $query = Product::query()
            ->with(['productUnits', 'productBatches'])
            ->orderBy('id');

        if (!empty($productIDs)) {
            $query->whereIn('id', $productIDs);
        }

        if ($keyword) {
            $query->whereSearch(['code', 'name'], $keyword);
        }

        $orderSupplierDetailsGroups = null;
        $purchaseOrderDetailsGroups = null;
        if ($plan) {
            $orderSupplierDetails = OrderSupplierDetail::query()
                ->with('orderSupplier:id,code,status')
                ->where('inventoryPlanId', $plan->id)
                ->get();

            $purchaseOrderDetails = PurchaseOrderDetail::query()
                ->with('purchaseOrder:id,code,status')
                ->where('inventoryPlanId', $plan->id)
                ->get();


            $orderSupplierDetailsGroups = $orderSupplierDetails->groupBy('productId');
            $purchaseOrderDetailsGroups = $purchaseOrderDetails->groupBy('productId');

            $productIds = $orderSupplierDetails->pluck('productId')->toArray();

            $query->whereIn('id', $productIds);
        } else {
            $query->with(['productBranchMeta']);
        }


        $products = $query->paginate(
            $limit, ['*'], 'page', $request->getInt('page'));

        if ($plan) {
            foreach ($products as $product) {
                $product->inventories = [];
                $product->lastPurchaseOrderDetail = null;
                $product->product_branch_meta = [];
                $product->currentProgressStatus = 'Chờ xử lý';
                if (isset($orderSupplierDetailsGroups[$product->id])) {
                    $orderSupplierDetail = $orderSupplierDetailsGroups[$product->id][0];
                    $purchaseOrderDetail= $purchaseOrderDetailsGroups[$product->id][0] ?? null;

                    $product->lastPurchaseOrderDetail = $orderSupplierDetail;
                    $product->product_branch_meta = $orderSupplierDetail->productBranchMeta;
                    $product->inventories = $orderSupplierDetail->inventories;
                    $product->purchaseOrderDetail = $purchaseOrderDetail;

                    $orderSupplier = $orderSupplierDetail->orderSupplier;
                    $purchaseOrder = $purchaseOrderDetail ? $purchaseOrderDetail->purchaseOrder : null;

                    if ($orderSupplier) {
                        $product->currentProgressStatus = 'Đã đặt hàng nhập';
                    }

                    if ($purchaseOrder) {
                        if ($purchaseOrder->status == PurchaseOrder::STATUS_DRAFT) {
                            $product->currentProgressStatus = 'Đang nhập hàng';
                        } else if ($purchaseOrder->status == PurchaseOrder::STATUS_IMPORTED) {
                            $product->currentProgressStatus = 'Đã nhập hàng';
                        }

                    }

                }
            }
        } else {
            Inventory::attachInventoryAll($products);
            PurchaseOrder::attachLastPurchaseOrder($products);
        }

        return [
            'code' => 200,
            'data' => [
                'branches' => $branches,
                'products' => $products->items(),
                'plan' => $plan,
                'paginate' => [
                    'currentPage' => $products->currentPage(),
                    'lastPage' => $products->lastPage(),
                    'total' => $products->total()
                ],
            ]
        ];
    }


    public function createAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405];
        }

        $plan = new InventoryPlan();
        $plan->createdBy = auth()->email;
        $plan->status = InventoryPlan::STATUS_NEW;
        $plan->status = 0;
        //$plan->totalProduct
        DB::beginTransaction();
        $plan->save();
        $plan->code = 'PDNDK' . $plan->id;
        $plan->save();

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã tạo plan',
            'data' => $plan
        ];
    }

    public function createOrderSuppliersAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405];
        }

        $entry = $request->get('entry');
        DB::beginTransaction();
        $orderSupplier = new OrderSupplier();
        $orderSupplier->inventoryPlanId = $entry['inventoryPlanId'];
        $orderSupplier->supplierId = $entry['supplierId'];
        $orderSupplier->branchId = Branch::KHO_TONG_ID;
        $orderSupplier->userId = auth()->id;
        $orderSupplier->status = OrderSupplier::STATUS_DRAFT;
        $orderSupplier->save();
        $orderSupplier->code = 'PDN' . $orderSupplier->id;
        $orderSupplier->createdBy = auth()->email;
        $subTotal = 0;
        $now = date('Y-m-d H:i:s');

        $countProduct = count($entry['details']);
        $totalQuantity = 0;

        foreach ($entry['details'] as &$detail) {
            $totalQuantity += $detail['quantity'];
            $detail['inventoryPlanId'] = $entry['inventoryPlanId'];
            $detail['orderSupplierId'] = $orderSupplier->id;
            $detail['createdAt'] = $now;
            $detail['updatedAt'] = $now;
            $detail['inventories'] = json_encode($detail['inventories']);
            $detail['productBranchMeta'] = json_encode($detail['productBranchMeta']);
            $subTotal += $detail['subTotal'];
        }

        $orderSupplier->subTotal = $subTotal;
        $orderSupplier->total = $subTotal;
        $orderSupplier->totalQuantity = $totalQuantity;
        $orderSupplier->totalProductType = $countProduct;
        $orderSupplier->save();

        DB::table('order_supplier_details')->insert($entry['details']);
        DB::update('UPDATE inventory_plans SET countOrderSupplier=countOrderSupplier+1,
                           countProduct=countProduct+? WHERE id=?', [
            $countProduct, $orderSupplier->inventoryPlanId
        ]);

        DB::commit();

        return [
            'code' => 200,
            'message' => 'OK'
        ];
    }

    public function dataAction(Request $request)
    {
        $page = $request->getInt('page');
        $record = $request->getInt('record');

        $query = InventoryPlan::query()->orderBy('id', 'desc');

        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ],
                'entries' => $entries->items()
            ]
        ];
    }

    public function productsDataAction(Request $request)
    {

        $page = $request->getInt('page');
        $record = $request->getInt('record');
        $query = Product::query()
            ->orderBy('id', 'desc')
            ->selectRaw('id,code,name')
            ->with(['currentBranchMeta']);

        $keyword = $request->getTrimmed('keyword');
        if ($keyword) {

            $query->whereSearch(['name', 'code'], $keyword);
        }
        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);
        $products = $entries->items();

        foreach ($products as $product) {
            $currentBranchMeta = $product->currentBranchMeta;
            if ($currentBranchMeta) {
                $product->meta = [
                    'id' => $currentBranchMeta->id,
                    'onShowRoomQuantity' => $currentBranchMeta->onShowRoomQuantity,
                    'avgSold3Months' => $currentBranchMeta->avgSold3Months,
                    'minimalQuantity' => $currentBranchMeta->minimalQuantity,
                    'suggestedMinimalQuantity' => $currentBranchMeta->minimalQuantity,
                    'maximumQuantity' => $currentBranchMeta->maximumQuantity,
                ];
            } else {
                $product->meta = [
                    'id' => null,
                    'holdQuantity' => 0,
                    'avgSold3Months' => 0,
                    'suggestedMinimalQuantity' => 0,
                    'minimalQuantity' => 0,
                    'maximumQuantity' => 0,
                ];
            }

        }

        Inventory::attachInventoryInfo($products, $request->currentBranchId());

        return [
            'code' => 200,
            'data' => [
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ],
                'entries' => $entries->items()
            ]
        ];
    }
}
