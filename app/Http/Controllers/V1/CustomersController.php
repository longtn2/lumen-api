<?php

namespace App\Http\Controllers\V1;


use App\Models\Address;
use App\Models\Customer;
use App\Http\Controllers\AuthenticatedController;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Tag;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Support\Facades\DB;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\V;

class CustomersController extends AuthenticatedController
{

    /**
    * @uri /v1/customers/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Customer::with(['addresses' => function ($q) {
            $q->orderByDesc('is_default');
        }])->find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
     * @uri /v1/customers/show
     * @return array
     */
    public function detailAddressAction(Request $req)
    {
        $id = $req->id;
        $entry = Address::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
     * @uri /v1/customers/show
     * @return array
     */
    public function getAddressAction(Request $req)
    {
        $id = $req->id;
        $addresses = Address::where('customer_id', $id)->orderByDesc('id')->get();

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'addresses' => $addresses,
            ]
        ];
    }

    /**
    * @uri /v1/customers/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Customer::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri /v1/customers/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');
        $addressData = $req->get('address');

        V::make($data)
            ->rule(V::required, [
                'name',
            ])
            ->validOrFail();


        /**
        * @var Customer $entry
        */
        $redirect = true;

        if (isset($data['id'])) {
            $entry = Customer::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Customer();
        }

        $entry->fill($data);

        $entry->save();

        if (!isset($entry->code)) {
            $entry->code = 'KH'.$entry->id;
            $entry->save();
        }

        if (isset($addressData['id'])) {
            $address = Address::find($addressData['id']);
            if (!$address) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy địa chỉ',
                ];
            }

        } else {
            $address = new Address();
            $address->customer_id = $entry->id;
            $address->name = $entry->name;
            $address->phone = $entry->contactNumber;
        }

        $address->fill($addressData);
        if ($address->typeAddress !== Address::TYPE_APARTMENT) {
            unset($address->apartmentName, $address->apartmentBuilding, $address->apartmentRoom);
        }
        if ($address->is_default === 1) {
            $arrAddress = Address::where('customer_id', $entry->id)->get();
            if (count($arrAddress) > 0) {
                foreach ($arrAddress as $item) {
                    $item->is_default = 0;
                    $item->save();
                }
            }
        }
        $address->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/customers/index' : false,
            'data' => $entry->toArray()
        ];
    }
    public function saveAddressAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $customerId = $req->get('customer_id');
        $data = $req->get('address');

        V::make($data)
            ->rule(V::required, [
                'name',
                'phone',
                'addressValue'
            ])
            ->validOrFail();


        /**
         * @var Address $entry
         */

        if (isset($data['id'])) {
            $entry = Address::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy địa chỉ',
                ];
            }

        } else {
            $entry = new Address();
            $entry->customer_id = $customerId;
        }

        $entry->fill($data);
        if ($entry->typeAddress !== Address::TYPE_APARTMENT) {
            unset($entry->apartmentName, $entry->apartmentBuilding, $entry->apartmentRoom);
        }
        $entry->save();
        if ($entry->is_default === 1) {
            $arrAddress = Address::where('customer_id', $customerId)->where('id', '<>', $entry->id)->get();
            if (count($arrAddress) > 0) {
                foreach ($arrAddress as $item) {
                    $item->is_default = 0;
                    $item->save();
                }
            }
        }

        $addresses = Address::where('customer_id', $customerId)->orderByDesc('is_default')->get();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'data' => $entry->toArray(),
            'addresses' => $addresses
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/customers/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Customer::query()->select($select)->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->where('name', 'LIKE', '%' . $req->keyword. '%')
                ->orWhere('contactNumber', 'LIKE', '%' . $req->keyword. '%')
                ->orWhere('code', 'LIKE', '%' . $req->keyword. '%');
        }

        if ($req->status) {
            $query->where('status', $req->status);
        }

        if ($req->branchId) {
            $query->where('branchId', $req->branchId);
        }

        $query->createdIn($req->created);

        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/customers/dataOrder
     * @return array
     */
    public function dataOrderAction(Request $req)
    {
        $customerId = $req->get('id');
        $page = $req->getInt('page');

        $query = Order::query()->with('invoice')->where('customerId', $customerId)->orderByDesc('id');

        $entries = $query->paginate(15, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/customers/dataInvoice
     * @return array
     */
    public function dataInvoiceAction(Request $req)
    {
        $customerId = $req->get('id');
        $page = $req->getInt('page');

        $query = Invoice::query()->with('returns')->where('customerId', $customerId)->orderByDesc('id');

        $entries = $query->paginate(10, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/customers/dataInvoice
     * @return array
     */
    public function dataCartAction(Request $req)
    {
        $customerId = $req->get('id');

        $products = [];
        $orders = Order::query()->where('customerId', $customerId)->orderByDesc('id')->pluck('id')->toArray();

        $orderItems = OrderDetail::query()->with('order')->whereIn('orderId', $orders)->orderByDesc('id');
        $items = $orderItems->get();

        $ids = $orderItems->groupBy('productId')->pluck('productId')->toArray();

        foreach ($ids as $id) {
            $arrItems = [];
            foreach ($items as $item) {
                if ($item->productId === $id) {
                    $arrItems[] = $item;
                }
            }
            $products[$id] = $arrItems;
        }

        return [
            'code' => 200,
            'data' => [
                'products' => $products
            ]

        ];
    }

    public function loadCustomerTagsAction(Request  $request)
    {
        $id = $request->get('id');
        $productUnitIDs = ProductUnitTag::query()->where('tagId', $id)->get()->pluck('productUnitId')->toArray();
        $productUnits = ProductUnit::query()->with('product')->selectRaw('id, productId, unitName')->whereIn('id', $productUnitIDs)->get();
        $result = [];

        foreach ($productUnits as $pu) {
            $product = $pu->product;

            if ($product) {
                $result[] = [
                    'id' => $pu->id,
                    'name' => $pu->product->name . ' - ' . $pu->unitName . ' - ' . $product->code
                ];
            }

        }

        return [
            'code' => 200,
            'data' => $result
        ];
    }
    public function loadCustomerGroupAction(Request  $request)
    {
       $data = Tag::where('tagTypeId', 2)->get();

        return [
            'code' => 200,
            'data' => $data
        ];
    }



    private function getDisplayFields(): array
    {
        $fields = [
    [
        'field' => 'id',
        'name' => 'ID',
    ],
    [
        'field' => 'name',
        'name' => 'Họ tên',
    ],
    [
        'field' => 'code',
        'name' => 'Mã khách hàng',
    ],
    [
        'field' => 'gender',
        'name' => 'Giới tính',
        'render' => 'gender'
    ],
    [
        'field' => 'contactNumber',
        'name' => 'Số điện thoại',
    ],
//    [
//        'field' => 'retailerId',
//        'name' => 'RetailerId',
//    ],
//    [
//        'field' => 'branchId',
//        'name' => 'Chi nhánh',
//    ],
    [
        'field' => 'email',
        'name' => 'Email',
    ],
    [
        'field' => 'modifiedDate',
        'name' => 'Cập nhật',
        'render' => 'date'
    ],
    [
        'field' => 'createdDate',
        'name' => 'Ngày tạo',
        'render' => 'date'
    ],
    [
        'field' => 'birthDate',
        'name' => 'Ngày sinh',
    ],
    [
        'field' => 'type',
        'name' => 'Type',
    ],
    [
        'field' => 'groups',
        'name' => 'Groups',
    ],
    [
        'field' => 'debt',
        'name' => 'Debt',
    ],
    [
        'field' => 'rewardPoint',
        'name' => 'RewardPoint',
    ],
    [
        'field' => 'address',
        'name' => 'Address',
    ],
    [
        'field' => 'locationName',
        'name' => 'LocationName',
    ],
    [
        'field' => 'wardName',
        'name' => 'WardName',
    ],
    [
        'field' => 'organization',
        'name' => 'Organization',
    ],
    [
        'field' => 'comments',
        'name' => 'Comments',
    ],
    [
        'field' => 'taxCode',
        'name' => 'TaxCode',
    ],
];
        return array_chunk($fields, 10)[0];
    }
}
