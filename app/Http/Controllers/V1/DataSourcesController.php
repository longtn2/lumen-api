<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Branch;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Delivery;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\SysPermission as Permission;
use App\Models\Product;
use App\Models\ProductBatch;
use App\Models\ProductUnit;
use App\Models\SaleChannel;
use App\Models\ServiceType;
use App\Models\Supplier;
use App\Models\Transporter;
use App\Models\User;
use App\Models\SysRole as Role;
use App\Models\ProductCharateristic;
use App\Types\Inventory\InventoryOnHandDetail;
use OmiCare\Http\Request;
use \Illuminate\Database\Eloquent\Builder;

class DataSourcesController extends AuthenticatedController
{
    public function indexAction(Request $request)
    {
        $datasource = $request->get('datasource');     
        $method = 'ds' . ucfirst($datasource);           
        $keyword = $request->get('query');        
        $values = $request->get('values');       
        $values = $values ? explode(',', $values): null;
        $params = $request->all();        
        return $this->$method($request, $keyword, $values, $params);
    }

    public function getManyAction(Request $request)
    {
        $entries = $request->get('entries');
        $result = [];
        foreach ($entries as $entry) {
            try {
                $requestId = $entry['request_id'];
                $datasource = $entry['datasource'];
                $params = $entry['params'];

                $keyword = $params['query'] ?? '';
                $values = $params['values'] ?? null;
                $values = $values ? explode(',', $values): null;
                $keyword = trim($keyword);

                $method = 'ds' . ucfirst($datasource);
                $response = $this->$method($request, $keyword, $values, $params);
                $response['datasource'] = $datasource;
                $result[$requestId] = $response;
            } catch (\Exception $e) {
                $result[] = [
                    'code' => 503,
                    'data' => [],
                    'message' => $e->getMessage()
                ];
            }
        }


        return [
            'code' => 200,
            'data' => $result,
        ];
    }

    /**
     * @param $class
     * @param array $options
     * @return array
     */
    private function searchByModel(string $class, array $options): array
    {

        $rawSelect = $options['select'];
        $searchFields = $options['search'];
        $keyword = $options['keyword'];
        $limit = $options['limit'] ?? 15;
        $values = $options['values'];
        $primaryKey = $options['primaryKey'] ?? 'id';
        $format = $options['format'] ?? null;
        $callback = $options['query'] ?? null;
        $order = $options['order'] ?? ['by' => 'id', 'direction' => 'desc'];

        /**
         * @var Builder $query
         */
        $query = $class::query()
            ->orderBy($order['by'], $order['direction'])
            ->selectRaw($rawSelect)
            ->limit(15);

        if ($callback) {
            $callback($query);
        }

        $selectedEntries = collect([]);
        if (!empty($values)) {
            $newQuery = clone $query;
            $selectedEntries = $newQuery->whereIn($primaryKey, $values)->get();
            $query->whereNotIn($primaryKey, $values);

            if ($callback) {
                $callback($newQuery);
            }
        }

        if ($keyword) {
            $query->where(function(Builder $query) use($keyword, $searchFields) {
                foreach ($searchFields as $field) {
                    $query->orWhere($field, 'LIKE', '%' . $keyword . '%');
                }
            });
        }

        $entries = $query->limit($limit)->get();
        $mapEntries = [];

        foreach ($selectedEntries as $e) {
            $mapEntries[] = $e;
        }

        foreach ($entries as $e) {
            $mapEntries[] = $e;
        }

        if ($format) {
            $mapEntries = array_map($format, $mapEntries);
        }

        return [
            'code' => 200,
            'data' => $mapEntries,
        ];
    }

    private function dsCategories(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            Category::class,
            [
                'select' => 'id, `name`',
                'search' => ['categoryName'],
                'limit' => 1000,
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }
    public function dsProductCharateristic(Request $request, $keyword, $values){
        return $this->searchByModel(
            ProductCharateristic::class,
            [
                'select' => 'id, `name`',
                'search' => ['categoryName'],
                'limit' => 1000,
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }
    private function dsParentPermissions(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            Permission::class,
            [
                'select' => 'id, `name`, parentId, displayName',
                'search' => ['displayName'],
                'limit' => 50,
                'keyword' => $keyword,
                'values' => $values,
                'format' => function ($permission) {
                    if ($permission->displayName) {
                        $permission->label = $permission->displayName;
                    } else {
                        $permission->label = $permission->name;
                    }

                    return $permission;
                }
            ]
        );
    }

    private function dsTreeCategories(Request $request, $keyword, $values)
    {
        $categories = Category::with('children')->whereNull('parentId')->get();

        foreach ($categories as $category) {
            $category->id = $category->categoryId;
            $category->label = $category->categoryName;
            if ($category->children && count($category->children) > 0) {
                foreach ($category->children as $child) {
                    $child->id = $child->categoryId;
                    $child->label = $child->categoryName;
                }
            }
        }
        return [
            'code' => 200,
            'data' => $categories
        ];
    }

    private function dsProducts(Request $request, $keyword, $values)
    {

        $res = $this->searchByModel(
            Product::class,
            [
                'select' => 'id, `name`, `code`, capitalPrice',
                'search' => ['name', 'code'],
                'query' => function($query) {
                    $query->with('productUnits');
                },
                'format' => function($product) {
                    $product->orignalName= $product->name;
                    $product->name = $product->code . ' - ' . $product->name;
                    return $product;
                },
                'limit' => 20,
                'keyword' => $keyword,
                'values' => $values
            ]
        );

        Inventory::attachInventoryInfo($res['data'], $request->currentBranchId());
        return $res;
    }

    public function dsInventoriesProductBatches(Request  $request, $keyword, $values, $params)
    {
        $productId = $params['productId'] ?? null;
        $isReturnAction = @$params['actionType'] === 'return';
        $refCode = $params['refCode'] ?? null;
        $currentBranchId = $request->currentBranchId();
        $query = ProductBatch::query()
            ->selectRaw('product_batches.*,inventories.id as inventoryId')
            ->join('inventories', 'inventories.productBatchId', '=', 'product_batches.id')
            ->groupBy('product_batches.id')
            ->where('inventories.branchId', $currentBranchId)
            ->where('product_batches.productId', $productId);

        if ($refCode) {
            $query->where('parentRefCode', $refCode);
        }

        $selectedValues = collect([]);

        if (!empty($values)) {
            $query->whereNotIn('product_batches.id', $values);
            if ($isReturnAction) {
                $selectedValues = ProductBatch::query()
                    ->selectRaw('product_batches.*,inventories.id as inventoryId')
                    ->join('inventories', 'inventories.productBatchId', '=', 'product_batches.id')
                    ->where('inventories.branchId', $currentBranchId)
                    ->groupBy('product_batches.id')
                    ->whereIn('product_batches.id', $values)->get();
            } else {
                $selectedValues = ProductBatch::query()->whereIn('id', $values)->get();
            }

        }

        $productBatches = $query->get();
        $productBatchIDs = array_merge(
            $productBatches->pluck('id')->toArray(),
            $selectedValues->pluck('id')->toArray()
        );
        // Truong hop lay data tu phieu nhap
        $refCodeMap = Inventory::findInventoryByRefCode($currentBranchId, $refCode, $productBatchIDs);

        $result = [];

        foreach ([$selectedValues, $productBatches] as $batches) {
            foreach ($batches as $batch) {

                $inv = $refCodeMap[$batch->id] ?? new InventoryOnHandDetail();
                $onHand = $refCode ? $inv->current : $inv->all;
                $onHandAll = $inv->all;

                $result[] = [
                    'id' => $batch->id,
                    'name' => $batch->getFormattedName() /*. ($isReturnAction ? ' - N: ' . appDateFormat($batch->createdAt) : '')*/,
                    'onHand' => $batch->onHand,
                    'inventoryId' => $batch->inventoryId,
                    'inventory' => [
                        'onHand' => $onHand,
                        'onHandAll' => $onHandAll
                    ]
                ];
            }
        }

        return [
            'code' => 200,
            'data' => $result,
        ];
    }


    public function dsProductBatches(Request  $request, $keyword, $values, $params)
    {
        $productUnitId = $params['productUnitId'] ?? null;
        $actionType = $params['actionType'] ?? null;

        $result = $this->searchByModel(
            ProductBatch::class,
            [
                'select' => 'id, `name`, quantity, expireDate',
                'search' => ['name'],
                'limit' => 50,
                'keyword' => $keyword,
                'values' => $values,
                'query' => function(Builder $q) use($productUnitId, $actionType) {
                    $q->where('product_batches.productUnitId', $productUnitId);
                },
                'format' => function($entry) {
                    return [
                        'id' => $entry->id,
                        'name' => $entry->getFormattedName(),
                        'quantity' => $entry->quantity
                    ];
                }

            ]
        );

        return $result;
    }

    private function dsProductUnits(Request $request, $keyword, $values)
    {
        $result = $this->searchByModel(
            Product::class,
            [
                'select' => 'id, `name`, `code`, `basePrice`, `capitalPrice`',
                'search' => ['name'],
                'limit' => 50,
                'keyword' => $keyword,
                'values' => $values,
                'query' => function($q) {
                    $q->with('productUnits');
                }
            ]
        );

        $productUnits = [];
        foreach ($result['data'] as $product) {
            $productUnit = $product->toArray();
            foreach ($productUnit['product_units'] as $pu) {
                $productUnit['id'] = $pu['id'];
                $productUnit['productUnitId'] = $pu['id'];
                $productUnit['name'] .=  ' - ' .$pu['unitName'] . ' - ' .$product->code;
                $productUnit['conversionValue'] = $pu['conversionValue'] ?? 1;
                unset($productUnit['product_unit']);
                $productUnits[] = $productUnit;
            }
        }

        return [
            'code' => 200,
            'data' => $productUnits
        ];
    }

    private function dsUsers(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            User::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'limit' => 25,
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }

    private function dsUserEmails(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            User::class,
            [
                'select' => "`email`, `name`",
                'search' => ['name', 'email'],
                'primaryKey' => 'email',
                'limit' => 25,
                'keyword' => $keyword,
                'format' => function($user) {
                    return [
                        'id' => $user->email,
                        'name' => $user->name
                    ];
                },
                'values' => $values
            ]
        );
    }

    private function dsCustomers(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            Customer::class,
            [
                'select' => "id, `name`, contactNumber",
                'search' => ['name', 'contactNumber'],
                'limit' => 25,
                'keyword' => $keyword,
                'values' => $values,
                'format' => function ($customer) {
                    if ($customer->contactNumber) {
                        $customer->label = $customer->name . ' - ' . $customer->contactNumber;
                    }

                    return $customer;
                }
            ]
        );
    }

    private function dsSuppliers(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            Supplier::class,
            [
                'select' => "id, `name`, `code` ",
                'search' => ['name'],
//                'format' => function($entry) {
//                    $entry->name = $entry->name;
//
//                    return $entry;
//                },
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }

    private function dsBranches(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            Branch::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'keyword' => $keyword,
                'values' => $values,
                'query' => function(Builder $q) {
                    $q->where('status', 1);
                },
            ]
        );
    }
    private function dsBranchesIndex(Request $request, $keyword, $values)
    {
        $user = auth();
        return $this->searchByModel(
            Branch::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'keyword' => $keyword,
                'values' => $values,
                'query' => function(Builder $q) use ($user) {
                    $q->whereIn('id', $user->branchIDs)->where('status', 1);
                },
            ]
        );
    }

    private function dsSaleChannels(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            SaleChannel::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }

    private function dsSales(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            User::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }

    private function dsOrderStatus(Request $request, $keyword, $values)
    {
        return [
            'code' => 200,
            'data' => Order::$listStatus
        ];
    }

    private function dsInvoiceStatus(Request $request, $keyword, $values)
    {
        return [
            'code' => 200,
            'data' => Invoice::$listStatus
        ];
    }

    private function dsDeliveryStatus(Request $request, $keyword, $values)
    {
        return [
            'code' => 200,
            'data' => Delivery::$listStatus
        ];
    }

    private function dsTransporters(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            Transporter::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }

    private function dsServiceTypes(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            ServiceType::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }

    private function dsRoles(Request $request, $keyword, $values)
    {
        return $this->searchByModel(
            Role::class,
            [
                'select' => "id, `name`",
                'search' => ['name'],
                'keyword' => $keyword,
                'values' => $values
            ]
        );
    }

    private function dsInvoicePackStatus(Request $request, $keyword, $values)
    {
        return [
            'code' => 200,
            'data' => Invoice::$listStatusPack
        ];
    }
}
