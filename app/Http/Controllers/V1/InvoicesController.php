<?php

namespace App\Http\Controllers\V1;


use App\Models\Branch;
use App\Models\Cashflow;
use App\Models\Delivery;
use App\Models\History;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Http\Controllers\AuthenticatedController;
use App\Models\User;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class InvoicesController extends AuthenticatedController
{

    /**
     * @uri /v1/invoices/show
     * @return array
     */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $currentBranchID = $req->currentBranchId();

        $entry = Invoice::query()
            ->with(['invoiceItems' => function($q) {
                $q->with('inventories.productBatch', 'productUnit:id,unitName');
            }, 'cashFlows.user', 'customer', 'branch', 'order'])
            ->find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->createdByName = User::getNameFromId($entry->createdBy);
        $entry->statusValue = Invoice::getStatusLabel($entry->status);

        $cashFlowMethodMap = array_column(Cashflow::$listPaymentMethod, 'name', 'id');
        foreach ($entry->cashFlows as $cashFlow) {
            $cashFlow->statusValue = Cashflow::$listStatus[$cashFlow->status] ?? null;
            $cashFlow->methodValue = $cashFlowMethodMap[$cashFlow->method] ?? null;
        }

        $productIds = $entry->invoiceItems->map(function ($item) {return $item->productId;});
        $inventoryProductBatchMap = Inventory::query()
            ->selectRaw('*,SUM(onHand*conversionValue) as quantity')
            ->whereIn('productId', $productIds)
            ->where('branchId', $currentBranchID)
            ->whereNotNull('productBatchId')
            ->groupBy('productBatchId')
            ->pluck('quantity', 'productBatchId');

        foreach ($entry->invoiceItems as $item) {
            $item->product_batches = $item->inventories->map(function ($inventory) use ($inventoryProductBatchMap) {
                $batch = $inventory->productBatch ?? new \stdClass();
                $batch->quantity = abs($inventory->onHand);

                $quantityOrigin = '--';
                if (!empty($inventoryProductBatchMap[$batch->id])) {
                    $conversionValue = $inventory->conversionValue ?? 1;
                    $quantityOrigin = $inventoryProductBatchMap[$batch->id];

                    $quantityOrigin = $conversionValue ? floor($quantityOrigin / $conversionValue) : 0;
                }
                $batch->quantityOrigin = $quantityOrigin;

                return $batch;
            });

            unset($item->inventories);
        }

        $listStatusPack = Invoice::$listStatusPack;

        $orderDelivery = Delivery::query()
            ->where('type', Delivery::TYPE_INVOICE)
            ->where('invoiceId', $entry->id)
            ->orderByDesc('id')
            ->first();

        $receiverLocation = new \stdClass();
        if ($orderDelivery) {
            $orderDelivery->decode = Branch::decode($orderDelivery->branchId);
            $receiverLocation->provinceId = $orderDelivery->receiverProvinceId;
            $receiverLocation->districtId = $orderDelivery->receiverDistrictId;
            $receiverLocation->wardId = $orderDelivery->receiverWardId;
        }
        $listDeliveryStatus = Delivery::$listStatus;

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields(),
                'list_status_pack' => $listStatusPack,

                'delivery' => $orderDelivery,
                'receiver_location' => $receiverLocation,
                'list_delivery_status' => $listDeliveryStatus
            ]
        ];
    }

    /**
     * @uri /v1/invoices/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Invoice::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/invoices/save
     * @return array
     */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');
        $delivery = $req->get('delivery');

        V::make($data)
            ->rule(V::required, [])
            ->validOrFail();

        DB::beginTransaction();

        /**
         * @var Invoice $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Invoice::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Invoice();
        }

        if (!empty($delivery)) {
            if (!empty($delivery['id'])) {
                $invoiceDelivery = Delivery::query()
                    ->where('type', Delivery::TYPE_INVOICE)
                    ->where('invoiceId', $entry->id)
                    ->first();
            } else {
                $invoiceDelivery = new Delivery();
            }

            if ($invoiceDelivery) {
                if ($invoiceDelivery->status != $delivery['status']) {
                    $invoiceDelivery->saveStatus($data['status'], '', false);
                }

                $invoiceDelivery->fill($delivery);
                $invoiceDelivery->saveHistory(History::ACTION_UPDATED);
                $invoiceDelivery->save();
            }
        }

        $entry->fill($data);
        $entry->saveHistory(History::ACTION_UPDATED);
        $entry->save();

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/invoices/index' : false,
            'data' => $entry->toArray()
        ];
    }

    public function saveStatusAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $id = $request->get('id');
        $action = $request->get('action');

        $invoice = Invoice::find($id);
        if (!$invoice) {
            return [
                'code' => 404,
                'message' => 'Invoice not found'
            ];
        }

        DB::beginTransaction();

        switch ($action) {
            case "complete":
                $invoice->saveStatus(Invoice::STATUS_COMPLETE, '', false);
                $invoice->saveHistory(History::ACTION_UPDATED);
                $invoice->save();
                break;
            case "cancel":
                $invoice->saveStatus(Invoice::STATUS_CANCELED);
                $invoice->saveHistory(History::ACTION_CANCEL);
                break;
            default:
                break;
        }

        $invoice->removeInventories();

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Thành công!'
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/invoices/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $fieldSelects = [];
        $fieldDisplays = [];

        foreach ($fields as $field) {
            if (!empty($field['isColumn']) && !empty($field['field'])) {
                $fieldSelects[] = $field['field'];
            }

            if (!empty($field['relatedField'])) {
                if (is_array($field['relatedField'])) {
                    $fieldSelects = array_merge($fieldSelects, $field['relatedField']);
                } else {
                    $fieldSelects[] = $field['relatedField'];
                }
            }

            if (empty($field['hide'])) {
                $field['selected'] = empty($field['selected']) ? false : true;
                $fieldDisplays[] = $field;
            }
        }

        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');
        $action = $req->get('_action');

        $keyword = $req->get('keyword');
        $branchIds = $req->get('branch_ids');
        $saleChannelIds = $req->get('sale_channel_ids');
        $purchaseDate = $req->get('purchase_date');
        $status = $req->get('status');
        $createdByIds = $req->get('created_by_ids');
        $soldByIds = $req->get('sold_by_ids');
        $methods = $req->get('methods');
        $deliveryStatus = $req->get('delivery_status');
        $isPack = $req->get('is_pack', null);

        $query = Invoice::query()
            ->select($fieldSelects)
            ->with(['customer', 'returns', 'saleChannel', 'userCreated', 'order', 'branch', 'soldBy', 'delivery.transporter', 'cashFlows']);

        if ($keyword) {
            $query->where('code', 'LIKE', '%' . $keyword. '%');
        }

        $userBranchIds = array_column(Branch::getUserBranches()->toArray(), 'id');
        if ($branchIds) {
            $filterBranchIds = array_intersect($userBranchIds, explode(',', $branchIds));
            $query->whereIn('branchId', $filterBranchIds);
        } else {
            $query->whereIn('branchId', $userBranchIds);
        }

        if ($saleChannelIds) {
            $query->whereIn('saleChannelId', explode(',', $saleChannelIds));
        }

        if ($status) {
            $query->whereIn('status', explode(',', $status));
        }

        if ($purchaseDate) {
            $query->columnIn('purchaseDate', $purchaseDate);
        }

        if ($createdByIds) {
            $query->whereIn('createdBy', explode(',', $createdByIds));
        }

        if ($soldByIds) {
            $query->whereIn('soldById', explode(',', $soldByIds));
        }

        if ($methods) {
            $query->whereHas('cashFlows', function ($q) use ($methods) {
                $q->whereIn('method', explode(',', $methods));
            });
        }

        if ($deliveryStatus) {
            $query->whereHas('delivery', function ($q) use ($deliveryStatus) {
                $q->whereIn('status', explode(',', $deliveryStatus));
            });
        }

        if ($isPack !== null) {
            $query->where('isPack', $isPack ? 1 : 0);
        }

        $query->createdIn($req->created)->orderBy($sortField, $sortDirection);

        if ($action == 'excel') {
            $entries = $query->addSelect(DB::raw('totalOrigin - total as discountPrice, ROW_NUMBER() OVER (ORDER BY ' . $sortField . ' ' . $sortDirection . ') as stt'))
                ->get();

            (new Invoice())->saveHistory(History::ACTION_EXPORT);
            return [
                'code' => 200,
                'data' => $entries
            ];
        }
        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        $mapDeliveryStatus = array_column(Delivery::$listStatus, 'name', 'id');
        $mapStatus = array_column(Invoice::$listStatus, 'name', 'id');
        $mapStatusPack = array_column(Invoice::$listStatusPack, 'name', 'id');
        foreach ($entries as $item) {
            if (count($item->returns)) {
                $returnCodes = array_column($item->returns->toArray(), 'code');
                $item->return_codes = implode(',', $returnCodes);
                unset($item->returns);
            }

            if ($item->status == Invoice::STATUS_PROCESS) {
                $item->total_need = max($item->total - $item->totalPayment, 0);
            } else {
                $item->total_need = null;
            }

            $item->orderCode = $item->order->code ?? null;
            $item->customerName = $item->customer->name ?? null;
            $item->customerContactNumber = $item->customer->contactNumber ?? null;
            $item->customerEmail = $item->customer->email ?? null;
            $item->customerAddress = $item->customer->address ?? null;
            $item->customerLocationName = $item->customer->locationName ?? null;
            $item->customerWardName = $item->customer->wardName ?? null;
            $item->customerBirthDate = $item->customer->birthDate ?? null;
            $item->branchName = $item->branch->name ?? null;
            $item->saleChannelName = $item->saleChannel->name ?? null;
            $item->soldByName = $item->soldBy->name ?? null;
            $item->userCreatedName = $item->userCreated->name ?? null;
            $item->deliveryCode = $item->delivery->code ?? null;
            $item->deliveryStatus = $mapDeliveryStatus[@$item->delivery->status] ?? null;
            $item->status = $mapStatus[$item->status] ?? '';
            $item->deliveryTransporterName = $item->delivery->transporter->name ?? null;
            $item->discountPrice = $item->totalOrigin - $item->total;
            $item->isPack = $mapStatusPack[$item->isPack] ?? '';

//            $item->deliveryPrice = $item->delivery->price ?? null;
        }

        return [
            'code' => 200,
            'data' => [
                'fields' => $fieldDisplays,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    public function dataReturnAction(Request $req)
    {
        $keyword = $req->get('keyword');
        $purchaseDate = $req->get('purchaseDate');
        $page = $req->get('page');

        $query = Invoice::query()
            ->with(['customer', 'userCreated:id,name'])
        ->orderByDesc('id');

        if ($keyword) {
            $query->where('code', 'LIKE', '%' . $keyword. '%')
                ->orWhere('customerName', 'LIKE', '%' . $keyword. '%')
                ->orWhere('customerCode', 'LIKE', '%' . $keyword. '%')
                ->orWhere('soldByName', 'LIKE', '%' . $keyword. '%');
        }

        if ($purchaseDate) {
            $query->columnIn('purchaseDate', $purchaseDate);
        }

        $query->createdIn($req->created);


        $entries = $query->paginate(8, ['*'], 'page', $page);

//        foreach ($entries as $item) {
//            if (count($item->returns)) {
//                $returnCodes = array_column($item->returns->toArray(), 'code');
//                $item->return_codes = implode(',', $returnCodes);
//                unset($item->returns);
//            }
//
//            if ($item->status == Invoice::STATUS_PROCESS) {
//                $item->total_need = max($item->total - $item->totalPayment, 0);
//            } else {
//                $item->total_need = null;
//            }
//
//        }

        return [
            'code' => 200,
            'data' => [
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
                'isColumn' => true,
                'hide' => true
            ],
            [
                'field' => 'code',
                'name' => 'Mã hóa đơn',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'deliveryCode',
                'name' => 'Mã vận đơn',
            ],
            [
                'field' => 'deliveryStatus',
                'name' => 'Trạng thái giao hàng',
            ],
            [
                'field' => 'purchaseDate',
                'name' => 'Thời gian',
                'render' => 'datetime',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'createdAt',
                'name' => 'Thời gian tạo',
                'render' => 'datetime',
                'isColumn' => true
            ],
            [
                'field' => 'updatedAt',
                'name' => 'Ngày cập nhật',
                'render' => 'datetime',
                'isColumn' => true
            ],
            [
                'field' => 'orderCode',
                'relatedField' => 'orderId',
                'name' => 'Mã đặt hàng',
                'selected' => true
            ],
            [
                'field' => 'return_codes',
                'name' => 'Mã trả hàng',
            ],
            [
                'field' => 'customerName',
                'relatedField' => 'customerId',
                'name' => 'Khách hàng',
                'selected' => true
            ],
            [
                'field' => 'customerContactNumber',
                'name' => 'Điện thoại',
                'selected' => true
            ],
            [
                'field' => 'customerEmail',
                'name' => 'Email',
            ],
            [
                'field' => 'customerAddress',
                'name' => 'Địa chỉ'
            ],
            [
                'field' => 'customerLocationName',
                'name' => 'Khu vực'
            ],
            [
                'field' => 'customerWardName',
                'name' => 'Phường/Xã'
            ],
            [
                'field' => 'customerBirthDate',
                'name' => 'Ngày sinh',
                'render' => 'datetime',
            ],
            [
                'field' => 'branchName',
                'relatedField' => 'branchId',
                'name' => 'Chi nhánh',
            ],
            [
                'field' => 'soldByName',
                'relatedField' => 'soldById',
                'name' => 'Người bán'
            ],
            [
                'field' => 'userCreatedName',
                'relatedField' => 'createdBy',
                'name' => 'Người tạo',
            ],
            [
                'field' => 'saleChannelName',
                'relatedField' => 'saleChannelId',
                'name' => 'Kênh bán',
            ],
            [
                'field' => 'deliveryTransporterName',
                'name' => 'Đối tác giao hàng'
            ],
            [
                'field' => 'description',
                'name' => 'Ghi chú',
                'isColumn' => true,
            ],
            [
                'field' => 'totalOrigin',
                'name' => 'Tổng tiền hàng',
                'isColumn' => true,
                'render' => 'number',
                'selected' => true
            ],
            [
                'field' => 'discountPrice',
                'relatedField' => ['discountType', 'discountValue'],
                'name' => 'Giảm giá',
                'render' => 'number',
                'selected' => true
            ],
            [
                'field' => 'total',
                'name' => 'Khách cần trả',
                'isColumn' => true,
                'render' => 'number',
                'selected' => true
            ],
            [
                'field' => 'totalPayment',
                'name' => 'Khách đã trả',
                'isColumn' => true,
                'render' => 'number',
                'selected' => true
            ],
            [
                'field' => 'total_need',
                'name' => 'Còn cần thu (COD)',
                'render' => 'number'
            ],
//            [
//                'field' => 'deliveryPrice',
//                'name' => 'Phí trả DTGH',
//                'render' => 'number'
//            ],
//            [
//                'field' => 'Ghi chú trạng thái giao hàng',
//                'name' => '',
//            ],
            [
                'field' => 'status',
                'name' => 'Trạng thái',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'isPack',
                'name' => 'Trạng thái đóng gói',
                'isColumn' => true,
            ]
        ];
        return $fields;
    }
}
