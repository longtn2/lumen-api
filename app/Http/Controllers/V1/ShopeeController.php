<?php
namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Inventory;
use App\Models\InvoiceDetail;
use App\Models\OrderSupplierDetail;
use App\Models\Product;
use App\Models\ProductBatch;
use App\Models\ProductUnit;
use App\Models\PurchaseOrderDetail;
use App\Models\SyncSaleChannels;
use Illuminate\Support\Facades\Http;
use OmiCare\Utils\V;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\Order;
use App\Models\SaleChannel;
use App\Models\OmiSyncLink;
use OmiCare\Http\Request;
use App\Models\Delivery;
use App\Models\OrderDetail;
use OmiCare\Utils\DB;
use App\Models\SyncProduct;
use App\Models\SyncProductError;
//Get api
use App\Utils\HttpClient;
use App\Utils\ShopeeHelper;
use Date;
use Exception;
use OmiCare\Utils\EventEmitter;

class ShopeeController extends AuthenticatedController
{
    const TYPE_INVOICE = 'invoice';
    const TYPE_ORDER = 'order';
    const TYPE_RETURN = 'return';
    const ACTION_UPDATE_ORDER = 'updateOrder';
    // Get danh sách trạng thái
    
    public function checkProductLink($sync_prd_id){
        $omi_syn_link = OmiSyncLink::where('sync_prd_id',$sync_prd_id)->whereNull('deletedAt')-> first();
        $status = false;
        if(count($omi_syn_link)>=1) $status = true;
        return $status;

    }
    public function getStatus($status)
    {
        $result = [];
        switch ($status) {            
            case 'SHIPPED':    
                    $result = [
                        'status' => 4,
                        'value' => "Hoàn thành",
                        'shopee_status' => 4,
                        'shopee_value' => "Đang giao",
                    ];
                    break;
            case 'CANCELLED':
                    $result = [
                        'status' => 5,
                        'value' => "Đã huỷ",
                        'shopee_status' => 2,
                        'shopee_value' => "Đã hủy",
                    ];
                    break;
            case 'TO_CONFIRM_RECEIVE':
                    $result = [
                        'status' => 4,
                        'value' => "Hoàn thành",
                        'shopee_status' =>4,
                        'shopee_value' => "Đang giao",
                    ];
                    break;
            case 'COMPLETED':    
                    $result = [
                        'status' => 4,
                        'value' => "Hoàn thành",
                        'shopee_status' => 5,
                        'shopee_value' => "Đã giao",
                    ];
                    break;  
            case 'PROCESSED':
                    $result = [
                        'status' => 4,
                        'value' => "Hoàn thành",
                        'shopee_status' => 3,
                        'shopee_value' => "Chờ lấy hàng",
                    ];
            case 'READY_TO_SHIP':
                    $result = [
                        'status' => 4,
                        'value' => "Hoàn thành",
                        'shopee_status' => 3,
                        'shopee_value' => "Chờ lấy hàng",
                    ];
            case 'UNPAID':
                    $result = [
                        'status' => 1,
                        'value' => "Chờ duyệt",
                        'shopee_status' => 1,
                        'shopee_value' => "Chờ xác nhận",
                    ];
            case 'IN_CANCEL':
                    $result = [
                        'status' => 1,
                        'value' => "Chờ duyệt",
                        'shopee_status' => 1,
                        'shopee_value' => "Chờ xác nhận",
                    ];
            case 'INVOICE_PENDING':
                    $result = [
                        'status' => 1,
                        'value' => "Chờ duyệt",
                        'shopee_status' => 1,
                        'shopee_value' => "Chờ xác nhận",
                    ];  
        }
        return $result;
    }   
    

    public function getShopAction(Request $req){
        $saleChannelID = SaleChannel::select('id')->where('name', "like", "%Shopee%")->first();
        $entries = SyncSaleChannels::query()->whereNull('deleted_at')->where('sale_channel_id', $saleChannelID['id'])->get();
        return [
            'code' => 200,
            'message' => "success",
            'data' => [
                'entry' => $entries
            ],
        ];
    }

    public function showAction(Request $req){
        if($req->get('id') && $req->get('id') != null){
            $id = $req->get('id');
            $entries = SyncSaleChannels::query()->where('id', $id)->first();
            return [
                'code' => 200,
                'message' => "success",
                'data' => [
                    'entry' => $entries
                ],
            ];
        }else if($req->get('sale_channel_id') && $req->get('sale_channel_id') != null){
            $sale_channel_id = $req->get('sale_channel_id');
            $entries = SyncSaleChannels::query()->where('sale_channel_id', $sale_channel_id)->whereNull('deleted_at')->get();
            return [
                'code' => 200,
                'message' => "success",
                'data' => [
                    'entry' => $entries
                ],
            ];

        }
    }

    public function getAllPriceAction(Request $req){
        $entries = [
            [
                "id"=> 0,
                "name"=> "Bảng giá chung",
            ],
            [
                "id"=> 1,
                "name"=> "Bảng giá theo VAT",
            ],
        ];
        return [
            'code' => 200,
            'message' => "success",
            'data' => [
                'entry' => $entries
            ],

        ];
    }

    public function addConnectAction(Request $req){
        $entries = [
            'active' => 0,
            'auth_id' => 500389,
            'identity_key' => "235695546",
            'connect' => false,
            'name' => "SnakeFaShop",
            'status' => 0,
        ];
        return [
            'code' => 200,
            'message' => "success",
            'data' => [
                'entry' => $entries
            ],
        ];
    }
    public function isShopExist($shop_name){        
        $status = false;
        $entry = SyncSaleChannels::where('name', 'LIKE', "%".$shop_name."%")
        ->whereNull('deleted_at')
        -> first();
        if(isset($entry -> id)) $status = true;
        return $status;
    }
    public function getShopIdByName($shop_name){
        $entry = SyncSaleChannels::where('name', 'LIKE', "%".$shop_name."%") -> first();
        return $entry -> id;
    }

    public function saveConnectShopeeAction(Request $req)
    {
        DB::beginTransaction();
        // Kiểm tra dữ liệu từ frontend
        if($req->get('entry')){
            // Lấy id của sàn Lazada
            $saleChannelID = SaleChannel::select('id')->where('name', "LIKE", "%Shopee%")->first();
            //Kiểm tra data từ frontend truyền vào: nếu có dữ liệu thì update, ngược lại thêm mới kết nối
            $data = $req->get('entry');

            //Nếu tồn tại id hoặc tên shop đã tồn tại
            if(isset($data['id'])){
                if(isset($data['id'])){
                    $entry = SyncSaleChannels::find($data['id']);
                    if (!$entry) {
                        return [
                            'code' => 404,
                            'message' => 'Không tìm thấy',
                        ];
                    }
                }
            }
            else{
                $checkIdentityKey = SyncSaleChannels::where('identity_key', $data['identity_key'])->whereNull('deleted_at')->count();
                if ($checkIdentityKey > 0) {
                    return [
                        'code' => 501,
                        'message' => $data['name'] . ' đã kết nối với cửa hàng. Vui lòng chọn shop khác!',
                    ];
                }else{
                    $getID = SyncSaleChannels::where('identity_key', $data['identity_key'])->first();
                    if(isset($getID->id)){
                        $entry = SyncSaleChannels::find($getID->id);
                    }else{
                        $entry = new SyncSaleChannels();
                    }

                    //Get đơn hàng
                }
            }
            $entry->fill($data);
            $today = new \DateTime();
            try{
                $token_expired_time = $data['is_active']['expired_time'];
            }catch(Exception $e){
                $data['is_active'] = json_decode($data['is_active']);
                $token_expired_time = $data['is_active']->expired_time;
            }

            $entry -> channel_type = SyncSaleChannels::TYPE_SHOPEE;
            $entry -> deleted_at = null;
            $entry -> created_at = $today;
            $entry -> updated_at = $today;
            $entry -> expire_time = date('Y-m-d H:i:s', strtotime('+'.$token_expired_time.'seconds'));
            $entry -> retailer_id = auth() -> id;
            $entry -> is_active = json_encode($data['is_active']);
            $entry -> sale_channel_id = $saleChannelID['id'];   

            $entry->save();

            $emitter = new EventEmitter();
            //Get orders list
            //Todo emit
            // $emitter::instance()->emit('Order',$data['shop_id']);
            // $this->OrderListShopee($entry);

            //Get product list
            //Sử dụng emitter để hệ thống tự đồng bộ sản phẩm
            $emitter::instance()->emit('SyncProductShopee',$data['shop_id']);

            DB::commit();
            return [
                'code' => 200,
                'message' => 'Thông tin kết nối được cập nhật thành công!',
            ];
        }
        else{
            return [
                'code' => 400,
                'message' => 'Kiểm tra lại thông tin kết nối!',
            ];
        }
    }
  
    public function disConnectAction(Request $req)
    {
        $today = new \DateTime();
        if(isset($req['id'])){
            $entry = SyncSaleChannels::whereNull('deleted_at')->find($req['id']);
            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy!',
                ];
            }else{
                $entry->deleted_at = $today;
                $entry->save();
                return [
                    'code' => 200,
                    'message' => 'Xóa dữ liệu thành công!',
                ];
            }
        }
    }

    public function signAction(Request $req){      
          
        $path = $req -> path;    
        $timest = time();
        $partnerId =  $req -> partnerId;
        $partnerKey= $req -> partnerKey;        
        $baseString = sprintf("%s%s%s", $partnerId, $path, $timest);
        $sign = hash_hmac('sha256', $baseString, $partnerKey);
        $data = [
            'code' => 200,
            'data' => [
                'sign' => $sign,
                'timestamp' => $timest,   
            ]         
        ];        
        return $data;
    }
    //Get token trực tiếp từ font end
    public function tokenAction(Request $req){
      
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://partner.test-stable.shopeemobile.com/api/v2/auth/token/get',
        
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
            "code":"'. $req -> code.'",
            "shop_id":'.$req -> shop_id.',
            "partner_id": '.$req -> partner_id .'
        }',
        CURLOPT_HTTPHEADER => array(
                'Content-Type: text/plain'
            ),
        ));

        $response = curl_exec($curl);  
        
        curl_close($curl);
       
        $result = json_decode($response);
        $data = [
            'code' => 200,
            'message' => 'Get token success',
            'data' => [
                'refresh_token' => $result -> refresh_token,
                'access_token' => $result -> access_token,
                'expire_time' => $result -> expire_time
                ]     
        ];          
        
        return $data;
    }
    


    // Update
    public function getParamAction(Request $req){         
        
        $path = $req -> path;        
        $timest = time();
        $partnerId = $req -> partner_id;
        $partnerKey= $req -> partnerKey;  
        $baseString = sprintf("%s%s%s", $partnerId, $path, $timest);
        $sign = hash_hmac('sha256', $baseString, $partnerKey);   

        $data = [
            'partner_id' => $partnerId,
            'redirect'   =>  $partnerKey,
            'timestamp'  =>  time(),
            'sign' =>  $sign
        ];


        return [
            'code' => 200,
            'message' => 'success',
            'data' => $data,
        ];
    }
 
    public function getToken1($host, $code, $shop_id, $partner_id){
      
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $host. '/api/v2/auth/token/get',
        
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => '{
            "code":"'. $code.'",
            "shop_id":'.$shop_id.',
            "partner_id": '.$partner_id .'
        }',
        CURLOPT_HTTPHEADER => array(
                'Content-Type: text/plain'
            ),
        ));

        $response = curl_exec($curl);  
        
        curl_close($curl);
       
        $result = json_decode($response);
                 
        
        return $result;
    }

    public function getToken($code, $shop_id, $host){        
        $partner_id = appenv('APP_V2_PARTNER_ID');
        $partner_key = appenv('APP_V2_PARTNER_KEY');   
        $path = "/api/v2/auth/token/get";
        $timest = time();
        $body = array("code" => $code,  "shop_id" => (int)$shop_id, "partner_id" => (int)$partner_id);
        $baseString = sprintf("%s%s%s", (int)$partner_id, $path, $timest);
        $sign = hash_hmac('sha256', $baseString, $partner_key);
        $url = sprintf("%s%s?partner_id=%s&timestamp=%s&sign=%s", $host, $path, (int)$partner_id, $timest, $sign);    
        $c = curl_init($url);
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($body));
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        $resp = curl_exec($c);    
        $ret = json_decode($resp, true);   
       
        $accessToken = $ret["access_token"];
        $newRefreshToken = $ret["refresh_token"];
        $expire_in = $ret['expire_in'];
        $data = [
            'access_token' => $accessToken,
            'refresh_token' => $newRefreshToken,
            'expired_time' => $expire_in
        ];
        
        return $data;
    
    }

    // public function getReFreshToken($param){
    //     $httpClient = new HttpClient();
    //     $host= appenv('APP_V2_SHOPEE_API_URL');
    //     $path = "/api/v2/auth/access_token/get";
    //     $url = $host.$path;
    //     $request = $httpClient -> postJSON($url,$param);
    //     return $request;
    // }

    public function infoAction(Request $req){
        $host= appenv('APP_V2_SHOPEE_API_URL');
        $access_token ='';
        $refresh_token ='';
        $path = $req -> path;        
        $timest = time();
        $partnerId = $req -> partner_id;
        $partnerKey = $req -> partnerKey;  
        $baseString = sprintf("%s%s%s", $partnerId, $path, $timest);
        $sign = hash_hmac('sha256', $baseString, $partnerKey);         
        if(isset($req -> code) && isset($req -> shop_id))  {
            $shopPath = "/api/v2/shop/get_shop_info";            
            $data = $this->getToken($req -> code, $req -> shop_id, $host);     
            // Get token
            $access_token = $data['access_token'];
            $refresh_token = $data['refresh_token'];
            $expired_time = $data['expired_time'];            
            $newBaseString = sprintf("%s%s%s%s%s",$partnerId, $shopPath, $timest, $access_token,$req -> shop_id);          
            $newSign = hash_hmac('sha256', $newBaseString, $partnerKey);  
            $data = [
                'host' => $req -> host,
                'access_token' => $access_token,
                'partner_id' =>   $req -> partner_id,
                'partner_key' => $partnerKey,
                'timestamp' =>   $timest,                
                'sign' => $newSign,
                'shop_id' => $req -> shop_id,
                'code' => $req -> code
            ];           
            // Lấy thông tin shop
            $shop = json_decode($this->shopInformation($data));
            $dShop = [
                'shop_id' => $req -> shop_id,
                'shop_name' => $shop -> shop_name,
                'access_token' => $access_token,
                'refresh_token' => $refresh_token,
                'expired_time' => $expired_time
            ];                        
            return [
                'code' => 200,
                'data' => $dShop
            ];
           
        }
    }
    // public function get_token_by_shop_id($shop_id, $refresh){
    //     $partner_id = appenv('APP_V2_PARTNER_ID');
    //     $model = SyncSaleChannels::where('identity_key' , $shop_id) -> first();
    //     $expired_time = strtotime($model['expire_time']);
    //     $refresh_token = json_decode($model['is_active'])->refresh_token;
    //     $today = strtotime(date('Y-m-d H:i:s'));
    //     $is_active = json_decode($model['is_active']);
    //     $enviroment = appenv('APP_ENV');

    //     if($today >= $expired_time || $enviroment !== 'production' || $refresh){
    //         $param = [
    //             'shop_id' => (int)$shop_id,
    //             'refresh_token' => $refresh_token,
    //             'partner_id' => (int)$partner_id
    //         ];
    //         $newToken = json_decode($this->getReFreshToken($param));

    //         $data = [
    //             'access_token' => $newToken->access_token,
    //             'refresh_token' => $newToken->refresh_token,
    //             'expired_time' => $newToken->expire_in
    //         ];

    //         $model['is_active'] = json_encode($data);
    //         $model['expire_time'] = date('Y-m-d H:i:s', strtotime('+'.$newToken->expire_in.'seconds'));
    //         $model->save();

    //         return $newToken->access_token;
    //     }

    //     return $is_active->access_token;
    // }

    public function getPurchase($prdId){
        $quantity =0;
        $purchase = PurchaseOrderDetail::where('productId',$prdId)->first();
        if (isset($purchase)){
            return $purchase->quantity;
        }
        return $quantity;
    }

    public function checkOnhand($shopid){
        $sync= SyncSaleChannels::where('identity_key',$shopid)->first();
        if(isset($sync)){
            $onHand = $sync->sync_onhand;
            if ($onHand == 1){
                $disSale = $sync->dis_sale_products;
                if ($disSale == 0){
                    return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    //Sửa lại sau
    // public function urlStockAction(Request $req){
    //     $prdId = $req->omi_prd_id;
    //     $shop_id = $req->shop_id;
    //     $checkOnhand = $this->checkOnhand($shop_id);
    //     if ($checkOnhand == false){
    //         return[
    //             'code'=>200
    //         ];
    //     }
    //     $item_id = $req->sync_prd_id;
    //     $path = '/api/v2/product/update_stock';
    //     $httpClient = new HttpClient();
    //     $param = "&item_id=".$item_id;
    //     $urlModel = $this->ModelUrlAction($shop_id);
    //     $response = $httpClient -> getResponse($urlModel,$param);
    //     $response = json_decode($response);
    //     $model_id = $response->response->model;
    //     $quanty = $this->getPurchase($prdId);
    //     foreach ($model_id as $item){
    //         $id_model = $item->model_id;
    //     }
    //     $requestParams =
	// 	[
	// 		"seller_stock" =>
	// 			[
    //                 "stock"=> $quanty
	// 			]
	// 	];
    //     $newUrl = $this->URL($path,$requestParams,$shop_id);
    //     $httpClient = new HttpClient();
    //     $param2 =
    //         [
    //             "shop_id" => $shop_id,
    //             "item_id"=> $item_id,
    //             "stock_list"=> [
    //                 [
    //                     "model_id"=>$id_model,

    //                     "seller_stock"=> [
    //                         [
    //                             "stock"=> $quanty
    //                         ]
    //                     ]
    //                 ]
	//             ]
    //         ];
    //     $response2 = $httpClient -> postJSON($newUrl,$param2);
    //     $a = json_decode($response2);
    //     if($a->error == ""){
    //         return[
    //             'code'=>200
    //         ];
    //     }else{
    //         return[
    //             "measeage"=>"Lỗi"
    //         ];
    //     }
    // }

    // public function ModelUrlAction($id){
    //     $shop_id = $id;
    //     $path = '/api/v2/product/get_model_list';
    //     $requestParams =
    //         [
    //             "seller_stock" =>
    //                 [
    //                     "stock"=> 1
    //                 ]
    //         ];
    //     $newUrl = $this->URL($path,$requestParams,$shop_id);
    //     return $newUrl;
    // }

    // public function getModer($path,$requestParams, $shop_id){
    //     $urlModel = $this->URL($path,$requestParams,$shop_id);
    //     return $urlModel;
    // }
    
    // public function URL($path,$requestParams, $shop_id, $refresh = null){        
    //     $shopeeHelper = new ShopeeHelper();
    //     $access_token = $this->get_token_by_shop_id($shop_id, $refresh);
    //     $timestamp = time();
    //     $partner_id = appenv('APP_V2_PARTNER_ID');
    //     $partner_key = appenv('APP_V2_PARTNER_KEY');
    //     $host =  appenv('APP_V2_SHOPEE_API_URL');                
    //     $commParam = [                       
    //         'timestamp' => $timestamp,            
    //         'partner_id' => $partner_id,
    //         'partner_key' => $partner_key,
    //         'access_token' => $access_token,
    //         'path' => $path,
    //         'merchant_id' => 0,
    //         'shop_id' => $shop_id            
    //     ];
    //     $url = $shopeeHelper -> generateRequestUrl($host, $path, $commParam, $requestParams);
    //     return $url;
    // }

    // //Sử dụng khi request api shopee với access_token
    // private function makeShopeeRequest ($path, $requestParams, $shop_id) {
    //     $httpClient = new HttpClient();
    //     $url = $this->URL($path, $requestParams, $shop_id);
    //     $trys = 2;
    //     //Thực hiện request toi shopee
    //     do {
    //         $request = $httpClient -> getShopee($url);
    //         if(isset($request['error']) && $trys > 1 ){
    //             //Nếu có lỗi khi request lần đầu thử refresh token
    //             $url = $this->URL($path, $requestParams, $shop_id, true);
    //         }else if(isset($request['error'])){
    //             //Nếu thử refresh token không được bắn lỗi về client
    //             throw new Exception($request['message']);
    //         }else{
    //             break;
    //         }
    //         $trys--;
    //     } while($trys > 0);
    //     $response = json_decode($request);
    //     return $response;
    // }

    // public function getProductByStatus(Request $req, $item_status){
    //     $products = null;
    //     $shop_id = $req -> shop_id;            
    //     $httpClient = new HttpClient();
    //     $path = '/api/v2/product/get_item_list';
    //     $requestParams = [
    //         'offset' => 0,
    //         'page_size' => 100,
    //         'item_status' =>  $item_status
    //     ];
    //     $url = $this->URL($path, $requestParams, $shop_id);
    //     // $res = json_decode($httpClient -> getShopee($url));
    //     $res = $this->makeShopeeRequest($path, $requestParams, $shop_id); 
    //     $product = $res -> response;
    //     if($product -> total_count > 0){
    //         $listIP = $this->create_product_id_list($product);
    //         // Get name sản phẩm
    //         $newPath = '/api/v2/product/get_item_base_info';
    //         $newRequest = [
    //             'item_id_list' => $listIP
    //         ];
    //         $newUrl = $this->URL($newPath,$newRequest,$shop_id);                
    //         // $newRes = json_decode($httpClient -> getShopee($newUrl));
    //         $newRes = $this->makeShopeeRequest($newPath, $newRequest, $shop_id); 
    //         $products = $newRes->response->item_list;
    //     }
    //     return $products;
    // }

    // public function ProductListAction(Request $req){
    //     $httpClient = new HttpClient();
    //     $products = [];
    //     $item_status = [
    //         // 'NORMAL','BANNED','DELETED','UNLIST'            
    //         'NORMAL','BANNED','DELETED','UNLIST'            
    //     ];
    //     $lstID = $this->get_list_id_product($req, $item_status);
    //     $products = $this->get_base_list_product($lstID,$req -> shop_id, $httpClient,  $req);
    //     $total = $products['total'];
    //     $paginate = $products['pagination'];      
    //     $products = $this->getSyncProduct($products['products'],$req);
    //     // $products = $this->getByPropertiesData($products,$req->shop_id);
    //     $products = $this->propertiesData($products,$req->shop_id);
    //     if(isset($req->link) && !empty($req->link) || isset($req->sync) && !empty($req->sync)){
    //         $products = $this->filterProductForStatus($products, $req->link, $req->sync);
    //     }

        
    //     $data = [
    //         'code' => 200,
    //         'data' => $products,
    //         'total' => $total,
    //         'pagination' => $paginate       
    //     ];
        
    //     return $data;
    // }

    public function ProductListAction(Request $req){
        $shop_id = $req->shop_id;
        $page = $req->page;
        $limit = $req->number_of_records;
        
        $query = SyncProduct::where('shop_id',$shop_id);
        $total = count($query->get());
        $products = $query->paginate($limit, ['*'], 'page', $page);
        $data = $products->items();

        //Format data for client 
        foreach ($products as $product) {
            $product->image = json_decode($product->image);
            $product->dimension = json_decode($product->dimension);
            $product->logistic_info = json_decode($product->logistic_info);
            $product->pre_order = json_decode($product->pre_order);
            $product->brand = json_decode($product->brand);
            $product->description_info = json_decode($product->description_info);
            if(isset($product->property)){ $product->property = json_decode($product->property); }
            if(isset($product->status)){ $product->prd_sync = json_decode($product->prd_sync); }
            if(isset($product->price_info)){ $product->price_info = json_decode($product->price_info); }
            if(isset($product->stock_info)){ $product->stock_info = json_decode($product->stock_info); }
            if(isset($product->stock_info_v2)){ $product->stock_info_v2 = json_decode($product->stock_info_v2); }
            if(isset($product->promotion_id)){ $product->promotion_id = json_decode($product->promotion_id); }
        }

        if(isset($req->link) && !empty($req->link) || isset($req->sync) && !empty($req->sync)){
            $data = $this->filterProductForStatus($products, $req->link, $req->sync);
        }


        return [
            'code' => 200,
            'data' => $data,
            'total' => $total,
            'pagination' => [
                'currentPage' => $products->currentPage(),
                'lastPage' => $products->lastPage(),
            ] 
        ];
    }

//     private function getSyncProduct ($data,$req = null){
//         $entries = OmiSyncLink::where('sale_channel_name', 'Shopee')->whereNull('deletedAt')->get();
//         if(!empty($entries)){
//             foreach ($data as $item){
//                 foreach ($entries as $entry){
// //                    $getOmiPrd = Product::query()->where('id', $entry->omi_prd_id)->get();
//                     $getOmiPrd = $this->getProductData($req,[ $entry->omi_prd_id]);

//                     if($item->item_id === $entry->sync_prd_id){
//                         $item->prd_sync = $getOmiPrd;
//                         $item->status = $entry->status;
//                         $item->message = $entry->message;
//                         $item->linked = 1;
//                     }
//                 }
//             }
//         }
//         return $data;
//     }

    private function getSyncProduct ($data){
        $entries = OmiSyncLink::where('sale_channel_name', 'Shopee')->whereNull('deletedAt')->get();
        if(!empty($entries)){
            foreach ($data as $item){
                foreach ($entries as $entry){
                    $getOmiPrd = Product::query()->where('id', $entry->omi_prd_id)->get();
                    if(isset($item->model_id) && $item->model_id === $entry->sync_prd_id){
                        $item->prd_sync = $getOmiPrd;
                        $item->status = $entry->status;
                        $item->message = $entry->message;
                        $item->linked = 1;
                    }else if(isset($item->item_id) && $item->item_id === $entry->sync_prd_id){
                        $item->prd_sync = $getOmiPrd;
                        $item->status = $entry->status;
                        $item->message = $entry->message;
                        $item->linked = 1;
                    }
                }
            }
        }
        return $data;
    }

    private function filterProductForStatus($data, $link, $sync){
        $filtered = array();
        if($link === null){
            $filtered = $data;
        }else{
            switch($link){
                case 'filter_link_all' :
                    $filtered = $data;
                    break;
                case 'filter_link_linked' :
                    foreach ($data as $value) {
                        if (isset($value->status) && $value->status == 0) {
                            array_push($filtered,$value);
                        }
                    }
                    break;
                case 'filter_link_noLink' :
                    foreach ($data as $value) {
                        if (!isset($value->status)) {
                            array_push($filtered,$value);
                        }
                    }
                    break;
            }
        }
        //Can update sau khi HuyTQ lam xong sync product
        $filtered_array = array();
        if($sync === null){
            $filtered_array = $filtered;
        }else{
            switch($sync){
                case 'filter_sync_all' :
                    $filtered_array = $filtered;
                    break;
                case 'filter_sync_success' :
                    foreach ($filtered as $value) {
                        $status = $this->checkSyncError($value['item_id']);
                        if($status === 0){
                            $filtered_array[] = $value;
                        }
                    }
                    break;
                case 'filter_sync_error' :
                    foreach ($filtered as $value) {
                        $status = $this->checkSyncError($value['item_id']);
                        if($status === 1){
                            $filtered_array[] = $value;
                        }
                    }
                    break;
            }
        }
        return $filtered_array;
    }

    private function checkSyncError($prd_id){
        $status = 0;
        $in_omi_syn_link = OmiSyncLink::query()->where('sync_prd_id', $prd_id)->where('status', 1)->whereNull('deletedAt')->count();
        if($in_omi_syn_link > 0){
            $status = 1;
        }else{
            $status = 0;
        }
        return $status;
    }

    // public function UpdateStockAction(Request $req){
    //     $products = [];
    //         if($this->getProductUpdate($req) != null) {
    //             $products =$this->getProductUpdate($req);
    //         };
    //     return [
    //         'code' => 200,
    //         'data' => $products
    //     ];
    // }

    // public function getProductUpdate(Request $req){
    //     $products = null;
    //     $shop_id = $req -> shop_id;
    //     $httpClient = new HttpClient();
    //     $path = '/api/v2/product/update_item';
    //     $requestParams = [
    //         'offset' => 0,
    //         'page_size' => 100,
    //         'item_status' =>'123'
    //     ];
    //     $url = $this->URL($path, $requestParams, $shop_id);
    //     $res = json_decode($httpClient -> getShopee($url));
    //     $product = $res -> response;
    //     return $products;
    // }

    // public function get_base_list_product($data, $shop_id, $httpClient, Request $req){
    //     $newPath = '/api/v2/product/get_item_base_info';            
    //         $products = [];                    
    //         $newRequest = [
    //             'item_id_list' => $data['listId']
    //         ];            
    //         $newUrl = $this->URL($newPath,$newRequest,$shop_id); 
    //         $newRes = $this->makeShopeeRequest($newPath, $newRequest, $shop_id);         
    //         $products = $newRes->response->item_list;
    //         $totalPage = $data['total'] / $req -> number_of_records;
    //         $lastPage = $data['total'] % $req ->number_of_records > 0 ? ($totalPage + 1) : $totalPage;
    //         $data = [
    //             'products' => $products, 
    //             'total' => $data['total'], 
    //             'pagination' => [
    //                 'currentPage' => (int)$req -> page,
    //                 'lastPage' => (int)$lastPage
    //             ],
    //         ];
            
    //         return $data;  
    // }

    // public function get_list_id_product(Request $req, $item_status){
    //     $listID = null;
    //     $shop_id = $req -> shop_id;            
    //     $httpClient = new HttpClient();
    //     $path = '/api/v2/product/get_item_list';
    //     $requestParams = [
    //         'offset' => (int)$req -> offset,                                   
    //         'page_size' =>  $req -> number_of_records,
    //         'item_status' =>  $item_status,
    //     ];        
    //     $number_of_records = $req -> number_of_records;
    //     $page = $req -> page;
    //     $res = $this->makeShopeeRequest($path, $requestParams, $shop_id); 
        
    //     $product = $res -> response;
        
    //     if($product -> total_count > 0){            
    //         $listID = $this->create_product_id_list($product,$number_of_records, $page); 
    //     }        
    //     $data = [
    //         'listId' => $listID,
    //         'total' => $product -> total_count
    //     ];
    //     return $data;
    // }
    // public function create_product_id_list($products){
    //     $id = [];   
    //     foreach ($products->item as $item) {     
    //       $id[] =  $item -> item_id;        
    //     }           
       
    //     return $id;
       
    // }
  
    public function shopInformation($data){
        $httpClient = new HttpClient();
        $param = [
            'access_token' => $data['access_token'],
            'code' => $data['code'],
            'partner_id'=> $data['partner_id'],
            'shop_id' => $data['shop_id'],
            'sign' => $data['sign'],
            'timestamp' => $data['timestamp'],
        ];
        $url = $data['host'] . '/api/v2/shop/get_shop_info';  

        $response = $httpClient -> getJSON($url, $param);
       return $response;
    }

    // Orders
    public function isOrderExist($code){
        $status =  false;
        $order = Order::where('code', 'DHSPE_' . $code) -> first();
        if($order == null) {
            $status = false;
        }
        else {
            $status = true;
        }        
        return $status;
    }
    
    // Lưu chi tiết sản phẩm trong đơn hàng
   
    public function storeOrderDetails($ordDetail,$orders){      
           
            if(count($ordDetail)<=1){           
                $order_details = new OrderDetail(); 
                $order_details -> orderId = $orders;
                $order_details -> productCode = $ordDetail['productCode'];
                $order_details -> productName = $ordDetail['productName'];                
                $order_details -> productId = $ordDetail['productId'];
                $order_details -> price = $ordDetail['price'];
                $order_details -> note = $ordDetail['note'];
                $order_details -> discount = $ordDetail['discount'];                
                $order_details -> quantity = $ordDetail['quantity'];  
                $order_details -> created_at = date('Y-m-d H:i:s'); 
                $order_details -> updated_at = date('Y-m-d H:i:s'); 
                $order_details -> save();    
            }
            else {     
                
                foreach ($ordDetail as $value) {                         
                    $order_details = new OrderDetail(); 
                    try {
                        $order_details -> orderId = $orders;
                        $order_details -> productCode = $value['productCode'];
                        $order_details -> productName = $value['productName'];                
                        $order_details -> productId = $value['productId'];
                        $order_details -> price = $value['price'];
                        $order_details -> note = $value['note'];
                        $order_details -> discount = $value['discount'];                
                        $order_details -> quantity = $value['quantity'];  
                        $order_details -> created_at = date('Y-m-d H:i:s'); 
                        $order_details -> updated_at = date('Y-m-d H:i:s');                         
                        $order_details -> save(); 
                          
                    } catch (\Throwable $th) {
                        echo 'message:' . $th -> getMessage();
                    }                    
                }
                // DB::commit(); 
            }
            
              
    }
    public function getTimestamp(){
        $today = date('Y-m-d H:i:s');
        $timestamp = strtotime($today);
        return $timestamp;
    }
      //Lấy dữ liệu order từ shopee
    public function OrderListShopee($entry){
       
        $shopeeHelper = new ShopeeHelper();
        $httpClient = new HttpClient();
        $is_active = json_decode($entry['is_active']);
        $access_token = $is_active -> access_token ;
        $host = appenv('APP_V2_SHOPEE_API_URL');
        $path = "/api/v2/order/get_order_list";
        $times = $this->getTimestamp();         
        $time_from = 1669884743;
        $time_to = $times;
        $commonParams = [
            'partner_key' => appenv('APP_V2_PARTNER_KEY'),
            'partner_id' => appenv('APP_V2_PARTNER_ID'),
            'path' => $path,
            'access_token' => $access_token,
            'shop_id' => $entry['identity_key'],
            'merchant_id' => null
        ];
        $requestParams = [
            'time_range_field' => 'create_time',
            'time_from' =>  $time_from,
            'time_to' => $time_to,
            'page_size' => 100,
            'cursor'=> null,
            
        ];  
        $url = $shopeeHelper -> generateRequestUrl($host, $path, $commonParams, $requestParams);        
        $response = json_decode($httpClient -> getShopee($url));    
        
        $lstOrders = [];
        $orders = $response -> response -> order_list;
        if($orders !== null) {
            foreach ($orders as $order) {
                $lstOrders[] = $order -> order_sn;
            }      
            //Lấy dữ liệu
            $type = "order";
            $data = $this->get_order_details($lstOrders, $commonParams, $access_token, $entry['identity_key']);        
            // Đồng bộ đơn hàng
            $this->SyncOrderShopee($data, $entry, $type);                               
        }       
      
    }
   
    public function get_order_details($orders, $commonParams, $access_token, $shop_id){
        
        $ordDetail = [];
        $shopeeHelper = new ShopeeHelper();
        $httpClient = new HttpClient();
        $host = appenv('APP_V2_SHOPEE_API_URL');
        $path = "/api/v2/order/get_order_detail";
        $commonParams = [
            'partner_key' => appenv('APP_V2_PARTNER_KEY'),
            'partner_id' => appenv('APP_V2_PARTNER_ID'),
            'path' => $path,
            'access_token' => $access_token,
            'shop_id' => $shop_id,
            'merchant_id' => null
        ];
        foreach ($orders as $ord_item) {            
                $requestParams = [
                    'order_sn_list' => $ord_item,
                    'response_optional_fields' =>[
                        "actual_shipping_fee, actual_shipping_fee_confirmed,buyer_cancel_reason, 
                        buyer_cpf_id,buyer_user_id,buyer_username,buyer_username,buyer_username,cancel_by,cancel_reason,dropshipper_phone,edt,estimated_shipping_fee,estimated_shipping_fee, 	
                        fulfillment_flag,goods_to_declare,invoice_data,item_list,item_list,note,note_update_time,order_chargeable_weight_gram,
                        package_list,pay_time,payment_method,pickup_done_time,prescription_check_status	prescription_images,recipient_address,
                        reverse_shipping_fee,shipping_carrier,split_up,total_amount, attribute_list, package_list"]
                ];
                $url = $shopeeHelper -> generateRequestUrl($host, $path, $commonParams, $requestParams);
                $res = json_decode($httpClient -> getShopee($url)) -> response -> order_list;                
                $ordDetail[] = $res[0];
        }
       
        //Store Orders Details
        // $this->storeOrdDetail($ordDetail, $orders);               
        return $ordDetail;        
    }
  
    public function SyncOrderShopee($data, $entry, $type){
       
        $status = [];
        $sale_channel_id = $entry['sale_channel_id'];
        $saleChannel = SaleChannel::select('name')->find($sale_channel_id);
        $sale_channel_name = $saleChannel -> name;        
        if(!isset($data) || $data == null){
            return [
                'code' => 4,
                'message' => 'Phiếu hàng đang trống!',
            ]; 
        }      
        if ($type == self::TYPE_ORDER){          
            foreach ($data as $item) {
                

                //Lưu khách hàng
                //-----------------------------------------------------------------------
                    $cus_shopee = '';
                    $customer = Customer::where('status', 1)
                                ->where('contactNumber', 'KHSPE_' . $item -> recipient_address -> phone,)->first();                                        
                    if(!isset($customer) || $customer == null){                         
                        $cus_data = [
                            'name' => $item -> recipient_address -> name,
                            'code' => 'KHSPE_' . $item -> buyer_user_id,
                            'contactNumber' =>  $item -> recipient_address -> phone,
                            'address' => $item -> recipient_address -> full_address,
                            'status' => 1,                                
                            'createdAt' => date('Y-m-d H:i:s'),
                            'updatedAt' => date('Y-m-d H:i:s'),
                        ];                            
                        $cus_shopee = $this->storeCustomer($cus_data); 
                        
                    } 
                    else {  
                    
                        $cus_shopee = [
                            'id' =>  $customer -> id,
                            'name' =>  $customer -> name,
                            'code' =>  $customer -> code,
                            'contactNumber' =>  $customer -> contactNumber,
                            'address' => $customer -> address,
                        ];                             
                    }                           
                //-----------------------------------------------------------------------
                    // Khu vực lưu vào bảng order
                    $ordID = '';
                    $order_sn = 'DHSPE_' . $item -> order_sn;
                    $ordStatus = Order::where('code', $order_sn) -> first();
                    if(!isset($ordStatus) || $ordStatus == null){
                        $order = new Order();
                        $order->code = 'DHSPE_' . $item -> order_sn;
                        $order->purchaseDate = date('Y-m-d H:i:s', $item -> create_time);                 
                        $status = $this->getStatus($item -> order_status);
                        $order->status = $status['status'];
                        $order->statusValue = $status['value'];
                        $order->shopee_status = $status['shopee_status'];
                        $order->shopee_value = $status['shopee_value'];
                        $order->createdBy = auth()->id;
                        $order->usingCod = "";
                        $order->branchId = $entry['branch_id'];
                        $order->branchName = $entry['branch_name'];
                        $order->soldById = auth()->id;
                        $order->soldByName = auth()->name;
                        //Thông tin khách hàng
                        $order->customerId = $cus_shopee['id'] ?? null;
                        $order->customerCode = $cus_shopee['code'] ?? null;
                        $order->customerName = $cus_shopee['name'] ?? null;
                        //Thanh toán
                        $order->totalOrigin = $item -> total_amount ?? 0;
                        $order->total = $item -> total_amount ?? 0;
                        $order->totalPayment = $item -> total_amount ?? 0;
                        $order->discountRatio = $item ->discountRatio ?? 0;
                        $order->discount = $item -> discount ?? 0;
                        $order->description = $item -> note ?? '';
                        $order->orderDetails = json_encode($item -> item_list);
                        $order->saleChannelId = $sale_channel_id;
                        $order->saleChannelName = $sale_channel_name;
                        $order->retailerId = $entry['identity_key'] ?? null;
                        $order->orderDelivery = ''; 
                        $order->save();    
                        $ordID = $order->id;                                        
                    }
                    else {
                        $ordID = $ordStatus -> id;
                    }
                //-----------------------------------------------------------------------
                // Lưu vận chuyển
                
                $delivery_data = [
                    "code" => $item -> order_sn, //Mã vận chuyển                    
                    "type" => 2,
                    "price" => $item -> total_amount, //Giá tiền
                    "receiver" => $item -> recipient_address -> name,
                    "contactNumber" =>  $item -> recipient_address -> phone,
                    "address" =>  $item -> recipient_address -> district . ' ' . $item -> recipient_address -> city,
                    "locationName" => $item -> recipient_address -> district . ' ' . $item -> recipient_address -> city,                   
                    "orderID" => isset($ordID) && $ordID != null ? $ordID : ''
                ];   
                
                //Kiểm tra
                $orderDelivery = Delivery::query()
                ->where('type', Delivery::TYPE_ORDER)
                ->where('orderId', $ordID)
                ->first();                       
                if (empty($orderDelivery)) {                    
                    $this->storeDelivery($delivery_data);
                }                     
                //------------------------------------------------------------------------------- 

            }
        }        
        // return $order;
    }    
    public function storeCustomer($data){           
        $customer = new Customer();
        $customer -> name = $data['name'];
        $customer -> code = $data['code'];
        $customer -> contactNumber = $data['contactNumber']; 
        $customer -> address = $data['address']; 
        $customer -> createdDate = $data['createdAt'];
        $customer -> modifiedDate = $data['updatedAt']; 
        $customer->save();
        return [
            'id' =>  $customer -> id,
            'name' =>  $customer -> name,
            'code' =>  $customer -> code,
            'contactNumber' =>  $customer -> contactNumber,
            'address' => $customer -> address,
        ];
    }

    public function storeDelivery($delivery){                
        $orderDelivery = new Delivery();        
        $orderDelivery -> code = $delivery['code'];
        $orderDelivery -> type = $delivery['type'];
        $orderDelivery -> status = 1;
        $orderDelivery -> price = $delivery['price'];
        $orderDelivery -> orderId = $delivery['orderID'];
        $orderDelivery -> receiverName = $delivery['receiver'];
        $orderDelivery -> receiverPhone = $delivery['contactNumber'];
        $orderDelivery -> receiverAddress = $delivery['address'] . ' ' . $delivery['locationName'];                     
        $orderDelivery -> save();   
    }

    // private function getByPropertiesData($data, $shop_id){
    //     $data = $this->propertiesData($data,$shop_id);
    //     dd($data);
    //     $newData = [];
    //     foreach ($data as $val) {
    //         if( $val->model->model != []){
    //                     foreach ($val->model->model as $value) {
    //                         array_push($newData, [
    //                             'item_name' => $val->item_name,
    //                             'item_id' => $value->model_id,
    //                             'item_sku' => $value->model_sku,
    //                         ]);
    //                     }
    //         }else{
    //             array_push($newData, [
    //                 'item_name' => $val->item_name,
    //                 'item_id' => $val->item_id,
    //                 'item_sku' => $val->item_sku,
    //             ]);

    //         }
    //     }
    //     return $newData;
    // }

    // private function propertiesData($data, $shop_id)
    // {
    //     $newData = [];
    //     foreach ($data as $val) {
    //         if(!$val->has_model){
    //             $val -> model_id = null;
    //             array_push($newData,$val);
    //         }else{
    //             $option_list = $this->getModelList($val->item_id, $shop_id);
    //             $val -> model_id = null;
    //             if (!empty($option_list->response)){
    //                 $productModel = $option_list->response->model;
    //                 $productVariation = $option_list->response->tier_variation;
    //                 if($productModel && $productVariation){
    //                     foreach($productModel as $modal){
    //                         $newVal = clone $val;
    //                         $newVal -> property = [];
    //                         $newVal -> model_id = $modal -> model_id;
    //                         foreach($modal -> tier_index as $key => $tier){
    //                             $newVal -> property[$productVariation[$key]->name] =  $productVariation[$key]->option_list[$tier]->option;
    //                             $newVal -> item_name =  $newVal -> item_name . '-' . $productVariation[$key]->option_list[$tier]->option;
    //                         }
    //                         array_push($newData,$newVal);
    //                     }
    //                 }
    //             }
    //         }
    //     }
    //     return $newData;
    // }

    // private function getModelList($item_id, $shop_id)
    // {
    //     $httpClient = new HttpClient();
    //     $param = '';
    //     $path = '/api/v2/product/get_model_list';
    //     $requestParams =
    //         [
    //             "seller_stock" =>
    //                 [
    //                     "stock" => 1
    //                 ]
    //         ];
    //     $param .= '&item_id=' . $item_id;
    //     $newUrl = $this->URL($path, $requestParams, $shop_id);
    //     $response = $httpClient->getResponse($newUrl, $param);
    //     return $response;
    // }

    public function getProductData(Request $request, $productIds)
    {
        $keyword = $request->get('keyword');
        $currentBranchID = $request->header('X-Branch-Id');
        $type = $request->get('type', null);
        $id = $request->get('id', null);
        $productCode = $request->get('product_code');

        $query = Product::query()
            ->select('id', 'code', 'name', 'images', 'type')
            ->with(['productUnits', 'productBatches' => function ($q) {
                $q->orderBy('expireDate');
            }])
            ->where('isActive', 1);

        if ($keyword) {
            $keyword = "%" . $keyword . '%';

            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'LIKE', $keyword)
                    ->orWhere('code', 'LIKE', $keyword);
            })
                ->orderByDesc('id')
                ->limit(25);

        }

        if (!empty($productIds)) {
            if (is_string($productIds)) {
                $productIds = explode(',', $productIds);
            }
            $query->whereIn('id', $productIds);
        }

        if ($productCode) {
            $query->where('code', $productCode);
        }

        $products = $query->get();

        if (empty($productIds)) {
            $productIds = $products->pluck('id');
        }

        $actualInventoryMap = Inventory::getActualInventoryMap($productIds, $currentBranchID);
        $systemInventoryMap = Inventory::getSystemInventoryMap($productIds, $currentBranchID);
        $inventoryProductBatchMap = Inventory::getInventoryProductBatchMap($productIds, $currentBranchID);
        $orderSupplierSumByProductMap = OrderSupplierDetail::getOrderSupplierSumByProductMap($productIds, $currentBranchID);
        $orderDetailsSumByProductMap = OrderDetail::getOrderDetailsSumByProductMap($productIds, $currentBranchID);

        if (!empty($type) && !empty($id)) {
            if ($type == self::TYPE_ORDER) {
                ;
                $refTable = Inventory::REF_CODE_ORDER_DETAIL;
                $refIds = OrderDetail::where('orderId', $id)->pluck('id');
            }

            if ($type == self::TYPE_INVOICE) {
                $refTable = Inventory::REF_CODE_INVOICE_DETAIL;
                $refIds = InvoiceDetail::where('invoiceId', $id)->pluck('id');
            }

            if (!empty($refIds) && !empty($refTable)) {
                $systemInventoryUpdatedMap = Inventory::query()
                    ->selectRaw('id, productId, branchId, SUM(ABS(onHand*conversionValue)) as quantity')
                    ->where('refTable', $refTable)
                    ->whereIn('refId', $refIds)
                    ->where('branchId', $currentBranchID)
                    ->groupBy('productId')
                    ->pluck('quantity', 'productId');

                $inventoryProductBatchUpdatedMap = Inventory::query()
                    ->selectRaw('*,SUM(ABS(onHand*conversionValue)) as quantity')
                    ->where('refTable', $refTable)
                    ->whereIn('refId', $refIds)
                    ->where('branchId', $currentBranchID)
                    ->whereNotNull('productBatchId')
                    ->groupBy('productBatchId')
                    ->pluck('quantity', 'productBatchId');
            }
        }

        /**
         * @var Product $product
         * @var ProductUnit $productUnit
         * @var ProductBatch $proBatch
         */
        foreach ($products as $product) {
            $product->thumb_url = get_thumb_product($product->images ?? null);
            $product->isTypeService = Product::isTypeService($product);

            if (count($product->productUnits)) {
                foreach ($product->productUnits as $productUnit) {
                    $conversionValue = $productUnit->conversionValue ?? 1;

                    $quantityOrigin = isset($systemInventoryMap[$product->id]) ? $systemInventoryMap[$product->id] : 0;

                    if (!empty($systemInventoryUpdatedMap) && !empty($systemInventoryUpdatedMap[$product->id])) {
                        $quantityOrigin += $systemInventoryUpdatedMap[$product->id];
                    }
                    $productUnit->quantityOrigin = $conversionValue ? floor($quantityOrigin / $conversionValue) : 0;

                    $quantityActual = isset($actualInventoryMap[$product->id]) ? $actualInventoryMap[$product->id] : 0;
                    $productUnit->quantityActual = $conversionValue ? floor($quantityActual / $conversionValue) : 0;

                    $supplierCount = $orderSupplierSumByProductMap[$product->id] ?? 0;
                    $supplierCount = $conversionValue ? floor($supplierCount / $conversionValue) : 0;
                    $productUnit->supplier_count = $supplierCount;

                    $orderDetailsCount = $orderDetailsSumByProductMap[$product->id] ?? 0;
                    $orderDetailsCount = $conversionValue ? floor($orderDetailsCount / $conversionValue) : 0;
                    $productUnit->order_details_count = $orderDetailsCount;
                    $productUnit->promotions = (new PromotionsController())->getProductUnitPromotions(new Request([], ['id' => $productUnit->id]));
                }
            }

            if (count($product->productBatches)) {
                $updatedFirstBatch = false;
                foreach ($product->productBatches as $key => $proBatch) {
                    if (!empty($inventoryProductBatchMap[$proBatch->id])) {
                        $proBatch->quantityOrigin = $inventoryProductBatchMap[$proBatch->id] ?? 0;

                        if (!empty($inventoryProductBatchUpdatedMap) && !empty($inventoryProductBatchUpdatedMap[$proBatch->id])) {
                            $proBatch->quantityOrigin += $inventoryProductBatchUpdatedMap[$proBatch->id];
                        }

                        if (!$updatedFirstBatch) {
                            $proBatch->selected = true;
                            $proBatch->quantity = 1;
                            $updatedFirstBatch = true;
                        } else {
                            $proBatch->selected = false;
                            $proBatch->quantity = 0;
                        }
                    } else {
                        unset($product->productBatches[$key]);
                    }
                }

                $productBatches = array_values($product->productBatches->toArray());
                unset($product->productBatches);
                $product->product_batches = $productBatches;
            }

            $product->productUnit = $product->productUnits[0] ?? null;
            $product->productUnitId = $product->productUnits[0]->id ?? null;
            $product->price = $product->productUnits[0]->price ?? 0;
            $product->subTotal = $product->price;
            $product->actualInventory = (!$product->isTypeService && $product->productUnit) ? $product->productUnit->quantityActual : 0;
            $product->quantityOrder = $product->productUnit ? $product->productUnit->order_details_count : 0;
            $product->supplierCount = $product->productUnit ? $product->productUnit->supplier_count : 0;
        }

        return $products;
    }

    public function syncProductShopeeAction(Request $req){
        $shopeeHelper = new ShopeeHelper();
        $response = $shopeeHelper->handleSyncProductShopee((int)($req->shop_id));
        return $response;
    }
}
