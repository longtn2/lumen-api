<?php


namespace App\Http\Controllers\V1;



use App\Http\Controllers\Controller;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;

class LocationsController extends Controller
{
    public function provincesAction(Request $request)
    {
        $provinceId = $request->get('provinceId');
        $districtId = $request->get('districtId');
        $provinces = DB::select("SELECT id,`name` FROM local_provinces ORDER BY `order` desc,`name` asc");
        $districts = [];
        $wards = [];

        if ($provinceId) {
            $districts = DB::select("SELECT id,`name` FROM local_districts WHERE provinceId=?", [$provinceId]);
        }

        if ($districtId) {
            $wards = DB::select("SELECT id,`name` FROM local_wards WHERE districtId=?", [$districtId]);
        }

        return [
            'code' => 200,
            'data' => [
                'provinces' => $provinces,
                'districts' => $districts,
                'wards' => $wards,
            ]
        ];

    }

    public function wardsAction(Request $request)
    {
        $districtId = $request->get('districtId');

        $wards = DB::select("SELECT id,`name` FROM local_wards WHERE districtId=?", [$districtId]);

        return [
            'code' => 200,
            'data' => $wards
        ];

    }

    public function districtsAction(Request $request)
    {
        $provinceId = $request->get('provinceId');

        $districts = DB::select("SELECT id,`name` FROM local_districts WHERE provinceId=?", [$provinceId]);

        return [
            'code' => 200,
            'data' => $districts
        ];

    }
}
