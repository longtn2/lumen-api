<?php

namespace App\Http\Controllers\V1;


use App\Models\Delivery;
use App\Http\Controllers\AuthenticatedController;
use App\Models\DeliveryStatus;
use App\Models\History;
use App\Models\ServiceType;
use App\Models\Transporter;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class DeliveriesController extends AuthenticatedController
{

    /**
     * @uri /v1/deliveries/show
     * @return array
     */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Delivery::query()
            ->with(['invoice' => function ($q) {
                $q->with('customer')->withCount('invoiceItems');
            }, 'branch', 'userCreated', 'transporter'])
            ->where('type', Delivery::TYPE_INVOICE)
            ->find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Delivery not found'
            ];
        }

        $location = new \stdClass();
        $location->provinceId = $entry->receiverProvinceId;
        $location->districtId = $entry->receiverDistrictId;
        $location->wardId = $entry->receiverWardId;

        $listStatus = Delivery::$listStatus;
        $listWeightUnit = Delivery::$listWeightUnit;
        $listTransporter = Transporter::select(['id', 'name'])->orderBy('name')->get();
        $listServiceType = ServiceType::select(['id', 'name'])->where('isActive', 1)->orderBy('name')->get();

        $deliveryStatus = DeliveryStatus::query()
            ->where('deliveryId', $entry->id)
            ->orderByDesc('id')
            ->get();

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'location' => $location,
                'list_status' => $listStatus,
                'list_weight_unit' => $listWeightUnit,
                'list_transporter' => $listTransporter,
                'list_service_type' => $listServiceType,
                'delivery_status' => $deliveryStatus
            ]
        ];
    }

    /**
     * @uri /v1/deliveries/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Delivery::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/deliveries/save
     * @return array
     */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::required, [])
            ->validOrFail();

        DB::beginTransaction();
        /**
         * @var Delivery $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Delivery::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Delivery();
        }

        if ($entry->status != $data['status']) {
            $entry->saveStatus($data['status'], '', false);

            $isCancelDeli = $data['status'] == Delivery::STATUS_CANCEL;
        }

        $entry->fill($data);
        $entry->saveHistory(!empty($isCancelDeli) ? History::ACTION_CANCEL : History::ACTION_UPDATED);
        $entry->save();

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/deliveries/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/deliveries/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $fieldSelects = [];
        $fieldDisplays = [];

        foreach ($fields as $field) {
            if (!empty($field['isColumn']) && !empty($field['field'])) {
                $fieldSelects[] = $field['field'];
            }

            if (!empty($field['relatedField'])) {
                $fieldSelects[] = $field['relatedField'];
            }

            if (empty($field['hide'])) {
                $field['selected'] = empty($field['selected']) ? false : true;
                $fieldDisplays[] = $field;
            }
        }

        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $keyword = $req->get('keyword');
        $branchIds = $req->get('branch_ids');
        $transporterIds = $req->get('transporter_ids');
        $status = $req->get('status');
        $createdByIds = $req->get('created_by_ids');
        $soldByIds = $req->get('sold_by_ids');
        $created = $req->get('created');

        $query = Delivery::query()
            ->with(['invoice.customer', 'userCreated', 'branch', 'transporter', 'serviceType', 'soldBy',
                'province', 'district', 'ward'])
            ->select($fieldSelects)
            ->where('type', Delivery::TYPE_INVOICE)
            ->orderBy($sortField, $sortDirection);

        if ($keyword) {
            $query->where('code', 'LIKE', '%' . $keyword. '%');
        }

        if ($branchIds) {
            $query->whereIn('branchId', explode(',', $branchIds));
        }

        if ($transporterIds) {
            $query->where('transporterId', explode(',', $transporterIds));
        }

        if ($status) {
            $query->whereIn('status', explode(',', $status));
        }

        if ($createdByIds) {
            $query->whereIn('createdBy', explode(',', $createdByIds));
        }

        if ($soldByIds) {
            $query->whereIn('soldById', explode(',', $soldByIds));
        }

        $query->createdIn($created);
        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        $mapStatus = array_column(Delivery::$listStatus, 'name', 'id');
        foreach ($entries as $item) {
            if (!$item->usingPriceCod) {
                $item->priceCodPayment = null;
            }

            $item->status = $mapStatus[$item->status] ?? '';
            $item->invoiceCode = $item->invoice->code ?? null;
            $item->userCreatedName = $item->userCreated->name ?? null;
            $item->invoiceCustomerName = $item->invoice->customer->name ?? null;
            $item->branchName = $item->branch->name ?? null;
            $item->soldByName = $item->soldBy->name ?? null;
            $item->provinceName = $item->province->name ?? null;
            $item->districtName = $item->district->name ?? null;
            $item->wardName = $item->ward->name ?? null;
            $item->transporterName = $item->transporter->name ?? null;
            $item->serviceTypeName = $item->serviceType->name ?? null;
        }

        return [
            'code' => 200,
            'data' => [
                'fields' => $fieldDisplays,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        return [
            [
                'field' => 'id',
                'name' => 'ID',
                'isColumn' => true,
            ],
            [
                'field' => 'code',
                'name' => 'Mã vận đơn',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'invoiceCode',
                'relatedField' => 'invoiceId',
                'name' => 'Mã hóa đơn',
                'selected' => true
            ],
            [
                'field' => 'createdAt',
                'render' => 'datetime',
                'name' => 'Thời gian tạo',
                'selected' => true,
                'isColumn' => true
            ],
            [
                'field' => 'deliveryTime',
                'render' => 'datetime',
                'name' => 'Thời gian giao',
                'isColumn' => true
            ],
            [
                'field' => 'userCreatedName',
                'relatedField' => 'createdBy',
                'name' => 'Người tạo',
            ],
            [
                'field' => 'invoiceCustomerName',
                'name' => 'Khách hàng',
                'selected' => true
            ],
            [
                'field' => 'branchName',
                'relatedField' => 'branchId',
                'name' => 'Chi nhánh',
            ],
            [
                'field' => 'soldByName',
                'relatedField' => 'soldById',
                'name' => 'Nhân viên bán',
            ],
            [
                'field' => 'receiverName',
                'name' => 'Người nhận',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'receiverPhone',
                'name' => 'Điện thoại',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'receiverAddress',
                'name' => 'Địa chỉ',
                'isColumn' => true,
            ],
            [
                'field' => 'provinceName',
                'relatedField' => 'receiverProvinceId',
                'name' => 'Tỉnh/Thành phố',
            ],
            [
                'field' => 'districtName',
                'relatedField' => 'receiverDistrictId',
                'name' => 'Quận/huyện',
            ],
            [
                'field' => 'wardName',
                'relatedField' => 'receiverWardId',
                'name' => 'Phường/xã',
            ],
            [
                'field' => 'transporterName',
                'relatedField' => 'transporterId',
                'name' => 'Đối tác giao hàng',
                'selected' => true
            ],
            [
                'field' => 'note',
                'name' => 'Ghi chú giao hàng',
                'isColumn' => true
            ],
            [
                'field' => 'status',
                'name' => 'Trạng thái giao',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'serviceTypeName',
                'relatedField' => 'serviceTypeId',
                'name' => 'Dịch vụ',
            ],
            [
                'field' => 'priceCodPayment',
                'relatedField' => 'usingPriceCod',
                'name' => 'Cần thu hộ (COD)',
                'render' => 'number',
                'isColumn' => true,
                'selected' => true
            ],
//            [
//                'field' => '',
//                'name' => 'Tổng cước phí',
//            ],
//            [
//                'field' => '',
//                'name' => 'Mã đổi soát',
//            ],
//            [
//                'field' => 'price',
//                'name' => 'Phí trả DTGH',
//                'render' => 'number',
//                'isColumn' => true,
//            ],
//            [
//                'field' => '',
//                'name' => 'Ghi chú trạng thái giao hàng',
//            ],
        ];
    }
}
