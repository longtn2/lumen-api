<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Customer;
use App\Models\Invoice;
use App\Models\Product;
use App\Models\ProductUnit;
use App\Models\SaleChannel;
use OmiCare\Utils\DB;
use Illuminate\Support\Str;
use OmiCare\Http\Request;
use OmiCare\Utils\Auth;

class HomesController extends Controller
{
    public function indexAction(Request $request)
    {
        $currentBranchID = $request->currentBranchId();
        $startOfMonth = date('Y-m-01 00:00:00');

        $firstDayOfLastMonth = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m') - 1, 1));
        $lastDayOfLastMonth = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), 0));
        $todayOfLastMonth = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m') - 1, date('d')));
        $today = date('Y-m-d H:i:s');
        $lastDay = date('Y-m-d H:i:s', mktime(23, 59, 59, date('m'), date('d') -1));
        $beginLastDay = date('Y-m-d 00:00:00', mktime(0, 0, 0, date('m'), date('d') -1));
        $beginToday = date('Y-m-d 00:00:00');

        $invoicesInThisMonth = DB::selectOne('SELECT COUNT(*) as `totalInvoice`, SUM(totalPayment) as `totalAmount` FROM invoices WHERE status=? AND branchId=? AND createdAt>=? AND createdAt<=?', [1, $currentBranchID, $startOfMonth, $today]);
        $invoicesToday = DB::selectOne('SELECT COUNT(*) as `totalInvoice`, SUM(totalPayment) as `totalAmount` FROM invoices WHERE status=? AND branchId=? AND createdAt>=? AND createdAt<=?', [1, $currentBranchID, $beginToday, $today]);
        $invoicesLastDay = DB::selectOne('SELECT COUNT(*) as `totalInvoice`, SUM(totalPayment) as `totalAmount` FROM invoices WHERE status=? AND branchId=? AND createdAt>=? AND createdAt<=?', [1, $currentBranchID, $beginLastDay, $lastDay]);
        $invoicesInLastMonth = DB::selectOne('SELECT COUNT(*) as `totalInvoice`, SUM(totalPayment) as `totalAmount` FROM invoices WHERE status=? AND branchId=? AND createdAt>=? AND createdAt<=?', [1, $currentBranchID, $firstDayOfLastMonth, $lastDayOfLastMonth]);
        $invoicesTodayOfLastMonth = DB::selectOne('SELECT COUNT(*) as `totalInvoice`, SUM(totalPayment) as `totalAmount` FROM invoices WHERE status=? AND branchId=? AND createdAt>=? AND createdAt<=?', [1, $currentBranchID, $firstDayOfLastMonth, $todayOfLastMonth]);
        $returnToday = DB::selectOne('SELECT COUNT(*) as `totalReturn` FROM `returns` WHERE status=? AND branchId=? AND returnDate>=? AND returnDate<=?', [1, $currentBranchID, $beginToday, $today]);

        $invoicesInMonth = Invoice::query()->where('branchId', $currentBranchID)
            ->where('status', 1)
            ->orderBy('createdAt')
            ->where('createdAt', '>=', $startOfMonth)
            ->where('createdAt', '<=', $today)
            ->get()
            ->groupBy(function($item) {
                return $item->createdAt->format('Y-m-d');
            });

        $labelInvoice = [];
        $values = [];
        $invoiceByDay = [];
        $totalByDay = [];
        $days = date('d', strtotime($today));
        foreach ($invoicesInMonth as $date => $item) {
            $d = date('j', strtotime($date));
            $sum = 0;
            $invoiceByDay[$date] = count($item);
            foreach ($item as $i) {
                $sum += intval($i->totalPayment);
            }

            $values[$d] = $sum;
            $totalByDay[$date] = $sum;
        }
        $valueInvoice = [];
        foreach (range(1,$days) as $day) {
            $labelInvoice[] = $day;
            $valueInvoice[] = $values[$day] ?? 0;
        }

        $dataInvoice = new \stdClass();
        $dataInvoice->labels = $labelInvoice;
        $dataInvoice->data = $valueInvoice;

        return [
            'code' => 200,
            'message' => 'OK',
            'invoicesInThisMonth' => $invoicesInThisMonth,
            'invoicesToday' => $invoicesToday,
            'invoicesInLastMonth' => $invoicesInLastMonth,
            'returnToday' => $returnToday,
            'invoicesTodayOfLastMonth' => $invoicesTodayOfLastMonth,
            'invoicesLastDay' => $invoicesLastDay,
            'dataInvoice' => $dataInvoice,
            'invoiceByDay' => $invoiceByDay,
            'totalByDay' => $totalByDay
        ];
    }

    public function chartTopProductDataAction(Request $request)
    {
        $type = (int) $request->get('type');
        $export = (int) $request->get('export');
        $limit = (int) $request->get('limit');
        $keyword = (string) $request->get('keyword');
        $currentBranchID = $request->currentBranchId();

        if ($limit > 200 || $limit <= 0) {
            $limit = 20;
        }

        if ($keyword) {
            $keyword = '%'.$keyword.'%';
        }

        if ($export) {
            $limit = 200;
        }

        list($startTime, $endTime, $span) = $this->getDateInfo($request);
//        $sourceIDSQl = $this->getSourceIDsSql();

        if ($type !== 1 && $type !== 2) {
            $type = 1;
        }

        if ($type === 1) { // Sắp xếp theo số lượng
            if ($keyword) {
                $logs = DB::select("SELECT invoice_details.productId, count(*) `value`
                FROM invoice_details
                INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                INNER JOIN products ON products.id=invoice_details.productId
                WHERE products.name LIKE ? AND invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                GROUP BY invoice_details.productId
                ORDER BY `value` DESC LIMIT ?", [$keyword, 1, $currentBranchID, $startTime, $endTime, $limit]);
            } else {
                $logs = DB::select("SELECT invoice_details.productId, count(*) `value`
                FROM invoice_details
                INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                WHERE invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                GROUP BY invoice_details.productId
                ORDER BY `value` DESC LIMIT ?", [1, $currentBranchID, $startTime, $endTime, $limit]);
            }

        } else { // Sắp xếp theo số tiền
            if ($keyword) {
                $logs = DB::select("SELECT invoice_details.productId, SUM(invoice_details.price*invoice_details.quantity) `value`
                FROM invoice_details
                INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                INNER JOIN products ON products.id=invoice_details.productId
                WHERE products.name LIKE ? AND invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                GROUP BY invoice_details.productId
                ORDER BY `value` DESC LIMIT ?", [$keyword, 1, $currentBranchID, $startTime, $endTime, $limit]);
            } else {
                $logs = DB::select("SELECT invoice_details.productId, SUM(invoice_details.price*invoice_details.quantity) `value`
                FROM invoice_details
                INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                WHERE invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                GROUP BY invoice_details.productId
                ORDER BY `value` DESC LIMIT ?", [1, $currentBranchID, $startTime, $endTime, $limit]);
            }

        }

//        dd($logs);
        foreach ($logs as $log) {
            if ($type === 2) {
                $log->value += 0;
                $log->value /= 1000;
            }
        }

        $logs = collect($logs);
        $productIds = $logs->pluck('productId');

        $productNameMaps = Product::query()->whereIn('id', $productIds)->get()->pluck('name', 'id');

        if ($export) {
            $excelValues = [[], []];

            foreach ($logs as $log) {
                $excelValues[0][] = $productNameMaps[$log->product_id] ?? '';
                $excelValues[1][] = $log->value ?? 0;
            }

            return $this->doExport(['Tên sản phẩm', 'Lượt mua'], $excelValues);
        }

        $productMap = [];
        $productIdMap = [];
        $labels = array_map(function ($productId) use ($productNameMaps, &$productMap, &$productIdMap) {
            $name = $productNameMaps[$productId] ?? '';
            $limit = Str::limit($name, 50);
            $productMap[$limit] = $name;
            $productIdMap[$limit] = $productId;

            return $limit;
        }, $productIds->toArray());

        return [
            'code' => 200,
            'data' => [
                'chart1' => [
                    'data' => $logs->pluck('value'),
                    'labels' => $labels
                ],
                'productMap' => $productMap,
                'productIdMap' => $productIdMap
            ],
        ];
    }

    public function chartProductCategoryDataAction(Request $request)
    {
        $type = (int) $request->get('type');
        $export = (int) $request->get('export');
        $limit = (int) $request->get('limit');
        $keyword = (string) $request->get('keyword');

        $currentBranchID = $request->currentBranchId();

        if ($limit > 200 || $limit <= 0) {
            $limit = 20;
        }

        if ($export) {
            $limit = 200;
        }

        if ($keyword) {
            $keyword = '%'.$keyword.'%';
        }

        list($startTime, $endTime, $span, $startDate, $endDate) = $this->getDateInfo($request);

        if ($type === 1) { // Sắp xếp theo số lượng
            if ($keyword) {
                $logs = DB::select("SELECT invoice_details.productId, products.categoryId, count(*) `value`
                    FROM invoice_details
                    INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                    INNER JOIN products ON products.id=invoice_details.productId
                    INNER JOIN categories ON categories.id=products.categoryId
                    WHERE categories.name LIKE ? AND invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                    GROUP BY products.categoryId
                    ORDER BY `value` DESC LIMIT ?", [$keyword, 1, $currentBranchID, $startTime, $endTime, $limit]);
            } else {
                $logs = DB::select("SELECT invoice_details.productId, products.categoryId, count(*) `value`
                    FROM invoice_details
                    INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                    INNER JOIN products ON products.id=invoice_details.productId
                    WHERE invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                    GROUP BY products.categoryId
                    ORDER BY `value` DESC LIMIT ?", [1, $currentBranchID, $startTime, $endTime, $limit]);
            }

        } else { // Sắp xếp theo số tiền
            if ($keyword) {
                $logs = DB::select("SELECT invoice_details.productId, products.categoryId, SUM(invoice_details.price*invoice_details.quantity) `value`
                    FROM invoice_details
                    INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                    INNER JOIN products ON products.id=invoice_details.productId
                    INNER JOIN categories ON categories.id=products.categoryId
                    WHERE categories.name LIKE ? AND invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                    GROUP BY  products.categoryId
                    ORDER BY `value` DESC LIMIT ?", [$keyword, 1, $currentBranchID, $startTime, $endTime, $limit]);
            } else {
                $logs = DB::select("SELECT invoice_details.productId, products.categoryId, SUM(invoice_details.price*invoice_details.quantity) `value`
                    FROM invoice_details
                    INNER JOIN invoices ON invoices.id=invoice_details.invoiceId
                    INNER JOIN products ON products.id=invoice_details.productId
                    WHERE invoices.status=? AND invoices.branchId=? AND invoices.createdAt>=? AND invoices.createdAt <=?
                    GROUP BY  products.categoryId
                    ORDER BY `value` DESC LIMIT ?", [1, $currentBranchID, $startTime, $endTime, $limit]);
            }

        }

        foreach ($logs as $log) {
            if ($type === 2) {
                $log->value += 0;
                $log->value /= 1000;
            }
        }

        $logs = collect($logs);
        $categoryIds = $logs->pluck('categoryId');

        $categoryNameMaps = Category::query()->whereIn('id', $categoryIds)->get()->pluck('name', 'id');

        $categoryMap = [];
        $labels = array_map(function ($categoryId) use ($categoryNameMaps, &$categoryMap) {
            $name = $categoryNameMaps[$categoryId] ?? '';
            $limit = Str::limit($name, 50);
            $categoryMap[$limit] = $name;
            return $limit;
        }, $categoryIds->toArray());

        return [
            'code' => 200,
            'data' => [
                'chart1' => [
                    'data' => $logs->pluck('value'),
                    'labels' => $labels
                ],
                'categoryMap' => $categoryMap
            ]
        ];
    }

    public function chartTopCustomerDataAction(Request $request)
    {
        $type = (int) $request->get('type');
        $limit = (int) $request->get('limit');
        $currentBranchID = $request->currentBranchId();

        if ($limit > 200 || $limit <= 0) {
            $limit = 20;
        }

        list($startTime, $endTime, $span) = $this->getDateInfo($request);
//        $sourceIDSQL = $this->getSourceIDsSql();

        if ($type === 2) {
            $logs = DB::select("SELECT SUM(`totalPayment`) `value`, customerId FROM invoices
WHERE branchId=? AND createdAt>=? AND createdAt<=? AND customerId is not null AND `status`= 1
GROUP by customerId
ORDER BY `value` desc LIMIT ?", [
                $currentBranchID, $startTime, $endTime, $limit
            ]);
        } else {
            $logs = DB::select("SELECT COUNT(*) `value`, customerId FROM invoices
WHERE branchId=? AND createdAt>=? AND createdAt<=? AND customerId is not null AND `status`=1
GROUP by customerId
ORDER BY `value` desc LIMIT ?", [
                $currentBranchID, $startTime, $endTime, $limit
            ]);
        }

        $logs = collect($logs);
        $customerIds = $logs->pluck('customerId');
        $customers = Customer::query()->whereIn('id', $customerIds)->get();
        $customerMap = [];
        $customerMapName = [];

        foreach ($customers as $c) {
            $customerMap[$c->id] = $c;
        }

        $labels = [];
        $data = [];
        foreach ($logs as $log) {
            $log->value += 0;
            $c = $customerMap[$log->customerId] ?? null;

            if ($c) {
                $labels[] = $c->name.' - '.$c->phone;
                $data[] = $log->value;
            }
        }

        return [
            'code' => 200,
            'data' => [
                'labels' => $labels,
                'values' => $data
            ]
        ];
    }

    public function chartSaleChanelDataAction(Request $request)
    {
        $ignoredSources = $request->get('ignored_sources');
        $ignoredSourceMap = [];

        if ($ignoredSources) {
            $ignoredSources = explode(',', $ignoredSources);
            $ignoredSourceMap = array_flip($ignoredSources);
        }

        list($startTime, $endTime, $span) = $this->getDateInfo($request);
//        $sourceIDSql = $this->getSourceIDsSql();

        if ($span === 0) {
            $unit = 'hour';
            $logs = DB::select("SELECT SUM(totalPayment) `value`, saleChannelId, HOUR(createdAt) `label`  from invoices
where createdAt>=? and createdAt<=? AND `status` = 1 GROUP BY saleChannelId,HOUR(createdAt)
ORDER BY createdAt asc
", [
                $startTime, $endTime
            ]);
        } elseif ($span <= 31) {
            $unit = 'day';
            $logs = DB::select("SELECT SUM(totalPayment) `value`, saleChannelId, date(createdAt) `label`  from invoices
where createdAt>=? and createdAt<=? AND `status` = 1 GROUP BY saleChannelId,date(createdAt) ORDER BY createdAt asc", [
                $startTime, $endTime
            ]);
        } else {
            $unit = 'month';
            $logs = DB::select("SELECT SUM(totalPayment) `value`, saleChannelId, MONTH(createdAt) M,YEAR(createdAt) Y  from invoices
where createdAt>=? and createdAt<=? AND `status` = 1 GROUP BY saleChannelId,YEAR(createdAt),MONTH(createdAt)
 ORDER BY createdAt asc", [
                $startTime, $endTime
            ]);
        }

        $sourceMap = [];
        $datasets = [];
        $labelMap = [];
        $currentYear = date('Y');

        foreach ($logs as $log) {
            $log->value += 0;
            //$log->value /=1000;

            if ($unit === 'month') {
                $log->label = $log->M.'/'.$log->Y;
            } elseif ($unit === 'day') {
                $time = strtotime($log->label);
                $y = date('Y', $time);
                $log->label = date('d/m', $time);

                if ($y !== $currentYear) {
                    $log->label = $log->label.'/'.$y;
                }
            } else {
                $log->label .= 'H';
            }
            $labelMap[$log->label] = true;
            $sourceMap[$log->saleChannelId][$log->label] = $log->value;
        }

        $labels = array_keys($labelMap);
        $allSources = SaleChannel::where('isActive', 1)->get();
        $sourceNameMap = $allSources->pluck('name', 'id');
        $sourceColorMap = $allSources->pluck('color', 'id');

        foreach ($sourceMap as $sourceId => $sources) {
            if (isset($ignoredSourceMap[$sourceId])) {
                continue;
            }

            $color = $sourceColorMap[$sourceId] ?? '#ccc';
            $value = [
                'label' => $sourceNameMap[$sourceId] ?? 'Unknown',
                'total' => 0,
                'tension' => 0.4,
                'fill' => false,
                //   'cubicInterpolationMode' => 'monotone',
                'backgroundColor' => $color,
                'borderColor' => [$color],
                'data' => [],
                'borderWidth' => 1
            ];

            foreach ($labels as $label) {
                $v = $sources[$label] ?? 0;
                $value['data'][] = $v;
                $value['total'] += $v;
            }
            $datasets[] = $value;
        }

        return [
            'code' => 200,
            'data' => [
                'labels' => $labels,
                'datasets' => $datasets,
                'orderSources' => $allSources
            ]
        ];
    }

    private function getDateInfo(Request $request): array
    {
        $created = $request->get('created');
        $startDate = date('Y-m-d', strtotime('-30 days'));
        $endDate = date('Y-m-d');

        if ($created) {
            list($startDate, $endDate) = explode('_', $created);
        }

        $startTime = $startDate.' 00:00:00';
        $endTime = $endDate.' 23:59:59';

        $span = (strtotime($endDate) - strtotime($startDate)) / 86400;

        return [$startTime, $endTime, $span, $startDate, $endDate];
    }

}
