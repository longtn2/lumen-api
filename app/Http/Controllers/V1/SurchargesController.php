<?php

namespace App\Http\Controllers\V1;


use App\Models\Surcharge;
use App\Http\Controllers\AuthenticatedController;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\V;

class SurchargesController extends AuthenticatedController
{

    /**
     * @uri /v1/surcharges/show
     * @return array
     */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Surcharge::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
     * @uri /v1/surcharges/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Surcharge::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/surcharges/save
     * @return array
     */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        if (empty($data['code'])) {
            $data['code'] = Surcharge::generalCode();
        }

        $uniqRule = ['surcharges'];
        if (isset($data['id'])) {
            $uniqRule = ['surcharges', 'id', $data['id'] ];
        }
        V::make($data)
            ->rule(V::required, ['type'])->message('Loại thu là bắt buộc!')
            ->rule(V::unique, ['code'], $uniqRule)->message('Mã thu khác đã tồn tại')
            ->validOrFail();


        /**
         * @var Surcharge $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Surcharge::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Surcharge();
        }

        $entry->fill($data);
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/surcharges/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/surcharges/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        foreach ($fields as $field) {
            if (!empty($field['relatedField'])) {
                if (is_array($field['relatedField'])) {
                    $select = array_merge($select, $field['relatedField']);
                } else {
                    $select[] = $field['relatedField'];
                }
            }
        }

        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Surcharge::query()->select($select)->orderBy($sortField, $sortDirection);

        if ($keyword = $req->keyword) {
            $query->where('code', 'LIKE', '%' . $keyword. '%')
                ->orWhere('type', 'LIKE', '%' . $keyword. '%');
        }

        $query->createdIn($req->created);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        $listStatusMap = array_column(Surcharge::$listStatus, 'name', 'id');
        foreach ($entries as $entry) {
            $entry->status = $listStatusMap[$entry->status] ?? '--';
            if ($entry->priceType == DISCOUNT_TYPE_PERCENT) {
                $entry->priceValue = (float) $entry->priceValue . ' %';
            } else {
                $entry->priceValue = number_format($entry->priceValue) . ' VNĐ';
            }
        }

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
            ],
            [
                'field' => 'code',
                'name' => 'Mã thu',
            ],
            [
                'field' => 'type',
                'name' => 'Loại thu',
            ],
            [
                'relatedField' => ['priceType', 'priceValue'],
                'field' => 'priceValue',
                'name' => 'Giá trị',
            ],
            [
                'field' => 'status',
                'name' => 'Trạng thái',
            ],
            [
                'field' => 'order',
                'name' => 'Sắp xếp',
            ],
        ];
        return $fields;
    }
}
