<?php

namespace App\Http\Controllers\V1;


use App\Models\Branch;
use App\Models\Delivery;
use App\Models\History;
use App\Models\InvoiceDetail;
use App\Models\Order;
use App\Http\Controllers\AuthenticatedController;
use App\Models\SaleChannel;
use App\Models\SyncSaleChannels;
use App\Models\User;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;
use App\Models\OmiSyncLink;

class OrdersController extends AuthenticatedController
{

    /**
     * @uri /v1/orders/show
     * @return array
     */
    public function showAction(Request $req)
    {        
        $id = $req->id;
        $entry = Order::query()
            ->with(['orderItems.productUnit:id,unitName', 'invoices', 'cashFlows.user', 'customer', 'branch'])
            ->find($id);
        
        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }
        
        if (!empty($entry->invoices)) {
            $invoiceIds = $entry->invoices->map(function ($i) {
                return $i->id;
            });

            $invoiceDetailSumByProductUnitMap = InvoiceDetail::query()
                ->selectRaw('productUnitId, SUM(quantity) as qty')
                ->whereHas('invoice', function ($q) use ($invoiceIds) {
                    $q->whereIn('id', $invoiceIds);
                })
                ->groupBy('productUnitId')
                ->get()
                ->pluck('qty', 'productUnitId');
        }
       
        foreach ($entry->orderItems as $item) {
            $item->invoicedQuantity = $invoiceDetailSumByProductUnitMap[$item->productUnitId] ?? 0;
        }
       
       
        $entry->createdByName = User::getNameFromId($entry->createdBy);

        $listStatus = Order::$listStatus;

        $orderDelivery = Delivery::query()
            ->where('type', Delivery::TYPE_ORDER)
            ->where('orderID', $entry->id)
            ->orderByDesc('id')
            ->first();

        $receiverLocation = new \stdClass();
        if ($orderDelivery) {
            $orderDelivery->decode = Branch::decode($orderDelivery->branchId);
            $receiverLocation->provinceId = $orderDelivery->receiverProvinceId;
            $receiverLocation->districtId = $orderDelivery->receiverDistrictId;
            $receiverLocation->wardId = $orderDelivery->receiverWardId;
        }
        //
        $data = [
            'entry' => $entry->toArray(),
            'fields' => $this->getDisplayFields(),
            'list_status' => $listStatus,
            'shopee_value' => $this->listShopeeStatus(),
            'omipos_value' => $this->lstOmiInvoice(),
            'shop_name' =>  $this->shopName($entry -> saleChannelId),
            'delivery' => $orderDelivery,
            'receiver_location' => $receiverLocation
        ];       
             
        return [
            'code' => 200,
            'message' => 'OK',
            'data' => $data
        ];
    }

    public function shopName($sync_sale_channels){
        $shop = SyncSaleChannels::where('sale_channel_id',$sync_sale_channels)->get();
        $data = [];        
        foreach ($shop as $item) {
            $data [] = [
                'id' => $item['identity_key'],
                'name' => $item['name'],
            ];
        }        
        return $data;
    }
    public function lstOmiInvoice(){
        $shopee_list = [          
            [
                'id' => 3,
                'name' => "Đang xử lý"
            ],
            [
                'id' => 4,
                'name' => "Đang xử lý"
            ],
            [
                'id' => 5,
                'name' => "Đang xử lý"
            ],
            [
                'id' => 6,
                'name' => "Đã huỷ"
            ]
          
        ];
        return $shopee_list;
    }
    public function listShopeeStatus(){
        $shopee_list = [
            [
                'id' => 1,
                'name' => "Chờ xác nhận"
            ],
            [
                'id' => 2,
                'name' => "Đã huỷ"
            ],
            [
                'id' => 3,
                'name' => "Chờ lấy hàng"
            ],
            [
                'id' => 4,
                'name' => "Đang giao"
            ],
            [
                'id' => 5,
                'name' => "Đã giao"
            ],
            [
                'id' => 6,
                'name' => "Đã huỷ"
            ]
          
        ];
        return $shopee_list;
    }
    /**
     * @uri /v1/orders/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Order::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Order not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/orders/save
     * @return array
     */
    public function saveAction(Request $req)
    {
        
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');
        $delivery = $req->get('delivery');

        V::make($data)
            ->rule(V::required, [])
            ->validOrFail();

        DB::beginTransaction();
        /**
         * @var Order $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Order::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Order();
        }

        if (!empty($delivery)) {
            if (!empty($delivery['id'])) {
                $orderDelivery = Delivery::query()
                    ->where('type', Delivery::TYPE_ORDER)
                    ->where('orderId', $entry->id)
                    ->first();
            } else {
                $orderDelivery = new Delivery();
            }

            if ($orderDelivery) {
                if ($orderDelivery->status != $delivery['status']) {
                    $orderDelivery->saveStatus($data['status'], '', false);
                }

                $orderDelivery->fill($delivery);
                $orderDelivery->saveHistory(History::ACTION_UPDATED);
                $orderDelivery->save();
            }
        }
        
        $entry->fill($data);
        $entry->saveHistory(History::ACTION_UPDATED);
        $entry->save();

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/orders/index' : false,
            'data' => $entry->toArray()
        ];
    }

    public function saveStatusAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $id = $request->get('id');
        $action = $request->get('action');

        $order = Order::find($id);
        if (!$order) {
            return [
                'code' => 404,
                'message' => 'Order not found'
            ];
        }

        DB::beginTransaction();

        switch ($action) {
            case "complete":
                $order->saveStatus(Order::STATUS_COMPLETE, 'Kết thúc đơn đặt hàng', false);
                $order->saveHistory(History::ACTION_UPDATED);
                $order->removeInventories();
                $order->save();
                break;
            case "cancel":
                $order->saveStatus(Order::STATUS_CANCELED);
                $order->saveHistory(History::ACTION_CANCEL);
                $order->removeInventories();
                break;
            case "approve":
                $order->saveStatus(Order::STATUS_CONFIRMED, 'Phê duyệt đơn đặt hàng', false);
                $order->saveHistory(History::ACTION_UPDATED);
                $order->save();
                break;
            default:
                break;
        }

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Thành công!'
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/orders/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $fieldSelects = [];
        $fieldDisplays = [];

        foreach ($fields as $field) {
            if (!empty($field['isColumn']) && !empty($field['field'])) {
                $fieldSelects[] = $field['field'];
            }

            if (!empty($field['relatedField'])) {
                if (is_array($field['relatedField'])) {
                    $fieldSelects = array_merge($fieldSelects, $field['relatedField']);
                } else {
                    $fieldSelects[] = $field['relatedField'];
                }
            }

            if (empty($field['hide'])) {
                $field['selected'] = empty($field['selected']) ? false : true;
                $fieldDisplays[] = $field;
            }
        }

        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $keyword = $req->get('keyword');
        $branchIds = $req->get('branch_ids');
        $saleChannelIds = $req->get('sale_channel_ids');
        $purchaseDate = $req->get('purchase_date');
        $status = $req->get('status');
        $createdByIds = $req->get('created_by_ids');
        $methods = $req->get('methods');

        $query = Order::query()
            ->select($fieldSelects)
            ->with(['customer', 'invoices', 'saleChannel', 'branch', 'soldBy', 'userCreated', 'delivery']);

        if ($keyword) {
            $query->where('code', 'LIKE', '%' . $keyword. '%');
        }

        $userBranchIds = array_column(Branch::getUserBranches()->toArray(), 'id');

        if ($branchIds) {
            $filterBranchIds = explode(',', $branchIds);
            $query->whereIn('branchId', $filterBranchIds);
        } else {
            $query->whereIn('branchId', $userBranchIds);
        }

        if ($saleChannelIds) {
            $query->whereIn('saleChannelId', explode(',', $saleChannelIds));
        }

        if ($status !== null) {
            $query->whereIn('status', explode(',', $status));
        }

        if ($purchaseDate) {
            $query->columnIn('purchaseDate', $purchaseDate);
        }

        if ($createdByIds) {
            $query->whereIn('createdBy', explode(',', $createdByIds));
        }

        if ($methods) {
            $query->whereHas('cashFlows', function ($q) use ($methods) {
                $q->whereIn('method', explode(',', $methods));
            });
        }

        $query->createdIn($req->created);

        $totalRecord = $query->get();
        $entries = $query->orderBy($sortField, $sortDirection)
            ->paginate($record, ['*'], 'page', $page);

        $mapStatus = array_column(Order::$listStatus, 'name', 'id');
        foreach ($entries as $item) {
            if (count($item->invoices)) {
                $invoiceCodes = array_column($item->invoices->toArray(), 'code');
                $item->invoice_codes = implode(',', $invoiceCodes);
                unset($item->invoices);
            }

            $item->status = $mapStatus[$item->status] ?? '';
            $item->customerName = $item->customer->name ?? null;
            $item->customerContactNumber = $item->customer->contactNumber ?? null;
            $item->customerAddress = $item->customer->address ?? null;
            $item->customerLocationName = $item->customer->locationName ?? null;
            $item->customerWardName = $item->customer->wardName ?? null;
            $item->customerBirthDate = $item->customer->birthDate ?? null;
            $item->branchName = $item->branch->name ?? null;
            $item->saleChannelName = $item->saleChannel->name ?? null;
            $item->soldByName = $item->soldBy->name ?? null;
            $item->userCreatedName = $item->userCreated->name ?? null;
            $item->deliveryCode = $item->delivery->code ?? null;
            $item->deliveryTransporterName = $item->delivery->transporter->name ?? null;
            $item->discountPrice = $item->totalOrigin - $item->total;
        }

        return [
            'code' => 200,
            'data' => [
                'fields' => $fieldDisplays,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }
    public function checkProductLinkAction(Request $req){
        $omi_syn_link = OmiSyncLink::where('sync_prd_id',$req->get('sync_prd_id'))->whereNull('deletedAt')->count();
        return [
            'status' => $omi_syn_link,
        ];
    }
    private function getDisplayFields(): array
    {
        return [
            [
                'field' => 'id',
                'name' => 'ID',
                'isColumn' => true,
            ],
            [
                'field' => 'deliveryCode',
                'name' => 'Mã vận đơn',
            ],
            [
                'field' => 'code',
                'name' => 'Mã đặt hàng',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'invoice_codes',
                'name' => 'Mã hóa đơn',
                'selected' => true
            ],
            [
                'field' => 'purchaseDate',
                'name' => 'Thời gian',
                'render' => 'datetime',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'createdAt',
                'name' => 'Thời gian tạo',
                'render' => 'datetime',
                'isColumn' => true
            ],
            [
                'field' => 'updatedAt',
                'name' => 'Ngày cập nhật',
                'render' => 'datetime',
                'isColumn' => true
            ],
            [
                'field' => 'customerName',
                'relatedField' => 'customerId',
                'name' => 'Khách hàng',
                'selected' => true
            ],
            [
                'field' => 'customerContactNumber',
                'name' => 'Điện thoại',
                'selected' => true
            ],
            [
                'field' => 'customerAddress',
                'name' => 'Địa chỉ'
            ],
            [
                'field' => 'customerLocationName',
                'name' => 'Khu vực'
            ],
            [
                'field' => 'customerWardName',
                'name' => 'Phường/Xã'
            ],
            [
                'field' => 'customerBirthDate',
                'name' => 'Ngày sinh',
                'render' => 'datetime',
            ],
            [
                'field' => 'deliveryTransporterName',
                'name' => 'Đối tác giao hàng'
            ],
            [
                'field' => 'branchName',
                'relatedField' => 'branchId',
                'name' => 'Chi nhánh',
                'selected' => true
            ],
            [
                'field' => 'soldByName',
                'relatedField' => 'soldById',
                'name' => 'Người nhận đặt'
            ],
            [
                'field' => 'userCreatedName',
                'relatedField' => 'createdBy',
                'name' => 'Người tạo',
            ],
            [
                'field' => 'saleChannelName',
                'relatedField' => 'saleChannelId',
                'name' => 'Kênh bán',
            ],
            [
                'field' => 'description',
                'name' => 'Ghi chú',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'totalOrigin',
                'name' => 'Tổng tiền hàng',
                'render' => 'number',
                'isColumn' => true,
                'selected' => true
            ],
            [
                'field' => 'discountPrice',
                'relatedField' => ['discountType', 'discountValue'],
                'name' => 'Giảm giá',
                'render' => 'number'
            ],
            [
                'field' => 'total',
                'name' => 'Khách cần trả',
                'isColumn' => true,
                'render' => 'number'
            ],
            [
                'field' => 'totalPayment',
                'name' => 'Khách đã trả',
                'isColumn' => true,
                'render' => 'number'
            ],
            [
                'field' => 'status',                
                'name' => 'Trạng thái',
                'selected' => true,
                'isColumn' => true,
            ],
            [
                'field' => 'shopee_value',
                'name' => 'Trạng thái thực tế',
                'selected' => true,
                'isColumn' => true,
            ],
            [
                'field' => 'orderDetails',
                'name' => 'Chi tiết đơn hàng',
                'selected' => false,
                'isColumn' => true,
            ],
        ];
    }
}
