<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\SysPermission;
use Illuminate\Contracts\View\View;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;
use App\Models\Permission;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class PermissionsController extends AuthenticatedController
{

    public function assigns(Request $req)
    {
        if ($req->isMethod('POST')) {
            $table  = $req->get('table');
            $values  = $req->get('values');
            $entryId = $req->get('entry_id');
            if ($table === 'sys_user_permissions') {
                $field = 'userId';
            } else if ($table === 'sys_role_permissions') {
                $field = 'roleId';
            } else {
                return [
                    'code' => 404,
                    'message' => 'Invalid table'
                ];
            }

            DB::beginTransaction();
            DB::table($table)->where($field, $entryId)->delete();
            $bulkData = array_map(function($v) use($field, $entryId) {
                return [
                    $field => $entryId,
                    'permissionId' => $v
                ];
            }, $values);
            DB::table($table)->insert($bulkData);
            DB::commit();

            return [
                'code' => 200,
                'message' => 'Đã lưu'
            ];
        }

        $entryId = 0;
        $table = '';
        $field = '';

        if (isset($req->user_id)) {
            $table = 'sys_user_permissions';
            $field = 'userId';
            $entryId = $req->user_id;
        } elseif (isset($req->roleId)) {
            $table = 'sys_role_permissions';
            $field = 'roleId';
            $entryId = $req->roleId;
        }

        if (!$table) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy'
            ];
        }

        $permissionValues = DB::table($table)->where($field, $entryId)->get();
        $values = [];
        foreach ($permissionValues as $v) {
            $values[] = $v->permission_id;
        }

        $model = [
            'table' => $table,
            'entry_id' => $entryId,
            'values' => $values
        ];

        $tree = SysPermission::getTree();

        return [
            'code' => 200,
            'tree' => $tree,
            'model' => $model
        ];
    }


    /**
    * @uri  /xadmin/permissions/edit?id=$id
    * @return  array
    */
    public function showAction (Request $req) {
        $id = $req->id;
        $entry = SysPermission::find($id);

        if (!$entry) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy bản ghi'
            ];
        }

        return [
            'code' => 200,
            'entry' => $entry
        ];
    }

    /**
    * @uri  /xadmin/permissions/remove
    * @return  array
    */
    public function removeAction(Request $req) {
        $id = $req->id;
        $entry = SysPermission::find($id);

        if (!$entry) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy bản ghi'
            ];
        }

        $entry->delete();

        return [
            'code' => 0,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri  /xadmin/permissions/save
    * @return  array
    */
    public function saveAction(Request $req) {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)->rule(V::required, [
            'name',
            'displayName',
            'action'
        ])->validOrFail();

        /**
        * @var  SysPermission $entry
        */
        if (isset($data['id'])) {
            $entry = SysPermission::find($data['id']);
            if (!$entry) {
                return [
                    'code' => 3,
                    'message' => 'Không tìm thấy',
                ];
            }

            $entry->fill($data);
            $entry->save();

            return [
                'code' => 0,
                'message' => 'Đã cập nhật',
                'redirect' => false,
                'id' => $entry->id
            ];
        } else {
            $entry = new SysPermission();
            $entry->fill($data);
            $entry->save();

            return [
                'code' => 0,
                'message' => 'Đã thêm',
                'redirect' => '/permissions/index',
                'id' => $entry->id
            ];
        }
    }

    /**
    * @param  Request $req
    */
    public function toggleStatus(Request $req)
    {
        $id = $req->get('id');
        $entry = SysPermission::find($id);

        if (!$id) {
            return [
                'code' => 404,
                'message' => 'Not Found'
            ];
        }

        $entry->status = $req->status ? 1 : 0;
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã lưu'
        ];
    }

    /**
    * Ajax data for index page
    * @uri  /xadmin/permissions/data
    * @return  array
    */
    public function dataAction(Request $req) {
        $query = SysPermission::query()->with('parent')->orderBy('id', 'desc');

        if ($req->keyword) {
            $query->where('name', 'LIKE', '%' . $req->keyword. '%')
                ->orWhere('displayName', 'LIKE', '%' . $req->keyword. '%');
        }
        $page = $req->getInt('page');

        $query->createdIn($req->created);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        return [
            'code' => 0,
            'data' => $entries->items(),
            'paginate' => [
                'currentPage' => $entries->currentPage(),
                'lastPage' => $entries->lastPage(),
            ]
        ];
    }

    public function export() {
                $keys = [
                            'name' => ['A', 'name'],
                            'class' => ['B', 'class'],
                            'action' => ['C', 'action'],
                            ];

        $query = SysPermission::query()->orderBy('id', 'desc');

        $entries = $query->paginate();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($keys as $key => $v) {
            if (is_string($v)) {
                $sheet->setCellValue($v . "1", $key);
            } elseif (is_array($v)) {
                list($c, $n) = $v;
                 $sheet->setCellValue($c . "1", $n);
            }
        }


        foreach ($entries as $index => $entry) {
            $idx = $index + 2;
            foreach ($keys as $key => $v) {
                if (is_string($v)) {
                    $sheet->setCellValue("$v$idx", data_get($entry->toArray(), $key));
                } elseif (is_array($v)) {
                    list($c, $n) = $v;
                    $sheet->setCellValue("$c$idx", data_get($entry->toArray(), $key));
                }
            }
        }
        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        $filename = uniqid() . '-' . date('Y_m_d H_i') . ".xlsx";

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        // Write file to the browser
        $writer->save('php://output');
        die;
    }

    public function permissionDataAction(Request $request) {
        $permissions = SysPermission::query()->with(['children', 'module'])->where('parentId', 1)->groupBy('moduleId')->get();
        $module = ModulePermission::get();
        foreach ($module as $m) {
            $pers = SysPermission::with('children')->where('moduleId', $m->id)->where('parentId', 1)->get();

            $m->tree = $pers;
        }

        return [
            'code' => 200,
            'items' => $module
        ];
    }
}
