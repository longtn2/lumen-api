<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;

use App\Models\File;
use Illuminate\Support\Str;
use OmiCare\Http\Request;
use OmiCare\Utils\Auth;

class FilesController extends AuthenticatedController
{
    public function dataAction(Request $request)
    {
        $keyword = $request->get('keyword');
        $sortDirection = $request->get('sort_direction') === 'asc' ? 'asc' : 'desc';
        $sortField = $request->get('sort_field');
        $fileType = $request->get('file_type');
        $group = $request->get('group');

        $allowedSortFiled = [
            'id' => true,
            'name' => true,
            'extension' => true,
            'size' => true,
            'created_at' => true
        ];

        if (!isset($allowedSortFiled[$sortField])) {
            $sortField = 'id';
        }

        $query = File::query()
            ->where('userId', Auth::user()->id)
            ->orderBy($sortField, $sortDirection);

        if ($keyword) {
            $query->where('name', 'LIKE', '%'.$keyword.'%');
        }

        if ($group) {
            $query->where('group', $group);
        }

        if ($fileType === 'image') {
            $query->where('isImage', 1);
        } elseif ($fileType === 'excel') {
            $query->where('extension', 'xlsx');
        }

        $query->createdIn($request->get('created'));
        $page = (int) max(1, $request->get('page'));

        $files = $query->paginate(15, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'entries' => $files->items(),
                'paginate' => [
                    'total' => $files->total(),
                    'currentPage' => (int) max(1, $request->get('page')),
                    'lastPage' => $files->lastPage(),
                ]
            ]
        ];
    }

    /**
     * error: 0
     * name: "10951873_433905603431654_87195138_n.jpg"
     * size: 55820
     * tmp_name: "C:\xampp74\tmp\php9E00.tmp"
     * type: "image/jpeg".
     *
     * @return array
     */
    public function uploadAction(Request  $request)
    {
        if (empty($_FILES['file_0'])) {
            return [
                'code' => 1,
                'message' => 'File Missing'
            ];
        }

        $group = $request->get('group');
        $file0 = $_FILES['file_0'];

        if ($file0['error']) {
            return [
                'code' => 1,
                'message' => 'File Error Code: '.$file0['error']
            ];
        }

        $y = date('Y');
        $m = date('m');

        $dir = ROOT . "/public/files/attachments/{$y}/{$m}";

        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        $allowed = [
            'jpg', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'png'
        ];

        $imageExtension = [
            'jpg' => true, 'jpeg' => true, 'png' => true
        ];

        $info = pathinfo($file0['name']);
        $extension = strtolower($info['extension']);

        if (!in_array($extension, $allowed)) {
            return [
                'code' => 3,
                'message' => 'Extension: '.$extension.' is now allowed'
            ];
        }

        $hash = sha1(uniqid());
        $newFilePath = $dir.'/'.$hash.'.'.$extension;

        $newUrl = image_url("/files/attachments/{$y}/{$m}/{$hash}.{$extension}");

        $ok = move_uploaded_file($file0['tmp_name'], $newFilePath);

        if (!$ok) {
            return [
                'code' => 4,
                'message' => 'Move uploaded failed'
            ];
        }

        $user = Auth::user();
        $file = new File();
        $file->type = $file0['type'];
        $file->group = $group;
        $file->hash = sha1($newFilePath);
        $file->url = $newUrl;
        $file->isImage = isset($imageExtension[$extension]) ? 1 : 0;

        if ($file->isImage) {
            $imgSize = getimagesize($newFilePath);
            if ($imgSize) {
                $file->width = $imgSize[0];
                $file->height = $imgSize[1];
            }
        }

        $file->size = $file0['size'];
        $file->name = $info['filename'];
        $file->path = $newFilePath;
        $file->userId = $user->id;
        $file->extension = $extension;
        $file->save();

        return [
            'code' => 200,
            'file' => $file,
            'message' => 'Upload thành công'
        ];
    }

    public function uploadFromURLAction(Request $request)
    {
        $inputURL = $request->get('url');
        $group = $request->get('group');
        $isURL = Str::startsWith($inputURL, 'http://') || Str::startsWith($inputURL, 'https://');

        if (!$isURL) {
            return [
                'code' => 1,
                'message' => 'URL không hợp lệ'
            ];
        }

        $imgsize = getimagesize($inputURL);

        if ($imgsize === false) {
            return [
                'code' => 2,
                'message' => 'Ảnh từ URL không hợp lệ'
            ];
        }

        $hash = sha1(uniqid());
        $y = date('Y');
        $m = date('m');
        $extension = 'jpg';
        $dir = ROOT . "/public/files/attachments/{$y}/{$m}";
        $newFilePath = $dir.'/'.$hash.'.'.$extension;

        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }

        $this->resizeImage(file_get_contents($inputURL), $newFilePath);

        if (!is_file($newFilePath)) {
            return [
                'code' => 4,
                'message' => 'Move uploaded failed'
            ];
        }

        $newUrl = image_url("/files/attachments/{$y}/{$m}/{$hash}.{$extension}");

        $user = Auth::user();
        $file = new File();
        $file->type = $imgsize['mime'];
        $file->hash = sha1($newFilePath);
        $file->group = $group;
        $file->url = $newUrl;
        $file->isImage = 1;
        $file->width = $imgsize[0];
        $file->height = $imgsize[1];
        $file->size = filesize($newFilePath);
        $file->name = 'Download';
        $file->path = $newFilePath;
        $file->uploadedBy = $user->email;
        $file->userId = $user->id;
        $file->extension = $extension;
        $file->save();

        return [
            'code' => 200,
            'file' => $file,
            'message' => 'Upload thành công'
        ];

    }

    public function renameAction(Request $request)
    {
        $id = $request->get('id');
        $newName = trim($request->get('new_name'));

        if (!$newName) {
            return [
                'code' => 404,
                'message' => 'Vui lòng nhập tên file mới'
            ];
        }

        $file = File::where('userId', Auth::user()->id)->where('id', $id)->first();

        if (!$file) {
            return [
                'code' => 404,
                'message' => 'File not found'
            ];
        }

        $file->name = $newName;
        $file->save();

        return [
            'code' => 200,
            'message' => 'Đã lưu'
        ];
    }

    public function excelDataAction(Request $request)
    {
        $id = $request->get('id');
        $file = File::where('userId', Auth::user()->id)->where('id', $id)->first();

        if (!$file) {
            return [
                'code' => 404,
                'message' => 'File not found'
            ];
        }

        return [
            'code' => 200,
            'data' => $file->toExcelData()
        ];
    }

    public function removeAction(Request $request)
    {
        $id = $request->get('id');
        $file = File::where('userId', Auth::user()->id)->where('id', $id)->first();

        if (!$file) {
            return [
                'code' => 404,
                'message' => 'File not found'
            ];
        }

        try {
            if (is_file($file->path)) {
                @unlink($file->path);
            }

            $file->delete();

            return [
                'code' => 200,
                'message' => 'Đã xóa file'
            ];
        } catch (\Exception $e) {
            return [
                'code' => 503,
                'message' => $e->getMessage()
            ];
        }
    }

    private function resizeImage(string $input,string $output)
    {
        $manager = new \Intervention\Image\ImageManager(['driver' => 'gd']);
        $manager->make($input)
            ->resize(800, 600, function ($constraint) {
                $constraint->aspectRatio();
            })
            ->encode('jpg', 100)
            ->save($output);
    }
}
