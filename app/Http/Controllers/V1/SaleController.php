<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Branch;
use App\Models\Cashflow;
use App\Models\Coupon;
use App\Models\CouponCode;
use App\Models\Customer;
use App\Models\Delivery;
use App\Models\District;
use App\Models\History;
use App\Models\Inventory;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoiceSurcharge;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderSupplier;
use App\Models\OrderSupplierDetail;
use App\Models\OrderSurcharge;
use App\Models\Product;
use App\Models\ProductBatch;
use App\Models\ProductUnit;
use App\Models\Promotion;
use App\Models\Province;
use App\Models\ReturnDetail;
use App\Models\Returns;
use App\Models\ReturnSurcharge;
use App\Models\Surcharge;
use App\Models\User;
use App\Models\Ward;
use App\Services\Promotion\Transactions\InvoicePromotionTransaction;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class SaleController extends AuthenticatedController
{
    const TYPE_INVOICE = 'invoice';
    const TYPE_ORDER = 'order';
    const TYPE_RETURN = 'return';

    const ACTION_CREATE = 'create';
    const ACTION_ORDER_PROCESS = 'orderProcessing';
    const ACTION_UPDATE_INVOICE = 'updateInvoice';
    const ACTION_RETURN_INVOICE = 'returnInvoice';
    const ACTION_UPDATE_ORDER = 'updateOrder';

    public function getAction(Request $request) {
        $code = $request->get('code');
        $type = $request->get('type', 'invoice');

        if ($type == 'invoice') {
            /**
             * @var Invoice $data
             */
            $data = Invoice::query()
                ->with('invoiceItems.inventories.productBatch', 'cashFlows', 'delivery', 'customer')
                ->where('code', $code)
                ->orderByDesc('id')
                ->first();

            if (!$data) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy hóa đơn!'
                ];
            }

            if (!empty($data->delivery)) {
                $data->isTickDelivery = true;
            }

            if (!empty($data->cashFlows)) {
                $paymentMethodMap = array_column(Cashflow::$listPaymentMethod, 'name', 'id');
                foreach ($data->cashFlows as $cashFlow) {
                    $cashFlow->label = $paymentMethodMap[$cashFlow->method] ?? '';
                }
            }

            if ($data->invoiceSurcharges()->count()) {
                $surchargeCheckedIds = [];
                $surchargeAmount = 0;
                $surchargeLabel = [];

                foreach ($data->invoiceSurcharges()->get() as $invoiceSurcharge) {
                    $surcharge = $invoiceSurcharge->surcharge()->first();

                    $surchargeCheckedIds[] = $invoiceSurcharge->surchargeId;
                    $surchargeAmount += $invoiceSurcharge->amount;
                    $surchargeLabel[] = $surcharge->code ?? null;
                }

                $data->surchargeCheckedIds = $surchargeCheckedIds;
                $data->surchargeAmount = $surchargeAmount;
                $data->surchargeLabel = implode(', ', $surchargeLabel);
            }

            $data->items = $data->invoiceItems->map(function ($item) {
                $data = new \stdClass();
                $data->id = $item->productId;
                $data->code = $item->productCode;
                $data->detailId = $item->id;
                $data->name = $item->productName;
                $data->price = (int) $item->price;
                $data->productUnitId = $item->productUnitId;
                $data->quantity = $item->quantity;
                $data->subTotal = $item->subTotal;
                $data->discountType = $item->discountType;
                $data->discountValue = $item->discountValue;
                $data->note = $item->note;
                $data->isUpdated = true;

                $data->product_batches = $item->inventories->map(function ($inventory) {
                    $batch = $inventory->productBatch ?? new \stdClass();
                    $batch->quantity = abs($inventory->onHand);
                    $batch->selected = true;

                    return $batch;
                });
                $data->product_units = [];
                return $data;
            });
            unset($data->invoiceItems);
        }

        if ($type == 'order') {
            /**
             * @var Order $data
             */
            $data = Order::query()
                ->with('orderItems.inventories.productBatch', 'cashFlows', 'delivery', 'customer')
                ->where('code', $code)
                ->orderByDesc('id')
                ->first();

            if (!$data) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy đặt hàng!'
                ];
            }

            if (!empty($data->delivery)) {
                $data->isTickDelivery = true;
            }

            if (!empty($data->cashFlows)) {
                $paymentMethodMap = array_column(Cashflow::$listPaymentMethod, 'name', 'id');
                foreach ($data->cashFlows as $cashFlow) {
                    $cashFlow->label = $paymentMethodMap[$cashFlow->method] ?? '';
                }
            }

            if ($data->orderSurcharges()->count()) {
                $surchargeCheckedIds = [];
                $surchargeAmount = 0;
                $surchargeLabel = [];

                foreach ($data->orderSurcharges()->get() as $orderSurcharge) {
                    $surcharge = $orderSurcharge->surcharge()->first();

                    $surchargeCheckedIds[] = $orderSurcharge->surchargeId;
                    $surchargeAmount += $orderSurcharge->amount;
                    $surchargeLabel[] = $surcharge->code ?? null;
                }

                $data->surchargeCheckedIds = $surchargeCheckedIds;
                $data->surchargeAmount = $surchargeAmount;
                $data->surchargeLabel = implode(', ', $surchargeLabel);
            }

            $invoiceProcessedQuantityMap = InvoiceDetail::query()
                ->selectRaw('invoice_details.id, invoice_details.productUnitId, SUM(invoice_details.quantity) as quantity, 
                    invoice_details.invoiceId, i.orderId')
                ->join('invoices as i', 'i.id', '=', 'invoice_details.invoiceId')
                ->where('i.orderId', $data->id)
                ->groupBy('invoice_details.productUnitId')
                ->pluck('invoice_details.quantity', 'invoice_details.productUnitId');

            $data->items = $data->orderItems->map(function ($item) use ($invoiceProcessedQuantityMap) {
                $data = new \stdClass();
                $data->id = $item->productId;
                $data->code = $item->productCode;
                $data->detailId = $item->id;
                $data->name = $item->productName;
                $data->price = (int) $item->price;
                $data->productUnitId = $item->productUnitId;
                $data->quantity = $item->quantity;
                $data->subTotal = $item->subTotal;
                $data->discountType = $item->discountType;
                $data->discountValue = $item->discountValue;
                $data->note = $item->note;
                $data->isUpdated = true;

                $data->quantityTotal = $item->quantity;
                $data->quantityProcessed = $invoiceProcessedQuantityMap[$item->productUnitId] ?? 0;
                $data->quantityRemaining = max($data->quantityTotal - $data->quantityProcessed, 0);

                $data->product_batches = $item->inventories->map(function ($inventory) {
                    $batch = $inventory->productBatch ?? new \stdClass();
                    $batch->quantity = abs($inventory->onHand);
                    $batch->selected = true;

                    return $batch;
                });
                $data->product_units = [];
                return $data;
            });
            unset($data->orderItems);
        }

        return [
            'code' => 200,
            'data' => $data ?? null
        ];
    }

    public function searchProductsAction(Request $request) {
        $keyword = $request->get('keyword');

        if (!$keyword) {
            return [
                'code' => 200,
                'data' => []
            ];
        }

        $products = $this->getProductData($request);
   
        return [
            'code' => 200,
            'message' => 'OK!',
            'data' => $products
        ];
    }

    public function productBarcodeScannerAction(Request $request) {
        $productCode = $request->get('product_code');

        if (!$productCode) {
            return [
                'code' => 1,
                'message' => 'Không quét được mã vạch!',
            ];
        }

        $products = $this->getProductData($request);

        if (count($products)) {
            return [
                'code' => 200,
                'message' => 'Quét mã vạch thành công',
                'data' => $products[0] ?? []
            ];
        } else {
            return [
                'code' => 404,
                'message' => 'Không tìm thấy sản phẩm này!',
            ];
        }
    }
    public function getDataCollumn($mang){
        $id = [];        
        foreach ($mang as $key => $value) {
            if(isset($value) && $value !=null) {                
                $id [] = $value;
            }
            else{
                
                if(isset($value['productId']) && $value['productId'] !=null) {               
                    $id [] = $value;
                }   
            }            
        }        
        return $id;
    }
    public function saveAction(Request $req) {          
        $data = $req->only(['id', 'name', 'soldById', 'customerId', 'saleChannelId', 'invoiceId', 'orderId', 'isPack',
            'description', 'total', 'totalOrigin', 'totalPayment', 'discountType', 'discountValue', 'action']);       
        
        $type = $req->get('type', self::TYPE_INVOICE);
        $items = $req->get('items', []);     
        $isTickDelivery = $req->get('isTickDelivery') ?? false;
        $delivery = $isTickDelivery ? $req->get('delivery') : null;
        $cashFlows = $req->get('cash_flows', null);        
        $surchargeCheckedIds = $req->get('surchargeCheckedIds');
        $currentBranchID = $req->currentBranchId();
    
        if (!empty($data['customerId'])) {
            $customer = Customer::where('status', 1)->find($data['customerId']);
            if (!$customer) {
                return [
                    'code' => 1,
                    'message' => 'Không tìm thấy khách hàng. Mã lỗi 01!'
                ];
            }
        }

        if (!$items || !count($items)) {
            return [
                'code' => 4,
                'message' => 'Phiếu hàng đang trống!'
            ];
        }

        DB::beginTransaction();

        if ($type == self::TYPE_INVOICE) {
            $isUpdateInvoice = !empty($data['invoiceId']) && $data['action'] == self::ACTION_UPDATE_INVOICE;
            $isOrderProcessing = !empty($data['orderId']) && $data['action'] == self::ACTION_ORDER_PROCESS;
            $isCreateNewInvoice = !$isUpdateInvoice && !$isOrderProcessing;

            $invoice = new Invoice();
            $invoice->soldById = $data['soldById'];
            $invoice->customerId = $customer->id ?? null;
            $invoice->customerCode = $customer->code ?? null;
            $invoice->customerName = $customer->name ?? null;

            // action update invoice
            if ($isUpdateInvoice) {
                $invoiceOrigin = Invoice::find($data['invoiceId']);
                if (!$invoiceOrigin) {
                    return [
                        'code' => 2,
                        'message' => 'Không tìm thấy hóa đơn này. Mã lỗi 02!'
                    ];
                }

                if ($invoiceOrigin->branchId != $currentBranchID) {
                    return [
                        'code' => 3,
                        'message' => 'Không thể cập nhật do hóa đơn này không thuộc chi nhánh hiện tại!'
                    ];
                }

                if ($invoiceOrigin->status != Invoice::STATUS_PROCESS) {
                    $statusLabel = Invoice::getStatusLabel($invoiceOrigin->status);
                    return [
                        'code' => 3,
                        'message' => 'Không thể cập nhật hóa đơn có trạng thái ' . $statusLabel
                    ];
                }

                $originCode = $invoiceOrigin->code;
                $codeVer = explode('.', $originCode)[1] ?? 0;
                $originCode = explode('.', $originCode)[0] ?? Invoice::generalCode();
                $originCode = $originCode . '.' . sprintf("%02d", (int)$codeVer + 1);
                $invoice->code = $originCode;

                $invoice->purchaseDate = $invoiceOrigin->purchaseDate;
                $invoice->branchId = $invoiceOrigin->branchId;
                $invoice->orderCrmCode = $invoiceOrigin->orderCrmCode;
                $invoice->orderId = $invoiceOrigin->orderId;
                $invoice->orderCode = $invoiceOrigin->orderCode;

                $invoiceOrigin->saveStatus(Invoice::STATUS_CANCELED, 'Cập nhật hóa đơn');
                $invoiceOrigin->removeInventories();
                $invoiceOrigin->saveHistory(History::ACTION_CANCEL, ['isUpdateInvoice' => true]);
            } else {
                $invoice->code = Invoice::generalCode();
                $invoice->purchaseDate = date('Y-m-d H:i:s');
                $invoice->branchId = $currentBranchID;
            }

            // action order processing
            if ($isOrderProcessing) {
                $order = Order::find($data['orderId']);

                if (!$order) {
                    return [
                        'code' => 2,
                        'message' => 'Không tìm thấy đơn đặt hàng này. Mã lỗi 02!'
                    ];
                }

                if ($order->branchId != $currentBranchID) {
                    return [
                        'code' => 3,
                        'message' => 'Không thể tạo hóa đơn do đơn hàng này không thuộc chi nhánh hiện tại!'
                    ];
                }

                if (in_array($order->status, [Order::STATUS_COMPLETE, Order::STATUS_CANCELED])) {
                    $statusLabel = Order::getStatusLabel($order->status);
                    return [
                        'code' => 3,
                        'message' => 'Không thể tạo hóa đơn cho đơn hàng có trạng thái ' . $statusLabel
                    ];
                }

                $orderDetailIdMap = OrderDetail::query()
                    ->where('orderId', $order->id)
                    ->pluck('id', 'productUnitId');

                $quantityOTotalMap = OrderDetail::query()
                    ->where('orderId', $order->id)
                    ->get()
                    ->pluck('quantity', 'productUnitId')
                    ->toArray();

                $quantityProcessedMap = InvoiceDetail::query()
                    ->selectRaw('invoice_details.id, invoice_details.productUnitId, SUM(invoice_details.quantity) as quantity, 
                    invoice_details.invoiceId, i.orderId')
                    ->join('invoices as i', 'i.id', '=', 'invoice_details.invoiceId')
                    ->where('i.orderId', $order->id)
                    ->groupBy('invoice_details.productUnitId')
                    ->pluck('invoice_details.quantity', 'invoice_details.productUnitId');

                $isPartialOrderProcessing = false;
                foreach ($items as $item) {
                    $item = (object) $item;
                    $quantityTotal = $quantityOTotalMap[$item->productUnitId] ?? 0;
                    $quantityProcessed = $quantityProcessedMap[$item->productUnitId] ?? 0;
                    $quantityRemaining = max($quantityTotal - $quantityProcessed, 0);

                    $orderDetailId = $orderDetailIdMap[$item->productUnitId] ?? null;
                    if ($item->quantity < $quantityRemaining) {
                        $isPartialOrderProcessing = true;

                        OrderDetail::updateQuantityInventory($orderDetailId, $item->quantity);
                    } else {
                        OrderDetail::removeInventory($orderDetailId);
                    }
                }

                if ($isPartialOrderProcessing) {
                    $order->saveStatus(Order::STATUS_DELIVERY, 'Xử lí một phần đơn hàng');
                } else {
                    $order->saveStatus(Order::STATUS_COMPLETE, 'Đã tạo hóa đơn');
                }

                $invoice->orderId = $order->id;
                $invoice->orderCode = $order->code;
            }

            $invoice->totalOrigin = $data['totalOrigin'];
            $invoice->total = $data['total'];
            $invoice->totalPayment = $data['totalPayment'];
            $invoice->discountType = $data['discountType'] ?? null;
            $invoice->discountValue = $data['discountValue'] ?? 0;
            $invoice->status = $isTickDelivery ? Invoice::STATUS_PROCESS : Invoice::STATUS_COMPLETE;
            $invoice->statusValue = Invoice::getStatusLabel($invoice->status);
            $invoice->description = $data['description'];
            $invoice->invoiceDetails = json_encode($items);
            $invoice->saleChannelId = $data['saleChannelId'];
            $invoice->createdBy = auth()->id;
            $invoice->isPack = $data['isPack'] ? 1 : 0;
            $invoice->save();
            $invoice->saveStatus($invoice->status, 'Tạo mới hóa đơn');      
            $productIds = [];   
           
            if($this->getDataCollumn(array_column($items, 'id'))!=null)  $productIds = $this->getDataCollumn(array_column($items, 'id'));
            else  $productIds = $this->getDataCollumn(array_column($items, 'productId'));   
            
            $productSystemInventoryMap = Inventory::getSystemInventoryMap($productIds, $currentBranchID);
          
            if ($isOrderProcessing || $isUpdateInvoice) {
                $invoiceItemIds = InvoiceDetail::where('invoiceId', $invoice->id)->pluck('id');

                $systemInventoryUpdatedMap = Inventory::query()
                    ->selectRaw('id, productId, branchId, SUM(ABS(onHand*conversionValue)) as quantity')
                    ->where('refTable', Inventory::REF_CODE_INVOICE_DETAIL)
                    ->whereIn('refId', $invoiceItemIds)
                    ->where('branchId', $currentBranchID)
                    ->groupBy('productId')
                    ->pluck('quantity', 'productId');

            }
            
            foreach ($items as $item) {
                
                $item = (object) $item;
                /**
                 * @var Product $product
                 */                
                if(isset($item->id) && $item->id != null) $prdID = $item->id;
                else $prdID = $item->productId;
                $product = Product::query()
                    ->where('isActive', 1)
                    ->find($prdID);                
                if (!$product) {
                    return [
                        'code' => 5,
                        'message' => 'Không tìm thấy sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }

                $productUnit = ProductUnit::query()
                    ->where('productId', $product->id)
                    ->find($item->productUnitId);

                if (!$productUnit) {
                    return [
                        'code' => 6,
                        'message' => 'Không tìm thấy đơn vị sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }

                $conversionValue = $productUnit->conversionValue ?? 0;
                
                // Kiểm tra tồn kho
                //---------------------------------------------------------------------------------------------------------
              
                $quantityOrigin = isset($productSystemInventoryMap[$item->id? $item->id : $item->productId ]) ? $productSystemInventoryMap[$item->id? $item->id : $item->productId] : 0;                
                if (!empty($systemInventoryUpdatedMap) && !empty($systemInventoryUpdatedMap[$item->id])) {
                    $quantityOrigin += $systemInventoryUpdatedMap[$item->id];
                }
                
                $quantityOrigin = $conversionValue ? floor($quantityOrigin / $conversionValue) : 0;
                $isProductTypeService = Product::isTypeService($product);           
               
                if ($item->quantity > $quantityOrigin && !$isProductTypeService) {
                    return [
                        'code' => 20,
                        'message' => 'Số lượng bán sản phẩm ' . $item->name . ' - ' . $item->code . ' vượt quá tồn kho hệ thống'
                    ];
                }
                //--------------------------------------------------------------------------------------------------------- 
                if(isset($item->product_batches) && $item->product_batches != null)  $productBatches = $item->product_batches ?? [];
                else $productBatches = $item -> productBatches;
               
                if (!$isProductTypeService && (!$productBatches || !count($productBatches))) {
                    return [
                        'code' => 7,
                        'message' => 'SP ' . $product->name . ' không có lô date nào'
                    ];
                }
            
                $productBatches = array_filter($productBatches, function ($batch) {      
                   
                    return !empty($batch['selected']) && !empty($batch['quantity']);
                });
                
                if (!$isProductTypeService && (!$productBatches || !count($productBatches))) {
                    return [
                        'code' => 8,
                        'message' => 'Chưa chọn lô date cho SP ' . $product->name
                    ];
                }
                
               
                /**
                 * @var InvoiceDetail $detail
                 */
                $detail = new InvoiceDetail();
                $detail->productId = $item->id;
                $detail->productUnitId = $item->productUnitId;
                $detail->productCode = $product->code;
                $detail->invoiceId = $invoice->id;
                $detail->productName = $product->name;
                $detail->quantity = $item->quantity;
                $detail->price = $productUnit->price;
                $detail->subTotal = $item->subTotal;
                $detail->note = $item->note ?? null;
                $detail->productBatchExpire = json_encode($productBatches);
                $detail->discountType = $item->discountType ?? null;
                $detail->discountValue = $item->discountValue ?? 0;                
                $detail->save();
                
                foreach ($productBatches as $productBatch) {                     
                    if ($isCreateNewInvoice && $productBatch['quantity'] > $productBatch['quantityOrigin']) {
                        return [
                            'code' => 9,
                            'message' => 'SP ' . $product->name . ': vượt quá tồn kho!'
                        ];
                    }

                    $inventory = new Inventory();
                    $inventory->branchId = $currentBranchID;
                    $inventory->refId = $detail->id;
                    $inventory->refTable = Inventory::REF_CODE_INVOICE_DETAIL;
                    $inventory->productId = $item->id;
                    $inventory->productUnitId = $item->productUnitId;
                    $inventory->productBatchId = $productBatch['id'];
                    $inventory->onHand = $productBatch['quantity'] * -1;
                    $inventory->onHold = 0;
                    $inventory->onSale = $inventory->onHand;
                    $inventory->originalTotal = $productBatch['quantity'];
                    $inventory->conversionValue = $productUnit->conversionValue;                   
                    $inventory->save();
                  
                }
            }
            
            if ($cashFlows && is_array($cashFlows)) {
                $payments = [];
                foreach ($cashFlows as $payment) {
                    $cashFlow = new Cashflow();
                    $cashFlow->code = Cashflow::generalCode(Cashflow::CODE_INVOICE);
                    $cashFlow->userId = auth()->id;

                    if ($delivery) {
                        $delivery = (object) $delivery;
                        $cashFlow->address = $delivery->receiverAddress;
                        $cashFlow->locationName = District::getNameById($delivery->receiverDistrictId) . ' - ' . Province::getNameById($delivery->receiverProvinceId);
                        $cashFlow->wardName = Ward::getNameById($delivery->receiverWardId);
                        $cashFlow->contactNumber = $delivery->receiverPhone;
                    }

                    if ($payment['method'] == Cashflow::PAYMENT_METHOD_VOUCHER) {
                        if (!empty($couponCode = $payment['couponCode'])) {
                            $couponCode = CouponCode::find($couponCode['id']);

                            if ($couponCode->status == 1) {
                                return [
                                    'code' => 18,
                                    'message' => 'Coupon ' . $couponCode->code . ' đã được sử dụng. Vui lòng sử dụng coupon khác!'
                                ];
                            }

                            $couponCode->status = 1;
                            $couponCode->usedAt = date('Y-m-d H:i:s');
                            $couponCode->save();

                            $cashFlow->couponCodeId = $couponCode->id;
                        }
                    }

                    $cashFlow->status = Cashflow::STATUS_PAID;
                    $cashFlow->origin = Cashflow::ORIGIN_PAY;
                    $cashFlow->cashFlowGroupId = Cashflow::CASH_FLOW_GROUP_CUSTOMER;
                    $cashFlow->cashGroup = Cashflow::$listCashFlowGroup[$cashFlow->cashFlowGroupId] ?? null;
                    $cashFlow->method = $payment['method'] ?? null;
                    $cashFlow->partnerType = Cashflow::PARTNER_TYPE_CUSTOMER;
                    $cashFlow->partnerId = $customer->id ?? null;
                    $cashFlow->branchId = $currentBranchID;
                    $cashFlow->transDate = date('Y-m-d H:i:s');
                    $cashFlow->amount = $payment['amount'] ?? 0;
                    $cashFlow->partnerName = $customer->name ?? null;
                    $cashFlow->invoiceId = $invoice->id;
                    $cashFlow->save();
                    $payments[] = $cashFlow->toArray();
                }

                $invoice->payments = json_encode($payments);
                $invoice->save();
            }

            if ($delivery) {
                $deli = new Delivery();
                $deli->type = Delivery::TYPE_INVOICE;
                $deli->createdBy = auth()->id;
                $deli->invoiceId = $invoice->id;
                $deli->soldById = $invoice->soldById;
                $deli->fill($delivery);
                $deli->priceCodPayment = $deli->usingPriceCod ? max($invoice->total - $invoice->totalPayment, 0) : null;
                $deli->save();
                $deli->saveStatus($deli->status);
                $deli->saveHistory(History::ACTION_CREATED);

                $deli->decode();
                $invoice->delivery = $deli;
            }

            if (is_array($surchargeCheckedIds)) {
                foreach ($surchargeCheckedIds as $surchargeId) {
                    $surcharge = Surcharge::find($surchargeId);

                    if (!$surcharge) {
                        throw new \Exception('Có lỗi xảy ra với Thu khác, vui lòng chọn lại!');
                    }

                    $invoiceSurcharge = new InvoiceSurcharge();
                    $invoiceSurcharge->invoiceId = $invoice->id;
                    $invoiceSurcharge->surchargeId = $surchargeId;
                    $invoiceSurcharge->amount = $surcharge->getInvoiceOrderAmount($invoice->totalOrigin);
                    $invoiceSurcharge->save();
                }
            }

            $invoice->saveHistory(History::ACTION_CREATED, [
                'isUpdateInvoice' => $isUpdateInvoice,
                'invoiceOriginCode' => $invoiceOrigin->code ?? null,
                'isOrderProcessing' => $isOrderProcessing,
                'isCreateNewInvoice' => $isCreateNewInvoice
            ]);
        }

        if ($type == self::TYPE_ORDER) 
        {   
           
            $isUpdateOrder = !empty($data['orderId']) && $data['action'] == self::ACTION_UPDATE_ORDER;            
            if ($isUpdateOrder) {
                // action update order
                $order = Order::find($data['orderId']);

                if (!$order) {
                    return [
                        'code' => 15,
                        'message' => 'Không tìm thấy đơn đặt hàng này. Mã lỗi 15!'
                    ];
                }

                if ($order->branchId != $currentBranchID) {
                    return [
                        'code' => 3,
                        'message' => 'Không thể cập nhật do đơn hàng này không thuộc chi nhánh hiện tại!'
                    ];
                }

                if (in_array($order->status, [Order::STATUS_COMPLETE, Order::STATUS_CANCELED, Order::STATUS_DELIVERY])) {
                    $statusLabel = Order::getStatusLabel($order->status);
                    return [
                        'code' => 16,
                        'message' => 'Không thể cập nhật đơn hàng có trạng thái ' . $statusLabel
                    ];
                }

                $order->removeInventories();
            } 
            else {
                // action create order
                $order = new Order();
                $order->code = Order::generalCode();    
                          
                $order->purchaseDate = date('Y-m-d H:i:s');
                $order->branchId = $currentBranchID;
                $order->statusValue = Order::getStatusLabel($order->status);
                $order->createdBy = auth()->id;

                $currentBranch = Branch::find($currentBranchID);
                if (!$currentBranch) {
                    return [
                        'code' => 20,
                        'message' => 'Chi nhánh này không tồn tại!'
                    ];
                }

                $order->status = $currentBranch->requiredOrderApproval ? Order::STATUS_WAIT : Order::STATUS_CONFIRMED;
            }
                $order->soldById = $data['soldById'];
                $order->customerId = $customer->id ?? null;
                $order->customerCode = $customer->code ?? null;
                $order->customerName = $customer->name ?? null;
                $order->totalOrigin = $data['totalOrigin'];
                $order->total = $data['total'];
                $order->totalPayment = $data['totalPayment'];
                $order->discountType = $data['discountType'] ?? null;
                $order->discountValue = $data['discountValue'] ?? 0;
                $order->description = $data['description'];
                $order->orderDetails = json_encode($items);
                $order->saleChannelId = $data['saleChannelId'];
                $order->orderDelivery = json_encode($delivery);
                
            if ($isUpdateOrder) {
                $order->saveHistory(History::ACTION_UPDATED);
                $order->save();
            } else {
                
                $order->save();
                $order->saveStatus($order->status, 'Tạo mới đơn hàng');
                $order->saveHistory(History::ACTION_CREATED);
            }
           
            $productIds = array_column($items, 'id');
            $productSystemInventoryMap = Inventory::getSystemInventoryMap($productIds, $currentBranchID);
            if ($isUpdateOrder) {
                $orderItemIds = OrderDetail::where('orderId', $order->id)->pluck('id');

                $systemInventoryUpdatedMap = Inventory::query()
                    ->selectRaw('id, productId, branchId, SUM(ABS(onHand*conversionValue)) as quantity')
                    ->where('refTable', Inventory::REF_CODE_ORDER_DETAIL)
                    ->whereIn('refId', $orderItemIds)
                    ->where('branchId', $currentBranchID)
                    ->groupBy('productId')
                    ->pluck('quantity', 'productId');

            }
            
            foreach ($items as $item) {
                
                $item = (object) $item;
               
                $isUpdateDetail = !empty($item->detailId);
                if(isset($item->id) && $item->id != null) $prdID = $item->id;
                else $prdID = $item->productId;
                $product = Product::query()
                    ->where('isActive', 1)
                    ->find($prdID);

                if (!$product) {
                    return [
                        'code' => 10,
                        'message' => 'Không tìm thấy sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }
               
                $productUnit = ProductUnit::query()
                    ->where('productId', $product->id)
                    ->find($item->productUnitId);
                
                if (!$productUnit) {
                    return [
                        'code' => 11,
                        'message' => 'Không tìm thấy đơn vị sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }
               
                $conversionValue = $productUnit->conversionValue ?? 0;
                $quantityOrigin = isset($productSystemInventoryMap[$item->id]) ? $productSystemInventoryMap[$item->id] : 0;
           
                if (!empty($systemInventoryUpdatedMap) && !empty($systemInventoryUpdatedMap[$item->id])) {
                    $quantityOrigin += $systemInventoryUpdatedMap[$item->id];
                }
                $quantityOrigin = $conversionValue ? floor($quantityOrigin / $conversionValue) : 0;
                
                if ($item->quantity > $quantityOrigin && !$item->isTypeService) {
                    return [
                        'code' => 21,
                        'message' => 'Số lượng bán sản phẩm ' . $item->name . ' - ' . $item->code . ' vượt quá tồn kho hệ thống'
                    ];
                }

                $productBatches = $item->product_batches ?? [];
               

                /**
                 * @var OrderDetail $detail
                 */
                if ($isUpdateDetail) {
                    $detail = OrderDetail::find($item->detailId);

                    if (!$detail) {
                        return [
                            'code' => 18,
                            'message' => 'Phiếu hàng của sản phẩm ' . $product->name . ' không còn tồn tai nữa!'
                        ];
                    }
                } 
                else {
                    //                    if (!$productBatches || !count($productBatches)) {
                    //                        return [
                    //                            'code' => 12,
                    //                            'message' => 'SP ' . $product->name . ' không có lô date nào'
                    //                        ];
                    //                    }
                    //
                    //                    $productBatches = array_filter($productBatches, function ($batch) {
                    //                        return !empty($batch['selected']) && !empty($batch['quantity']);
                    //                    });
                    //                    if (!$productBatches || !count($productBatches)) {
                    //                        return [
                    //                            'code' => 13,
                    //                            'message' => 'Chưa chọn lô date cho SP ' . $product->name
                    //                        ];
                    //                    }

                    $detail = new OrderDetail();
                    $detail->productId = $item->id;
                    $detail->productUnitId = $item->productUnitId;
                    $detail->productCode = $product->code;
                    $detail->orderId = $order->id;
                    $detail->productName = $product->name;
                }
                $detail->quantity = $item->quantity;
                // $detail->productBatchExpire = json_encode($productBatches);
                $detail->price = $productUnit->price;
                $detail->discountType = $item->discountType ?? null;
                $detail->discountValue = $item->discountValue ?? 0;
                $detail->subTotal = $item->subTotal;
                $detail->note = $item->note ?? null;
                $detail->save();

                //                foreach ($productBatches as $productBatch) {
                //                    if ($productBatch['quantity'] > $productBatch['quantityOrigin']) {
                //                        return [
                //                            'code' => 14,
                //                            'message' => 'SP ' . $product->name . ': vượt quá tồn kho!'
                //                        ];
                //                    }

                    $inventory = new Inventory();
                    $inventory->branchId = $currentBranchID;
                    $inventory->refId = $detail->id;
                    $inventory->refTable = Inventory::REF_CODE_ORDER_DETAIL;
                    $inventory->productId = $item->id;
                    $inventory->productUnitId = $item->productUnitId;
                    $inventory->productBatchId = null;
                    $inventory->onHand = $detail->quantity * -1;
                    $inventory->onHold = 0;
                    $inventory->onSale = $inventory->onHand;
                    $inventory->originalTotal = $detail->quantity;
                    $inventory->conversionValue = $productUnit->conversionValue;
                    $inventory->save();
                //                }
            }

            if ($cashFlows && is_array($cashFlows)) {
                // remove cash flow
                $updateCashFlowIds = [];
                foreach ($cashFlows as $payment) {
                    if (!empty($payment['id'])) {
                        $updateCashFlowIds[] = $payment['id'];
                    }
                }
                Cashflow::query()
                    ->where('orderId', $order->id)
                    ->whereNotIn('id', $updateCashFlowIds)
                    ->delete();


                // update or create new cash flow
                $payments = [];
                foreach ($cashFlows as $payment) {
                    if (isset($payment['id'])) {
                        $cashFlow = Cashflow::find($payment['id']);

                        if (!$cashFlow) {
                            return [
                                'code' => 17,
                                'message' => 'Không tìm thấy thanh toán này!'
                            ];
                        }
                    } else {
                        $cashFlow = new Cashflow();
                        $cashFlow->code = Cashflow::generalCode(Cashflow::CODE_ORDER);
                        $cashFlow->userId = auth()->id;
                        $cashFlow->status = Cashflow::STATUS_PAID;
                        $cashFlow->origin = Cashflow::ORIGIN_PAY;
                        $cashFlow->cashFlowGroupId = Cashflow::CASH_FLOW_GROUP_CUSTOMER;
                        $cashFlow->cashGroup = Cashflow::$listCashFlowGroup[$cashFlow->cashFlowGroupId] ?? null;
                        $cashFlow->partnerType = Cashflow::PARTNER_TYPE_CUSTOMER;
                        $cashFlow->branchId = $currentBranchID;
                        $cashFlow->transDate = date('Y-m-d H:i:s');
                        $cashFlow->orderId = $order->id;
                    }

                    if ($delivery) {
                        $delivery = (object) $delivery;
                        $cashFlow->address = $delivery->receiverAddress;
                        $cashFlow->locationName = District::getNameById($delivery->receiverDistrictId) . ' - ' . Province::getNameById($delivery->receiverProvinceId);
                        $cashFlow->wardName = Ward::getNameById($delivery->receiverWardId);
                        $cashFlow->contactNumber = $delivery->receiverPhone;
                    }

                    if ($payment['method'] == Cashflow::PAYMENT_METHOD_VOUCHER) {
                        if (!empty($couponCode = $payment['couponCode'])) {
                            $couponCode = CouponCode::find($couponCode['id']);

                            if ($couponCode->status == 1) {
                                return [
                                    'code' => 19,
                                    'message' => 'Coupon ' . $couponCode->code . ' đã được sử dụng. Vui lòng sử dụng coupon khác!'
                                ];
                            }

                            $couponCode->status = 1;
                            $couponCode->usedAt = date('Y-m-d H:i:s');
                            $couponCode->save();

                            $cashFlow->couponCodeId = $couponCode->id;
                        }
                    }

                    $cashFlow->method = $payment['method'] ?? null;
                    $cashFlow->partnerId = $customer->id ?? null;
                    $cashFlow->amount = $payment['amount'] ?? 0;
                    $cashFlow->partnerName = $customer->name ?? null;
                    $cashFlow->save();

                    $payments[] = $cashFlow->toArray();
                }

                $order->payments = json_encode($payments);
                $order->save();
            }

            if ($delivery) {
                if ($isUpdateOrder) {
                    $deli = Delivery::query()
                        ->where('type', Delivery::TYPE_ORDER)
                        ->where('orderId', $order->id)
                        ->orderByDesc('id')
                        ->first();
                }

                if (empty($deli)) {
                    $deli = new Delivery();
                    $deli->type = Delivery::TYPE_ORDER;
                    $deli->createdBy = auth()->id;
                    $deli->orderId = $order->id;
                    $deli->soldById = $order->soldById;
                } else {
                    if (isset($delivery['status'])) {
                        $isChangeStatusDeli = $delivery['status'] != $deli->status;
                        $isCancelDeli = $isChangeStatusDeli && $delivery['status'] == Delivery::STATUS_CANCEL;
                    }
                }

                $deli->fill($delivery);
                $deli->priceCodPayment = $deli->usingPriceCod ? max($order->total - $order->totalPayment, 0) : null;

                if ($isUpdateOrder) {
                    $deli->saveHistory(!empty($isCancelDeli) ? History::ACTION_CANCEL : History::ACTION_UPDATED);
                    $deli->save();

                    if (!empty($isChangeStatusDeli)) {
                        $deli->saveStatus($deli->status);
                    }
                } else {
                    $deli->save();
                    $deli->saveStatus($deli->status);
                    $deli->saveHistory(History::ACTION_CREATED);
                }

                $deli->decode();
                $order->delivery = $deli;
            }

            if (is_array($surchargeCheckedIds)) {
                OrderSurcharge::query()
                    ->where('orderId', $order->id)
                    ->whereNotIn('surchargeId', $surchargeCheckedIds)
                    ->delete();


                foreach ($surchargeCheckedIds as $surchargeId) {
                    $surcharge = Surcharge::find($surchargeId);

                    if (!$surcharge) {
                        throw new \Exception('Có lỗi xảy ra với Thu khác, vui lòng chọn lại!');
                    }

                    $orderSurcharge = OrderSurcharge::query()
                        ->where('orderId', $order->id)
                        ->where('surchargeId', $surchargeId)
                        ->orderByDesc('id')
                        ->first();

                    if (!$orderSurcharge) {
                        $orderSurcharge = new OrderSurcharge();
                        $orderSurcharge->orderId = $order->id;
                        $orderSurcharge->surchargeId = $surchargeId;
                    }

                    $orderSurcharge->amount = $surcharge->getInvoiceOrderAmount($order->totalOrigin);
                    $orderSurcharge->save();
                }
            }
        }

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Thành công!',
            'data' => $invoice ?? $order ?? null,
            'isPartialOrderProcessing' => $isPartialOrderProcessing ?? false
        ];
    }

    public function saveReturnAction(Request $req) {
        $data = $req->only(['id', 'name', 'soldById', 'customerId', 'saleChannelId', 'invoiceId', 'orderId', 'isPack',
            'description', 'total', 'totalOrigin', 'totalPayment', 'discountType', 'discountValue', 'action']);
        $type = $req->get('type', self::TYPE_INVOICE);
        $items = $req->get('items', []);
        $isTickDelivery = $req->get('isTickDelivery') ?? false;
        $delivery = $isTickDelivery ? $req->get('delivery') : null;
        $cashFlows = $req->get('cash_flows', null);
        $surchargeCheckedIds = $req->get('surchargeCheckedIds');
        $currentBranchID = $req->currentBranchId();

        if (!empty($data['customerId'])) {
            $customer = Customer::where('status', 1)->find($data['customerId']);
            if (!$customer) {
                return [
                    'code' => 1,
                    'message' => 'Không tìm thấy khách hàng. Mã lỗi 01!'
                ];
            }
        }

        if (!$items || !count($items)) {
            return [
                'code' => 4,
                'message' => 'Phiếu trả hàng đang trống!'
            ];
        }

        DB::beginTransaction();

        if (!empty($data['invoiceId'])) {
            $isReturnInvoice = $data['action'] == self::ACTION_RETURN_INVOICE;

            $return = new Returns();
            $return->soldById = $data['soldById'];
            $return->soldByName = User::getNameFromId($data['soldById']);
            $return->customerId = $customer->id ?? null;
            $return->invoiceId = $data['invoiceId'] ?? null;
            $return->customerCode = $customer->code ?? null;
            $return->customerName = $customer->name ?? null;
            $return->saleChannelId = $data['saleChannelId'];
            $invoiceOrigin = null;


            // action return from invoice
            if ($isReturnInvoice) {
                $invoiceOrigin = Invoice::find($data['invoiceId']);

                if (!$invoiceOrigin) {
                    return [
                        'code' => 2,
                        'message' => 'Không tìm thấy hóa đơn này. Mã lỗi 02!'
                    ];
                }

                if ($invoiceOrigin->branchId != $currentBranchID) {

                    return [
                        'code' => 3,
                        'message' => 'Không thể trả hàng do hóa đơn này không thuộc chi nhánh hiện tại!'
                    ];
                }

                $return->branchId = $invoiceOrigin->branchId;

            } else {
                $return->branchId = $currentBranchID;
            }

//            $return->code = Returns::generateCode();
            $return->returnDate = date('Y-m-d H:i:s');
            $return->returnTotal = $data['total'];
            $return->totalPayment = $data['totalPayment'];
            $return->returnDiscount = $data['discountValue'] ?? 0;
            $return->status = Returns::STATUS_COMPLETE;
            $return->branchName = Branch::getNameById($return->branchId);
            $return->statusValue = Returns::getStatusLabel($return->status);
            $return->description = $data['description'];
            $return->returnDetails = json_encode($items);
            $return->saleChannelId = $data['saleChannelId'];
            $return->receivedById = auth()->id;
            $return->receiverName = User::getNameFromId($return->receivedById);
            $return->save();
            if (!$return->code) {
                $return->code = 'TH' . $return->id;
                $return->save();
            }

            $productIds = array_column($items, 'id');
            $productSystemInventoryMap = Inventory::getSystemInventoryMap($productIds, $currentBranchID);

            foreach ($items as $item) {
                $item = (object) $item;
                /**
                 * @var Product $product
                 */
                $product = Product::query()
                    ->where('isActive', 1)
                    ->find($item->id);

                if (!$product) {
                    return [
                        'code' => 5,
                        'message' => 'Không tìm thấy sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }

                $productUnit = ProductUnit::query()
                    ->where('productId', $product->id)
                    ->find($item->productUnitId);

                if (!$productUnit) {
                    return [
                        'code' => 6,
                        'message' => 'Không tìm thấy đơn vị sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }

                $conversionValue = $productUnit->conversionValue ?? 0;
                $quantityOrigin = isset($productSystemInventoryMap[$item->id]) ? $productSystemInventoryMap[$item->id] : 0;
                if (!empty($systemInventoryUpdatedMap) && !empty($systemInventoryUpdatedMap[$item->id])) {
                    $quantityOrigin += $systemInventoryUpdatedMap[$item->id];
                }
                if ($item->detailId && $invoiceOrigin) {
                    $invoiceDetailOrigin = InvoiceDetail::find($item->detailId);
                    if (!$invoiceDetailOrigin) {
                        return [
                            'code' => 6,
                            'message' => 'Không tìm thấy sản phẩm ' . $item->name . ' - ' . $item->code
                                . ' trong hóa đơn' . $invoiceOrigin->code
                        ];

                    }
                    if ($item->quantity > $invoiceDetailOrigin->quantity) {
                        return [
                            'code' => 20,
                            'message' => 'Số lượng bán sản phẩm ' . $item->name . ' - ' . $item->code
                                . ' vượt quá số lượng trong hóa đơn' . $invoiceOrigin->code
                        ];
                    }
                }

                $isProductTypeService = Product::isTypeService($product);

                $productBatches = $item->product_batches ?? [];

                if (!$isProductTypeService && (!$productBatches || !count($productBatches))) {
                    return [
                        'code' => 7,
                        'message' => 'SP ' . $product->name . ' không có lô date nào'
                    ];
                }

                $productBatches = array_filter($productBatches, function ($batch) {
                    return !empty($batch['selected']) && !empty($batch['quantity']);
                });
                if (!$isProductTypeService && (!$productBatches || !count($productBatches))) {
                    return [
                        'code' => 8,
                        'message' => 'Chưa chọn lô date cho SP ' . $product->name
                    ];
                }
                
                /**
                 * @var ReturnDetail $detail
                 */
                $detail = new ReturnDetail();
                $detail->productId = $item->id;
                $detail->returnId = $return->id;
                $detail->productUnitId = $item->productUnitId;
                $detail->productCode = $product->code;
                $detail->productName = $product->name;
                $detail->quantity = $item->quantity;
                $detail->price = $productUnit->price;
                $detail->subTotal = $item->subTotal;
                $detail->note = $item->note ?? null;
                $detail->productBatchExpire = json_encode($productBatches);
                $detail->discountType = $item->discountType ?? null;
                $detail->discountValue = $item->discountValue ?? 0;
                $detail->save();


                foreach ($productBatches as $productBatch) {

                    $inventory = new Inventory();
                    $inventory->branchId = $currentBranchID;
                    $inventory->refId = $detail->id;
                    $inventory->refCode = $return->code;
                    $inventory->refTable = Inventory::REF_CODE_RETURN_DETAIL;
                    $inventory->productId = $item->id;
                    $inventory->productUnitId = $item->productUnitId;
                    $inventory->productBatchId = $productBatch['id'];
                    $inventory->onHand = $productBatch['quantity'];
                    $inventory->onHold = 0;
                    $inventory->onSale = $inventory->onHand;
                    $inventory->originalTotal = $productBatch['quantity'];
                    $inventory->conversionValue = $productUnit->conversionValue;
                    $inventory->save();
                }
            }

            if ($cashFlows && is_array($cashFlows)) {
                $payments = [];
                foreach ($cashFlows as $payment) {
                    $cashFlow = new Cashflow();
                    $cashFlow->code = Cashflow::generalCode(Cashflow::CODE_RETURN);
                    $cashFlow->userId = auth()->id;

                    if ($payment['method'] == Cashflow::PAYMENT_METHOD_VOUCHER) {
                        if (!empty($couponCode = $payment['couponCode'])) {
                            $couponCode = CouponCode::find($couponCode['id']);

                            if ($couponCode->status == 1) {
                                return [
                                    'code' => 18,
                                    'message' => 'Coupon ' . $couponCode->code . ' đã được sử dụng. Vui lòng sử dụng coupon khác!'
                                ];
                            }

                            $couponCode->status = 1;
                            $couponCode->usedAt = date('Y-m-d H:i:s');
                            $couponCode->save();

                            $cashFlow->couponCodeId = $couponCode->id;
                        }
                    }

                    $cashFlow->status = Cashflow::STATUS_PAID;
                    $cashFlow->origin = Cashflow::ORIGIN_PAY;
                    $cashFlow->cashFlowGroupId = Cashflow::CASH_FLOW_GROUP_CUSTOMER;
                    $cashFlow->cashGroup = Cashflow::$listCashFlowGroup[$cashFlow->cashFlowGroupId] ?? null;
                    $cashFlow->method = $payment['method'] ?? null;
                    $cashFlow->partnerType = Cashflow::PARTNER_TYPE_CUSTOMER;
                    $cashFlow->partnerId = $customer->id ?? null;
                    $cashFlow->branchId = $currentBranchID;
                    $cashFlow->transDate = date('Y-m-d H:i:s');
                    $cashFlow->amount = $payment['amount'] ?? 0;
                    $cashFlow->partnerName = $customer->name ?? null;
                    $cashFlow->returnId = $return->id;
                    $cashFlow->save();

                    $payments[] = $cashFlow->toArray();
                }

                $return->payments = json_encode($payments);
                $return->save();
            }

            if (is_array($surchargeCheckedIds)) {
                foreach ($surchargeCheckedIds as $surchargeId) {
                    $surcharge = Surcharge::find($surchargeId);

                    if (!$surcharge) {
                        throw new \Exception('Có lỗi xảy ra với Thu khác, vui lòng chọn lại!');
                    }

                    $invoiceSurcharge = new ReturnSurcharge();
                    $invoiceSurcharge->returnId = $return->id;
                    $invoiceSurcharge->surchargeId = $surchargeId;
                    $invoiceSurcharge->amount = $surcharge->getInvoiceOrderAmount($return->totalOrigin);
                    $invoiceSurcharge->save();
                }
            }
        } else {

            $return = new Returns();
            $return->soldById = $data['soldById'];
            $return->soldByName = User::getNameFromId($data['soldById']);
            $return->customerId = $customer->id ?? null;
            $return->invoiceId = null;
            $return->customerCode = $customer->code ?? null;
            $return->customerName = $customer->name ?? null;
            $return->saleChannelId = $data['saleChannelId'];

            $return->returnDate = date('Y-m-d H:i:s');
            $return->branchId = $currentBranchID;

            $return->returnTotal = $data['total'];
            $return->totalPayment = $data['totalPayment'];
            $return->returnDiscount = $data['discountValue'] ?? 0;
            $return->status = Returns::STATUS_COMPLETE;
            $return->branchName = Branch::getNameById($return->branchId);
            $return->statusValue = Returns::getStatusLabel($return->status);
            $return->description = $data['description'];
            $return->returnDetails = json_encode($items);
            $return->saleChannelId = $data['saleChannelId'];
            $return->receivedById = auth()->id;
            $return->receiverName = User::getNameFromId($return->receivedById);
            $return->save();
            if (!$return->code) {
                $return->code = 'TH' . $return->id;
                $return->save();
            }

            $productIds = array_column($items, 'id');

            foreach ($items as $item) {
                $item = (object) $item;
                /**
                 * @var Product $product
                 */
                $product = Product::query()
                    ->where('isActive', 1)
                    ->find($item->id);

                if (!$product) {
                    return [
                        'code' => 5,
                        'message' => 'Không tìm thấy sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }

                $productUnit = ProductUnit::query()
                    ->where('productId', $product->id)
                    ->find($item->productUnitId);

                if (!$productUnit) {
                    return [
                        'code' => 6,
                        'message' => 'Không tìm thấy đơn vị sản phẩm ' . $item->name . ' - ' . $item->code
                    ];
                }

                $isProductTypeService = Product::isTypeService($product);

                $productBatches = $item->product_batches ?? [];

                if (!$isProductTypeService && (!$productBatches || !count($productBatches))) {
                    return [
                        'code' => 7,
                        'message' => 'SP ' . $product->name . ' không có lô date nào'
                    ];
                }

                $productBatches = array_filter($productBatches, function ($batch) {
                    return !empty($batch['selected']) && !empty($batch['quantity']);
                });
                if (!$isProductTypeService && (!$productBatches || !count($productBatches))) {
                    return [
                        'code' => 8,
                        'message' => 'Chưa chọn lô date cho SP ' . $product->name
                    ];
                }

                /**
                 * @var ReturnDetail $detail
                 */
                $detail = new ReturnDetail();
                $detail->productId = $item->id;
                $detail->productUnitId = $item->productUnitId;
                $detail->productCode = $product->code;
                $detail->returnId = $return->id;
                $detail->productName = $product->name;
                $detail->quantity = $item->quantity;
                $detail->price = $productUnit->price;
                $detail->subTotal = $item->subTotal;
                $detail->note = $item->note ?? null;
                $detail->productBatchExpire = json_encode($productBatches);
                $detail->discountType = $item->discountType ?? null;
                $detail->discountValue = $item->discountValue ?? 0;
                $detail->save();

                foreach ($productBatches as $productBatch) {

                    $inventory = new Inventory();
                    $inventory->branchId = $currentBranchID;
                    $inventory->refId = $detail->id;
                    $inventory->refCode = $return->code;
                    $inventory->refTable = Inventory::REF_CODE_RETURN_DETAIL;
                    $inventory->productId = $item->id;
                    $inventory->productUnitId = $item->productUnitId;
                    $inventory->productBatchId = $productBatch['id'];
                    $inventory->onHand = $productBatch['quantity'];
                    $inventory->onHold = 0;
                    $inventory->onSale = $inventory->onHand;
                    $inventory->originalTotal = $productBatch['quantity'];
                    $inventory->conversionValue = $productUnit->conversionValue;
                    $inventory->save();
                }
            }

            if ($cashFlows && is_array($cashFlows)) {
                $payments = [];
                foreach ($cashFlows as $payment) {
                    $cashFlow = new Cashflow();
                    $cashFlow->code = Cashflow::generalCode(Cashflow::CODE_RETURN);
                    $cashFlow->userId = auth()->id;

                    if ($payment['method'] == Cashflow::PAYMENT_METHOD_VOUCHER) {
                        if (!empty($couponCode = $payment['couponCode'])) {
                            $couponCode = CouponCode::find($couponCode['id']);

                            if ($couponCode->status == 1) {
                                return [
                                    'code' => 18,
                                    'message' => 'Coupon ' . $couponCode->code . ' đã được sử dụng. Vui lòng sử dụng coupon khác!'
                                ];
                            }

                            $couponCode->status = 1;
                            $couponCode->usedAt = date('Y-m-d H:i:s');
                            $couponCode->save();

                            $cashFlow->couponCodeId = $couponCode->id;
                        }
                    }

                    $cashFlow->status = Cashflow::STATUS_PAID;
                    $cashFlow->origin = Cashflow::ORIGIN_PAY;
                    $cashFlow->cashFlowGroupId = Cashflow::CASH_FLOW_GROUP_CUSTOMER;
                    $cashFlow->cashGroup = Cashflow::$listCashFlowGroup[$cashFlow->cashFlowGroupId] ?? null;
                    $cashFlow->method = $payment['method'] ?? null;
                    $cashFlow->partnerType = Cashflow::PARTNER_TYPE_CUSTOMER;
                    $cashFlow->partnerId = $customer->id ?? null;
                    $cashFlow->branchId = $currentBranchID;
                    $cashFlow->transDate = date('Y-m-d H:i:s');
                    $cashFlow->amount = $payment['amount'] ?? 0;
                    $cashFlow->partnerName = $customer->name ?? null;
                    $cashFlow->returnId = $return->id;
                    $cashFlow->save();

                    $payments[] = $cashFlow->toArray();
                }

                $return->payments = json_encode($payments);
                $return->save();
            }

            if (is_array($surchargeCheckedIds)) {
                foreach ($surchargeCheckedIds as $surchargeId) {
                    $surcharge = Surcharge::find($surchargeId);

                    if (!$surcharge) {
                        throw new \Exception('Có lỗi xảy ra với Thu khác, vui lòng chọn lại!');
                    }

                    $invoiceSurcharge = new ReturnSurcharge();
                    $invoiceSurcharge->returnId = $return->id;
                    $invoiceSurcharge->surchargeId = $surchargeId;
                    $invoiceSurcharge->amount = $surcharge->getInvoiceOrderAmount($return->totalOrigin);
                    $invoiceSurcharge->save();
                }
            }
        }

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Thành công!',
            'data' => $invoice ?? $order ?? null,
            'isPartialOrderProcessing' => $isPartialOrderProcessing ?? false
        ];
    }
    public function updateProductDataAction(Request $request) {
        $products = $this->getProductData($request);

        return [
            'code' => 200,
            'data' => $products
        ];
    }

    private function getProductData(Request $request) {
        
        $productIds = $request->get('product_ids', []);
        $keyword = $request->get('keyword');
        $currentBranchID = $request->header('X-Branch-Id');

        $type = $request->get('type', null);
        $id = $request->get('id', null);
        $productCode = $request->get('product_code');

        $query = Product::query()
            ->select('id', 'code', 'name', 'images', 'type')
            ->with(['productUnits', 'productBatches' => function ($q) {
                $q->orderBy('expireDate');
            }])
            ->where('isActive', 1);

        if ($keyword) {
            $keyword = "%" . $keyword . '%';

            $query->where(function ($q) use ($keyword) {
                $q->where('name', 'LIKE', $keyword)
                    ->orWhere('code', 'LIKE', $keyword);
            })
                ->orderByDesc('id')
                ->limit(25);

        }

        if (!empty($productIds)) {
            if (is_string($productIds)) {
                $productIds = explode(',', $productIds);
            }
            $query->whereIn('id', $productIds);
        }

        if ($productCode) {
            $query->where('code', $productCode);
        }

        $products = $query->get();

        if (empty($productIds)) {
            $productIds = $products->pluck('id');
        }

        $actualInventoryMap = Inventory::getActualInventoryMap($productIds, $currentBranchID);
        $systemInventoryMap = Inventory::getSystemInventoryMap($productIds, $currentBranchID);
        $inventoryProductBatchMap = Inventory::getInventoryProductBatchMap($productIds, $currentBranchID);
        $orderSupplierSumByProductMap = OrderSupplierDetail::getOrderSupplierSumByProductMap($productIds, $currentBranchID);
        $orderDetailsSumByProductMap = OrderDetail::getOrderDetailsSumByProductMap($productIds, $currentBranchID);

        if (!empty($type) && !empty($id)) {
            if ($type == self::TYPE_ORDER) {;
                $refTable = Inventory::REF_CODE_ORDER_DETAIL;
                $refIds = OrderDetail::where('orderId', $id)->pluck('id');
            }

            if ($type == self::TYPE_INVOICE) {
                $refTable = Inventory::REF_CODE_INVOICE_DETAIL;
                $refIds = InvoiceDetail::where('invoiceId', $id)->pluck('id');
            }

            if (!empty($refIds) && !empty($refTable)) {
                $systemInventoryUpdatedMap = Inventory::query()
                    ->selectRaw('id, productId, branchId, SUM(ABS(onHand*conversionValue)) as quantity')
                    ->where('refTable', $refTable)
                    ->whereIn('refId', $refIds)
                    ->where('branchId', $currentBranchID)
                    ->groupBy('productId')
                    ->pluck('quantity', 'productId');

                $inventoryProductBatchUpdatedMap = Inventory::query()
                    ->selectRaw('*,SUM(ABS(onHand*conversionValue)) as quantity')
                    ->where('refTable', $refTable)
                    ->whereIn('refId', $refIds)
                    ->where('branchId', $currentBranchID)
                    ->whereNotNull('productBatchId')
                    ->groupBy('productBatchId')
                    ->pluck('quantity', 'productBatchId');
            }
        }

        /**
         * @var Product $product
         * @var ProductUnit $productUnit
         * @var ProductBatch $proBatch
         */
        foreach ($products as $product) {
            $product->thumb_url = get_thumb_product($product->images ?? null);
            $product->isTypeService = Product::isTypeService($product);

            if (count($product->productUnits)) {
                foreach ($product->productUnits as $productUnit) {
                    $conversionValue = $productUnit->conversionValue ?? 1;

                    $quantityOrigin = isset($systemInventoryMap[$product->id]) ? $systemInventoryMap[$product->id] : 0;
                 
                    if (!empty($systemInventoryUpdatedMap) && !empty($systemInventoryUpdatedMap[$product->id])) {
                        $quantityOrigin += $systemInventoryUpdatedMap[$product->id];
                    }
                    $productUnit->quantityOrigin = $conversionValue ? floor($quantityOrigin / $conversionValue) : 0;

                    $quantityActual = isset($actualInventoryMap[$product->id]) ? $actualInventoryMap[$product->id] : 0;
                    $productUnit->quantityActual = $conversionValue ? floor($quantityActual / $conversionValue) : 0;

                    $supplierCount = $orderSupplierSumByProductMap[$product->id] ?? 0;
                    $supplierCount = $conversionValue ? floor($supplierCount / $conversionValue) : 0;
                    $productUnit->supplier_count = $supplierCount;

                    $orderDetailsCount = $orderDetailsSumByProductMap[$product->id] ?? 0;
                    $orderDetailsCount = $conversionValue ? floor($orderDetailsCount / $conversionValue) : 0;
                    $productUnit->order_details_count = $orderDetailsCount;
                    $productUnit->promotions = (new PromotionsController())->getProductUnitPromotions(new Request([], ['id' => $productUnit->id]));
                }
            }

            if (count($product->productBatches)) {
                $updatedFirstBatch = false;
                foreach ($product->productBatches as $key => $proBatch) {
                    if (!empty($inventoryProductBatchMap[$proBatch->id])) {
                        $proBatch->quantityOrigin = $inventoryProductBatchMap[$proBatch->id] ?? 0;

                        if (!empty($inventoryProductBatchUpdatedMap) && !empty($inventoryProductBatchUpdatedMap[$proBatch->id])) {
                            $proBatch->quantityOrigin += $inventoryProductBatchUpdatedMap[$proBatch->id];
                        }

                        if (!$updatedFirstBatch) {
                            $proBatch->selected = true;
                            $proBatch->quantity = 1;
                            $updatedFirstBatch = true;
                        } else {
                            $proBatch->selected = false;
                            $proBatch->quantity = 0;
                        }
                    } else {
                        unset($product->productBatches[$key]);
                    }
                }

                $productBatches = array_values($product->productBatches->toArray());
                unset($product->productBatches);
                $product->product_batches = $productBatches;
            }

            $product->productUnit = $product->productUnits[0] ?? null;
            $product->productUnitId = $product->productUnits[0]->id ?? null;
            $product->price = $product->productUnits[0]->price ?? 0;
            $product->subTotal = $product->price;
        }

        return $products;
    }

    public function getProductInventoryByBranchAction(Request $request) {
        $productUnitId = $request->get('product_unit_id');
        $currentBranchID =  $request->currentBranchId();
        $user = auth();

        $branches = Branch::query()->whereIn('id', $user->branchIDs)->get();

        $productUnit = ProductUnit::query()
            ->whereHas('product', function ($q) {
                $q->where('isActive', 1);
            })
            ->find($productUnitId);

        if (!$productUnit) {
            return [
                'code' => 404,
                'message' => 'Không tìm thấy đơn vị sản phẩm ' . $productUnitId
            ];
        }

        $actualInventoryMap = Inventory::query()
            ->where('productId', $productUnit->productId)
            ->where('refTable', '!=', Inventory::REF_CODE_ORDER_DETAIL)
            ->selectRaw('id, productId, branchId, SUM(onHand*conversionValue) as quantity')
            ->groupBy('branchId')
            ->pluck('quantity', 'branchId');

        $systemInventoryMap = Inventory::query()
            ->where('productId', $productUnit->productId)
            ->selectRaw('id, productId, branchId, SUM(onHand*conversionValue) as quantity')
            ->groupBy('branchId')
            ->pluck('quantity', 'branchId');

        $orderSupplierSumMap = OrderSupplierDetail::query()
            ->selectRaw('SUM(`order_supplier_details`.`quantity` * `pu`.`conversionValue`) as supplier_sum, os.branchId')
            ->join('order_suppliers as os', 'os.id', '=', 'order_supplier_details.orderSupplierId')
            ->join('product_units as pu', 'pu.id', '=', 'order_supplier_details.productUnitId')
            ->where('order_supplier_details.productId', $productUnit->productId)
            ->where('os.status', OrderSupplier::STATUS_CONFIRMED)
            ->groupBy('os.branchId')
            ->get()
            ->pluck('supplier_sum', 'branchId');

        $orderDetailsSumMap = OrderDetail::query()
            ->selectRaw('SUM(ABS(`i`.`onHand` * `i`.`conversionValue`)) as order_details_sum, o.branchId')
            ->join('orders as o', 'o.id', '=', 'order_details.orderId')
            ->join('inventories as i', 'i.refId', '=', 'order_details.id')
            ->where('i.refTable', Inventory::REF_CODE_ORDER_DETAIL)
            ->where('order_details.productId', $productUnit->productId)
            ->whereIn('o.status', [Order::STATUS_WAIT, Order::STATUS_CONFIRMED, Order::STATUS_DELIVERY])
            ->groupBy('o.branchId')
            ->get()
            ->pluck('order_details_sum', 'branchId');

        foreach ($branches as $branch) {
            $supplierCount = $orderSupplierSumMap[$branch->id] ?? 0;
            $supplierCount = $productUnit->conversionValue ? $supplierCount / $productUnit->conversionValue : 0;
            $branch->supplier_count = $supplierCount;

            $orderDetailsCount = $orderDetailsSumMap[$branch->id] ?? 0;
            $orderDetailsCount = $productUnit->conversionValue ? $orderDetailsCount / $productUnit->conversionValue : 0;
            $branch->order_details_count = $orderDetailsCount;

            $quantityOrigin = $systemInventoryMap[$branch->id] ?? null;
            $quantityActual = $actualInventoryMap[$branch->id] ?? null;
            $branch->quantityOrigin = ($quantityOrigin && $productUnit->conversionValue) ? $quantityOrigin / $productUnit->conversionValue : 0;
            $branch->quantityActual = ($quantityActual && $productUnit->conversionValue) ? $quantityActual / $productUnit->conversionValue : 0;
            $branch->current = $branch->id == $currentBranchID;
        }

        return [
            'code' => 200,
            'data' => $branches
        ];
    }

    public function applyPromotionsAction(Request $req) {
        $data = $req->get('data');
        $items = $data['items'] ?? [];
        $promotions = $req->get('promotions', []);

        $promotionIds = [];
        $discountProductUnitMap = [];
        $productUnitApplyIdMap = [];
        foreach ($promotions as $p) {
            $promotionIds[] = $p['id'];

            if (!empty($p['discountProductUnitsSelected'])) {
                $discountProductUnitMap[$p['id']] = $p['discountProductUnitsSelected'];
                $productUnitApplyIdMap[$p['id']] = $p['productUnitApplyId'];
            }
        }

        $promotions = Promotion::query()
            ->whereIn('id', $promotionIds)
            ->where('status', 1)
            ->get()->all();


        $invoice = new Invoice();
        $invoice->forceFill($data);

        $details = [];
        foreach ($items as $item) {
            $detail = new InvoiceDetail();
            $detail->forceFill($item);
            $details[] = $detail;
        }

        $transaction = new InvoicePromotionTransaction($invoice, $details, false, $promotions);
        $res = $transaction->commit();

        $appliedPromotions = [];
        if (is_array($res)) {
            foreach ($res as $resItem) {
                if (is_array($resItem)) {
                    foreach ($resItem as $promotionRes) {
                        if (!empty($promotionRes['if']) && !empty($promotionRes['then'])) {
                            $promotionApply = $promotionRes['then'];
                            if (isset($discountProductUnitMap[$promotionApply['promotionId']])) {
                                $promotionApply['discountProductsSelected'] = $discountProductUnitMap[$promotionApply['promotionId']];
                                $promotionApply['productUnitApplyId'] = $productUnitApplyIdMap[$promotionApply['promotionId']] ?? null;
                            }

                            $appliedPromotions[] = $promotionApply;
                        }
                    }
                }
            }
        }

        if (!count($appliedPromotions)) {
            return [
                'code' => 1,
                'message' => 'Áp dụng khuyến mãi không thành công',
            ];
        }

        foreach ($appliedPromotions as $promotion) {
            if (!empty($promotion['discountProductsSelected']) && !empty($promotion['productUnitApplyId'])) {
                foreach ($details as $key => $detail) {
                    if ($detail->productUnitId == $promotion['productUnitApplyId']) {
                        $detail->discountProducts = $promotion['discountProductsSelected'];
                    }
                }
            }
        }

//        $transaction->rollback();

        $invoice->items = $details;

        return [
            'code' => 200,
            'message' => 'Áp dụng khuyến mãi thành công',
            'data' => $invoice
        ];
    }

    public function applyCouponAction(Request $request) {
        $customerId = $request->get('customer_id');
        $saleChannelId = $request->get('sale_channel_id');
        $branchId = $request->currentBranchId();
        $soldById = $request->get('sold_by_id');
        $invoiceAmount = $request->get('invoice_amount');
        $code = $request->get('coupon_code');
        $currentCouponCodeIds = $request->get('current_coupon_code_ids');

        /**
         * @var CouponCode $couponCode
         * @var Coupon $coupon
         */

        $code = trim($code);
        $couponCode = CouponCode::query()
            ->where('code', $code)
            ->orderByDesc('id')
            ->first();

        if (!$couponCode) {
            return [
                'code' => 404,
                'message' => 'Mã Coupon này không tồn tại!'
            ];
        }

        $coupon = Coupon::query()
            ->where('status', 1)
            ->where('startTime', '<=', date('Y-m-d H:i:s'))
            ->where('endTime', '>=', date('Y-m-d H:i:s'))
            ->find($couponCode->couponId);

        if (!$coupon) {
            return [
                'code' => 404,
                'message' => 'Coupon không tồn tại hoặc đã hết hạn!'
            ];
        }

        if ($couponCode->status == CouponCode::STATUS_USED) {
            return [
                'code' => 1,
                'message' => 'Mã Coupon này đã được sử dụng!'
            ];
        }

        if ($invoiceAmount < $coupon->minInvoiceAmount) {
            return [
                'code' => 2,
                'message' => 'Giá trị đơn hàng không đủ để áp dụng coupon!'
            ];
        }

        $couponApplyTo = $coupon->getApplyTO();

        if ($coupon->applyForBranch) {
            $appliedBranchIds = $couponApplyTo['branchIDs'] ?? [];

            if ($appliedBranchIds && count($appliedBranchIds)) {
                if (!in_array($branchId, $appliedBranchIds)) {
                    return [
                        'code' => 3,
                        'message' => 'Coupon không được áp dụng cho chi nhánh này!'
                    ];
                }
            }
        }

        if ($coupon->applyForSale) {
            $appliedSaleIds = $couponApplyTo['saleIDs'] ?? [];

            if ($appliedSaleIds && count($appliedSaleIds)) {
                if (!in_array($soldById, $appliedSaleIds)) {
                    return [
                        'code' => 4,
                        'message' => 'Coupon không được áp dụng cho người bán này!'
                    ];
                }
            }
        }

        if ($coupon->applyForSaleChannel) {
            $appliedSaleChannelIds = $couponApplyTo['saleChannelIDs'] ?? [];

            if ($appliedSaleChannelIds && count($appliedSaleChannelIds)) {
                if (!in_array($saleChannelId, $appliedSaleChannelIds)) {
                    return [
                        'code' => 5,
                        'message' => 'Coupon không được áp dụng cho kênh bán này!'
                    ];
                }
            }
        }

        if ($coupon->applyForCustomerGroup == 2) {
            $appliedCustomerIds = $couponApplyTo['customerIDs'] ?? [];

            if (!$appliedCustomerIds || !count($appliedCustomerIds) || !in_array($customerId, $appliedCustomerIds)) {
                return [
                    'code' => 6,
                    'message' => 'Coupon không được áp dụng cho khách hàng này!'
                ];
            }
        }

        if ($currentCouponCodeIds) {
            if (!$coupon->allowManyCoupons) {
                return [
                    'code' => 7,
                    'message' => "Coupon " . $code . " không hợp lệ.<br/>Không Áp dụng gộp nhiều coupon trên một hoá đơn"
                ];
            } else {
                $currentCouponNotAllowMany = Coupon::query()
                    ->join('coupon_codes as cp', 'cp.couponId', '=', 'coupons.id')
                    ->whereIn('cp.id', $currentCouponCodeIds)
                    ->where('coupons.allowManyCoupons', 0)
                    ->exists();

                if ($currentCouponNotAllowMany) {
                    return [
                        'code' => 8,
                        'message' => "Coupon không hợp lệ.<br/>Không Áp dụng gộp nhiều coupon trên một hoá đơn"
                    ];
                }
            }
        }

        return [
            'code' => 200,
            'message' => 'Áp dụng coupon thành công!',
            'coupon' => $coupon,
            'couponCode' => $couponCode,
        ];
    }

    public function getCustomerAction(Request $request) {
        $customerId = $request->get('customer_id');

        $customer = Customer::query()
            ->where('status', 1)
            ->find($customerId);

        if (!$customer) {
            return [
                'code' => 404,
                'message' => 'Không tìm thấy khách hàng này!'
            ];
        }

        $address = Customer::getDefaultAddress($customerId);

        return [
            'code' => 200,
            'data' => $customer,
            'address' => $address
        ];
    }

    public function getSurchargesAction() {
        $data = Surcharge::query()
            ->where('status', 1)
            ->orderByDesc('order')
            ->orderByDesc('id')
            ->get();

        return [
            'code' => 200,
            'data' => $data
        ];
    }

    public function getOrderProcessingAction(Request $request) {
        $page = $request->get('page', 1);
        $code = $request->get('code');
        $customerName = $request->get('customerName');
        $description = $request->get('description');
        $productCode = $request->get('productCode');
        $productName = $request->get('productName');
        $purchaseDate = $request->get('purchaseDate');

        $orders = Order::query()
            ->with(['customer:id,name'])
            ->whereIn('status', [Order::STATUS_WAIT, Order::STATUS_CONFIRMED, Order::STATUS_DELIVERY]);

        if ($code) {
            $orders->where('code', 'LIKE', '%' . $code . '%');
        }

        if ($customerName) {
            $orders->whereHas('customer', function($q) use ($customerName) {
                $q->where('name', 'LIKE', '%' . $customerName . '%');
            });
        }

        if ($description) {
            $orders->where('description', 'LIKE', '%' . $description . '%');
        }

        if ($productCode || $productName) {
            $orders->whereHas('products', function ($q) use ($productCode, $productName) {
                if ($productCode) {
                    $q->where('code', 'LIKE', '%' . $productCode . '%');
                }

                if ($productName) {
                    $q->where('name', 'LIKE', '%' . $productName . '%');
                }
            });
        }

        $orders->timeFieldIn('purchaseDate', $purchaseDate);

        $orders = $orders->orderByDesc('id')
            ->paginate(10, ['*'], 'page', $page);

        $listOrderStatusMap = array_column(Order::$listStatus, 'name', 'id');
        foreach ($orders->items() as $order) {
            $order->statusLabel = $listOrderStatusMap[$order->status] ?? '--';
        }

        return [
            'code' => 200,
            'data' => $orders->items(),
            'paginate' => [
                'currentPage' => $orders->currentPage(),
                'lastPage' => $orders->lastPage(),
                'perPage' => $orders->perPage(),
            ]
        ];
    }
}
