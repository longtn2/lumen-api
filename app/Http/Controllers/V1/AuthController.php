<?php

namespace App\Http\Controllers\V1;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Firebase\JWT\JWT;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $req){
        $this->validate($req, [
            'password' => 'required',
            'email' => 'required|email'
        ]);

        $email = $req->get('email');
        $password = $req->get('password');

        $user = User::query()->where('email', $email)->first();

        if (!$user) {
            return [
                'code' => 1,
                'message' => 'Tên đăng nhập hoặc mật khẩu không hợp lệ',
            ];
        }
        if($user -> status == 0){
            return [
                'code' => 1,
                'message' => 'Tài khoản bị khóa! Vui lòng liên hệ quản trị viên',
            ]; 
        }

        if (password_verify($password, $user->password)) {
            return $this->sign($user);
        }

        return [
            'code' => 2,
            'message' => 'Tên đăng nhập hoặc mật khẩu không hợp lệ',
        ];
    }

    private function sign($user): array
    {
        $time = time();

        $payload = [
            'iss' => 'omipos',
            'aud' => 'omipos',
            'iat' => $time,
            'nbf' => $time,
            'exp' => $time + 86400000,
            'user' => [
                'id' => $user->id,
            ],
        ];

        $jwtSecret = env('JWT_SECRET');

        if (empty($jwtSecret)) {
            return [
                'code' => 503,
                'message' => 'Missign env.JWT_SECRET'
            ];
        }

        $jwt = JWT::encode($payload,$jwtSecret,'HS256');

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'token' => $jwt,
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'branch_ids' => $user->branchIDs
                ]
            ],
        ];
    }

    //
}
