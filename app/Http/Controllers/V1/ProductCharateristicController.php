<?php

namespace App\Http\Controllers\V1;

use OmiCare\Http\Request;
use App\Models\Category;
use App\Http\Controllers\AuthenticatedController;
use Illuminate\Support\Collection;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Utils\V;
use App\Models\ProductCharateristic;
use DB;
class ProductCharateristicController extends AuthenticatedController {

   public function showAction(){
        // $model = ProductCharateristic::all();         
       
        $entry = ProductCharateristic::all();  
        if (!$entry) $entry = [];
        return [
            'code' => 200,
            'message' => "success",
            'data' => $entry
        ];
    }
}