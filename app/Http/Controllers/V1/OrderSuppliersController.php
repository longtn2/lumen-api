<?php

namespace App\Http\Controllers\V1;


use App\Models\OrderSupplier;
use App\Http\Controllers\AuthenticatedController;
use App\Models\OrderSupplierDetail;
use App\Models\Product;
use App\Models\PurchaseOrder;
use App\Models\PurchaseOrderDetail;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\V;
use OmiCare\Utils\Auth;
use OmiCare\Utils\DB;

class OrderSuppliersController extends AuthenticatedController
{

    /**
     * @uri /v1/order-suppliers/show
     * @return array
     */
    public function showAction(Request $req)
    {
        $code = $req->code;
        $entry = OrderSupplier::where('code', $code)->first();

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $orderDetails = OrderSupplierDetail::query()
            ->with('productUnit')
            ->where("orderSupplierId", $entry->id)
            ->get();

        foreach ($orderDetails as $orderDetail) {

            $product = Product::with('productUnits')->where("id", $orderDetail->productId)->first();
            $orderDetail->product = $product;
        }

        $purchaseOrders = PurchaseOrder::query()
            ->with("purchaseOrderDetails")
            ->where('orderSupplierCode', $entry->id)
            ->select(['id', 'code', 'purchaseDate', 'purchaseName', 'status'])
            ->get();

        foreach ($purchaseOrders as $purchaseOrder) {
            $purchaseOrder->statusName = PurchaseOrder::STATUS_NAME_MAP[$purchaseOrder->status] ?? '';
        }

        $entry->purchaseOrders = $purchaseOrders;
        $entry->orderDetails = $orderDetails;

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'details' => $orderDetails
            ]
        ];
    }

    /**
     * @uri /v1/order-suppliers/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = OrderSupplier::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/order-suppliers/toggle
     * @return array
     */
    public function toggleAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        $entry = OrderSupplier::find($data['id']);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $redirect = true;

        $entry->status = $data['status'];
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/order-suppliers/detail?code=' . $entry->code : false,
        ];
    }

    /**
     * @uri /v1/order-suppliers/update
     * @return array
     */
    public function updateAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::required, [
                'createdBy',
                'createdAt'
            ])
            ->validOrFail();


        /**
         * @var OrderSupplier $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = OrderSupplier::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new OrderSupplier();
        }

        $entry->fill($data);

        if (isset($data['expectedAt'])) {
            $datetime1 = date_create($data["expectedAt"]);
            $datetime2 = date_create($data["createdAt"]);

            $interval = date_diff($datetime1, $datetime2);

            $entry->waitDay = $interval->format("%d");
        }

        $currentUserEmail = Auth::user()->id;

        $entry->updatedBy = $currentUserEmail;


        if (!$entry->code) {
            $entry->code = 'PDN' . $entry->id;
        }

        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/order-suppliers/detail?code=' . $entry->code : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * @uri /v1/order-suppliers/save
     * @return array
     */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');
        $orderSupplierDetails = $req->get('details');

        V::make($data)
            ->rule(V::required, [
                'createdBy',
                'createdAt'
            ])
            ->validOrFail();


        /**
         * @var OrderSupplier $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = OrderSupplier::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new OrderSupplier();
        }

        $entry->fill($data);

        if (isset($data['expectedAt'])) {
            $datetime1 = date_create($data["expectedAt"]);
            $datetime2 = date_create($data["createdAt"]);

            $interval = date_diff($datetime1, $datetime2);
            $entry->waitDay = $interval->format("%d");
        }

        $currentUserEmail = Auth::user()->id;

        $entry->updatedBy = $currentUserEmail;

        DB::beginTransaction();

        $entry->save();

        if (!$entry->code) {
            $entry->code = 'PDN' . $entry->id;
        }

        $totalQuantity = 0;
        $productMap = [];

        foreach ($orderSupplierDetails as $detail) {
            $orderSupplierDetailId = $detail['id'] ?? null;

            $orderSupplierDetailData = [
                'orderSupplierId' => $entry->id,
                'productId' => $detail['productId'],
                'productUnitId' => $detail['productUnitId'],
                'quantity' => $detail['quantity'],
                'price' => $detail['price'] ?? 0,
                'discount' => $detail['discount'] ?? 0,
                'subTotal' => $detail['subTotal'] ?? 0,
            ];

            $productMap[$detail['productId']] = true;
            $totalQuantity += $detail['quantity'];

            if ($orderSupplierDetailId) {
                $productEntry = OrderSupplierDetail::find($orderSupplierDetailId);
                if (!$productEntry) {
                    return [
                        'code' => 1,
                        'message' => 'Không tìm thấy sản phẩm'
                    ];
                }
            } else {
                $productEntry = new OrderSupplierDetail();
            }


            $productEntry->fill($orderSupplierDetailData);
            $productEntry->save();
        }

        $entry->totalQuantity = $totalQuantity;
        $entry->totalProductType = count($productMap);

        if ($totalQuantity === 0) {
            DB::rollBack();

            return [
                'code' => 3,
                'message' => 'Vui lòng chọn sản phẩm để nhập'
            ];
        }

        $entry->save();
        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/order-suppliers/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/order-suppliers/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $selectFlip = array_flip(array_column($fields, 'field'));
        unset($selectFlip['statusName']);
        $select = array_keys($selectFlip);
        $select[] = 'status';

        $currentBranchId = $req->currentBranchId();


        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');
        $status = $req->get('status');
        $branches = $req->get('branches');
        $suppliers = $req->get('suppliers');
        $createdBy = $req->get('createdBy');
        $orderBy = $req->get('orderBy');

        $query = OrderSupplier::query()
            ->with("supplier")
            ->select($select)
            ->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->where('code', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);

        if ($status) {
            $statusFilter = $status ? explode(',', $status) : [];
            $query->whereIn('status', $statusFilter);
        }

        if ($branches) {
            $branchIDs = $branches ? explode(',', $branches) : [];
            $query->whereIn('branchId', $branchIDs);
        } else {
            $query->where('branchId', $currentBranchId);
        }

        if ($suppliers) {
            $supplierIDs = $suppliers ? explode(',', $suppliers) : [];
            $query->whereIn('supplierId', $supplierIDs);
        }

        if ($createdBy) {
            $createdByIDs = $createdBy ? explode(',', $createdBy) : [];
            $query->whereIn('createdBy', $createdByIDs);
        }

        if ($orderBy) {
            $orderByIDs = $orderBy ? explode(',', $orderBy) : [];
            $query->whereIn('orderBy', $orderByIDs);
        }
        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    public function detailDataAction(Request $req)
    {
        $productId = $req->get('productId');
        $detail = OrderSupplierDetail::query()->where('productId', $productId)->orderBy('id', 'desc')->first();

        if (!$detail) {
            return ['code' => 404];
        }

        return ['code' => 200, 'data' => $detail];
    }

    private function getDisplayFields(): array
    {
        $fields = [

            [
                'field' => 'code',
                'name' => 'Mã đặt hàng nhập',
            ],
            [
                'field' => 'purchaseOrderCodes',
                'name' => 'Mã nhập hàng',
            ],
            [
                'field' => 'supplierId',
                'name' => 'Nhà cung cấp',
            ],
            [
                'field' => 'createdAt',
                'name' => 'Thời gian',
                'render' => 'date'
            ],
            [
                'field' => 'expectedAt',
                'name' => 'Ngày nhập dự kiến',
                'render' => 'date'
            ],
            [
                'field' => 'waitDay',
                'name' => 'Số ngày chờ',
            ],
            [
                'field' => 'totalQuantity',
                'name' => 'Tổng số lượng',
            ],
            [
                'field' => 'statusName',
                'name' => 'Trạng Thái',
            ],
            [
                'field' => 'totalProductType',
                'name' => 'Tổng số mặt hàng',
            ],

        ];

        return $fields;
    }
}
