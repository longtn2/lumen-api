<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Http\Controllers\Controller;
use App\Models\DataChangeRequest;
use OmiCare\Http\Request;

class DataChangeRequestsController extends AuthenticatedController
{
    public function submitAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405];
        }

        $currentUser = auth();
        $data = $request->get('entry');

        $changeRequest = new DataChangeRequest();
        $changeRequest->branchId = $request->currentBranchId();
        $changeRequest->requestedBy = $currentUser->email;
        $changeRequest->requestType = $data['requestType'];
        $changeRequest->requestedAt = date('Y-m-d H:i:s');
        $changeRequest->note = $data['note'];
        $changeRequest->reason = $data['reason'];
        $changeRequest->applyData = $data['applyData'];

        DataChangeRequest::verifyWhiteList($data['applyData']);

        $changeRequest->save();

        return [
            'code' => 200,
            'message' => 'Đã gửi yêu cầu'
        ];
    }

    public function dataAction(Request  $request)
    {
        $page = $request->getInt('page');
        $type = $request->getInt('type');

        $paginate = DataChangeRequest::query()
            ->orderBy('id', 'desc')
            ->where('requestType', $type)
            ->paginate(5, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'entries' => $paginate->items(),
                'paginate' => [
                    'currentPage' => $paginate->currentPage(),
                    'lastPage' => $paginate->lastPage(),
                ],
            ]
        ];

    }

    public function applyAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return [
                'code' => 405
            ];
        }

        $action = $request->get('action');
        $entry = DataChangeRequest::find($request->getInt('id'));

        if (!$entry) {
            return [
                'code' => 404
            ];
        }

       /* if ($entry->status != DataChangeRequest::STATUS_NEW) {
            return [
                'code' => 1,
                'message' => 'Trạng thái không hợp lệ'
            ];
        }*/


        if ( $action === 'approve') {
            $entry->approve();

            return [
                'code' => 200,
                'message' => 'Đã duyệt',
            ];
        } else {
            $entry->reject();

            return [
                'code' => 5,
                'message' => 'Đã từ chối',
            ];
        }

    }
}
