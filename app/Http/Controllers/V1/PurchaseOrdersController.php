<?php

namespace App\Http\Controllers\V1;


use App\Models\Branch;
use App\Models\File;
use App\Models\Inventory;
use App\Models\OrderSupplier;
use App\Models\Product;
use App\Models\ProductBatch;
use App\Models\ProductUnit;
use App\Models\PurchaseOrder;
use App\Http\Controllers\AuthenticatedController;
use App\Models\PurchaseOrderDetail;
use App\Models\User;
use App\Services\Inventory\InventoryImportTransaction;
use App\Services\Inventory\InventorySupplierReturnTransaction;
use Illuminate\Database\Eloquent\Collection;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\Auth;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class PurchaseOrdersController extends AuthenticatedController
{

    /**
    * @uri /v1/purchase-orders/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $actionType = $req->get('action');
        $code = $req->get('code');
        $refCode = $req->get('ref_code');
        $isRef = false;

        /**
         * @var PurchaseOrder $entry
         * @var PurchaseOrderDetail $pod
         */
        if ($refCode) {
            $entry = PurchaseOrder::with(['supplier', 'branch'])->where('code', $refCode)->first();
            $isRef = true;
        } else {
            $entry = PurchaseOrder::with(['supplier', 'branch', 'selfRef'])->where('code', $code)->first();
            if ($entry) {
                $actionType = $entry->actionType;
                $entry->checkReadonlyState();
            }
        }


        if (!in_array($actionType, [PurchaseOrder::ACTION_RETURN, PurchaseOrder::ACTION_IMPORT])) {
            return [
                'code' => 1,
                'message' => 'Invalid action type'
            ];
        }

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        //$actionType = $entry->actionType;
        $purchaseOrderDetails = PurchaseOrderDetail::with(['productBatch'])
            ->where('purchaseOrderId', $entry->id)
            ->get();

        if ($actionType === PurchaseOrder::ACTION_RETURN) {

            if (!$isRef) {
                if ($entry->refCode) {
                    $refCode = $entry->refCode;
                }
            }


            $invProductBatchIds = $purchaseOrderDetails->pluck('productBatchId')->toArray();
            $inventoryMap = Inventory::findInventoryByRefCode(
                $req->currentBranchId(),
                $refCode,
                $invProductBatchIds
            );



            foreach ($purchaseOrderDetails as $p) {
                if (isset($inventoryMap[$p->productBatchId])) {
                    $e = $inventoryMap[$p->productBatchId];
                    $p->baseOnHand = $e->current;
                    $p->baseOnHandAll = $e->all;
                } else {
                    $p->baseOnHand = 0;
                    $p->baseOnHandAll = 0;
                }
            }

        }

        $batchGroups = [];
        $productUnitMap = [];
        $productIDs = $purchaseOrderDetails->pluck('productId')->toArray();
        $productUnitsMap = Product::with('productUnits')
            ->whereIn('id', $productIDs)
            ->get()->pluck('productUnits', 'id')->toArray();

        foreach ($purchaseOrderDetails as $pod) {
            $productBatch = $pod->productBatch;

            if ($productBatch) {
                $productBatch->name_formatted = $productBatch->getFormattedName();
            }

            $batchGroups[$pod->productUnitId][] = $pod;
            $productUnitMap[$pod->productUnitId] = $pod;
        }

        $details = [];
        $sumQuantity = 0;
        $totalProductCount = count($batchGroups);

        foreach ($batchGroups as  $productUnitId => $batches) {
            $p = $productUnitMap[$productUnitId];
            $totalQuantity = 0;

            $productUnits = $productUnitsMap[$p->productId] ?? [];
            $productUnitName = '';
            $currentConversionValue = 1;
            $currentSalePrice = 0;
            foreach ($productUnits as $pu) {
                if ($pu['id'] === $productUnitId) {
                    $productUnitName = $pu['unitName'];
                    $currentConversionValue = $pu['conversionValue'];
                    $currentSalePrice = $pu['price'];
                    break;
                }
            }

            foreach ($batches as $batch) {
                $batch->deleted = false;
                $batch->inventory = [
                    'onHand' => $batch->baseOnHand,
                    'onHandAll' => $batch->baseOnHandAll,
                ];
                $totalQuantity += $batch->quantity;
            }

            $sumQuantity += $totalQuantity;


            // Nếu tạo trả hàng nhập từ 1 phiếu nhập, thì bỏ hết các ID liên quan
            if ($isRef) {
                foreach ($batches as $batch) {
                    $batch->refId = $batch->id;
                    $batch->quantity = 0;
                    unset($batch->id, $batch->purchaseOrderId,$batch->inventoryId);
                }
            }

            $details[] = [
                'productId' => $p->productId,
                'inventoryPlanId' => $p->inventoryPlanId,
                'productUnitId' => $productUnitId,
                'productUnitName' => $productUnitName,
                'productUnits' => $productUnits,
                'quantity' => $totalQuantity,
                'productCode' => $p->productCode,
                'productName' => $p->productName,
                'currentConversionValue' => $currentConversionValue,
                'currentSalePrice' => $currentSalePrice,
                'totalQuantity' => $totalQuantity,
                'price' => $p->price,
                'productBatches' => $batches,
                'deleted' => false
            ];
        }

        if ($entry->actionType === 'return') {
            $title = 'Phiếu trả: ' . $entry->code;
        } else {
            $title = 'Phiếu nhập: ' . $entry->code;
        }

        if ($isRef) {
            $title = 'Tạo phiếu trả hàng nhập';
            $entry->refCode = $refCode;
            $entry->status = PurchaseOrder::STATUS_DRAFT;
            $entry->readonly = 0;
            $entry->actionType = PurchaseOrder::ACTION_RETURN;
            unset($entry->id);
            unset($entry->code);
        }


        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'title' => $title,
                'isRef' => $isRef,
                'details' => $details,
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields(),
                'info' => compact('sumQuantity', 'totalProductCount')
            ]
        ];
    }

    /**
     * @uri /v1/purchase_orders/remove
     * @return array
     */
    public function saveDescriptionAction(Request $req)
    {

        $id = $req->id;
        $entry = PurchaseOrder::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->description = $req->get('description');
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã lưu ghi chú'
        ];
    }

    /**
    * @uri /v1/purchase_orders/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = PurchaseOrder::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    public function cancelAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $id = $req->id;
        $entry = PurchaseOrder::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        if ($entry->actionType === PurchaseOrder::ACTION_IMPORT) {
            if ($entry->status !== PurchaseOrder::STATUS_IMPORTED) {
                return [
                    'code' => 3,
                    'message' => 'Phiếu nhập chưa hoàn thành'
                ];
            }

            $inventoryTransaction = new InventoryImportTransaction($entry->id);
        } else {
            if ($entry->status !== PurchaseOrder::STATUS_RETURNED) {
                return [
                    'code' => 3,
                    'message' => 'Phiếu trả chưa hoàn thành'
                ];
            }

            $inventoryTransaction = new InventorySupplierReturnTransaction($entry->id);
        }

        $entry->status = PurchaseOrder::STATUS_CANCELLED;
        DB::beginTransaction();
        $inventoryTransaction->rollback();
        $entry->save();
        DB::commit();

        return [
            'code' => 200,
            'message' => 'Hủy phiếu nhập thành công'
        ];

    }

    public function unlockAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $id = $req->id;
        $entry = PurchaseOrder::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->readonly = 0;
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Mở phiếu thành công'
        ];

    }

    public function excelDataAction(Request $req)
    {
        $id = $req->get('file_id');
        $file = File::where('userId', Auth::user()->id)->where('id', $id)->first();

        if (!$file) {
            return [
                'code' => 404
            ];
        }

        $excelData = $file->toExcelData();
        $count = count($excelData);
        $details = [];
        /**
         * @var Collection $productUnits
         */
        for ($i = 1; $i < $count; $i++) {
            $current = $excelData[$i];

            if (empty($current[0])) {
                continue;
            }

            $detail = new \stdClass();
            $productCode = $current[0];
            $unitName = $current[2] ?? '';
            $product = Product::query()->with('productUnits')->where('code', $productCode)->first();

            if (!$product) {
                return [
                    'code' => 2,
                    'message' => 'Không tồn tại sản phẩm với mã: ' . $productCode . '. Kiểm tra lại dòng ' . $i,
                ];
            }

            $productUnits = $product->productUnits;

            $found = $productUnits->filter(function ($pu) use($unitName) {
                return $pu->unitName === $unitName;
            });

            if ($found->count() === 0) {
                return [
                    'code' => 2,
                    'message' => 'Vui lòng chọn đơn vị cho sản phẩm: ' . $productCode,
                ];
            }

            $productUnit = $found->first();

            $detail->productId = $product->id;
            $detail->productCode = $productCode;
            $detail->productName = $product->name;
            $detail->productBatches = [];
            $detail->productUnitId = $productUnit->id;
            $detail->productUnitName = $productUnit->unitName;
            $detail->conversionValue = $productUnit->conversionValue;
            $detail->totalQuantity = 0;
            $detail->productUnits = $productUnits->toArray();
            $detail->deleted = false;

            $k = 7;
            $batchName = $current[$k] ?? null;

            while(!empty($batchName)) {
                $discount = $current[4] ?? 0;
                $price =  $current[3] ?? 0;
                $qty = $current[$k + 2] ?? 0;
                $expireDateRaw = $current[$k +1] ?? '';
                $d = \DateTime::createFromFormat('d/m/Y', $expireDateRaw);

                if (!$d) {
                    return [
                        'code' => 1,
                        'message' => 'Ngày hết hạn không hợp lệ tại dòng: ' . $i,

                    ];
                }

                $expireDate = $d->format('Y-m-d');

                if (isset($current[5])) {
                    $discount = getPriceByPercent($price, $current[5]);
                }

                $productBatch = ProductBatch::query()
                    ->where('productUnitId', $productUnit->id)
                    ->where('name', $batchName)
                    ->where('expireDate', $expireDate)
                    ->first();

                if (!$productBatch) {
                    $productBatch = new ProductBatch();
                    $productBatch->name = $batchName;
                    $productBatch->productId = $product->id;
                    $productBatch->productUnitId = $productUnit->id;
                    $productBatch->expireDate = $expireDate;
                }

                $productBatch->quantity = $qty;
                $productBatch->save();

                $batch = [
                    'batchName' => $batchName,
                    'expireDate' => $expireDate,
                    'productUnitId' => $productUnit->id,
                    'productName' => $product->name,
                    'productBatchId' => $productBatch->id,
                    'quantity' => $qty,
                    'price' => $price,
                    'discount' => $discount,
                    'deleted' => false
                ];

                $detail->totalQuantity += $qty;
                $k += 3;

                $batchName = $current[$k] ?? null;
                $detail->productBatches[] = $batch;

            }

            $details[] = $detail;


        }

        return [
            'code' => 200,
            'data' => $details,

        ];
    }

    /**
    * @uri /v1/purchase-orders/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');
        $actionType = $req->get('action_type');

        V::make($data)
            ->rule(V::required, 'supplierId')->message('Vui lòng chọn nhà cung cấp')
            ->rule(V::required, [
                'createdAt',
                'branchId',
            ])
            ->validOrFail();


        $purchaseOderDetails = $req->get('details');
        
        /**
        * @var PurchaseOrder $entry
        * @var OrderSupplier $orderSupplier
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = PurchaseOrder::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new PurchaseOrder();
        }


        $isReturnType = $actionType === PurchaseOrder::ACTION_RETURN;
        $entry->fill($data);
        $entry->actionType = $isReturnType ? PurchaseOrder::ACTION_RETURN : PurchaseOrder::ACTION_IMPORT;

        $branch = Branch::find($entry->branchId);

        if ($branch) {
            $entry->branchName = $branch->name;
        }

        $currentUserEmail = Auth::user()->email;

        $entry->updatedBy = $currentUserEmail;

        DB::beginTransaction();
        
        $entry->save();

        if (!$entry->code) {
            $entry->code = $isReturnType ? 'THN' . $entry->id :'PN' . $entry->id;
        }

        $entry->save();
        
        if ($entry->orderSupplierCode) {
            $orderSupplier = OrderSupplier::where('code', $entry->orderSupplierCode)->first();
            if ($orderSupplier) {
                $orderSupplier->status = OrderSupplier::STATUS_FINISHED;
                if (empty($orderSupplier->purchaseOrderCodes)) {
                    $orderSupplier->purchaseOrderCodes = $entry->code;
                } else {
                    $orderSupplier->purchaseOrderCodes .= ',' . $entry->code;
                }
                $orderSupplier->save();
            }
        }
        $deletedPurchaseOderDetailIDs = [];
        $deletedInventoryIDs = [];
        $totalQuantity = 0;
        $productUnitMap = [];        
        if(isset($purchaseOderDetails) && $purchaseOderDetails !== null ){
            foreach ($purchaseOderDetails as $detail) {
                if(isset($detail['productBatches']) && $detail['productBatches'] != null){
                    foreach ($detail['productBatches'] as $batch) {     
                        
                        $purchaseOderDetailId = $batch['id'] ?? null;
                        $inventoryId = $batch['inventoryId'] ?? null;
        
                        if (isset($batch['deleted']) && $batch['deleted'] != null) {
                            if ($purchaseOderDetailId) {
                                $deletedPurchaseOderDetailIDs[] = $purchaseOderDetailId;
                            }
        
                            if ($inventoryId) {
                                $deletedInventoryIDs[] = $inventoryId;
                            }        
                            continue;
                        }
        
                        $productUnitMap[$detail['productUnitId']] = true;
                        $productUnit = ProductUnit::find($detail['productUnitId']);
                        if (!$productUnit) {
                            return [
                                'code' => 3,
                                'message' => 'Không tìm thấy đơn vị'
                            ];
                        }
                        if (isset($batch['discount'])) $bDiscount = $batch['discount'];
                        else $bDiscount = $batch['discount'] = 0;
                        $purchaseOrderDetailData = [
                            'purchaseOrderId' => $entry->id,
                            'refId' => $batch['refId'] ?? null,
                            'inventoryId' => $inventoryId,
                            'productId' => $detail['productId'],
                            'productCode' => $detail['productCode'],
                            'inventoryPlanId' => $detail['inventoryPlanId'] ?? null,
                            'productName' => $detail['productName'],
                            'productUnitId' => $detail['productUnitId'],
                            'conversionValue' => $productUnit->conversionValue,
                            'quantity' => $batch['quantity'],
                            'price' => $batch['price'],
                            'discount' => $bDiscount,
                            'productBatchId' => $batch['productBatchId'],
                            'discountObject' => $batch['discountObject'] ?? null,
                        ];
                      
                        $totalQuantity += $batch['quantity'];
                       
                        if (!$purchaseOderDetailId) {
                          
                            $podEntry = new PurchaseOrderDetail();                           
                        } 
                        else {                            
                            $podEntry = PurchaseOrderDetail::find($purchaseOderDetailId);                          
                            if (!$podEntry) {
                                return [
                                    'code' => 1,
                                    'message' => 'Không tìm thấy phiếu nhập'
                                ];
                            }
                           
                        }        
        
                        $podEntry->fill($purchaseOrderDetailData);
                        $podEntry->save();
                    }
                }            
                
            }
        }
      
        

        if (count($deletedPurchaseOderDetailIDs) > 0) {
            PurchaseOrderDetail::query()->whereIn('id', $deletedPurchaseOderDetailIDs)->delete();
        }

        if (count($deletedInventoryIDs) > 0) {
            Inventory::query()->whereIn('id', $deletedInventoryIDs)->delete();
        }

        $entry->totalQuantity = $totalQuantity;
        $entry->totalProduct = count($productUnitMap);
        $importedRedirect = false;

        if ($totalQuantity === 0) {
            DB::rollBack();

            return [
                'code' => 3,
                'message' => 'Vui lòng chọn sản phẩm để nhập'
            ];
        }

        if ($entry->actionType === PurchaseOrder::ACTION_IMPORT) {
            if ($entry->status == PurchaseOrder::STATUS_IMPORTED) {
                $entry->readonly = 1;
                $inventoryImport = new InventoryImportTransaction( $entry->id );
                $inventoryImport->commit();
                $importedRedirect = '/purchase-orders/detail?code=' . $entry->code;
            }

        } else {
            if ($entry->status === PurchaseOrder::STATUS_RETURNED) {
                $inventoryImport = new InventorySupplierReturnTransaction(
                    $entry->id
                );
                $entry->readonly = 1;
                $inventoryImport->commit();
                $importedRedirect = '/purchase-orders/detail?code=' . $entry->code;
            }
        }

        if (!isValidEmail($entry->createdBy)) {
            return [
                'code' => 5,
                'message' => 'createdBy phải là email',
            ];
        }

        $entry->save();
        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/purchase-orders/form?code=' . $entry->code : false,
            'importedRedirect' => $importedRedirect,
            'data' => $entry->toArray()
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/purchase-orders/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();


        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $statusFilter = $req->getExplodedString('status');
        $createdByFilter = $req->getExplodedString('createdBy');
        $branchesFilter = $req->getExplodedString('branches');
        $actionType = $req->get('action');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');
        $currentBranchId = $req->currentBranchId();

        $userBranches = Branch::getUserBranches();
        $allowedBranchIds = array_column($userBranches->toArray(), 'id');
        $allowedBranchIdsMap = array_flip($allowedBranchIds);


        $query = PurchaseOrder::query()
            ->with(['supplier', 'branch'])
            ->where('actionType', $actionType)
            ->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->whereSearch('code', $req->keyword);
        }

        if (!empty($statusFilter)) {
            $query->whereIn('status', $statusFilter);
        }

        if (!empty($createdByFilter)) {
            $query->whereIn('createdBy', $createdByFilter);
        }

        $branchListFilter = [];
        if (!empty($branchesFilter)) {
            foreach ($branchesFilter as $bid) {
                if (isset($allowedBranchIdsMap[$bid])) {
                    $branchListFilter[] = $bid;
                }
            }
        } else {
            $branchListFilter = [$currentBranchId];
        }

        $query->whereIn('branchId', $branchListFilter);

        $query->createdIn($req->created);

        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);
        $createdByEmails = DB::select('SELECT createdBy FROM purchase_orders WHERE branchId=? GROUP BY createdBy', [$currentBranchId]);
        $createdUserList = [];
        foreach ($createdByEmails as $c) {
            if (isValidEmail($c->createdBy)) {
                $createdUserList[] = [
                    'id' => $c->createdBy,
                    'label' => $c->createdBy
                ];
            }

        }

        return [
            'code' => 200,
            'data' => [
                'title' => $actionType === 'return' ? 'Danh sách phiếu trả hàng nhập' : 'Danh sách phiếu nhập hàng',
                'fields' => $fields,
                'statusList' => PurchaseOrder::LIST_STATUS,
                'entries' => $entries->items(),
                'createdUserList' => $createdUserList,
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'code',
                'name' => 'Code',
            ],
            [
                'field' => 'createdAt',
                'name' => 'Thời gian',
                'render' => 'date'
            ],
            [
                'field' => 'supplier.name',
                'name' => 'Nhà cung cấp',
                'sort' => 'supplierId',
                'render' => 'dot'
            ],
            [
                'field' => 'totalQuantity',
                'name' => 'Tổng số lượng',
            ],
            [
                'field' => 'totalProduct',
                'name' => 'Số mặt hàng',
            ],
            [
                'field' => 'statusName',
                'name' => 'Trạng Thái',
            ]
        ];

        return $fields;
    }
}
