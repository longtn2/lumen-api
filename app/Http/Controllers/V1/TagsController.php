<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\CustomerTag;
use App\Models\ProductUnit;
use App\Models\ProductUnitTag;
use App\Models\Tag;
use App\Models\TagType;
use Cassandra\Custom;
use Illuminate\Support\Str;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class TagsController extends AuthenticatedController
{
    public function dataAction(Request  $request)
    {
        $page  = $request->get('page');
        $tagTypeId  = $request->getInt('tagTypeId');
        $numberRecord  = $request->getInt('numberRecord');
        $sortField  = $request->get('sort_field');
        $keyword  = $request->getTrimmed('keyword');
        $sortDirection  = $request->get('sort_direction') === 'asc' ? 'asc' : 'desc';
        $tagType = TagType::find($tagTypeId);

        $allowedSort = ['id', 'name', 'createdAt'];
        if (!in_array($sortField, $allowedSort)) {
            $sortField = 'id';
        }

        if (!$tagType) {
            return [
                'code'  => 404,
                'message' => 'Invalid Tag Type ID'
            ];
        }

        $query = Tag::query()->orderBy($sortField, $sortDirection)->where('tagTypeId', $tagTypeId);
        $fields = $this->getDisplayFields($tagType);

        if ($keyword) {
            $query->whereSearch(['name'], $keyword);
        }
        $totalRecord = $query->get();
        $entries = $query->paginate($numberRecord, ['*'], 'page', $page);

        if ($tagTypeId == 1) {
            Tag::attachItemCount($entries->items(), ProductUnitTag::class);
        } else if ($tagTypeId === 2) {
            Tag::attachItemCount($entries->items(), CustomerTag::class);
        }


        return [
            'code' => 200,
            'data' => [
                'entries' => $entries->items(),
                'fields' => $fields,
                'tagType' => $tagType,
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]
        ];
    }

    public function dataCustomerAction(Request  $request)
    {

        $fields = $this->getDisplayFields();

        $sortField = $request->get('sort_field', 'id');
        $sortDirection = $request->get('sort_direction', 'desc');
        $page  = $request->getInt('page');

        $query = Tag::query()->orderBy($sortField, $sortDirection)
            ->where('tagTypeId', 2);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    public function loadProductUnitsAction(Request  $request)
    {
        $id = $request->get('id');
        $productUnitIDs = ProductUnitTag::query()->where('tagId', $id)->get()->pluck('productUnitId')->toArray();
        $productUnits = ProductUnit::query()->with('product')->selectRaw('id, productId, unitName')->whereIn('id', $productUnitIDs)->get();
        $result = [];

        foreach ($productUnits as $pu) {
            $product = $pu->product;

            if ($product) {
                $result[] = [
                    'id' => $pu->id,
                    'name' => $pu->product->name . ' - ' . $pu->unitName . ' - ' . $product->code
                ];
            }

        }

        return [
            'code' => 200,
            'data' => $result
        ];
    }

    public function showAction(Request  $request)
    {
        $id = $request->get('id');
        $result = Tag::find($id);

        if (!$result) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy nhóm khách hàng'
            ];
        }

        return [
            'code' => 200,
            'data' => $result
        ];
    }

    public function removeAction(Request  $request)
    {
        $id = $request->get('id');
        $tag = Tag::find($id);

        if (!$tag) {
            return [
                'code' => 404,
                'message' => 'Không tìm thấy tag'
            ];
        }


        $tag->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    public function saveAction(Request  $request)
    {
        $data = $request->get('entry');

        V::make($data)
            ->rule(V::required, 'name')->message('Vui lòng nhập tên nhóm')
            ->validOrFail();

        if (isset($data['id'])) {
            $tag = Tag::find($data['id']);

            if (!$tag) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy nhóm'
                ];
            }
        } else {
            $tag = new Tag();
        }

        $tag->name = $data['name'];
        $tag->tagTypeId = $data['tagTypeId'];
        $tag->description = $data['description'];

        DB::beginTransaction();
        $tag->save();

        $bulkData = array_map(function($e) use($tag) {
            return [
                'productUnitId' => $e['id'],
                'tagId' => $tag->id,
            ];
        }, $data['productUnits']);

        DB::table('product_unit_tags')->where('tagId', $tag->id)->delete();
        DB::table('product_unit_tags')->insert($bulkData);

        DB::commit();


        return [
            'code' => 200,
            'message' => 'OK',
            '$data' =>$data,
        ];
    }

    public function saveEntryAction(Request  $request)
    {
        $data = $request->get('entry');

        V::make($data)
            ->rule(V::required, 'name')->message('Vui lòng nhập tên nhóm')
            ->validOrFail();

        $redirect = true;
        if (isset($data['id'])) {
            $tag = Tag::find($data['id']);

            if (!$tag) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy nhóm'
                ];
            }
            $redirect = false;
        } else {
            $tag = new Tag();

        }

        $tag->name = $data['name'];
        $tag->description = $data['description'] ?? '';
        $tag->tagTypeId = $data['tagTypeId'];
//        $tag->slug = isset($data['slug']) ? $data['slug'] : Str::slug($data['name']);
        $tag->save();

        $redirectLink = $redirect ? '/tags/form?tagTypeId=' . $tag->tagTypeId . '&id=' . $tag->id : null;

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirectLink ? $redirectLink : false,
            '$data' =>$data,
        ];
    }

    public function showTagTypeAction(Request  $request)
    {
        $tagTypeId = $request->get('tagTypeId');
        $tagType = TagType::find($tagTypeId);

        if (!$tagType) {
            return ['code' => 404];
        }


        return ['code' => 200, 'data' => $tagType];
    }

    private function getDisplayFields(TagType  $tagType): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
            ],
            [
                'field' => 'name',
                'name' => $tagType->name,
            ],
            [
                'field' => 'description',
                'name' => 'Mô tả',
            ],
            [
                'field' => 'createdAt',
                'name' => 'Ngày tạo',
                'render' => 'date'
            ],
        ];

        return array_chunk($fields, 10)[0];
    }
}
