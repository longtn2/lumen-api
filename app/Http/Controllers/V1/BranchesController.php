<?php

namespace App\Http\Controllers\V1;


use App\Models\Branch;
use App\Http\Controllers\AuthenticatedController;
use Illuminate\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;


class BranchesController extends AuthenticatedController
{

    /**
     * @uri /v1/branches/show
     * @return array
     */
    public function show(Request $req)
    {
        $id = $req->id;
        $entry = Branch::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $users = DB::select("SELECT  * FROM  users WHERE branchIDs LIKE ?", ['%' . $id . '%']);

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'users' => $users,
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
     * @uri /v1/branches/remove
     * @return array
     */
    public function remove(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Branch::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/branches/save
     * @return array
     */
    public function save(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::required, ['name', 'phone', 'address', 'provinceId', 'districtId', 'wardId'])
            ->validOrFail();


        /**
         * @var Branch $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Branch::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Branch();
        }

        $entry->fill($data);
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/branches/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/branches/data
     * @return array
     */
    public function data(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');
//        array_push($select, 'provinceId', 'districtId', 'wardId');

        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Branch::query()->with('province', 'district', 'ward')->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->where('name', 'LIKE', '%' . $req->keyword . '%');
        }

//        $query->createdIn($req->created);

        $entries = $query->paginate(25, ['*'], 'page', $page);
        foreach ($entries->items() as $br) {
            $br->address = $br->address. ', ' . ($br->ward ? $br->ward->name . ' - '  : '') . ($br->district ? $br->district->name . ' - ' : '') . ($br->province ? $br->province->name : '');
            $br->numberUser= $this->getUsersOfBranch($br->id);
        }

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/branches/getUsersOfBranch
     * @return array
     */
    public function getUsersOfBranch($branchId)
    {

       //$query = User::query()->where('branchIDs', 'LIKE', '%' . $branchId . '%')->get();

        $query = DB::select("SELECT  * FROM  users WHERE branchIDs LIKE ?", ['%' . $branchId . '%']);

        return count($query);
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
            ],
            [
                'field' => 'name',
                'name' => 'Tên chi nhánh',
            ],
            [
                'field' => 'phone',
                'name' => 'Số điện thoại',
            ],
//            [
//                'field' => 'email',
//                'name' => 'Email',
//            ],
            [
                'field' => 'address',
                'name' => 'Địa chỉ',
            ],
            [
                'field' => 'numberUser',
                'name' => 'Số lượng người dùng',
            ],
            [
                'field' => 'status',
                'name' => 'Trạng thái',
                'render' => 'statusActive'
            ],
        ];
        return $fields;
    }
}
