<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Invoice;
use App\Models\ReturnOrder;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class ReturnsController extends AuthenticatedController
{

    /**
    * @uri /v1/returns/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = ReturnOrder::with('returnDetails')->find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
    * @uri /v1/returns/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = ReturnOrder::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri /v1/returns/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::required, [])
            ->validOrFail();


        /**
        * @var ReturnOrder $entry
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = ReturnOrder::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new ReturnOrder();
        }

        $entry->fill($data);
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/returns/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/returns/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $keyword = $req->get('keyword');
        $branchIds = $req->get('branch_ids');
        $saleChannelIds = $req->get('sale_channel_ids');
        $returnDate = $req->get('return_date');
        $status = $req->get('status');
        $createdByIds = $req->get('created_by_ids');
        $soldByIds = $req->get('sold_by_ids');

        $query = ReturnOrder::query()->with('receiver')->select($select)->orderBy($sortField, $sortDirection);

        if ($keyword) {
            $query->where('code', 'LIKE', '%' . $keyword. '%')
                ->orWhere('branchName', 'LIKE', '%' . $keyword. '%')
                ->orWhere('soldByName', 'LIKE', '%' . $keyword. '%')
                ->orWhere('customerCode', 'LIKE', '%' . $keyword. '%');
        }

        if ($branchIds) {
            $query->whereIn('branchId', explode(',', $branchIds));
        }

        if ($saleChannelIds) {
            $query->whereIn('saleChannelId', explode(',', $saleChannelIds));
        }

        if ($status) {
            $query->where('status', $status);
        }

        if ($returnDate) {
            $query->columnIn('returnDate', $returnDate);
        }

        if ($createdByIds) {
            $query->whereIn('createdBy', explode(',', $createdByIds));
        }

        if ($soldByIds) {
            $query->whereIn('soldById', explode(',', $soldByIds));
        }

        $query->createdIn($req->created);

        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    public function saveStatusAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $id = $request->get('id');
        $action = $request->get('action');

        $returnOrder = ReturnOrder::find($id);
        if (!$returnOrder) {
            return [
                'code' => 404,
                'message' => 'Return order not found'
            ];
        }

        switch ($action) {
            case "complete":
                $returnOrder->status = ReturnOrder::STATUS_COMPLETE;
                break;
            case "cancel":
                $returnOrder->status = ReturnOrder::STATUS_CANCELED;
                break;
            default:
                break;
        }

        $returnOrder->save();

        return [
            'code' => 200,
            'message' => 'Thành công!'
        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
    [
        'field' => 'id',
        'name' => 'ID',
    ],
    [
        'field' => 'code',
        'name' => 'Mã trả hàng',
    ],
    [
        'field' => 'invoiceId',
        'name' => 'Mã hóa đơn',
    ],
    [
        'field' => 'returnDate',
        'name' => 'Ngày trả',
    ],
    [
        'field' => 'branchName',
        'name' => 'Chi nhánh',
    ],
    [
        'field' => 'receiverName',
        'name' => 'Người nhận trả',
    ],
    [
        'field' => 'soldByName',
        'name' => 'Người bán',
    ],
//    [
//        'field' => 'customerId',
//        'name' => 'CustomerId',
//    ],
//    [
//        'field' => 'customerCode',
//        'name' => 'Mã khách hàng',
//    ],
    [
        'field' => 'customerName',
        'name' => 'Tên khách hàng',
    ],
    [
        'field' => 'returnTotal',
        'name' => 'Cần trả khách',
        'render' => 'number',
    ],
//    [
//        'field' => 'returnDiscount',
//        'name' => 'ReturnDiscount',
//    ],
    [
        'field' => 'totalPayment',
        'name' => 'Đã trả khách',
        'render' => 'number',
    ],
//    [
//        'field' => 'returnFee',
//        'name' => 'ReturnFee',
//    ],
    [
        'field' => 'status',
        'name' => 'Trạng thái',
    ],
    [
        'field' => 'description',
        'name' => 'Description',
    ],
    [
        'field' => 'createdDate',
        'name' => 'CreatedDate',
    ],
    [
        'field' => 'returnDetails',
        'name' => 'ReturnDetails',
    ],
    [
        'field' => 'payments',
        'name' => 'Payments',
    ],
    [
        'field' => 'modifiedDate',
        'name' => 'ModifiedDate',
    ],
    [
        'field' => 'saleChannelId',
        'name' => 'SaleChannelId',
    ],
];
        return array_chunk($fields, 10)[0];
    }
}
