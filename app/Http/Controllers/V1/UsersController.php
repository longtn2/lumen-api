<?php

namespace App\Http\Controllers\V1;


use App\Models\Branch;
use App\Models\SysRolePermission;
use App\Models\SysUserPermission;
use App\Models\SysUserRole;
use App\Models\User;
use App\Http\Controllers\AuthenticatedController;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class UsersController extends AuthenticatedController
{

    /**
     * @uri /v1/users/show
     * @return array
     */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = User::find($id);
        $currentBranchId = $req->currentBranchId();
           
        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $userRoles = SysUserRole::query()->with('role')->where('userId', $id)->where('branchId', $currentBranchId)->get();     
       
        $roles = [];
        foreach ($userRoles as $ur) {
            $roles[] = $ur->roleId;
        }
        $entry->roleIDs = $roles;
        $branchIds = $entry->branchIDs ?? [];
        $arrUserPermission = [];
        foreach ($branchIds as $branchId) {
            $arrUserPermission[$branchId] = SysUserPermission::query()
                ->with('branch')
                ->where('userId', $id)
                ->where('branchId', $branchId)->get()
                ->pluck('permissionId');
        }
        $branchMaps = Branch::query()->whereIn('id', $branchIds)->get();
        $branchMapName = $branchMaps->pluck('name', 'id');
       
        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'arrUserPermission' => $arrUserPermission,
                'branchMapName' => $branchMapName,
                'branchMaps' => $branchMaps,
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
     * @uri /v1/users/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = User::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/users/save
     * @return array
     */
    public function saveAction(Request $req)
    {
       
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');   
               
        $currentBranchId = $req->currentBranchId();
       
        V::make($data)
        ->rule(V::required, 'name')->message('Vui lòng nhập Tên tài khoản')
        ->rule(V::required, 'email')->message('Vui lòng nhập đúng định dạng email')        
        ->rule(V::required, 'username')->message('Vui lòng nhập Tài khoản')            
        ->rule(V::required, 'phone')->message('Vui lòng nhập số điện thoại')
        ->rule(V::required, 'branchIDs')->message('Vui lòng chọn chi nhánh')
        ->rule(V::required, 'roleIDs')->message('Vui lòng chọn phân quyền')
        ->validOrFail();
       
        /**
         * @var User $entry
         */

        $redirect = true;
        if (isset($data['id'])) {
            $entry = User::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new User();
        }
        
        $entry->fill($data);
        if (!empty($data['password'])) {
            $entry->password = password_hash($data['password'], PASSWORD_DEFAULT);
        } else {
            unset($entry->password);
        }        
        $roleUserPermissionData = [];
        DB::beginTransaction();
        $entry->save();
        
        $roleIds = $data['roleIDs'];        
        $branchIds = $data['branchIDs'];
       
        foreach ($roleIds as $roleId) {
            foreach ($branchIds as $branchId) {
                $role = SysUserRole::where('userId', $entry->id)->where('roleId', $roleId)->where('branchId', $branchId)->first();
                if (!$role) {
                    $role = new SysUserRole();
                    $role->userId = $entry->id;
                    $role->roleId = $roleId;
                    $role->branchId = $branchId;                    
                    $role->save();
                }
                
                $pIds = SysRolePermission::query()->where('roleId', $roleId)->get()->pluck('permissionId');               
               
                foreach ($pIds as $pid) {
                    $roleUserPermissionData[] = [
                        'permissionId' => $pid,
                        'userId' => $entry->id,
                        'branchId' => $branchId
                    ];
                }
            }
        }
        
        SysUserPermission::query()->where('userId', $entry->id)->whereIn('branchId', $branchIds)->delete();        
        SysUserPermission::query()->insertOrIgnore($roleUserPermissionData);
        
        $unUseRoles = SysUserRole::where('userId', $entry->id)->whereIn('branchId', $branchIds)->whereNotIn('roleId', $data['roleIDs'])->get();
        if ($unUseRoles && count($unUseRoles) > 0) {
            foreach ($unUseRoles as $unUse) {
                $unUse->delete();
            }
        }
        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/users/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/users/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = User::query()->select($select)->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->where('name', 'LIKE', '%' . $req->keyword. '%')
            ->orWhere('email', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    public function onToggleStatusAction(Request $req)
    {
        $userId = $req->id;
        $query = User::query()->where('id', $userId)->first();
        if (!$query) {
            return [
                'code' => 500,
                'message' => "Không tìm thấy tài khoản"
            ];
        }
        $query->status = !$req->status;
        $query->save();

        return [
            'code' => 200,
            'message' => 'Cập nhật thành công'
        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
            ],
            [
                'field' => 'name',
                'name' => 'Tên',
            ],
            [
                'field' => 'email',
                'name' => 'Email',
            ],
//            [
//                'field' => 'remember_token',
//                'name' => 'Remember_token',
//            ],
//            [
//                'field' => 'created_at',
//                'name' => 'Ngày tạo',
//            ],
//            [
//                'field' => 'updated_at',
//                'name' => 'Updated_at',
//            ],
//            [
//                'field' => 'last_login',
//                'name' => 'Last_login',
//            ],
//            [
//                'field' => 'avatar',
//                'name' => 'Avatar',
//            ],
//            [
//                'field' => 'birthday',
//                'name' => 'Birthday',
//            ],
            [
                'field' => 'phone',
                'name' => 'Số điện thoại',
            ],
            [
                'field' => 'status',
                'name' => 'Trạng thái hoạt động',
                'render' => 'statusActive'
            ]
//            [
//                'field' => 'deleted_at',
//                'name' => 'Deleted_at',
//            ],
        ];
        return array_chunk($fields, 10)[0];
    }
}
