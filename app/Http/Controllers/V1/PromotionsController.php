<?php

namespace App\Http\Controllers\V1;


use App\Models\Promotion;
use App\Http\Controllers\AuthenticatedController;
use App\Services\Promotion\PromotionBootstrap;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class PromotionsController extends AuthenticatedController
{

    /**
    * @uri /v1/promotions/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Promotion::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->attachBranchIDs();

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    public function settingsAction()
    {
        $bootstrap = new PromotionBootstrap();

        return [
            'code' => 200,
            'data' => $bootstrap->getSettings()
        ];
    }

    /**
    * @uri /v1/promotions/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Promotion::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri /v1/promotions/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::required, 'name')->message('Vui lòng nhập tên chương trình')
            ->rule(V::required, 'code')->message('Vui lòng nhập mã khuyến mại')
            ->rule(V::unique, 'code', ['promotions', 'code', $data['code'] ?? null])->message('Mã khuyến mại đã tồn tại rồi')
            ->validOrFail();


        $branchIDs = $data['branchIDs'] ?? [];
        /**
        * @var Promotion $entry
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Promotion::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Promotion();
        }

        $entry->fill($data);

        DB::beginTransaction();
        $entry->save();
        $promotionBranches = [];
        foreach ($branchIDs as $branchID) {
            $promotionBranches[] = [
                'promotionId' => $entry->id,
                'branchId' => $branchID
            ];
        }

        DB::table('promotion_branches')->where('promotionId', $entry->id)->delete();
        DB::table('promotion_branches')->insert($promotionBranches);

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/promotions/form?id=' . $entry->id : false,
            'data' => $entry->toArray()
        ];
    }

    public function getProductUnitPromotions(Request $request)
    {
        $id = $request->get('id');
        $promotions = Promotion::getCurrentPromotions($request->currentBranchId());
        $map = [];
        foreach ($promotions as $promotion) {
            foreach ($promotion->appliedProductUnits as $pu) {
                $map[$pu->id][] = $promotion;
            }
        }

        return $map[$id] ?? [];
    }

    public function getInvoicePromotionsAction(Request $request) {
        $promotions = Promotion::getCurrentPromotions($request->currentBranchId(), 'IfInvoiceAmountFrom');


        return [
            'code' => 200,
            'data' => $promotions
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/promotions/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Promotion::query()->select($select)->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            //$query->where('title', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
    [
        'field' => 'id',
        'name' => 'ID',
    ],
    [
        'field' => 'code',
        'name' => 'Code',
    ],
    [
        'field' => 'name',
        'name' => 'Name',
    ],
    [
        'field' => 'status',
        'name' => 'Status',
    ],
    [
        'field' => 'note',
        'name' => 'Note',
    ],
];
        return array_chunk($fields, 10)[0];
    }
}
