<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\SysPermission as Permission;
use App\Models\SysRolePermission;
use Illuminate\Contracts\View\View;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;
use App\Models\SysRole as Role;
use phpDocumentor\Reflection\DocBlock\Description;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class RolesController extends AuthenticatedController
{

    public function assigns(Request $req)
    {
        if ($req->isMethod('POST')) {
            $table  = $req->get('table');
            $values  = $req->get('values');
            $entryId = $req->get('entry_id');
            if ($table === 'sys_user_permissions') {
                $field = 'userId';
            } else if ($table === 'sys_role_permissions') {
                $field = 'roleId';
            } else {
                return [
                    'code' => 404,
                    'message' => 'Invalid table'
                ];
            }

            DB::beginTransaction();
            DB::table($table)->where($field, $entryId)->delete();
            $bulkData = array_map(function($v) use($field, $entryId) {
                return [
                    $field => $entryId,
                    'permissionId' => $v
                ];
            }, $values);
            DB::table($table)->insert($bulkData);
            DB::commit();

            return [
                'code' => 200,
                'message' => 'Đã lưu'
            ];
        }

        $entryId = 0;
        $table = '';
        $field = '';

        if (isset($req->userId)) {
            $table = 'sys_user_permissions';
            $field = 'userId';
            $entryId = $req->userId;
        } elseif (isset($req->roleId)) {
            $table = 'sys_role_permissions';
            $field = 'roleId';
            $entryId = $req->roleId;
        }

        if (!$table) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy'
            ];
        }

        $permissionValues = DB::table($table)->where($field, $entryId)->get();
        $values = [];
        foreach ($permissionValues as $v) {
            $values[] = $v->permissionId;
        }

        $model = [
            'table' => $table,
            'entry_id' => $entryId,
            'values' => $values
        ];

        $tree = Permission::getTree();

        return [
            'code' => 200,
            'tree' => $tree,
            'model' => $model
        ];
    }


    /**
    * @uri  /xadmin/permissions/edit?id=$id
    * @return  array
    */
    public function showAction (Request $req) {
        $id = $req->id;
        $entry = Role::find($id);

        if (!$entry) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy bản ghi'
            ];
        }

        $rolePermissions = SysRolePermission::query()->where('roleId', $entry->id)->get()->pluck('permissionId');

        $entry->permissionIds = array_map('intval', $rolePermissions->toArray());

        return [
            'code' => 200,
            'entry' => $entry
        ];
    }

    /**
    * @uri  /xadmin/permissions/remove
    * @return  array
    */
    public function removeAction(Request $req) {
        $id = $req->id;
        $entry = Role::find($id);

        if (!$entry) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy bản ghi'
            ];
        }

        $entry->delete();

        return [
            'code' => 0,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri  /xadmin/permissions/save
    * @return  array
    */
    public function saveAction(Request $req) {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)->rule(V::required, [
            'name',
            'displayName',
        ])->validOrFail();

        /**
        * @var  Role $entry
        */
        if (isset($data['id'])) {
            $entry = Role::find($data['id']);
            if (!$entry) {
                return [
                    'code' => 3,
                    'message' => 'Không tìm thấy',
                ];
            }

            $rolePermissionData = [];



            $entry->fill($data);
            DB::beginTransaction();
            $entry->save();

            foreach ($data['permissionIds'] as $pid) {
                $rolePermissionData[] = [
                    'permissionId' => $pid,
                    'roleId' => $entry->id
                ];
            }

            SysRolePermission::query()->where('roleId', $entry->id)->delete();
            SysRolePermission::query()->insert($rolePermissionData);


            DB::commit();

            return [
                'code' => 0,
                'message' => 'Đã cập nhật',
                'redirect' => false,
                'id' => $entry->id
            ];
        } else {
            $entry = new Role();
            $entry->fill($data);
            $entry->save();

            return [
                'code' => 0,
                'message' => 'Đã thêm',
                'redirect' => '/roles/index',
                'id' => $entry->id
            ];
        }
    }

    /**
    * @param  Request $req
    */
    public function toggleStatus(Request $req)
    {
        $id = $req->get('id');
        $entry = Role::find($id);

        if (!$id) {
            return [
                'code' => 404,
                'message' => 'Not Found'
            ];
        }

        $entry->status = $req->status ? 1 : 0;
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã lưu'
        ];
    }

    /**
    * Ajax data for index page
    * @uri  v1/roles/data
    * @return  array
    */
    public function dataAction(Request $req) {
        $query = Role::query()->orderBy('id', 'desc');

        if ($req->keyword) {
            $query->where('name', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);


        $entries = $query->paginate();

        return [
            'code' => 0,
            'data' => $entries->items(),
            'paginate' => [
                'currentPage' => $entries->currentPage(),
                'lastPage' => $entries->lastPage(),
            ]
        ];
    }

    public static function getTreePermissionsAction()
    {
        $permissions = Permission::getTree();

        return [
            'tree' => $permissions,
            'items' => $permissions
        ];
    }

    public function export() {
                $keys = [
                            'name' => ['A', 'name'],
                            'class' => ['B', 'class'],
                            'action' => ['C', 'action'],
                            ];

        $query = Role::query()->orderBy('id', 'desc');

        $entries = $query->paginate();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        foreach ($keys as $key => $v) {
            if (is_string($v)) {
                $sheet->setCellValue($v . "1", $key);
            } elseif (is_array($v)) {
                list($c, $n) = $v;
                 $sheet->setCellValue($c . "1", $n);
            }
        }


        foreach ($entries as $index => $entry) {
            $idx = $index + 2;
            foreach ($keys as $key => $v) {
                if (is_string($v)) {
                    $sheet->setCellValue("$v$idx", data_get($entry->toArray(), $key));
                } elseif (is_array($v)) {
                    list($c, $n) = $v;
                    $sheet->setCellValue("$c$idx", data_get($entry->toArray(), $key));
                }
            }
        }
        $writer = new Xlsx($spreadsheet);
        // We'll be outputting an excel file
        header('Content-type: application/vnd.ms-excel');
        $filename = uniqid() . '-' . date('Y_m_d H_i') . ".xlsx";

        // It will be called file.xls
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        // Write file to the browser
        $writer->save('php://output');
        die;
    }
}
