<?php

namespace App\Http\Controllers\V1;


use App\Models\Unit;
use App\Http\Controllers\AuthenticatedController;
use OmiCare\Http\Request;
use OmiCare\Utils\V;

class UnitsController extends AuthenticatedController
{

    /**
    * @uri /v1/categories/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Unit::find($id);

        if (!$entry) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy bản ghi này'
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
    * @uri /v1/categories/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Unit::find($id);

        if (!$entry) {
            return [
                'code' => 3,
                'message' => 'Không tìm thấy bản ghi này'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri /v1/categories/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::required, ['name'])
            ->validOrFail();


        /**
        * @var Unit $entry
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Unit::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Unit();
        }

        $entry->fill($data);
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/units/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/categories/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Unit::query()->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            //$query->where('title', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
            ],
            [
                'field' => 'name',
                'name' => 'Tên đơn vị',
            ],
        ];
        return array_chunk($fields, 10)[0];
    }
}
