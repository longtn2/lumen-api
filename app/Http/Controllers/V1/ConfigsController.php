<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Cashflow;
use App\Models\Delivery;
use App\Models\ProductUnit;
use App\Models\SaleChannel;
use App\Models\ServiceType;
use App\Models\Transporter;
use App\Models\Unit;
use App\Models\User;
use App\Services\Permission\AppPermission;
use OmiCare\Http\Request;

class ConfigsController extends Controller
{
    public function indexAction(Request $request)
    {
        $currentBranchID = $request->header('X-Branch-Id');

        $listTransporter = Transporter::select(['id', 'name'])->orderBy('name')->get();
        $listServiceType = ServiceType::select(['id', 'name'])->where('isActive', 1)->orderBy('name')->get();
        $currentUser = auth();


        return [
            'code' => 200,
            'data' => [
                'units' => Unit::where('status',1)->get(),
                'branches' => Branch::get(),
                'branchAdmins' => User::getAdminByBranch($currentBranchID),
                'saleChannels' => SaleChannel::where('isActive', 1)->get(),
                'saleChannelDefaultId' => SaleChannel::getDefaultId(),
                'listWeightUnit' => Delivery::$listWeightUnit,
                'listPaymentMethod' => Cashflow::$listPaymentMethod,
                'listTransporter' => $listTransporter,
                'listServiceType' => $listServiceType,
                'userBranches' => Branch::getUserBranches(),
                'permissionInfo' => $currentUser ? AppPermission::getPermissionInfo($currentUser): null,
                'currentUser' => $currentUser ? [
                    'id' => $currentUser->id,
                    'name' => $currentUser->name
                ] : null
            ]
        ];
    }
}
