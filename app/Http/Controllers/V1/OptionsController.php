<?php

namespace App\Http\Controllers\V1;


use App\Models\Option;
use App\Http\Controllers\AuthenticatedController;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\V;

class OptionsController extends AuthenticatedController
{

    /**
    * @uri /v1/options/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Option::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }
        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
    * @uri /v1/options/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Option::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri /v1/options/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        /**
        * @var Option $entry
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Option::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Option();
        }

        $entry->fill($data);
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/options/index' : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * @uri /v1/options/save
     * @return array
     */
    public function saveAllAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $entries = $req->get('entries');

        /**
         * @var Option $entry
         */

        foreach ($entries as $data) {
            if (isset($data['id'])) {
                $entry = Option::find($data['id']);

                if (!$entry) {
                    return [
                        'code' => 404,
                        'message' => 'Không tìm thấy',
                    ];
                }

                $redirect = false;
            } else {
                $entry = new Option();
            }

            $entry->fill($data);
            $entry->save();
        }


        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'entries' => $entry->toArray()
        ];
    }
    /**
    * Ajax data for index page
    * @uri /v1/options/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Option::query()->select($select)
            ->where('type', 'settings')
            ->where('status', 1)
            ->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            //$query->where('title', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
    [
        'field' => 'id',
        'name' => 'ID',
    ],
    [
        'field' => 'name',
        'name' => 'Name',
    ],
    [
        'field' => 'value',
        'name' => 'Value',
    ],
    [
        'field' => 'values',
        'name' => 'Values',
    ],
    [
        'field' => 'status',
        'name' => 'Status',
    ],
    [
        'field' => 'type',
        'name' => 'Type',
    ],
    [
        'field' => 'typeValue',
        'name' => 'TypeValue',
    ],
    [
        'field' => 'description',
        'name' => 'Description',
    ],
];
        return array_chunk($fields, 10)[0];
    }
}
