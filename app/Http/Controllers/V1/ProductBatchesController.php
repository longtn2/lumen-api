<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Inventory;
use App\Models\ProductBatch;
use App\Models\ProductUnit;
use OmiCare\Http\Request;
use OmiCare\Utils\V;

class ProductBatchesController extends AuthenticatedController
{
    public function saveAction(Request  $request)
    {
        $data = $request->get('entry');

        V::make($data)
            ->rule(V::required, ['name', 'expireDate'])
            ->rule(V::min, 'quantity', 1)->message('Số lượng phải có tối thiểu 1')
            ->validOrFail();

        $entry = new ProductBatch();

        $entry->fill($data);
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã lưu',
            'data' => $entry->toArray()
        ];
    }

    public function inventoryDataAction(Request  $req)
    {
        $productId =  $req->getInt('productId');

        $currentBranchID = $req->header('X-Branch-Id');

        $items = Inventory::query()
            ->selectRaw('*,SUM(onHand*conversionValue) as qty')
            ->where('refTable', '!=', Inventory::REF_CODE_ORDER_DETAIL)
            ->where('productId', $productId)
            ->where('branchId', $currentBranchID)
            ->orderBy('onHand', 'desc')
            ->with('productBatch')
            ->groupBy('productBatchId')
            ->get();


        return [
            'code' => 200,
            'data' => [
                'inventories' => $items,
                'paginate' => [
                    'currentPage' => 1,
                    'lastPage' => 1
                ]
            ]
        ];
    }

    public function productBatchReportAction(Request $req) {

        $dateStr = $req->get('date');
        $start = '';
        $end = '';

        if ($dateStr) {
            $tmp = explode('_', $dateStr);

            if (count($tmp) >= 2) {
                list($start, $end) = $tmp;
                $start = date('Y-m-d', strtotime($start));
                $end = date('Y-m-d', strtotime($end));

            }
        } else {
            $start = date('Y-m-d');
            $end = date('Y-m-d', strtotime('+ 1 month'));
        }

        $query = ProductBatch::query();

        if ($req->type == ProductBatch::TYPE_OUT_OF_DATE) {
            $query->where('expireDate', '<' , $start);
        } else {
            $query->where('expireDate', '>=', $start)
                ->where('expireDate', '<=', $end);
        }


        $currentBranchID = $req->header('X-Branch-Id');
        $branchIds = [];

        if ($req->branch_ids) {
            $branchIds = explode(',', $req->branch_ids);
        } else {
            $branchIds[] = $currentBranchID;
        }

        $batchIds = $query->get()->pluck('id');

        $items = Inventory::query()
            ->selectRaw('*,SUM(onHand*conversionValue) as qty')

            ->whereIn('branchId', $branchIds)
            ->whereIn('productBatchId', $batchIds)
            ->orderBy('onHand', 'desc')
            ->with(['productUnit:id,unitName', 'product:id,code,name', 'productBatch'])
            ->groupBy('productBatchId')
            ->having('qty', '>', 0)
            ->get();

        foreach ($items as $i) {
            if ($i->productBatch) {
                $endDate = strtotime($i->productBatch->expireDate);
                $days = floor(($endDate - strtotime(date('Y-m-d')))/86400);
                $i->productBatch->daysToOutOfDate = $days;
            }
        }

        return [
            'code' => 200,
            'data' => $items
        ];
    }
}
