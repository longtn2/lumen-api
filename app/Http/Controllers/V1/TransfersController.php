<?php

namespace App\Http\Controllers\V1;


use App\Models\Inventory;
use App\Models\Product;
use App\Models\ProductBatch;
use App\Models\ProductUnit;
use App\Models\Transfer;
use App\Http\Controllers\AuthenticatedController;
use App\Models\TransferDetail;

use App\Services\Inventory\InventoryTransferTransaction;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class TransfersController extends AuthenticatedController
{

    /**
    * @uri /v1/transfers/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Transfer::with(['transferDetail' => function($q){
            return $q->with('productUnits');
        },'fromBranch', 'toBranch', 'createdByUser','receivedByUser'])->find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }
        $transferDetails = TransferDetail::with('productBatch')->where('transferId', $entry->id)->get();
        $batchGroups = [];
        $productUnitMap = [];

        $productIds = $transferDetails->pluck('productId')->toArray();

        $productUnitMap = Product::with('productUnits')
            ->whereIn('id', $productIds)
            ->get()->pluck('productUnits', 'id')->toArray();

        foreach ($transferDetails as $td) {
            $batchGroups[$td->productUnitId][] = $td;
            $productUnitMap[$td->productUnitId] = $td;
        }

        $details = [];
        $sumQuantity = 0;
        $totalProductCount = count($batchGroups);
        foreach ($batchGroups as  $productUnitId => $batches) {
            $p = $productUnitMap[$productUnitId];
            $totalQuantity = 0;

            foreach ($batches as $batch) {
                $batch->deleted = false;
                $totalQuantity += $batch->quantity;
            }

            $sumQuantity += $totalQuantity;

            $productUnits = $productUnitMap[$p->productId] ?? [];
            $productUnitName = '';
            $productUnit = [];
            $conversionValue = 1;
            foreach ($productUnits as $pu) {
                if ($pu['id'] === $productUnitId) {
                    $productUnit = $pu;
                    $productUnitName = $pu['unitName'];
                    $conversionValue = $pu['conversionValue'];
                    break;
                }
            }

            $details[] = [
                'productId' => $p->productId,
                'productUnitId' => $productUnitId,
                'productUnitName' => $productUnitName,
                'productUnit' => $productUnit,
                'conversionValue' => $conversionValue,
                'productUnits' => $productUnits,
                'productCode' => $p->productCode,
                'productName' => $p->productName,
                'totalQuantity' => $totalQuantity,
                'price' => $p->price,
                'productBatches' => $batches,
                'deleted' => false
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields(),
                'details' => $details,
                'info' => compact('sumQuantity', 'totalProductCount')
            ]
        ];
    }

    /**
    * @uri /v1/transfers/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Transfer::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri /v1/transfers/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');
        $details = $req->get('details');
        $currentId = $req->get('currentId');
        $redirectToNewTransfer = null;

        V::make($data)
            ->rule(V::required, 'toBranchId')->message('Vui lòng chọn chi nhánh nhận')
            ->validOrFail();

        if (isset($details) && count($details) === 0) {
            return [
                'code' => 2,
                'message' => 'Vui lòng chọn sản phẩm trước khi lưu'
            ];
        }


        /**
        * @var Transfer $entry
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Transfer::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy phiếu chuyển hàng',
                ];
            }
            if ($data['toBranchId'] !== $entry->toBranchId && $entry->type === 'transfer') {
                $invs = Inventory::where('refTable', 'transfer')
                    ->where('refId', $entry->id)
                    ->where('branchId', $entry->toBranchId)
                    ->get();

                if (count($invs) > 0) {
                    foreach ($invs as $inv) {
                        $inv->branchId = $data['toBranchId'];
                        $inv->save();
                    }
                }
            }

            $redirect = false;
        } else {
            $entry = new Transfer();
            $entry->fromBranchId = $currentId;
            $entry->createDate = date('Y-m-d H:i:s');
        }

        $entry->fill($data);
        if ($entry->type == 'transfer' && $entry->status == Transfer::STATUS_SUCCEED && !$data['receivedDate']) {
            $entry->receivedDate = date('Y-m-d H:i:s');
        }

        if (!isset($data['createdBy'])) {
            $entry->createdBy = auth()->id;
        }
        DB::beginTransaction();
        $entry->save();

        if (!isset($entry->code)) {
            if ($entry->type === 'request') {
                $entry->code = 'XH'.$entry->id;
            } else {
                $entry->code = 'CH'.$entry->id;
            }

            $entry->save();
        }

        $totalSend = 0;
        $totalReceive = 0;

        if (isset($details) && count($details) > 0) {
            $entry->totalProduct = count($details);
            foreach ($details as $detail) {

                if (!empty($detail['deleted'])) {
                    if (isset($detail['productBatches']) && count($detail['productBatches']) > 0) {
                        foreach ($detail['productBatches'] as $pb) {
                            if (isset($pb['id'])) {
                                TransferDetail::where('id', $pb['id'])->delete();
                            }
                        }

                    }
                    if ($entry->type === 'request') {
                        TransferDetail::where('transferId', $entry->id)
                            ->where('productUnitId', $detail['productUnitId'])->delete();
                    }
                } else {
                    if (isset($detail['productBatches']) && count($detail['productBatches']) > 0) {
                        foreach ($detail['productBatches'] as $pb) {
                            if ($pb['deleted'] && isset($pb['id'])) {
                                TransferDetail::where('id', $pb['id'])->delete();
                            } else if (!$pb['deleted']){
                                if (isset($pb['id'])) {
                                    $product = TransferDetail::find($pb['id']);
                                    if (!$product) {
                                        return [
                                            'code' => 3,
                                            'message' => 'Không tìm thấy bản ghi'
                                        ];
                                    }
                                } else {
                                    $product = new TransferDetail();
                                    $product->transferId = $entry->id;
                                }

                                $product->productId = $detail['productId'];
                                $product->productName = $detail['productName'];
                                $product->productCode = $detail['productCode'];
                                $product->productBatchId = $pb['productBatchId'];
                                $product->productUnitId = $pb['productUnitId'] ?? '';
                                $product->sendQuantity = @$pb['sendQuantity'];
                                $product->receiveQuantity = $pb['receiveQuantity'] ?? 0;
                                $product->price = $pb['price'] ?? 0;
                                $product->sendPrice = $pb['sendPrice'] ?? 0;
                                $product->receivePrice = $pb['receivePrice'] ?? 0;
                                $product->conversionValue = @$pb['conversionValue'];
                                $product->save();

                                $totalSend += intVal($product->sendQuantity);
                                $totalReceive += intval($product->receiveQuantity);
                            }

                        }
                    }
                    if ($entry->type === 'request') {
                        $product = TransferDetail::where('transferId', $entry->id)
                        ->where('productUnitId', $detail['productUnitId'])->first();
                        if (!$product) {
                            $product = new  TransferDetail();
                            $product->transferId = $entry->id;
                            $product->productId = $detail['productId'];
                            $product->productName = $detail['productName'];
                            $product->productCode = $detail['productCode'];
                            $product->productUnitId = $detail['productUnitId'] ?? '';
                        }

                        $product->sendQuantity = $detail['sendQuantity'];
                        $product->receiveQuantity = $detail['receiveQuantity'] ?? 0;
                        $product->conversionValue = $detail['conversionValue'];
                        $product->save();

                        $totalSend += intval($product->sendQuantity);
                        $totalReceive += intval($product->receiveQuantity);

                    }
                }
            }
        }

        if ($entry->type === 'transfer' && $entry->status !== Transfer::STATUS_DRAFT) {
            $inventoryTransfer = new InventoryTransferTransaction(
                $entry->id
            );
            if ($entry->status === Transfer::STATUS_CANCELED) {
                $inventoryTransfer->rollback();
            } else {
                $inventoryTransfer->commit();
            }

        }

        if ($entry->type === 'request' && $entry->status === Transfer::STATUS_SUCCEED) {
            if (!$entry->disabled) {
                $newTransfer = new Transfer();
                $newTransfer->fromBranchId = $entry->toBranchId;
                $newTransfer->toBranchId = $entry->fromBranchId;
                $newTransfer->status = Transfer::STATUS_DRAFT;
                $newTransfer->type = 'transfer';
                $newTransfer->refCode = $entry->code;
                $newTransfer->createdBy = auth()->id;
                $newTransfer->createDate = date('Y-m-d H:i:s');
                $newTransfer->save();

                $newTransfer->code = 'CH' . $newTransfer->id;

                $totalSendNew = 0;
                $totalReceiveNew = 0;

                if (isset($details) && count($details) > 0) {
                    foreach ($details as $detail) {
                        if (!$detail['deleted']) {
                            $product = new TransferDetail();
                            $product->transferId = $newTransfer->id;
                            $product->productId = $detail['productId'];
                            $product->productName = $detail['productName'];
                            $product->productCode = $detail['productCode'];
                            $product->productUnitId = $detail['productUnitId'] ?? '';
                            $product->conversionValue = $detail['conversionValue'];
                            $product->sendQuantity = $detail['sendQuantity'];
                            $product->receiveQuantity = 0;
                            $product->save();

                            $totalSendNew += intval($product->sendQuantity);
                            $totalReceiveNew += intval($product->receiveQuantity);
                        }

                    }
                }
                $newTransfer->totalSendQuantity = $totalSendNew;
                $newTransfer->totalReceiveQuantity = $totalReceiveNew;

                $newTransfer->save();

                $redirectToNewTransfer = '/transfers/form?id='. $newTransfer->id;
                $entry->disabled = true;
                $entry->receivedBy = auth()->id;

            }

        }
        $entry->totalSendQuantity = $data['totalSendQuantity'];
        $entry->totalReceiveQuantity = $totalReceive;

        $entry->save();

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/transfers/index' : ($redirectToNewTransfer ? $redirectToNewTransfer : '/transfers/detail?id='.$entry->id),
            'data' => $entry->toArray()
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/transfers/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');
        $currentBranchID = $req->header('X-Branch-Id');

        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $type = $req->get('typeRecord');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Transfer::query()->with('fromBranch', 'toBranch')
            ->where(function ($q) use ($currentBranchID) {
                $q->where('fromBranchId', $currentBranchID)
                    ->orWhere(function ($que) use ($currentBranchID){
                        $que->where('toBranchId', '=', $currentBranchID)
                            ->where('status', '!=', 1);
                    });
            })->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->where('code', 'LIKE', '%' . $req->keyword. '%')
                ->orWhere('description', 'LIKE', '%' . $req->keyword. '%');
        }
        if (isset($req->status)) {
            $query->where('status', $req->status);
        }

        if ($type) {
            $query->where('type', $type);
        }

        if ($req->fromBranchId) {
            $query->where('fromBranchId', $req->fromBranchId);
        }
        if ($req->toBranchId) {
            $query->where('toBranchId', $req->toBranchId);
        }
        if (isset($req->statusReceive)) {
            if ($req->statusReceive == 1) {
                $query->whereColumn('totalReceiveQuantity', '=', 'totalSendQuantity');
            } else if ($req->statusReceive == 2){
                $query->whereColumn('totalReceiveQuantity', '!=', 'totalSendQuantity');
            }

        }

        $query->createdIn($req->created);

        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        foreach ($entries->items() as $entry) {
            $fromBranch = $entry->fromBranch;
            $toBranch = $entry->toBranch;
            $entry->fromBranchName = $fromBranch ? $fromBranch->name : '';
            $entry->toBranchName = $toBranch ? $toBranch->name : '';
            $entry->typeName = $entry->type === 'request' ? 'Xin hàng' : 'Chuyển hàng';
            $entry->statusName = $entry->type === 'request' ? Transfer::STATUS_NAME_REQUEST_MAP[$entry->status] : (Transfer::STATUS_NAME_TRANSFER_MAP[$entry->status] ?? '');

    }

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    public function saveStatusAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $id = $request->get('id');
        $action = $request->get('action');

        $transfer = Transfer::find($id);
        if (!$transfer) {
            return [
                'code' => 404,
                'message' => 'Transfer not found'
            ];
        }

        switch ($action) {
            case "complete":
                $transfer->status = Transfer::STATUS_SUCCEED;
                $transfer->receivedDate = date('Y-m-d H:i:s');
                break;
            case "cancel":
                $transfer->status = Transfer::STATUS_CANCELED;
                break;
            default:
                break;
        }

        if ($transfer->type === 'transfer' && $transfer->status === Transfer::STATUS_CANCELED) {
            $inventoryTransfer = new InventoryTransferTransaction(
                $transfer->id
            );

            $inventoryTransfer->rollback();
        }

        $transfer->save();

        return [
            'code' => 200,
            'message' => 'Thành công!'
        ];
    }

    public function exportExcelAction(Request $req) {

        $branch_ids = $req->get('branch_ids');

        $query = Transfer::query()->with('fromBranch', 'toBranch')
            ->with(['transferDetail' => function ($q){
                $q->with(['productBatch', 'productUnit:id,unitName']);
        }])
            ->orderBy('id', 'desc');

        if ($branch_ids) {
            $branchIds = $branch_ids ? explode(',', $branch_ids) : [];
            $query->whereIn('fromBranchId', $branchIds)
            ->orWhereIn('toBranchId', $branchIds);
        }

        $entries = $query->createdIn($req->date)->get();

        $data = [];
        foreach ($entries as $entry) {
            $fromBranch = $entry->fromBranch;
            $toBranch = $entry->toBranch;
            $createdBy = $entry->createdByUser;
            $receivedBy = $entry->receivedByUser;
            $entry->fromBranchName = $fromBranch ? $fromBranch->name : '';
            $entry->toBranchName = $toBranch ? $toBranch->name : '';
            $entry->createdByName = $createdBy ? $createdBy->name : '';
            $entry->receivedByName = $receivedBy ? $receivedBy->name : '';
            $entry->typeName = $entry->type === 'request' ? 'Xin hàng' : 'Chuyển hàng';
            $entry->statusName = $entry->type === 'request' ? Transfer::STATUS_NAME_REQUEST_MAP[$entry->status] : (Transfer::STATUS_NAME_TRANSFER_MAP[$entry->status] ?? '');

            if ($entry->transferDetail) {
                $totalPrice = $this->calTotalPriceAction($entry->transferDetail);
                foreach ($entry->transferDetail as $detail) {
                    $detail->code = $entry->code;
                    $detail->fromBranchName = $entry->fromBranchName;
                    $detail->toBranchName = $entry->toBranchName;
                    $detail->typeName = $entry->typeName;
                    $detail->statusName = $entry->statusName;
                    $detail->totalSendQuantity = $entry->totalSendQuantity;
                    $detail->totalReceiveQuantity = $entry->totalReceiveQuantity;
                    $detail->totalSendPrice = $totalPrice['totalSendPrice'];
                    $detail->totalReceivePrice = $totalPrice['totalReceivePrice'];
                    $detail->totalProduct = $entry->totalProduct;
                    $detail->createdDate = $entry->createDate;
                    $detail->receivedDate = $entry->receivedDate;
                    $detail->createdByName = $entry->createdByName;
                    $detail->receivedByName = $entry->receivedByName;
                    $detail->batchName = $detail->productBatch ? $detail->productBatch->name : '';
                    $detail->batchDate = $detail->productBatch ? $detail->productBatch->expireDate : '';
                    $detail->productUnitName = $detail->productUnit ? $detail->productUnit->unitName : '';
                    $detail->sendDescription = $entry->sendDescription;
                    $detail->receiveDescription = $entry->receiveDescription;

                    $data[] = $detail;

                }
            }


        }

        return [
            'code' => 200,
            'data' => $data

        ];
    }

    public function calTotalPriceAction($details) {

        $totalSendPrice = 0;
        $totalReceivePrice = 0;

        foreach ($details as $detail) {
            $totalSendPrice += $detail->sendPrice;
            $totalReceivePrice += $detail->receivePrice;
        }

        return [

            'totalSendPrice' => $totalSendPrice,
            'totalReceivePrice' => $totalReceivePrice

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
//    [
//        'field' => 'id',
//        'name' => 'ID',
//        'isColumn' => true
//    ],
    [
        'field' => 'code',
        'name' => 'Mã chuyển hàng',
        'isColumn' => true
    ],

    [
        'field' => 'typeName',
        'name' => 'Loại phiếu',
    ],
//    [
//        'field' => 'description',
//        'name' => 'Description',
//    ],
    [
        'field' => 'fromBranchName',
        'name' => 'Chi nhánh chuyển',

    ],
    [
        'field' => 'toBranchName',
        'name' => 'Chi nhánh nhận',
    ],
    [
        'field' => 'createdAt',
        'name' => 'Ngày chuyển',
        'render' => 'date',
        'isColumn' => true
    ],
    [
        'field' => 'receivedDate',
        'name' => 'Ngày nhận',
        'render' => 'date',
        'isColumn' => true
    ],
    [
        'field' => 'totalSendQuantity',
        'name' => 'Số lượng chuyển',
        'isColumn' => true
    ],
    [
        'field' => 'totalReceiveQuantity',
        'name' => 'Số lượng nhận',
        'isColumn' => true
    ],
//    [
//        'field' => 'retailerId',
//        'name' => 'Tên cửa hàng',
//    ],
    [
        'field' => 'statusName',
        'name' => 'Trạng thái',
    ],
];
        return array_chunk($fields, 10)[0];
    }

    public function addProductAction(Request $req) {

        $data = $req->get('entry');
        $transferId = null;

        if (!isset($data['id'])) {
            $transfer = new Transfer();
            $transfer->fill($data);
            $transfer->status = Transfer::STATUS_DRAFT;
            $transfer->save();
            $transferId = $transfer->id;
        } else {
            $transferId = $data['id'];
        }
        $productId = $req->get('product_id');
        $exists = TransferDetail::where('transferId', $transferId)
            ->where('productId', $productId)
            ->first();

        if ($exists) {
            return [
                'code' => 1,
                'message' => 'Sản phẩm đã tồn tại',
            ];
        }
        $product = Product::find($productId);
        if (!$product) {
            return [
                'code' => 1,
                'message' => 'Không tìm thấy sản phẩm này',
            ];
        }

        $transferDetail = new TransferDetail();
        $transferDetail->productId = $productId;
        $transferDetail->transferId = $transferId;
        $transferDetail->productCode = $product->code;
        $transferDetail->productName = $product->fullName;
        $transferDetail->price = $product->basePrice;
        $transferDetail->sendQuantity = 0;
        $transferDetail->save();

        $transferDetails = TransferDetail::query()->with('productUnits')->where('transferId', $transferId)
            ->orderBy('createdAt', 'desc')->get();

        return [
            'code' => 200,
            'transferDetails' => $transferDetails,
            'message' => 'Đã thêm sản phẩm '
        ];
    }

    /**
     * @uri /v1/transfers/loadBatch
     * @return array
     */
    public function loadBatchAction(Request $req)
    {
        $productId = $req->get('productId');
        $productUnitId = $req->get('productUnitId');
        $batches = ProductBatch::where('productId', $productId)->where('productUnitId', $productUnitId)->get();

        return [
            'code' => 200,
            'message' => 'OK',
            'data' =>  $batches->toArray()
        ];
    }

    public function getQuantityFromBranchAction(Request $req) {
        $productId = $req->get('productId');
        $branchId = $req->get('branchId');

        $quantity = DB::selectOne("SELECT SUM(onHand*conversionValue) AS qty FROM inventories WHERE branchId=? AND productId=? AND refTable!=?",
            [$branchId, $productId, Inventory::REF_CODE_ORDER_DETAIL]);

        $totalQty = $quantity->qty ?? 0;

        $inventories = Inventory::query()->where('productId', $productId)
            ->where('branchId', $branchId)
            ->where('refTable', '!=', Inventory::REF_CODE_ORDER_DETAIL)
            ->selectRaw('id, productId, productBatchId, SUM(onHand*conversionValue) as quantity')
            ->groupBy('productBatchId')
            ->get()
            ->pluck('quantity', 'productBatchId');

        return [
            'totalQty' => $totalQty,
            'inventories' => $inventories
        ];


    }
}
