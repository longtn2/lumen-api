<?php

namespace App\Http\Controllers\V1;


use App\Models\Supplier;
use App\Http\Controllers\AuthenticatedController;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\V;

class SuppliersController extends AuthenticatedController
{

    /**
     * @uri /v1/suppliers/show
     * @return array
     */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Supplier::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields2()
            ]
        ];
    }

    /**
     * @uri /v1/suppliers/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Supplier::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/suppliers/save
     * @return array
     */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::email, [
                'email'
            ])
            ->rule(V::regex, 'contactNumber', '/^\d{10}$/')->message('Số điện thoại không hợp lệ!')
            ->rule(V::required, [
                'name',
                'code',
                'contactNumber',
                'taxCode',
                'address',
                'email'
            ])
            ->validOrFail();


        /**
         * @var Supplier $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Supplier::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $entry->fill($data);
            $entry->save();
            $redirect = false;
        } else {
            $entry = new Supplier();
            $entry->fill($data);
            $entry->save();
            $redirect = '/suppliers/form?id=' . $entry->id;
        }


        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? $redirect : false,
            'data' => $entry->toArray()
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/suppliers/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
//        $select = array_column($fields, 'field');
        $select = ['id', 'name', 'code', 'contactNumber'];
        $status = $req->get('status', '');
        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Supplier::query()->select($select)->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->where('name', 'LIKE', '%' . $req->keyword. '%');
        }

        if ($status != '') {
            $query->where('status', $status);
        }

//        $query->createdIn($req->created);

        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'code',
                'name' => 'Mã nhà cung cấp',
            ],
            [
                'field' => 'name',
                'name' => 'Tên nhà cung cấp',
            ],
            [
                'field' => 'contactNumber',
                'name' => 'Điện thoại',
            ],
        ];
        return array_chunk($fields, 10)[0];
    }

    private function getDisplayFields2(): array
    {
        $fields = [
            [
                'field' => 'code',
                'name' => 'Mã nhà cung cấp',
            ],
            [
                'field' => 'name',
                'name' => 'Tên nhà cung cấp',
            ],
            [
                'field' => 'contactNumber',
                'name' => 'Điện thoại',
            ],
            [
                'field' => 'address',
                'name' => 'Địa chỉ',
            ],
            [
                'field' => 'email',
                'name' => 'Email',
            ],
            [
                'field' => 'taxCode',
                'name' => 'Mã số thuế',
            ],
            [
                'field' => 'createdName',
                'name' => 'Người tạo',
            ],
            [
                'field' => 'createdDate',
                'name' => 'Ngày tạo',
            ],
        ];
        return array_chunk($fields, 10)[0];
    }
}
