<?php
namespace App\Http\Controllers\V1;

use App\Http\Controllers\AuthenticatedController;
use App\Models\Product;
use App\Models\SyncSaleChannels;
use OmiCare\Utils\V;
use App\Models\Branch;
use App\Models\Customer;
use App\Models\Order;
use App\Models\SaleChannel;
use App\Models\OmiSyncLink;
use OmiCare\Http\Request;
use App\Models\Delivery;
use App\Models\OrderDetail;
use OmiCare\Utils\DB;
use App\Utils\LazadaHelper;
use Lazada\LazopClient;
use Lazada\LazopRequest;
use Lazada\UrlConstants;
use App\Models\SyncProduct;
use App\Models\SyncProductError;
use Date;
use OmiCare\Utils\EventEmitter;

class LazadaController extends AuthenticatedController
{
    public $apiKey;
    public $apiSecret;

    public function __construct()
    {
        $this->apiKey = appenv('APP_KEY_LAZADA');
        $this->apiSecret = appenv('APP_SECRET_LAZADA');
    }
    // use lazada sdk 
    // lấy mã access token
    public function getAccessTokenAction(Request $req)
    {
        $lazadaHelper = new LazadaHelper();
        $lazadaUrl = UrlConstants::$api_authorization_url;
        $apiName = '/auth/token/create';
        $method = 'GET'; 
        return $lazadaHelper->getGenerate($lazadaUrl, $apiName, $method, $req->all());
    }
    // lấy mã refresh access token
    public function getRefreshAccessTokenAction(Request $req)
    {
        $lazadaHelper = new LazadaHelper();
        $lazadaUrl = UrlConstants::$api_authorization_url;
        $apiName = '/auth/token/refresh';
        $method = 'GET'; 
        return $lazadaHelper->getGenerate($lazadaUrl, $apiName, $method, $req->all());
    }
    // lấy thông tin người bán
    public function getSellerAction(Request $req)
    {
        $lazadaHelper = new LazadaHelper();
        $lazadaUrl = $lazadaHelper->getGenerateApiUrl($req->country);
        $apiName = '/seller/get';
        $method = 'GET';
        return $lazadaHelper->getGenerate($lazadaUrl, $apiName, $method, $req->all());
    }
    // lấy thông tin nhiều đơn hàng trên sàn
//    public function getOrdersAction(Request $req)
//    {
//        $lazadaUrl = $this->getGenerateApiUrl($req->country);
//        $apiName = '/orders/get';
//        $method = 'GET';
//        return $this->getGenerate($lazadaUrl, $apiName, $method, $req->all());
//    }
    // lấy thông tin 1 đơn hàng
//    public function getOrderAction(Request $req)
//    {
//        $lazadaUrl = $this->getGenerateApiUrl($req->country);
//        $apiName = '/order/get';
//        $method = 'GET';
//        return $this->getGenerate($lazadaUrl, $apiName, $method, $req->all());
//    }
    // lấy thông tin chi tiết đơn hàng
//    public function getOrderItemsAction(Request $req)
//    {
//        $lazadaUrl = $this->getGenerateApiUrl($req->country);
//        $apiName = '/order/items/get';
//        $method = 'GET';
//        return $this->getGenerate($lazadaUrl, $apiName, $method, $req->all());
//    }
    // lấy thông tin sản phẩm
//    public function getProductsAction(Request $req)
//    {
//        $lazadaUrl = $this->getGenerateApiUrl($req->country);
//        $apiName = '/products/get';
//        $method = 'GET';
//        return $this->getGenerate($lazadaUrl, $apiName, $method, $req->all());
//    }


    public function saveConnectLazadaAction(Request $req)
    {   
        
        // Kiểm tra dữ liệu từ frontend
        if($req->get('entry')){
            // Lấy id của sàn Lazada
            $saleChannelID = SaleChannel::select('id')->where('name', "LIKE", "%Lazada%")->first();
            //Kiểm tra data từ frontend truyền vào: nếu có dữ liệu thì update, ngược lại thêm mới kết nối
            $data = $req->get('entry');
            //Nếu tồn tại id hoặc tên shop đã tồn tại            
            if(isset($data['id'])){
                if(isset($data['id'])){
                    $entry = SyncSaleChannels::find($data['id']);                
                    if (!$entry) {
                        return [
                            'code' => 404,
                            'message' => 'Không tìm thấy',
                        ];
                    }
                }
            }
            else{
                $checkIdentityKey = SyncSaleChannels::where('identity_key', $data['identity_key'])->whereNull('deleted_at')->count();
                if ($checkIdentityKey > 0) {
                    return [
                        'code' => 501,
                        'message' => $data['name'] . ' đã kết nối với cửa hàng. Vui lòng chọn shop khác!',
                    ];
                }else{
                    $getID = SyncSaleChannels::where('identity_key', $data['identity_key'])->first();
                    if(isset($getID->id)){
                        $entry = SyncSaleChannels::find($getID->id); 
                    }else{
                        $entry = new SyncSaleChannels();
                    }
                }
            }
            $entry->fill($data);
            $entry -> channel_type = SyncSaleChannels::TYPE_LAZADA;
            $today = new \DateTime();
            $token_expired_time = $data['is_active']['expires_in'];
            $entry->deleted_at = null;
            $entry->created_at = $today;
            $entry->updated_at = $today;
            $entry->retailer_id = auth()->id;
            $entry -> expire_time = date('Y-m-d H:i:s', strtotime('+'.$token_expired_time.'seconds'));
            $entry->sale_channel_id = $saleChannelID['id'];
            $entry ->  is_active = json_encode($data['is_active']);
            $entry->save();

            $emitter = new EventEmitter();
            $emitter::instance()->emit('SyncProductShopee',$data['shop_id']);

            return [
                'code' => 200,
                'message' => 'Thông tin kết nối được cập nhật thành công!',
            ];
        }
        else{
            return [
                'code' => 400,
                'message' => 'Kiểm tra lại thông tin kết nối!',
            ];
        }
    }
    public function getShopAction(Request $req){
        $saleChannelID = SaleChannel::select('id')->where('name', "like", "%Lazada%")->first();
        $entries = SyncSaleChannels::query()->whereNull('deleted_at')->where('sale_channel_id', $saleChannelID['id'])->get();
        return [
            'code' => 200,
            'message' => "success",
            'data' => [
                'entry' => $entries
            ],
        ];
    }
    public function getDetailShopAction(Request $req){
        $entries = SyncSaleChannels::query()->where('identity_key', $req->identity_key)->get();
        return [
            'code' => 200,
            'message' => "success",
            'data' => [
                'entry' => $entries
            ],
        ];
    }
    // lấy thông tin sản phẩm
    public function getProductLazadaAction(Request $req)
    {
        $shop_id = $req->identity_key;
        $page = $req->page;
        $limit = $req->number_of_records;

        $query = SyncProduct::where('shop_id',$shop_id);
        $total = count($query->get());
        $products = $query->paginate($limit, ['*'], 'page', $page);
        $data = $products->items();

        foreach ($products as $product) {
            if(isset($product->status)){ $product->prd_sync = json_decode($product->prd_sync); }
            if(isset($product->stock_info)){ $product->stock_info = json_decode($product->stock_info); }
        }

        if(isset($req->link) && !empty($req->link) || isset($req->sync) && !empty($req->sync)){
            $data = $this->filterProductForStatus($data, $req->link, $req->sync);
        }

        return [
            'code' => 200,
            'data' => $data,
            'total' => $total,
            'pagination' => [
                'currentPage' => $products->currentPage(),
                'lastPage' => $products->lastPage(),
            ] 
        ];
    }

    public function getStatus($status)
    {
        $result = [];
        switch ($status) {
            case 'pending':
                $result = [
                    'status' => 1,
                    'value' => "Chờ duyệt",
                    'shopee_status' => 1,
                    'shopee_value' => "Chờ xác nhận",
                ];
                break;
            case 'canceled':
                $result = [
                    'status' => 5,
                    'value' => "Đã huỷ",
                    'shopee_status' => 2,
                    'shopee_value' => "Đã huỷ",
                ];
                break;   
        }
        return $result;
    } 
    public function isOrderExist($code){
        $status =  false;
        $order = Order::where('code', $code) -> first();
        if($order == null) {
            $status = false;
        }
        else {
            $status = true;
        }        
        return $status;
    }
    public function lazadaOrderSyncAction(Request $req)
    {       
        $data = array($req->entry);
        $saleChannelName = "Lazada";
        $saleChannelID = SaleChannel::select('id')->where('name', "LIKE", "%" . $saleChannelName . "%")->first();
        foreach ($data as $item) {
            foreach ($item as $value) {
                $orderExist = $this->isOrderExist($value['code']);
                if($orderExist) {
                    continue;
                }
                $order = new Order();
                $order->fill($value);
                $order->createdBy = auth()->id;
                $status = $this->getStatus($value['status']);
                $order->status = $status['status'];
                $order->statusValue = $status['value'];
                $order->shopee_status = $status['shopee_status'];
                $order->shopee_value = $status['shopee_value']; 
                $order->saleChannelId = $saleChannelID['id'];
                $order->saleChannelName = $saleChannelName;            
                $order->save();
            }
        }
        return [
            'code' => 200,
            'message' => 'Thành công!',
        ];

    } 
    public function storeCustomer($data){
           
        $customer = new Customer();
        $customer -> name = $data['name'];
        $customer -> code = $data['code'];
        $customer -> contactNumber = $data['contactNumber'];        
        $customer->save();
        DB::commit();
        return [
            'id' =>  $customer -> id,
            'name' =>  $customer -> name,
            'code' =>  $customer -> code,
            'contactNumber' =>  $customer -> contactNumber,
        ];
    }
    public function filterProductForStatus($data, $link, $sync)
    {
        $filtered = array();
        if($link === null){
            $filtered = $data;
        }else{
            switch($link){
                case 'filter_link_all' :
                    $filtered = $data;
                    break;
                case 'filter_link_linked' :
                    foreach ($data as $value) {
                        if (isset($value['status'])) {
                            $filtered[] = $value;
                        }
                    }
                    break;
                case 'filter_link_noLink' :
                    foreach ($data as $value) {
                        if (!isset($value['status'])) {
                            $filtered[] = $value;
                        }
                    }
                    break;
            }
        }
        $filtered_array = array();
        if($sync === null){
            $filtered_array = $filtered;
        }else{
            switch($sync){
                case 'filter_sync_all' :
                    $filtered_array = $filtered;
                    break;
                case 'filter_sync_success' :
                    foreach ($filtered as $value) {
                        $status = $this->checkSyncError($value['item_id']);
                        if($status === 0){
                            $filtered_array[] = $value;
                        }
                    }
                    break;
                case 'filter_sync_error' :
                    foreach ($filtered as $value) {
                        $status = $this->checkSyncError($value['item_id']);
                        if($status === 1){
                            $filtered_array[] = $value;
                        }
                    }
                    break;
            }
        }
        return $filtered_array;
    }
    public function checkSyncError($prd_id){
        $status = 0;
        $in_omi_syn_link = OmiSyncLink::query()->where('sync_prd_id', $prd_id)->where('status', 1)->whereNull('deletedAt')->count();
        if($in_omi_syn_link > 0){
            $status = 1;
        }else{
            $status = 0;
        }
        return $status;
    }
}
