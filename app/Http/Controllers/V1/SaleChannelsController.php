<?php

namespace App\Http\Controllers\V1;


use App\Models\SaleChannel;
use App\Http\Controllers\AuthenticatedController;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class SaleChannelsController extends AuthenticatedController
{

    /**
     * @uri /v1/sale-channels/show
     * @return array
     */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = SaleChannel::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
     * @uri /v1/sale-channels/remove
     * @return array
     */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = SaleChannel::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
     * @uri /v1/sale-channels/save
     * @return array
     */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');

        V::make($data)
            ->rule(V::required, [
                'name',
            ])
            ->validOrFail();


        /**
         * @var SaleChannel $entry
         */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = SaleChannel::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new SaleChannel();
        }

        $entry->fill($data);
        $entry->save();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? ('/sale-channels/form?id=' . $entry->id) : false,
            'data' => $entry->toArray()
        ];
    }

    public function toggleStatusAction(Request $request) {
        $id = $request->get('id');
        $saleChannel = SaleChannel::find($id);

        if (!$saleChannel) {
            return [
                'code' => 500,
                'message' => "Không tìm thấy kênh bán"
            ];
        }

        $saleChannel->isActive = !$saleChannel->isActive;
        $saleChannel->save();

        return [
            'code' => 200,
            'message' => 'Thay đổi trạng thái thành công!'
        ];
    }

    public function setDefaultAction(Request $request) {
        $id = $request->get('id');
        $saleChannel = SaleChannel::find($id);

        if (!$saleChannel) {
            return [
                'code' => 500,
                'message' => "Không tìm thấy kênh bán"
            ];
        }

        DB::beginTransaction();
        if ($saleChannel->isDefault) {
            $saleChannel->isDefault = 0;
            $saleChannel->save();

            SaleChannel::setDefault();
        } else {
            SaleChannel::query()
                ->where('isDefault', 1)
                ->update([
                    'isDefault' => 0
                ]);
            $saleChannel->isDefault = 1;
            $saleChannel->save();
        }

        DB::commit();

        return [
            'code' => 200,
            'message' => 'Thay đổi trạng thái thành công!'
        ];
    }

    /**
     * Ajax data for index page
     * @uri /v1/sale-channels/data
     * @return array
     */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $record = $req->getInt('record');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = SaleChannel::query()->select($select)->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            $query->where('name', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);

        $totalRecord = $query->get();
        $entries = $query->paginate($record, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'totalRecord' => $totalRecord,
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
            ],
            [
                'field' => 'name',
                'name' => 'Name',
            ],
            [
                'field' => 'isActive',
                'name' => 'Trạng thái',
            ],
            [
                'field' => 'isDefault',
                'name' => 'Mặc định',
            ]
        ];
        return array_chunk($fields, 10)[0];
    }
}
