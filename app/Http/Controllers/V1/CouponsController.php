<?php

namespace App\Http\Controllers\V1;


use App\Models\Coupon;
use App\Http\Controllers\AuthenticatedController;
use App\Models\CouponApply;
use App\Models\CouponCode;
use App\Models\Promotion;
use App\Traits\Importable;
use Illuminate\Support\Str;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Http\Request;
use OmiCare\Utils\DB;
use OmiCare\Utils\V;

class CouponsController extends AuthenticatedController
{
    use Importable;

    /**
    * @uri /v1/coupons/show
    * @return array
    */
    public function showAction(Request $req)
    {
        $id = $req->id;
        $entry = Coupon::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }


        return [
            'code' => 200,
            'message' => 'OK',
            'data' => [
                'entry' => $entry->toArray(),
                'applyTo' =>  $entry->getApplyTO(),
                'fields' => $this->getDisplayFields()
            ]
        ];
    }

    /**
    * @uri /v1/coupons/remove
    * @return array
    */
    public function removeAction(Request $req)
    {
        return ['code' => 405, 'message' => 'Method not allow'];

        $id = $req->id;
        $entry = Coupon::find($id);

        if (!$entry) {
            return [
                'code' => 404,
                'message' => 'Entry not found'
            ];
        }

        $entry->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    /**
    * @uri /v1/coupons/save
    * @return array
    */
    public function saveAction(Request $req)
    {
        if (!$req->isMethod('POST')) {
            return ['code' => 405, 'message' => 'Method not allow'];
        }

        $data = $req->get('entry');
        $applyTo = $req->get('applyTo');

        V::make($data)
            ->rule(V::required, [])
            ->validOrFail();


        /**
        * @var Coupon $entry
        */
        $redirect = true;
        if (isset($data['id'])) {
            $entry = Coupon::find($data['id']);

            if (!$entry) {
                return [
                    'code' => 404,
                    'message' => 'Không tìm thấy',
                ];
            }

            $redirect = false;
        } else {
            $entry = new Coupon();
        }

        $entry->fill($data);
        DB::beginTransaction();
        $entry->save();
        $entry->pushApplyTo('customers', $applyTo['customerIDs']);
        $entry->pushApplyTo('branches', $applyTo['branchIDs']);
        $entry->pushApplyTo('users', $applyTo['saleIDs']);
        $entry->pushApplyTo('sale_channels', $applyTo['saleChannelIDs']);
        $entry->saveApplyTo();
        DB::commit();

        return [
            'code' => 200,
            'message' => 'Đã cập nhật',
            'redirect' => $redirect ? '/coupons/form?id=' . $entry->id : false,
            'data' => $entry->toArray()
        ];
    }

    /**
    * Ajax data for index page
    * @uri /v1/coupons/data
    * @return array
    */
    public function dataAction(Request $req)
    {

        $fields = $this->getDisplayFields();
        $select = array_column($fields, 'field');

        $page = $req->getInt('page');
        $sortField = $req->get('sort_field', 'id');
        $sortDirection = $req->get('sort_direction', 'desc');

        $query = Coupon::query()->select($select)->orderBy($sortField, $sortDirection);

        if ($req->keyword) {
            //$query->where('title', 'LIKE', '%' . $req->keyword. '%');
        }

        $query->createdIn($req->created);


        $entries = $query->paginate(25, ['*'], 'page', $page);

        return [
            'code' => 200,
            'data' => [
                'fields' => $fields,
                'entries' => $entries->items(),
                'paginate' => [
                    'currentPage' => $entries->currentPage(),
                    'lastPage' => $entries->lastPage(),
                ]
            ]

        ];
    }

    public function activeCouponsAction(Request  $request)
    {
        $now = date('Y-m-d H:i:s');

        $entries = Coupon::query()
            ->where('status', 1)
            ->where('startTime', '<=', $now)
            ->where('endTime', '>=', $now)
            ->get();

        $couponIDs = $entries->pluck('id')->toArray();
        $usedGroup = CouponCode::query()
            ->orderBy('id', 'desc')
            ->selectRaw('couponId,COUNT(*) `count`, `status`')
            ->whereIn('couponId', $couponIDs)
            ->groupBy('couponId')
            ->groupBy('status')
            ->get()->groupBy('couponId')->toArray();

        foreach ($entries as $entry) {
            $entry->usages = (object) [
                'used' => 0,
                'not_used' => 0
            ];
            if (isset($usedGroup[$entry->id])) {
                $statusGroup = $usedGroup[$entry->id];
                foreach ($statusGroup as $g) {
                    if ($g['status'] == 0) {
                        $entry->usages->not_used = $g['count'];
                    } else {
                        $entry->usages->used = $g['count'];
                    }
                }
            }
        }


        return [
            'code' => 200,
            'data' => $entries,
            '$usedGroup' => $usedGroup,
        ];
    }

    public function codesAction(Request $request)
    {
        $couponId = $request->get('coupon_id');

        if (!$couponId) {
            return ['code' => 1, 'message' => 'Missing coupon_id'];
        }

        $query = CouponCode::query()->orderBy('id', 'desc');

        $query->where('couponId', $couponId);

        if (isset($request->status)) {
            $query->where('status', $request->status);
        }

        $query->timeFieldIn('createdAt', $request->get('created_at'));
        $query->timeFieldIn('usedAt', $request->get('used_at'));

        $usedGroup = CouponCode::query()
            ->orderBy('id', 'desc')
            ->selectRaw('COUNT(*) `count`, `status`')
            ->where('couponId', $couponId)
            ->groupBy('status')
            ->get()
            ->pluck('count', 'status')->toArray();

        $paginate = $query->paginate();

        return [
            'code' => 0,
            'data' => $paginate->items(),
            'usedGroup' => $usedGroup,
            'paginate' => [
                'currentPage' => $paginate->currentPage(),
                'lastPage' => $paginate->lastPage(),
            ],
        ];
    }

    public function generateAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405];
        }

        $couponId = $request->get('coupon_id');
        $coupon = Coupon::find($couponId);

        if (!$coupon) {
            return [
                'code' => 404,
                'message' => 'Không tìm thấy khuyến mại'
            ];
        }

        if (empty($coupon->codePrefix)) {
            return [
                'code' => 3,
                'message' => 'Bạn cần nhập Tiền tố của mã khuyến mại trước'
            ];
        }

        $count = (int) $request->get('count');

        if ($count > 100 || $count <= 0) {
            return [
                'code' => 1,
                'message' => 'Số lượng tối đa là 100'
            ];
        }

        $now = date('Y-m-d H:i:s');

        for ($i = 1; $i <= $count; ++$i) {
            $bulkData[] = [
                'couponId' => $coupon->id,
                'code' => strtoupper($coupon->codePrefix).strtoupper(Str::random(6)),
                'status' => 0,
                'createdAt' => $now,
                'updatedAt' => $now
            ];
        }

        try {
            DB::table('coupon_codes')->insert($bulkData);
        } catch (\Exception $e) {
            logfile_write($e);
            $message = $e->getMessage();

            if (preg_match('/Duplicate entry \'(\w+)\'/', $message, $matches)) {
                return [
                    'code' => 3,
                    'message' => 'Mã khuyến mại bị trùng: '.$matches[1]
                ];
            }

            return [
                'code' => 4,
                'message' => 'Có lỗi xảy ra',
            ];
        }

        return [
            'code' => 200,
            'message' => 'Đã khởi tạo mã khuyến mại'
        ];
    }


    public function removeCodesAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405];
        }

        $couponId = $request->get('coupon_id');
        $codes = $request->get('codes');

        if (!is_array($codes)) {
            return [
                'code' => 2,
                'message' => 'Codes must be an array'
            ];
        }

        $couponCodes = CouponCode::where('couponId', $couponId)
            ->whereIn('code', $codes)
            ->where('status', 0)
            ->get();

        if ($couponCodes->count() !== count($codes)) {
            return [
                'code' => 404,
                'message' => 'Không tìm thấy mã khuyến mại hoặc mã đã sử dụng. Bạn chỉ được xóa mã chưa sử dụng'
            ];
        }

        CouponCode::query()->where('couponId', $couponId)
            ->whereIn('code', $codes)
            ->delete();

        return [
            'code' => 200,
            'message' => 'Đã xóa'
        ];
    }

    public function uploadExcelAction(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return ['code' => 405];
        }

        $couponId = $request->get('coupon_id');
        $coupon = Coupon::find($couponId);

        if (!$couponId) {
            return [
                'code' => 404,
                'message' => 'Không tìm thấy khuyến mại'
            ];
        }

        $file  = $_FILES['file_0'];
        $rows = $this->importXlsx($file['tmp_name']);
        @unlink($file['tmp_name']);
        $now = date('Y-m-d H:i:s');
        $bulkData = [];

        foreach ($rows as $row) {
            if (!empty($row[0])) {
                $bulkData[] = [
                    'couponId' => $coupon->id,
                    'code' => $row[0],
                    'status' => 0,
                    'createdAt' => $now,
                    'updatedAt' => $now
                ];
            }
        }

        try {
            DB::table('coupon_codes')->insert($bulkData);
        } catch (\Exception $e) {
            logfile_write($e);
            $message = $e->getMessage();

            if (preg_match('/Duplicate entry \'(\w+)\'/', $message, $matches)) {
                return [
                    'code' => 3,
                    'message' => 'Mã khuyến mại bị trùng: '.$matches[1]
                ];
            }

            return [
                'code' => 4,
                'message' => 'Có lỗi xảy ra'
            ];
        }
        DB::table('coupon_codes')->insert($bulkData);

        return [
            'code' => 200,
            'message' => 'Đã nhập mã khuyến mại'
        ];
    }
    private function getDisplayFields(): array
    {
        $fields = [
            [
                'field' => 'id',
                'name' => 'ID',
            ],
            [
                'field' => 'code',
                'name' => 'Code',
            ],
            [
                'field' => 'name',
                'name' => 'Tên chương trình',
            ],
            [
                'field' => 'discountValue',
                'name' => 'Giảm giá',
                'render' => 'number'
            ],
            [
                'field' => 'minInvoiceAmount',
                'name' => 'Hóa đơn từ',
            ],
            [
                'field' => 'allowManyCoupons',
                'name' => 'Áp dụng đồng thời',
            ],
            [
                'field' => 'status',
                'name' => 'Trạng Thái',
            ],
            [
                'field' => 'startTime',
                'name' => 'Ngày bắt đầu',
                'render' => 'datetime'
            ],
            [
                'field' => 'endTime',
                'name' => 'Ngày kết thúc',
                'render' => 'datetime'
            ],
        ];

        return $fields;
    }
}
