<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $refTable
 * @property int $refId
 * @property int $branchId
 * @property int $action
 * @property string $diff
 * @property string $content
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property int $createdBy
 */
class History extends BaseModel
{
    protected $table = 'histories';
    protected $fillable = [
        'id',
        'refTable',
        'refId',
        'branchId',
        'action',
        'diff',
        'content',
        'createdBy'
    ];

    protected $casts = [
        'diff' => 'array'
    ];

    const ACTION_CREATED = 1;
    const ACTION_UPDATED = 2;
    const ACTION_DELETED = 3;
    const ACTION_CANCEL = 4;
    const ACTION_IMPORT = 5;
    const ACTION_EXPORT = 6;
    const ACTION_LOGIN = 7;
    const ACTION_LOGOUT = 8;

    public static $listAction = [
        self::ACTION_CREATED => 'Thêm mới',
        self::ACTION_UPDATED => 'Cập nhật',
        self::ACTION_DELETED => 'Xóa',
        self::ACTION_CANCEL => 'Hủy',
        self::ACTION_IMPORT => 'Import',
        self::ACTION_EXPORT => 'Export',
        self::ACTION_LOGIN => 'Login',
        self::ACTION_LOGOUT => 'Export',
    ];
}
