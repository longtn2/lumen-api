<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends BaseModel 
{
    // use Authenticatable, Authorizable, HasFactory;

    protected $table = 'users';

    protected $hidden = [
        'password'
    ];
    protected $fillable = [
        'name',
        'email',
        'remember_token',
        'created_at',
        'updated_at',
        'last_login',
        'branchIDs',
        'avatar',
        'birthday',
        'phone',
        'deleted_at',
        'status',
        'username',
        'roleIDs'
    ];

    public function getUserRoles()
    {
        $userRoles = SysUserRole::query()->with('role')->where('userId', $this->id)->get();
        $roles = [];
        foreach ($userRoles as $ur) {
            $roles[] = $ur->role;
        }

        return $roles;
    }

    public static function getAdminByBranch($branchId) {
        $currentUser = auth();

        if (!$currentUser) {
            return [];
        }

        $admins = User::query()
            ->select('id', 'name')
            ->where('status', 1)
            ->whereRaw('FIND_IN_SET(?,branchIDs)', [$branchId])
            ->get()->toArray();

        if (!in_array($branchId, $currentUser->branchIDs)) {
            array_unshift($admins, $currentUser->only('id', 'name'));
        }

        return $admins;
    }

    public static function getNameFromId($userId) {
        $user = User::query()->find($userId);

        if (!$user) {
            return null;
        }

        return $user->name;
    }
}
