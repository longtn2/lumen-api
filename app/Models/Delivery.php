<?php

namespace App\Models;

use OmiCare\Utils\Auth;

/**
 * @property int $id
 * @property string $code
 * @property string $type
 * @property int $status
 * @property double $price
 * @property int $invoiceId
 * @property int $orderId
 * @property string $receiverName
 * @property string $receiverPhone
 * @property string $receiverAddress
 * @property int $receiverProvinceId
 * @property int $receiverDistrictId
 * @property int $receiverWardId
 * @property double $weight
 * @property string $weightUnit
 * @property double $height
 * @property double $long
 * @property double $width
 * @property int $usingPriceCod
 * @property double $priceCodPayment
 * @property int $branchId
 * @property int $transporterId
 * @property int $serviceTypeId
 * @property int $soldById
 * @property \DateTime $deliveryTime
 * @property string $note
 * @property int $createdBy
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class Delivery extends BaseModel
{
    protected $table = 'deliveries';
    protected $fillable = [
        'code',
        'type',
        'status',
        'price',
        'invoiceId',
        'orderId',
        'receiverName',
        'receiverPhone',
        'receiverAddress',
        'receiverProvinceId',
        'receiverDistrictId',
        'receiverWardId',
        'weight',
        'weightUnit',
        'height',
        'long',
        'width',
        'usingPriceCod',
        'priceCodPayment',
        'branchId',
        'transporterId',
        'serviceTypeId',
        'soldById',
        'deliveryTime',
        'note',
        'createdBy',
    ];

    const TYPE_INVOICE = 'Invoice';
    const TYPE_ORDER = 'Order';

    const WEIGHT_UNIT_GRAM = 'gram';
    const WEIGHT_UNIT_KG = 'kg';

    const STATUS_CANCEL = 6;

    public static $listStatus = [
        [
            'id' => 1,
            'name' => 'Chờ xử lý',
        ],
        [
            'id' => 2,
            'name' => 'Đang giao hàng',
        ],
        [
            'id' => 3,
            'name' => 'Giao thành công',
        ],
        [
            'id' => 4,
            'name' => 'Đang chuyển hoàn',
        ],
        [
            'id' => 5,
            'name' => 'Đã chuyển hoàn',
        ],
        [
            'id' => self::STATUS_CANCEL,
            'name' => 'Đã hủy',
        ],
        [
            'id' => 7,
            'name' => 'Đang lấy hàng',
        ],
        [
            'id' => 8,
            'name' => 'Chờ lấy lại',
        ],
        [
            'id' => 9,
            'name' => 'Đã lấy hàng',
        ],
        [
            'id' => 10,
            'name' => 'Chờ giao lại',
        ],
        [
            'id' => 11,
            'name' => 'Chờ chuyển hàng',
        ],
        [
            'id' => 12,
            'name' => 'Chờ chuyển hoàn lại',
        ]
    ];

    public static $listWeightUnit = [
        [
            'id' => self::WEIGHT_UNIT_GRAM,
            'name' => 'Gram'
        ],
        [
            'id' => self::WEIGHT_UNIT_KG,
            'name' => 'Kg'
        ]
    ];

    public function invoice() {
        return $this->belongsTo(Invoice::class,'invoiceId');
    }

    public function serviceType() {
        return $this->belongsTo(ServiceType::class, 'serviceTypeId');
    }

    public function transporter() {
        return $this->belongsTo(Transporter::class,'transporterId');
    }

    public function branch() {
        return $this->belongsTo(Branch::class, 'branchId');
    }

    public function userCreated() {
        return $this->belongsTo(User::class, 'createdBy');
    }

    public function soldBy() {
        return $this->belongsTo(User::class, 'soldById');
    }

    public function province() {
        return $this->belongsTo(Province::class, 'receiverProvinceId');
    }

    public function district() {
        return $this->belongsTo(District::class, 'receiverDistrictId');
    }

    public function ward() {
        return $this->belongsTo(Ward::class, 'receiverWardId');
    }

    public function deliveryStatus() {
        return $this->hasMany(DeliveryStatus::class, 'deliveryId');
    }

    public function saveStatus($status, $note = null, $saveDeli = true) {
        $statusLabel = self::getStatusLabel($status);

        $this->status = $status;
        if ($saveDeli) {
            $this->save();
        }

        $orderStatus = new DeliveryStatus();
        $orderStatus->deliveryId = $this->id;
        $orderStatus->value = $status;
        $orderStatus->name = $statusLabel;
        $orderStatus->note = $note;
        $orderStatus->updatedBy = Auth::user()->name;
        $orderStatus->save();

        return true;
    }

    public static function getStatusLabel($status) {
        static $listStatusMap;

        if (!$listStatusMap) {
            $listStatusMap = array_column(self::$listStatus, 'name', 'id');
        }

        return $listStatusMap[$status] ?? null;
    }

    /**
     * @param $action
     * @return bool
     */
    public function saveHistory($action)
    {
        /**
         * @var User $user
         */
        $user = auth();
        $listStatusMap = array_column(self::$listStatus, 'name', 'id');
        $ignored = ['updated_at' => true, 'created_by' => true, 'created_at' => true];

        if (!$user) {
            $user = new User();
            $user->id = 0;
            $user->name = 'Bot';
            $user->email = 'omipos-bot@omicare.vn';
        }

        $log = new History();
        $log->refTable = $this->getTable();
        $log->refId = $this->id;
        $log->branchId = $this->branchId;
        $log->action = $action;
        $log->diff = [];
        $log->createdBy = $user->id;

        $isUpdateOrderDelivery = $this->orderId;
        $code = $isUpdateOrderDelivery ? Order::getCodeFromId($this->orderId) : Invoice::getCodeFromId($this->invoiceId);

        switch ($action) {
            case History::ACTION_CREATED:
                $content = 'Tạo vận đơn ' . $this->code . ' cho ' . ($isUpdateOrderDelivery ? 'đơn đặt hàng ' : 'hóa đơn ') . $code . ' bao gồm:<br/>';
                $content .= '- Mã vận đơn: ' . $this->code . '<br/>';
                $content .= '- Người giao: ' . Transporter::getNameFromId($this->transporterId) . '<br/>';
                $content .= '- Người nhận: ' . $this->receiverName . '<br/>';
                $content .= '- Số điện thoại: ' . $this->receiverPhone . '<br/>';
                $content .= '- Địa chỉ: ' . $this->receiverAddress . '<br/>';
                $content .= '- Phường/Xã: ' . Ward::getNameById($this->receiverWardId) . '<br/>';
                $content .= '- Quận/Huyện: ' . Province::getNameById($this->receiverProvinceId) . '<br/>';
                $content .= '- Tỉnh/TP: ' . District::getNameById($this->receiverDistrictId) . '<br/>';
                $content .= '- Trọng lượng: ' . $this->weight . $this->weightUnit . '<br/>';
                $content .= '- Kích thước: ' . $this->long . ' - ' . $this->width . ' - ' . $this->height . '<br/>';
                $content .= '- Loại dịch vụ: ' . ServiceType::getNameFromId($this->serviceTypeId) . '<br/>';
                $content .= '- Phí giao hàng: ' . number_format($this->price) . '<br/>';
                $content .= '- Thu hộ tiền hàng: ' . ($this->usingPriceCod ? 'Có' : 'Không') . '<br/>';
                $content .= '- Trạng thái giao: ' . ($listStatusMap[$this->status] ?? '');

                $log->content = $content;
                $log->save();

                break;
            case History::ACTION_UPDATED:
                $origins = $this->getOriginal();
                $changes = $this->getDirty();

                $content = 'Cập nhật vận đơn ' . $this->code . ' cho ' . ($isUpdateOrderDelivery ? 'đơn đặt hàng ' : 'hóa đơn ') . $code . ', bao gồm:<br/>';
                $content .= isset($changes['code']) ? '- Mã vận đơn: ' . ($origins['code'] ?? '') . ' -> ' . $changes['code'] . '<br/>' : '';
                $content .= isset($changes['transporterId']) ? '- Người giao: ' . Transporter::getNameFromId($origins['transporterId'] ?? null) . ' -> ' . Transporter::getNameFromId($changes['transporterId']) . '<br/>' : '';
                $content .= isset($changes['receiverName']) ? '- Người nhận: ' . ($origins['receiverName'] ?? '') . ' -> ' . $changes['receiverName'] . '<br/>' : '';
                $content .= isset($changes['receiverPhone']) ? '- Số điện thoại: ' . ($origins['receiverPhone'] ?? '') . ' -> ' . $changes['receiverPhone'] . '<br/>' : '';
                $content .= isset($changes['receiverAddress']) ? '- Địa chỉ: ' . ($origins['receiverAddress'] ?? '') . ' -> ' . $changes['receiverAddress'] . '<br/>' : '';
                $content .= isset($changes['receiverWardId']) ? '- Phường/Xã: ' . Ward::getNameById($origins['receiverWardId'] ?? null) . ' -> ' . Ward::getNameById($changes['receiverWardId']) . '<br/>' : '';
                $content .= isset($changes['receiverProvinceId']) ? '- Quận/Huyện: ' . Province::getNameById($origins['receiverProvinceId'] ?? null) . ' -> ' . Province::getNameById($changes['receiverProvinceId']) . '<br/>' : '';
                $content .= isset($changes['receiverDistrictId']) ? '- Tỉnh/TP: ' . District::getNameById($origins['receiverDistrictId'] ?? null) . ' -> ' . District::getNameById($changes['receiverDistrictId']) . '<br/>' : '';
                $content .= (isset($changes['weight']) || isset($changes['weightUnit'])) ?
                    '- Trọng lượng: ' . (($origins['weight'] ?? 0) . ($origins['weightUnit'] ?? '')) . ' -> ' . (($changes['weight'] ?? 0) . ($changes['weightUnit'] ?? '')) . '<br/>' : '';
                $content .= (isset($changes['long']) || isset($changes['width']) || isset($changes['height'])) ?
                    '- Kích thước: ' . (($origins['long'] ?? '') . ' - ' . ($origins['width'] ?? '') . ' - ' . ($origins['height'] ?? '')) .
                    ' -> ' . (($changes['long'] ?? '') . ' - ' . ($changes['width'] ?? '') . ' - ' . ($changes['height'] ?? '')) . '<br/>' : '';
                $content .= isset($changes['serviceTypeId']) ? '- Tỉnh/TP: ' . ServiceType::getNameFromId($origins['serviceTypeId'] ?? null) . ' -> ' . ServiceType::getNameFromId($changes['serviceTypeId']) . '<br/>' : '';
                $content .= isset($changes['price']) ? '- Phí giao hàng: ' . number_format($origins['price'] ?? 0) . ' -> ' . number_format($changes['price']) . '<br/>' : '';
                $content .= isset($changes['usingPriceCod']) ? '- Thu hộ tiền hàng: ' . (($origins['usingPriceCod'] ?? '') ? 'Có' : 'Không') . ' -> ' . ($changes['usingPriceCod'] ? 'Có' : 'Không') . '<br/>' : '';
                $content .= isset($changes['status']) ? '- Trạng thái giao: ' . ($listStatusMap[$origins['status']] ?? '') . ' -> ' . ($listStatusMap[$changes['status']] ?? '') . '<br/>' : '';

                $log->content = $content;

                $diff = [];

                foreach ($changes as $key => $value) {
                    $oldVal = $origins[$key] ?? null;

                    if (!isset($ignored[$key]) && $oldVal != $value) {
                        $diff[$key] = [$oldVal, $value];
                    }
                }

                if (count($diff)) {
                    $log->diff = $diff;
                    $log->save();
                }

                break;
            case History::ACTION_CANCEL:
                $log->content = 'Hủy vận đơn ' . $this->code . ' cho ' . ($isUpdateOrderDelivery ? 'đơn đặt hàng ' : 'hóa đơn ') . $code . ' trên ' . Transporter::getNameFromId($this->transporterId);
                $log->save();

                break;
            default:

                break;
        }

        return true;
    }

    public function decode() {
        $receiverProvince = $this->province()->first();
        $receiverDistrict = $this->district()->first();
        $receiverWard = $this->ward()->first();

        $this->addressDecode = $this->receiverAddress . ", " . ($receiverProvince->name ?? "") . ", " .
            ($receiverDistrict->name ?? "") . ", " . ($receiverWard->name ?? "");

        $branch = $this->branch()->first();
        if ($branch) {
            $branch->addressDecode = Branch::decode($this->branchId, false);
        }
        $this->branch = $branch ?? [];
        $this->transporter = $this->transporter()   ->first();
        $this->serviceType = $this->serviceType()->first();

        return true;
    }
}
