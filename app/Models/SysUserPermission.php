<?php

namespace App\Models;

/**
 * @property int       $userId
 * @property int       $permissionId
 */
class SysUserPermission extends BaseModel
{
    public $timestamps = false;
    protected $table = 'sys_user_permissions';
    protected $fillable = ['userId', 'permissionId'];

    public function branch() {
        return $this->belongsTo(Branch::class, 'branchId');
    }


}
