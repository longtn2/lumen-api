<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $code
 * @property int $userId
 * @property string $address
 * @property string $locationName
 * @property string $wardName
 * @property string $contactNumber
 * @property string $status
 * @property string $usedForFinancialReporting
 * @property string $origin
 * @property int $cashFlowGroupId
 * @property string $cashGroup
 * @property string $statusValue
 * @property string $method
 * @property string $description
 * @property string $partnerType
 * @property int $partnerId
 * @property int $branchId
 * @property int $retailerId
 * @property string $transDate
 * @property double $amount
 * @property string $partnerName
 * @property string $accountId
 * @property string $couponCodeId
 * @property int $orderId
 * @property int $invoiceId
 */
class Cashflow extends BaseModel
{
    protected $table = 'cashflow';
    protected $fillable = [
        'code',
        'userId',
        'address',
        'locationName',
        'wardName',
        'contactNumber',
        'status',
        'usedForFinancialReporting',
        'origin',
        'cashFlowGroupId',
        'cashGroup',
        'statusValue',
        'method',
        'description',
        'partnerType',
        'partnerId',
        'branchId',
        'retailerId',
        'transDate',
        'amount',
        'partnerName',
        'accountId',
        'invoiceId',
        'couponCodeId',
    ];

    const ORIGIN_PURCHASE = 'Purchase';
    const ORIGIN_PAY = 'Pay';

    const STATUS_PAID = 0; // đã thanh toán
    const STATUS_CANCEL = 1; // đã hủy

    const PAYMENT_METHOD_CASH = 'Cash';
    const PAYMENT_METHOD_TRANSFER = 'Transfer';
    const PAYMENT_METHOD_CARD = 'Card';
    const PAYMENT_METHOD_VOUCHER = 'Voucher';

    const CODE_INVOICE = 'TTHD';
    const CODE_ORDER = 'TTDH';
    const CODE_RETURN = 'TTTH';

    const CASH_FLOW_GROUP_CUSTOMER = -1;
    const CASH_FLOW_GROUP_SUPPLIER = -2;
    const CASH_FLOW_GROUP_REFUND_SUPPLIER = -3;

    // A: tất cả, C: khách hàng, S: nhà cung cấp, U: nhân viên, D: Đối tác giao hàng, O: khác
    const PARTNER_TYPE_ALL = 'A';
    const PARTNER_TYPE_CUSTOMER = 'C';
    const PARTNER_TYPE_SUPPLIER = 'S';
    const PARTNER_TYPE_USER = 'U';
    const PARTNER_TYPE_DELIVERY_PARTNER = 'D';
    const PARTNER_TYPE_OTHER = 'O';

    public static $listStatus = [
        self::STATUS_PAID => 'Đã thanh toán',
        self::STATUS_CANCEL => 'Đã hủy'
    ];

    public static $listPaymentMethod = [
        [
            'id' => self::PAYMENT_METHOD_CASH,
            'name' => 'Tiền mặt'
        ],
        [
            'id' => self::PAYMENT_METHOD_CARD,
            'name' => 'Thẻ'
        ],
        [
            'id' => self::PAYMENT_METHOD_TRANSFER,
            'name' => 'Chuyển khoản'
        ],
        [
            'id' => self::PAYMENT_METHOD_VOUCHER,
            'name' => 'Voucher'
        ],
    ];

    public static $listCashFlowGroup = [
        self::CASH_FLOW_GROUP_CUSTOMER => 'Tiền khách trả',
        self::CASH_FLOW_GROUP_SUPPLIER => 'Tiền trả NCC',
        self::CASH_FLOW_GROUP_REFUND_SUPPLIER => 'Tiền NCC hoàn trả'
    ];

    public function user() {
        return $this->hasOne(User::class,'id', 'userId');
    }

    public static function generalCode($typeCode) {
        /**
         * @var self latestEntry
         */
        $latestEntry = self::query()
            ->where('code', 'LIKE', $typeCode . '%')
            ->orderByDesc('id')
            ->first();

        if (!$latestEntry) {
            return $typeCode . '100000';
        }

        $latestCode = $latestEntry->code;
        $latestCode = substr($latestCode, strlen($typeCode));

        $code = (substr($latestCode, 0, 1) >= 1) ? $latestCode + 1 : '100000';
        $maxLop = 25;
        while (self::where('code', $typeCode . $code)->count()) {
            $code++;

            if ($maxLop <= 0) {
                throw new \Exception('Không thể tạo mã dòng tiền!');
            }

            $maxLop--;
        }

        return $typeCode . $code;
    }
}
