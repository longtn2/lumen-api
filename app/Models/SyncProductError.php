<?php

namespace App\Models;

class SyncProductError extends BaseModel
{
    public $timestamps = false;

    const TYPE_SAVE_ERROR = 0;
    const TYPE_FETCH_ERROR = 1;

    protected $table = 'sync_product_error';
    protected $fillable = [
        'item_id',
        'model_id',
        'failed_type',
        'failed_at_offset',
        'shop_id'
    ];
}
