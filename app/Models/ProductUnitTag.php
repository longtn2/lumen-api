<?php

namespace App\Models;

use App\Types\DiscountType;

class ProductUnitTag extends BaseModel
{
    protected $table = 'product_unit_tags';
    protected $fillable = ['tagId', 'productUnitId'];

    public function productUnit()
    {
        return $this->belongsTo(ProductUnit::class, 'productUnitId', 'id')
            ->selectRaw('id,productId,productName,unitId,unitName,conversionValue,price');
    }

}
