<?php
namespace App\Models;

 /**
 * @property int       $id
 * @property string    $name
 * @property string    $code
 * @property string    $contactNumber
 * @property string    $email
 * @property int       $status
 * @property string    $address
 * @property string    $createdDate
 * @property string    $createdName
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property string    $taxCode
 */
class Supplier extends BaseModel
{
    protected $table = 'suppliers';
    protected $fillable = [
    'name',
    'code',
    'contactNumber',
    'email',
    'status',
    'address',
    'createdDate',
    'createdName',
    'taxCode',
];
}
