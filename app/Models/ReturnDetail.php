<?php
namespace App\Models;

/**
 * @property int       $id
 * @property int       $productId
 * @property int       $returnId
 * @property int       $inventoryId
 * @property int       $productUnitId
 * @property string    $subTotal
 * @property string    $note
 * @property string    $productCode
 * @property string    $productName
 * @property int       $sendQuantity
 * @property int       $receiveQuantity
 * @property int       $status
 * @property string    $price
 * @property int       $quantity
 * @property int       $userPoint
 */
class ReturnDetail extends BaseModel
{
    protected $table = 'return_details';
}
