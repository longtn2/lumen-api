<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $productId
 * @property int       $unitId
 * @property string    $unitName
 * @property string    $productName
 * @property int       $price
 * @property int       $conversionValue
 * @property string    $skuId
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property \DateTime $deletedAt
 * @property int       $weight
 * @property int       $length
 * @property int       $height
 * @property int       $width
 * @property int       $volume
 * @property string    $image
 * @property string    $pack
 */
class ProductUnit extends BaseModel
{
    protected $table = 'product_units';
    protected $fillable = [
        'productId', 'unitId', 'unitName', 'price', 'conversionValue', 'productName'
    ];

    public function product() {
        return $this->belongsTo(Product::class, 'productId');
    }

    public function orderSupplierDetails() {
        return $this->hasMany(OrderSupplierDetail::class, 'productUnitId');
    }

    public function orderDetails() {
        return $this->hasMany(OrderDetail::class,'productUnitId');
    }

    public static function batchDecode($productUnits, $currentBranchID) {
        $productIds = [];
        foreach ($productUnits as $productUnit) {
            if (!empty($productUnit['productId'])) {
                $productIds[] = $productUnit['productId'];
            }
        }

        $productIds = array_unique($productIds);

        $actualInventoryMap = Inventory::getActualInventoryMap($productIds, $currentBranchID);
        $systemInventoryMap = Inventory::getSystemInventoryMap($productIds, $currentBranchID);
        $orderSupplierSumByProductMap = OrderSupplierDetail::getOrderSupplierSumByProductMap($productIds, $currentBranchID);
        $orderDetailsSumByProductMap = OrderDetail::getOrderDetailsSumByProductMap($productIds, $currentBranchID);

        foreach ($productUnits as $productUnit) {
            $conversionValue = $productUnit->conversionValue ?? 1;

            $quantityOrigin = isset($systemInventoryMap[$productUnit->productId]) ? $systemInventoryMap[$productUnit->productId] : 0;
            $productUnit->quantityOrigin = $conversionValue ? floor($quantityOrigin / $conversionValue) : 0;

            $quantityActual = isset($actualInventoryMap[$productUnit->productId]) ? $actualInventoryMap[$productUnit->productId] : 0;
            $productUnit->quantityActual = $conversionValue ? floor($quantityActual / $conversionValue) : 0;

            $supplierCount = $orderSupplierSumByProductMap[$productUnit->productId] ?? 0;
            $supplierCount = $conversionValue ? floor($supplierCount / $conversionValue) : 0;
            $productUnit->supplier_count = $supplierCount;

            $orderDetailsCount = $orderDetailsSumByProductMap[$productUnit->productId] ?? 0;
            $orderDetailsCount = $conversionValue ? floor($orderDetailsCount / $conversionValue) : 0;
            $productUnit->order_details_count = $orderDetailsCount;
        }

        return $productUnits;
    }
}
