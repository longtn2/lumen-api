<?php

namespace App\Models;

use OmiCare\Utils\DB;

/**
 * @property int       $id
 * @property int       $retailerId
 * @property string    $actionType
 * @property string    $orderSupplierCode
 * @property string    $code
 * @property string    $refCode
 * @property string    $description
 * @property int       $branchId
 * @property string    $branchName
 * @property string    $purchaseDate
 * @property double    $discount
 * @property int       $discountRatio
 * @property double    $total
 * @property double    $subTotal
 * @property double    $totalPayment
 * @property int       $status
 * @property string    $createdDate
 * @property int       $supplierId
 * @property string    $supplierName
 * @property string    $supplierCode
 * @property int       $purchaseById
 * @property string    $purchaseName
 * @property double    $exReturnSuppliers
 * @property double    $exReturnThirdParty
 * @property string    $_purchaseOrderDetails
 * @property string    $payments
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property string    $createdBy
 * @property string    $updatedBy
 * @property int       $createdById
 * @property int       $totalQuantity
 * @property int       $totalProduct
 * @property int       $readonly
 */

class PurchaseOrder extends BaseModel
{
    const STATUS_DRAFT = 1;
    const STATUS_IMPORTED = 3;
    const STATUS_CANCELLED = 4;
    const STATUS_RETURNED = 5;

    const ACTION_RETURN = 'return';
    const ACTION_IMPORT = 'import';

    const STATUS_NAME_MAP = [
        self::STATUS_DRAFT => 'Phiếu tạm',
        self::STATUS_IMPORTED => 'Đã nhập',
        self::STATUS_CANCELLED => 'Đã hủy',
        self::STATUS_RETURNED => 'Đã trả hàng',
    ];

    const LIST_STATUS = [
        [ 'id' =>  self::STATUS_DRAFT, 'name' => 'Phiếu tạm',],
        [ 'id' =>  self::STATUS_IMPORTED, 'name' => 'Đã nhập',],
        [ 'id' =>  self::STATUS_CANCELLED, 'name' => 'Đã hủy',],
        [ 'id' =>  self::STATUS_RETURNED, 'name' => 'Đã trả hàng',],
    ];

    protected $table = 'purchase_orders';
    protected $casts = [
        'discountObject' => 'array'
    ];

    protected $fillable = [
        'retailerId',
        'code',
        'description',
        'orderSupplierCode',
        'branchId',
        'branchName',
        'purchaseDate',
        'discount',
        'discountRatio',
        'total',
        'subTotal',
        'totalPayment',
        'status',
        'createdDate',
        'supplierId',
        'supplierName',
        'supplierCode',
        'purchaseById',
        'purchaseName',
        'exReturnSuppliers',
        'exReturnThirdParty',
        'inventoryPlanId',
        'payments',
        'createdById',
        'refCode',
        'createdBy',
        'discountObject'
    ];

    public function toArray()
    {
        $output = parent::toArray();

        $output['statusName'] =  PurchaseOrder::STATUS_NAME_MAP[$this->status] ?? '';


        return $output;
    }

    public function purchaseOrderDetails() {
        return $this->hasMany(PurchaseOrderDetail::class, 'purchaseOrderId', 'id');
    }

    public function productUnit() {
        return $this->belongsTo(ProductUnit::class, 'productUnitId', 'id');
    }

    public function supplier() {
        return $this->belongsTo(Supplier::class, 'supplierId', 'id');
    }

    public function branch() {
        return $this->belongsTo(Branch::class, 'branchId', 'id');
    }

    public function selfRef() {
        return $this->belongsTo(PurchaseOrder::class, 'refId', 'id');
    }

    public function checkReadonlyState(): bool
    {
        /**
         * @var PurchaseOrderDetail $detail
         */
        if ($this->status == static::STATUS_DRAFT) {
            $this->readonly = 0;

            return false;
        }

        $baseInventory = Inventory::query()->where('refCode', $this->code)->first();


        $details = PurchaseOrderDetail::query()->where('purchaseOrderId', $this->id)->get();

        foreach ($details as $detail) {
            $t = Inventory::query()
                ->selectRaw('SUM(onHand*conversionValue) as onHandAll, SUM(originalTotal) originalTotalAll')
                ->where('branchId', $this->branchId)
                ->where('productUnitId', $detail->productUnitId)
                ->where('productBatchId', $detail->productBatchId)
                ->when(isset($baseInventory), function($query) use($baseInventory) {
                    $query->where('createdAt', '>=', $baseInventory->createdAt);
                })
                ->first();

            $t->onHandAll = (int)$t->onHandAll;
            $t->originalTotalAll = (int)$t->originalTotalAll;

            if ($t->onHandAll < $t->originalTotalAll) {
                $this->readonly = 1;
                $this->save();
                return false;
            } else {
                $this->readonly = 0;
                $this->save();
                return true;
            }

        }

        return true;
    }

    public function getStatusName(): string
    {
        return self::STATUS_NAME_MAP[$this->status] ?? '';
    }


    public function getRelatedRefCodes(): array
    {
        $refCodes = static::query()->selectRaw('code')
            ->where('refCode', $this->code)
            ->groupBy('code')
            ->get()
            ->pluck('code')
            ->toArray();

        $refCodes[] = $this->code;

        return $refCodes;
    }

    public static function attachLastPurchaseOrder($products)
    {
        $productIDs = [];
        foreach ($products as $product) {
            $productIDs[] = $product->id;
        }

        $groups = PurchaseOrderDetail::query()
            ->selectRaw('GROUP_CONCAT(purchase_order_details.id ORDER BY purchase_order_details.id DESC) `idGroup`')
            ->join('purchase_orders', 'purchase_orders.id', '=', 'purchase_order_details.purchaseOrderId')
            ->whereIn('productId', $productIDs)
            ->groupBy('productId')
            ->where('purchase_orders.status', PurchaseOrder::STATUS_IMPORTED)
            ->get();

        $purchaseOrderDetailIds = [];
        foreach ($groups as $g) {
            list($lastId) = explode(',', $g->idGroup);
            $purchaseOrderDetailIds[] = $lastId;
        }

        $details = PurchaseOrderDetail::query()
            ->with(['purchaseOrder:id,code,supplierId'])
            ->whereIn('id', $purchaseOrderDetailIds)->get()->groupBy('productId');

        foreach ($products as $product) {
            $product->lastPurchaseOrderDetail = isset($details[$product->id]) ? $details[$product->id][0] : null;
        }

    }
}
