<?php
namespace App\Models;

 /**
 * @property int       $id
 * @property string    $code
 * @property int       $invoiceId
 * @property string    $returnDate
 * @property int       $branchId
 * @property string    $branchName
 * @property int       $receivedById
 * @property string    $soldByName
 * @property int       $customerId
 * @property string    $customerCode
 * @property string    $customerName
 * @property double    $returnTotal
 * @property double    $returnDiscount
 * @property double    $totalPayment
 * @property double    $returnFee
 * @property int       $status
 * @property string    $description
 * @property string    $createdDate
 * @property string    $returnDetails
 * @property string    $payments
 * @property string    $modifiedDate
 * @property string    $saleChannelId
 */
class ReturnOrder extends BaseModel
{
    protected $table = 'returns';
    protected $fillable = [
    'code',
    'invoiceId',
    'returnDate',
    'branchId',
    'branchName',
    'receivedById',
    'soldByName',
    'customerId',
    'customerCode',
    'customerName',
    'returnTotal',
    'returnDiscount',
    'totalPayment',
    'returnFee',
    'status',
    'description',
    'createdDate',
    'returnDetails',
    'payments',
    'modifiedDate',
    'saleChannelId',
];


    const STATUS_COMPLETE = 1;
    const STATUS_CANCELED = 2;

    public function returnDetails() {
        return $this->hasMany(ReturnDetail::class, 'returnId');
    }
    public function receiver() {
        return $this->belongsTo(User::class, 'receivedById');
    }
}
