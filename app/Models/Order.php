<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $code
 * @property string $purchaseDate
 * @property int $branchId
 * @property string $branchName
 * @property int $soldById
 * @property string $soldByName
 * @property int $customerId
 * @property string $customerCode
 * @property string $customerName
 * @property double $totalOrigin
 * @property double $total
 * @property double $totalPayment
 * @property int $status
 * @property string $statusValue
 * @property int $retailerId
 * @property string $usingCod
 * @property string $modifiedDate
 * @property string $createdDate
 * @property string $PriceBookId
 * @property string $Extra
 * @property string $orderDetails
 * @property string $discountType
 * @property int $discountValue
 * @property string $payments
 * @property string $orderDelivery
 * @property string $saleChannelId
 * @property string $saleChannelName
 * @property string $description
 * @property int $createdBy
 */
class Order extends BaseModel
{
    protected $table = 'orders';
    protected $fillable = [
        'code',
        'purchaseDate',
        'branchId',
        'branchName',
        'soldById',
        'soldByName',
        'customerId',
        'customerCode',
        'customerName',
        'totalOrigin',
        'total',
        'totalPayment',
        'status',
        'statusValue',
        'retailerId',
        'usingCod',
        'modifiedDate',
        'createdDate',
        'PriceBookId',
        'Extra',
        'orderDetails',
        'discountType',
        'discountValue',
        'payments',
        'orderDelivery',
        'saleChannelId',
        'saleChannelName',
        'description',
        'createdBy'
    ];

    const STATUS_WAIT = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_DELIVERY = 3;
    const STATUS_COMPLETE = 4;
    const STATUS_CANCELED = 5;
    const STATUS_EXPIRE = 6;    

    public static $listStatus = [
        [
            'id' => self::STATUS_WAIT,
            'name' => 'Chờ duyệt',
        ],
        [
            'id' => self::STATUS_CONFIRMED,
            'name' => 'Đã xác nhận',
        ],
        [
            'id' => self::STATUS_DELIVERY,
            'name' => 'Đang giao hàng',
        ],
        [
            'id' => self::STATUS_COMPLETE,
            'name' => 'Hoàn thành',
        ],
        [
            'id' => self::STATUS_CANCELED,
            'name' => 'Đã hủy',
        ],
        [
            'id' => self::STATUS_EXPIRE,
            'name' => 'Hết hạn',
        ]
    ];

    public function orderItems() {
        return $this->hasMany(OrderDetail::class, 'orderId');
    }

    public function invoices() {
        return $this->hasMany(Invoice::class, 'orderId');
    }

    public function invoice() {
        return $this->hasOne(Invoice::class, 'orderId');
    }

    public function cashFlows() {
        return $this->hasMany(Cashflow::class, 'orderId');
    }

    public function currentPayments() {
        return $this->hasMany(Cashflow::class, 'orderId');
    }

    public function customer() {
        return $this->belongsTo(Customer::class, 'customerId');
    }

    public function branch() {
        return $this->belongsTo(Branch::class,'branchId');
    }

    public function saleChannel() {
        return $this->belongsTo(SaleChannel::class,'saleChannelId');
    }

    public function soldBy() {
        return $this->belongsTo(User::class, 'soldById');
    }

    public function userCreated() {
        return $this->belongsTo(User::class, 'createdBy');
    }

    public function delivery() {
        return $this->hasOne(Delivery::class, 'orderId');
    }

    public function orderSurcharges() {
        return $this->hasMany(OrderSurcharge::class,'orderId');
    }

    public function products() {
        return $this->belongsToMany(Product::class, 'order_details', 'orderId', 'productId');
    }

    /**
     * @param $action
     * @param array $options
     * @return bool
     */
    public function saveHistory($action, $options = [])
    {
        /**
         * @var User $user
         */
        $user = auth();
        $ignored = ['updated_at' => true, 'created_by' => true, 'created_at' => true];

        if (!$user) {
            $user = new User();
            $user->id = 0;
            $user->name = 'Bot';
            $user->email = 'omipos-bot@omicare.vn';
        }

        $log = new History();
        $log->refTable = $this->getTable();
        $log->refId = $this->id;
        $log->branchId = $this->branchId;
        $log->action = $action;
        $log->diff = [];
        $log->createdBy = $user->id;

        switch ($action) {
            case History::ACTION_CREATED:
                $content = 'Tạo đơn đặt hàng: ' . $this->code . ', khách hàng: ' . $this->customerName . ', giảm giá: ' . number_format($this->getDiscountPrice()) .
                    ', giá trị: ' . number_format($this->total) . ', thời gian: ' . date('d/m/Y H:i:s', strtotime($this->purchaseDate)) .
                    ', trạng thái: ' . self::getStatusLabel($this->status) . ', Người tạo: ' . User::getNameFromId($this->createdBy) .
                    ', Người nhận đặt: ' . User::getNameFromId($this->soldById) . ', bao gồm: <br/><br/>' .
                    OrderDetail::decodeContentHistory($this->id);

                $log->content = $content;
                $log->save();

                break;
            case History::ACTION_UPDATED:
                $origins = $this->getOriginal();
                $changes = $this->getDirty();

                $content = 'Cập nhật thông tin đơn đặt hàng: ' . $this->code;

                if (isset($origins['customerName']) && isset($changes['customerName']) && $origins['customerName'] != $changes['customerName']) {
                    $content .= ', khách hàng: ' . $origins['customerName'] . ' -> ' . $changes['customerName'];
                }
                if (isset($origins['purchaseDate']) && isset($changes['purchaseDate']) && $origins['purchaseDate'] != $changes['purchaseDate']) {
                    $content .= ', thời gian: ' . date('d/m/Y H:i:s', strtotime($origins['purchaseDate'])) . ' -> ' . date('d/m/Y H:i:s', strtotime($changes['purchaseDate']));
                }
                if (isset($origins['status']) && isset($changes['status']) && $origins['status'] != $changes['status']) {
                    $content .= ', trạng thái: ' . self::getStatusLabel($origins['status']) . ' -> ' . self::getStatusLabel($changes['status']);
                }
                if (isset($origins['soldById']) && isset($changes['soldById']) && $origins['soldById'] != $changes['soldById']) {
                    $content .= ', Người bán: ' . User::getNameFromId($origins['soldById']) . ' -> ' . User::getNameFromId($changes['soldById']);
                }
                if (isset($origins['saleChannelId']) && isset($changes['saleChannelId']) && $origins['saleChannelId'] != $changes['saleChannelId']) {
                    $content .= ', Kênh bán: ' . SaleChannel::getNameFromId($origins['saleChannelId']) . ' -> ' . SaleChannel::getNameFromId($changes['saleChannelId']);
                }

                $content .= ', bao gồm: <br/><br/>' . InvoiceDetail::decodeContentHistory($this->id);
                $log->content = $content;

                $diff = [];

                foreach ($changes as $key => $value) {
                    $oldVal = $origins[$key] ?? null;

                    if (!isset($ignored[$key]) && $oldVal != $value) {
                        $diff[$key] = [$oldVal, $value];
                    }
                }

                if (count($diff)) {
                    $log->diff = $diff;
                    $log->save();
                }

                break;
            case History::ACTION_CANCEL:
                $log->content = 'Hủy đơn đặt hàng: ' . $this->code;
                $log->save();

                break;
            default:

                break;
        }

        return true;
    }

    public function saveStatus($status, $note = null, $saveOrder = true) {
        $statusLabel = Order::getStatusLabel($status);

        $this->status = $status;
        $this->statusValue = $statusLabel;
        if ($saveOrder) {
            $this->save();
        }

        $orderStatus = new OrderStatus();
        $orderStatus->orderId = $this->id;
        $orderStatus->value = $status;
        $orderStatus->name = $statusLabel;
        $orderStatus->note = $note;
        $orderStatus->save();

        return true;
    }

    public static function getStatusLabel($status)
    {
        
        static $listStatusMap;

        if (!$listStatusMap) {
            $listStatusMap = array_column(Order::$listStatus, 'name', 'id');
        }

        return $listStatusMap[$status] ?? null;
    }

    public function getDiscountPrice() {
        if ($this->discountType == DISCOUNT_TYPE_PERCENT) {
            $discountPrice = ceil($this->totalOrigin * $this->discountValue / 100);
        } else {
            $discountPrice = $this->discountValue;
        }

        return $discountPrice + (int) $this->discount;
    }


    public function removeInventories() {
        $orderDetailIds = OrderDetail::query()
            ->where('orderId', $this->id)
            ->pluck('id');

        Inventory::query()
            ->whereIn('refId', $orderDetailIds)
            ->where('refTable', Inventory::REF_CODE_ORDER_DETAIL)
            ->delete();

        return true;
    }
    public static function generalShopeeCode(){
        /**
         * @var self latestEntry
         */
        $latestEntry = self::query()
            ->orderByDesc('id')
            ->first();

        if (!$latestEntry) {
            return 'DHSPE_100000';
        }

        $latestCode = $latestEntry->code;
        if (($index = strrpos($latestCode, ".")) !== false) {
            $latestCode = substr($latestCode, 0, $index);
        }
        $latestCode = substr($latestCode, 2);

        $code = (substr($latestCode, 0, 1) >= 1) ? $latestCode + 1 : '100000';
        $maxLop = 25;
        while (self::where('code', 'DHSPE_' . $code)->count()) {
            $code++;

            if ($maxLop <= 0) {
                throw new \Exception('Không thể tạo mã hóa đơn!');
            }

            $maxLop--;
        }

        return 'DHSPE_' . $code;
    }
    public static function generalCode() {
        /**
         * @var self latestEntry
         */
        $latestEntry = self::query()
            ->orderByDesc('id')
            ->first();
        
        if (!$latestEntry) {
            return 'DH100000';
        }

        $latestCode = $latestEntry->code;
        
        if (($index = strrpos($latestCode, ".")) !== false) {
            $latestCode = substr($latestCode, 0, $index);
        }
        $latestCode = substr($latestCode, 2);
       
        $code = (substr($latestCode, 0, 1) >= 1) ? $latestCode + 1 : rand();     
        $maxLop = 25;
        while (self::where('code', 'ĐH' . $code)->count()) {
            $code++;
            if ($maxLop <= 0) {
                throw new \Exception('Không thể tạo mã hóa đơn!');
            }

            $maxLop--;
        }
        
        return 'DH' . $code;
    }

    public static function getCodeFromId($id) {
        $entry = self::find($id);

        if (!$entry) {
            return null;
        }

        return $entry->code;
    }
}
