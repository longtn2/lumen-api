<?php

namespace App\Models;

/**
 * @property int    $id
 * @property string $table
 * @property string $conn
 * @property int _id
 * @property string    $action
 * @property string    $origin_data
 * @property string    $diff
 * @property string    $table_id
 * @property string    $updated_by
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property \DateTime $keep_to
 */
class ZLog extends BaseModel
{
    public static $enabled = true;
    protected $table = '_logs';

    public function setDiff($data, array $origin)
    {
        $diff = [];

        foreach ($data as $key => $value) {
            $diff[$key] = [
                $origin[$key] ?? null,
                $value,
            ];
        }
        $this->diff = json_encode($diff, 128);
        $this->origin_data = json_encode($origin, 128);
    }

    public static function addUpdate($diff, $table, $connection = 'mysql')
    {
        $zLog = new static();
        $zLog->conn = $connection;
        $zLog->setAttribute('table', $table);
        $zLog->updated_by = auth()->user()->email;
        $newDiff = [];

        foreach ($diff as $key => $value) {
            $newDiff[$key] = [
                null, $value,
            ];
        }
        $zLog->diff = json_encode($newDiff, 128);
        $zLog->origin_data = '{}';
        $zLog->keep_to = date('Y-m-d H:i:s', strtotime('+15 days'));
        $zLog->action = 'UPDATE';
        $zLog->save();
    }
}
