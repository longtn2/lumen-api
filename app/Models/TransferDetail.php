<?php
namespace App\Models;

 /**
 * @property int       $id
 * @property string    $code
 * @property string    $description
 * @property int       $productId
 * @property int       $transferId
 * @property int       $inventoryId
 * @property int       $productUnitId
 * @property int       $productBatchId
 * @property int       $conversionValue
 * @property string    $productCode
 * @property string    $productName
 * @property int       $sendQuantity
 * @property int       $receiveQuantity
 * @property int       $status
 * @property string    $price
 * @property string    $sendPrice
 * @property string    $receivePrice
 */
class TransferDetail extends BaseModel
{
    protected $table = 'transfer_details';

    public function productUnits()
    {
        return $this->hasMany(ProductUnit::class, 'productId', 'productId');
    }

    public function productUnit()
    {
        return $this->belongsTo(ProductUnit::class, 'productUnitId');
    }

    public function productBatch() {
        return $this->belongsTo(ProductBatch::class, 'productBatchId', 'id');
    }

    public function inventory() {
        return $this->belongsTo(Inventory::class, 'inventory_id');
    }

}
