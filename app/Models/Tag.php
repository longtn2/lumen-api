<?php

namespace App\Models;

/**
 * @property string $name
 * @property string $description
 * @property int    $tagTypeId
 * @property int    $id
 */
class Tag extends BaseModel
{
    protected $table = 'tags';
    protected $fillable = ['name', 'tagTypeId', 'description'];

    /**
     * @param Tag[]  $tags
     * @param string $modelClass
     */
    public static function attachItemCount($tags, $modelClass)
    {
        $ids = [];
        foreach ($tags as $tag) {
            $ids[] = $tag->id;
        }
        /**
         *
         */

        $countMap = $modelClass::query()
            ->whereIn('tagId', $ids)
            ->selectRaw('tagId, count(*) `count`')
            ->groupBy('tagId')
            ->get()
            ->pluck('count', 'tagId')
            ->toArray();

        foreach ($tags as $tag) {
            $tag->itemCount = $countMap[$tag->id] ?? 0;
        }
    }
}
