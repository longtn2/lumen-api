<?php
namespace App\Models;

 /**
 * @property int       $id
 * @property string    $code
 * @property string    $type
 * @property string    $refCode
 * @property string    $description
 * @property int       $fromBranchId
 * @property int       $toBranchId
 * @property int       $inventoryId
 * @property string    $isActive
 * @property string    $receivedDate
 * @property int       $retailerId
 * @property int       $totalProduct
 * @property int       $status
 * @property int       $totalSendQuantity
 * @property int       $totalReceiveQuantity
 * @property string    $transferDetails
 */
class Transfer extends BaseModel
{
    protected $table = 'transfers';
    protected $fillable = [
        'code',
        'refCode',
        'description',
        'fromBranchId',
        'toBranchId',
        'isActive',
        'receivedDate',
        'retailerId',
        'status',
        'transferDetails',
        'sendDescription',
        'receiveDescription',
        'totalSendQuantity',
        'totalReceiveQuantity',
        'type',
        'createdBy',
        'receivedBy',
    ];

    const STATUS_DRAFT = 1;
    const STATUS_INPROGRESS = 2;
    const STATUS_SUCCEED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_NOT_ACCEPT = 5;

    const STATUS_NAME_TRANSFER_MAP = [
        self::STATUS_DRAFT => 'Phiếu tạm',
        self::STATUS_INPROGRESS => 'Đang chuyển',
        self::STATUS_SUCCEED => 'Thành công',
        self::STATUS_CANCELED => 'Đã hủy',
    ];
    const STATUS_NAME_REQUEST_MAP = [
        self::STATUS_DRAFT => 'Phiếu tạm',
        self::STATUS_INPROGRESS => 'Đang xin',
        self::STATUS_SUCCEED => 'Thành công',
        self::STATUS_CANCELED => 'Đã hủy',
        self::STATUS_NOT_ACCEPT => 'Từ chối',
    ];


    public function fromBranch() {
        return $this->belongsTo(Branch::class, 'fromBranchId');
    }
    public function toBranch() {
        return $this->belongsTo(Branch::class, 'toBranchId');
    }
    public function createdByUser() {
        return $this->belongsTo(User::class, 'createdBy');
    }
    public function receivedByUser() {
        return $this->belongsTo(User::class, 'receivedBy');
    }

    public function productUnit() {
        return $this->belongsTo(ProductUnit::class, 'productUnitId');
    }

    public function transferDetail() {
        return $this->hasMany(TransferDetail::class, 'transferId');
    }
}
