<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $userId
 * @property int       $roleId
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class SysUserRole extends BaseModel
{
    public $timestamps = false;
    protected $table = 'sys_user_roles';

    public function role()
    {
        return $this->belongsTo(SysRole::class, 'roleId');
    }


}
