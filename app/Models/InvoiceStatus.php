<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $invoiceId
 * @property int $value
 * @property string $name
 * @property string $note
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class InvoiceStatus extends BaseModel
{
    protected $table = 'invoice_status';
    protected $fillable = [
        'id',
        'invoiceId',
        'value',
        'name',
        'note'
    ];
}
