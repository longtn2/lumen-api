<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $code
 * @property string $orderDate
 * @property int $branchId
 * @property int $supplierId
 * @property int $inventoryPlanId
 * @property int $retailerId
 * @property int $userId
 * @property string $description
 * @property int $status
 * @property int $discountRatio
 * @property int $productQty
 * @property string $discount
 * @property string $createdDate
 * @property int $createdBy
 * @property string $orderSupplierDetails
 * @property string $total
 * @property string $exReturnSuppliers
 * @property string $exReturnThirdParty
 * @property string $totalAmt
 * @property int $totalQty
 * @property int $totalQuantity
 * @property int $totalProductType
 * @property string $subTotal
 * @property int $paidAmount
 * @property string $toComplete
 * @property string $statusValue
 * @property string $viewPrice
 * @property int $supplierDebt
 * @property int $supplierOldDebt
 * @property string $purchaseOrderCodes
 */
class OrderSupplier extends BaseModel
{
    protected $table = 'order_suppliers';
    protected $casts = [
        'expectedAt' => 'datetime'
    ];
    protected $fillable = [
        'code',
        'orderDate',
        'branchId',
        'retailerId',
        'userId',
        'description',
        'status',
        'discountRatio',
        'productQty',
        'discount',
        'createdDate',
        'createdBy',
        'updatedBy',
        'orderBy',
        'orderSupplierDetails',
        'total',
        'exReturnSuppliers',
        'exReturnThirdParty',
        'totalAmt',
        'totalQty',
        'totalQuantity',
        'totalProductType',
        'subTotal',
        'paidAmount',
        'toComplete',
        'statusValue',
        'viewPrice',
        'supplierDebt',
        'supplierOldDebt',
        'purchaseOrderCodes',
        'supplierId',
        'createdAt',
        'updatedAt',
        'expectedAt'
    ];

    const STATUS_DRAFT = 0;
    const STATUS_CONFIRMED = 1;
    const STATUS_IMPORTED = 2;
    const STATUS_FINISHED = 3;
    const STATUS_CANCELLED = 4;

    const STATUS_NAME_MAP = [
        self::STATUS_DRAFT => 'Phiếu tạm',
        self::STATUS_CONFIRMED => 'Đã xác nhận NCC',
        self::STATUS_IMPORTED => 'Đã nhập một phần',
        self::STATUS_FINISHED => 'Hoàn thành',
        self::STATUS_CANCELLED => 'Đã hủy',
    ];

    public function toArray()
    {
        $output = parent::toArray();

        $output['statusName'] =  self::STATUS_NAME_MAP[$this->status] ?? '';


        return $output;
    }

    public function getStatusName(): string
    {
        return self::STATUS_NAME_MAP[$this->status] ?? '';
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, "supplierId", "id");
    }
    <?php

    namespace App\Models;
    
    
    class OrderSupplierDetail extends BaseModel
    {
        protected $table = 'order_supplier_details';
        protected $casts = [
            'inventories' => 'array',
            'productBranchMeta' => 'array',
        ];
        protected $fillable = [
            'id',
            'orderSupplierId',
            'productId',
            'productUnitId',
            'subTotal',
            'quantity',
            'price',
            'inventories',
            'orderByNumber',
            'allocation',
            'description',
            'discount',
            'createdAt',
            'updatedAt'
        ];
    
        public function product()
        {
            return $this->hasOne(Product::class, "id", "productId");
        }
    
        public static function getOrderSupplierSumByProductMap($productIds, $currentBranchID) {
            return OrderSupplierDetail::query()
                ->selectRaw('SUM(`order_supplier_details`.`quantity` * `pu`.`conversionValue`) as supplier_sum, order_supplier_details.productId')
                ->join('order_suppliers as os', 'os.id', '=', 'order_supplier_details.orderSupplierId')
                ->join('product_units as pu', 'pu.id', '=', 'order_supplier_details.productUnitId')
                ->whereIn('order_supplier_details.productId', $productIds)
                ->where('os.status', OrderSupplier::STATUS_CONFIRMED)
                ->where('os.branchId', $currentBranchID)
                ->groupBy('order_supplier_details.productId')
                ->get()
                ->pluck('supplier_sum', 'productId');
        }
    
        public function orderSupplier()
        {
            return $this->belongsTo(OrderSupplier::class, 'orderSupplierId', 'id');
        }
    
        public function productUnit()
        {
            return $this->belongsTo(ProductUnit::class, 'productUnitId', 'id');
        }
    }
    
}
