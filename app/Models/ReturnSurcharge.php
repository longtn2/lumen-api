<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $invoiceId
 * @property int $surchargeId
 * @property double $amount
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class ReturnSurcharge extends BaseModel
{
    protected $table = 'return_surcharges';
    protected $fillable = [
        'returnId',
        'surchargeId',
        'amount',
    ];

    public function surcharge() {
        return $this->belongsTo(Surcharge::class, 'surchargeId');
    }
}
