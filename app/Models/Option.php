<?php
namespace App\Models;

 /**
 * @property int       $id
 * @property string    $name
 * @property string    $value
 * @property string    $values
 * @property int       $status
 * @property string    $title
 * @property string    $type
 * @property int       $typeValue
 * @property string    $description
 * @property string    $createdAt
 * @property string    $updatedAt
 * @property string    $deletedAt
 */
class Option extends BaseModel
{
    protected $table = 'options';

    protected $casts = [
        'values' => 'array',
        'value' => 'object'
    ];
    protected $fillable = [
    'name',
    'value',
    'values',
    'status',
    'title',
    'type',
    'typeValue',
    'description',
];
}
