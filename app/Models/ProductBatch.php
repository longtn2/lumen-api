<?php

namespace App\Models;

/**
 * @property int       $id
 * @property string    $code
 * @property int       $productUnitId
 * @property int       $productId
 * @property int       $quantity
 * @property \DateTime $expireDate
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property string    $name
 * @property string    $fullNameVirgule
 */
class ProductBatch extends BaseModel
{
    const TYPE_OUT_OF_DATE = 1;
    const TYPE_NOT_OUT_OF_DATE = 2;
    protected $table = 'product_batches';
    protected $fillable = [
        'name', 'expireDate', 'productId', 'productUnitId', 'quantity'
    ];

    public function getFormattedName(): string
    {
        return $this->name . ' - ' . appDateFormat($this->expireDate);
    }


    public function toArray()
    {
        $output = parent::toArray();

        $output['nameFormatted'] =  $this->getFormattedName();

        return $output;
    }
}
