<?php

namespace App\Models;

class SysRolePermission extends BaseModel
{
    public $timestamps = false;
    protected $table = 'sys_role_permissions';
    protected $fillable = ['roleId', 'permissionId'];

    public function permission()
    {
        return $this->belongsTo(SysPermission::class, 'permissionId');
    }
}
