<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $name
 * @property boolean $isActive
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class ServiceType extends BaseModel
{
    protected $table = 'service_types';
    protected $fillable = [
        'name',
        'isActive'
    ];

    public static function getNameFromId($id) {
        $entry = self::query()->find($id);

        if (!$entry) {
            return null;
        }

        return $entry->name;
    }
}
