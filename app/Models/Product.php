<?php

namespace App\Models;

use App\Models\Casts\StringSplitCast;
use OmiCare\Http\Request;

/**
 * @property int $id
 * @property string $retailerId
 * @property string $code
 * @property string $name
 * @property string $fullName
 * @property int $categoryId
 * @property string $categoryName
 * @property int $allowsSale
 * @property int $type
 * @property double $basePrice
 * @property string $unit
 * @property int $conversionValue
 * @property string $description
 * @property string $modifiedDate
 * @property string $createdDate
 * @property int $isActive
 * @property string $units
 * @property string $inventories
 * @property int $quantity
 * @property int $unitAutoPrice
 * @property string $note
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property int $status
 */
class Product extends BaseModel
{
    protected $table = 'products';
    protected $casts = [
        'branchIDs' => StringSplitCast::class,
        'categoryId' => StringSplitCast::class,
        'images' => 'array',
        'unitAutoPrice' => 'bool'
    ];

    protected $fillable = [
        'retailerId',
        'code',
        'name',
        'fullName',
        'categoryId',
        'categoryName',
        'allowsSale',
        'type',
        'basePrice',
        'unit',
        'conversionValue',
        'description',
        'modifiedDate',
        'createdDate',
        'isActive',
        'units',
        'inventories',
        'branchIDs',
        'quantity',
        'note',
        'status',
        'activeIngredient',
        'images',
        'weight',
        'capitalPrice',
        'manufacturer',
        'country',
        'position',
        'unitAutoPrice'
    ];

    const TYPE_PRODUCT_COMMON = 1;
    const TYPE_PRODUCT_SERVICE = 2;

    public function productUnits()
    {
        return $this->hasMany(ProductUnit::class, 'productId', 'id')
            ->selectRaw('id,unitName,productId,conversionValue,price')
            ->orderBy('conversionValue', 'asc');
    }

    public function productBatches() {
        return $this->hasMany(ProductBatch::class, 'productId')->orderBy('expireDate', 'asc');
    }

    public function productBranchMeta() {
        return $this->hasMany(ProductBranchMeta::class, 'productId');
    }

    public function currentBranchMeta() {
        return $this->hasOne(ProductBranchMeta::class, 'productId')->where('branchId', request()->currentBranchId());
    }

    /**
     * @param Product $product
     * @return bool
     */
    public static function isTypeService($product) {
        return $product->type == self::TYPE_PRODUCT_SERVICE;
    }

    public function saveHistory($action, $options = [])
    {
        /**
         * @var User $user
         */
        $user = auth();
        $ignored = ['updated_at' => true, 'created_by' => true, 'created_at' => true];

        if (!$user) {
            $user = new User();
            $user->id = 0;
            $user->name = 'Bot';
            $user->email = 'omipos-bot@omicare.vn';
        }

        $req = new Request();
        $currentBranchID = $req->header('X-Branch-Id');

        $log = new History();
        $log->refTable = $this->getTable();
        $log->refId = $this->id;
        $log->branchId = $currentBranchID;
        $log->action = $action;
        $log->diff = [];
        $log->createdBy = $user->id;

        switch ($action) {
            case History::ACTION_CREATED:
                $content = 'Thêm mới sản phẩm: ' . $this->code . ', tên SP: ' . $this->name . ', ' . Category::decodeContentHistory($this->categoryId)
                .', Giá tiền: '. $this->basePrice. ', Hoạt chất: '. $this->activeIngredient . ', Hãng sản xuất: '. $this->manufacturer
                .', Nước sản xuất: '.$this->country. ', Quy cách đóng gói: '.$this->unit. ', Mô tả: '. $this->description;
                $log->content = $content;
                $log->save();

                break;
            case History::ACTION_UPDATED:
                $origins = $this->getOriginal();
                $changes = $this->getDirty();

                $content = 'Cập nhật thông tin SP: ' . $this->code;

                if (isset($origins['categoryId']) && isset($changes['categoryId']) && $origins['categoryId'] != $changes['categoryId']) {
                    $content .= ', Danh mục: ' . Category::decodeContentHistory($origins['categoryId']) . ' -> ' . Category::decodeContentHistory($changes['categoryId']);
                }
                if (isset($origins['name']) && isset($changes['name']) && $origins['name'] != $changes['name']) {
                    $content .= ', Họ tên: ' . $origins['name'] . ' -> ' . $changes['name'];
                }
                $log->content = $content;

                $diff = [];

                foreach ($changes as $key => $value) {
                    $oldVal = $origins[$key] ?? null;

                    if (!isset($ignored[$key]) && $oldVal != $value) {
                        $diff[$key] = [$oldVal, $value];
                    }
                }

                if (count($diff)) {
                    $log->diff = $diff;
                    $log->save();
                }

                break;
            case History::ACTION_EXPORT:
                $log->content = 'Xuất file Danh Sách Hàng hóa';
                $log->save();

                break;
            default:

                break;
        }

        return true;
    }
}
