<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property int $status
 */
class Ward extends BaseModel
{
    protected $table = 'local_wards';

    public static function getNameById($id) {
        $entry = self::find($id);

        if (!$entry) {
            return null;
        }

        return $entry->name;
    }
}
