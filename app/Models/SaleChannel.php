<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $name
 * @property boolean $isActive
 * @property boolean $isDefault
 * @property string $img
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class SaleChannel extends BaseModel
{
    protected $table = 'sale_channels';
    protected $fillable = [
        'name',
        'isActive',
        'isDefault',
        'img',
        'color'
    ];

    protected $casts = [
        'img' => 'object'
    ];

    public static function setDefault() {
        $firstEntry = SaleChannel::first();

        if ($firstEntry) {
            $firstEntry->isDefault = 1;
            $firstEntry->save();
        }

        return true;
    }

    public static function getDefaultId() {
        $saleChannel = SaleChannel::query()
            ->where('isDefault', 1)
            ->first();

        return $saleChannel->id ?? null;
    }

    public static function getNameFromId($id) {
        $entry = self::query()->find($id);

        if (!$entry) {
            return null;
        }

        return $entry->name;
    }
}
