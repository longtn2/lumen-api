<?php


namespace App\Models;


use App\Traits\Treeable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int       $id
 * @property string    $name
 * @property string    $class
 * @property string    $action
 * @property int       $parentId
 * @property string    $displayName
 * @property string    $note
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class SysPermission extends BaseModel
{
    const ROOT_ID = 1;
    use Treeable;
    protected $autoSchema = false;
    protected $table = 'sys_permissions';
    protected $fillable = [
        'displayName', 'name', 'parentId', 'action'
    ];

    public function parent() {
        return $this->belongsTo(SysPermission::class, 'parentId');
    }

    public function children() {
        return $this->hasMany(SysPermission::class, 'parentId');
    }

    /**
     * @param $id
     * @return SysPermission|null
     */
    public static function findPermissionById($id)
    {
        static $map;

        if (!$map) {
            $entries = SysPermission::query()->selectRaw('id,name,displayName,parentId')->get();
            foreach ($entries as $entry) {
                $map[$entry->id] = $entry;
            }
        }

        return $map[$id] ?? null;
    }

    public static function findOrCreate(string $class, string $action): SysPermission
    {
        $permission = SysPermission::query()
            ->selectRaw('id,name,displayName,parentId')
            ->where('class', $class)
            ->where('action', $action)
            ->first();

        if ($permission) {
            return $permission;
        }

        $tmp = explode('\\', $class);
        $name = last($tmp);

        $parent = SysPermission::where('class', $class)->first();
        if (!$parent) {
            $parent = new SysPermission();
            $parent->name = $name;
            $parent->class = $class;
            $parent->action = '*';
            $parent->parentId = SysPermission::ROOT_ID;
            $parent->displayName = $class;
            $parent->note = 'Hệ thống tự động thêm';
            $parent->save();
        }

        $permission = new SysPermission();

        $permission->class = $class;
        $permission->action = $action;
        $permission->name = $name. '.' . $action;
        $permission->parentId = $parent->id;
        $permission->note = 'Hệ thống tự động thêm';
        $permission->displayName = $permission->name;
        $permission->save();


        return $permission;
    }

}
