<?php

namespace App\Models;

use App\Types\Inventory\InventoryOnHandDetail;
use OmiCare\Utils\DB;

/**
 * @property int       $id
 * @property int       $branchId
 * @property int       $productId
 * @property int       $productUnitId
 * @property int       $productBatchId
 * @property string    $refCode
 * @property string    $parentRefCode
 * @property int       $refId
 * @property string    $refTable
 * @property int       $onHand
 * @property int       $onHold
 * @property int       $originalTotal
 * @property int       $reserved
 * @property int       $actualReserved
 * @property int       $conversionValue
 * @property int       $supplierId
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class Inventory extends BaseModel
{
    protected $table = 'inventories';

    const REF_CODE_ORDER_DETAIL = 'order_details';
    const REF_CODE_INVOICE_DETAIL = 'invoice_details';
    const REF_CODE_RETURN_DETAIL = 'return_details';

    public function productBatch() {
        return $this->belongsTo(ProductBatch::class, 'productBatchId', 'id');
    }

    public function branch() {
        return $this->belongsTo(Branch::class, 'branchId', 'id');
    }
    public function product() {
        return $this->belongsTo(Product::class, 'productId');
    }

    public function productUnit() {
        return $this->belongsTo(ProductUnit::class, 'productUnitId', 'id');
    }

    public function purchaseOrderDetail() {
        return $this->belongsTo(PurchaseOrderDetail::class, 'id', 'inventoryId');
    }

    public function supplier() {
        return $this->belongsTo(Supplier::class, 'id', 'supplierId');
    }

    /**
     * Bổ sung thông tin tồn kho của tất cả branch
     * @param $products
     */
    public static function attachInventoryAll($products)
    {
        $productIDs = [];
        foreach ($products as $product) {
            $productIDs[] = $product->id;
        }

        $invInfo = DB::table('inventories')
            ->selectRaw('productId,branchId, SUM(onHand*conversionValue) onHand')
            ->whereIn('productId', $productIDs)
            ->groupBy('productId')
            ->get()->groupBy('productId');



        foreach ($products as $product) {
            $product->inventories = $invInfo[$product->id] ??  [];
        }

    }

    public static function attachInventoryInfo($products, $branchId)
    {
        $productIDs = [];
        foreach ($products as $product) {
            $productIDs[] = $product->id;
        }

        $invInfo = DB::table('inventories')
            ->selectRaw('productId,SUM(onHand*conversionValue) onHand')
            ->whereIn('productId', $productIDs)
            ->where('branchId', $branchId)
            ->groupBy('productId')
            ->get()->pluck('onHand', 'productId')->toArray();

        foreach ($products as $product) {
            $product->inventory = [
                'onHand' => intval($invInfo[$product->id] ?? 0)
            ];
        }

    }

    /**
     * @param $branchId
     * @param $refCode
     * @param array $invProductBatchIds
     * @return InventoryOnHandDetail[]
     */
    public static function findInventoryByRefCode($branchId, $refCode, array $invProductBatchIds)
    {
        $inventoryMap = [];
        $inventories = DB::table('inventories')
            ->selectRaw('productBatchId,parentRefCode,SUM(onHand*conversionValue) sumAll')
            ->whereIn('productBatchId', $invProductBatchIds)
            ->where('branchId', $branchId)
            ->groupBy('productBatchId')
            ->groupBy('parentRefCode')
            ->when($refCode, function($query) use($refCode) {
                // $query->whereIn('parentRefCode', [$refCode]);
            })
            ->get();

        foreach ($inventories as $iv) {
            $parentRefCode = $iv->parentRefCode ?? 'null';
            $inventoryMap[$iv->productBatchId][$parentRefCode][] = (int)$iv->sumAll;
        }

        $results  = [];
        foreach ($inventoryMap as $productBatchId => $group) {
            $invOnHandDetail = new InventoryOnHandDetail();


            foreach ($group as $ref => $values) {
                if ($ref === $refCode) {
                    $invOnHandDetail->current += array_sum($values);
                } else {
                    $invOnHandDetail->others  += array_sum($values);
                }
            }

            $invOnHandDetail->all = $invOnHandDetail->current + $invOnHandDetail->others;
            $results[$productBatchId] = $invOnHandDetail;
        }


        return $results;
    }


    public static function getActualInventoryMap($productIds, $currentBranchID) {
        return Inventory::query()
            ->whereIn('productId', $productIds)
            ->where('refTable', '!=', Inventory::REF_CODE_ORDER_DETAIL)
            ->selectRaw('id, productId, branchId, SUM(onHand*conversionValue) as quantity')
            ->where('branchId', $currentBranchID)
            ->groupBy('productId')
            ->pluck('quantity', 'productId');
    }

    public static function getSystemInventoryMap($productIds, $currentBranchID) {
        return Inventory::query()
            ->whereIn('productId', $productIds)
            ->selectRaw('id, productId, branchId, SUM(onHand*conversionValue) as quantity')
            ->where('branchId', $currentBranchID)
            ->groupBy('productId')
            ->pluck('quantity', 'productId');
    }

    public static function getInventoryProductBatchMap($productIds, $currentBranchID) {
        return Inventory::query()
            ->selectRaw('*,SUM(onHand*conversionValue) as quantity')
            ->whereIn('productId', $productIds)
            ->where('branchId', $currentBranchID)
            ->whereNotNull('productBatchId')
            ->havingRaw('quantity > 0')
            ->orderBy('onHand', 'desc')
            ->groupBy('productBatchId')
            ->pluck('quantity', 'productBatchId');
    }
}
