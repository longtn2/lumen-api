<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $couponId
 * @property string    $code
 * @property int       $status
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property \DateTime $usedAt
 * @property int       $customerId
 */
class CouponCode extends BaseModel
{
    protected $table = 'coupon_codes';

    const STATUS_ACTIVE = 0;
    const STATUS_USED = 1;

    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'couponId', 'id');
    }
}
