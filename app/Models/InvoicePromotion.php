<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $invoiceId
 * @property int       $promotionId
 * @property int       $discountAmount
 * @property int       $discountValue
 * @property int       $discountType
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property array     $discountProducts
 * @property array     $coupons
 * @property array     $quantity
 */
class InvoicePromotion extends BaseModel
{
    protected $table = 'invoice_promotions';
    protected $casts = [
        'discountProducts' => 'array',
        'coupons' => 'array',
    ];
}
