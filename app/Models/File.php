<?php

namespace App\Models;

use App\Utils\ExcelHelper;

/**
 * @property int       $id
 * @property string    $name
 * @property string    $path
 * @property string    $hash
 * @property string    $url
 * @property int       $size
 * @property string    $type
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property string    $uploadedBy
 * @property int       $isImage
 * @property string    $extension
 * @property string    $group
 * @property int       $userId
 * @property int       $width
 * @property int       $height
 */
class File extends BaseModel
{
    protected $table = 'files';
    protected $casts = [
        'is_image' => 'bool'
    ];

    public function toExcelData(): array
    {
        return ExcelHelper::convertToArray($this->path);
    }
}
