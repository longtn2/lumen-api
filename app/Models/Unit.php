<?php

namespace App\Models;

/**
 * @property int       $id
 * @property string    $name
 */
class Unit extends BaseModel
{
    protected $table = 'units';
    protected $fillable = [
        'name',

    ];

    public static function getUnitName($id): string
    {
        static $map;
        if (!$map) {
            $map = Unit::get()->pluck('name', 'id');
        }

        return $map[$id] ?? '';
    }
}
