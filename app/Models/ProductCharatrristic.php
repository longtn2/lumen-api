<?php
namespace App\Models;
use Illuminate\Support\Collection;
 /**
 * @property int       $id
 * @property string    $name
 * @property int       $status
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class ProductCharateristic extends BaseModel 
{
    protected $table = 'product_charateristic';
    protected $fillable = ['id', 'name', 'created_at', 'updated_at' ];
}