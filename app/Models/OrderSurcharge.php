<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $orderId
 * @property int $surchargeId
 * @property double $amount
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class OrderSurcharge extends BaseModel
{
    protected $table = 'order_surcharges';
    protected $fillable = [
        'orderId',
        'surchargeId',
        'amount',
    ];

    public function surcharge() {
        return $this->belongsTo(Surcharge::class, 'surchargeId');
    }
}
