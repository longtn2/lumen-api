<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $invoiceId
 * @property int $surchargeId
 * @property double $amount
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class InvoiceSurcharge extends BaseModel
{
    protected $table = 'invoice_surcharges';
    protected $fillable = [
        'invoiceId',
        'surchargeId',
        'amount',
    ];

    public function surcharge() {
        return $this->belongsTo(Surcharge::class, 'surchargeId');
    }
}
