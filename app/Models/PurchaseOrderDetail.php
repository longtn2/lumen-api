<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $purchaseOrderId
 * @property int       $refId
 * @property int       $productId
 * @property string    $productCode
 * @property string    $productName
 * @property int       $quantity
 * @property double    $price
 * @property double    $discount
 * @property string    $productBatchExpire
 * @property int       $productBatchId
 * @property string    $discountRatio
 * @property int       $productUnitId
 * @property int       $conversionValue
 * @property int       $inventoryId
 * @property int       $inventoryPlanId
 * @property Inventory $inventory
 * @property array     $discountObject
 */
class PurchaseOrderDetail extends BaseModel
{
    protected $table = 'purchase_order_details';
    protected $fillable = [
        'purchaseOrderId', 'productId', 'productUnitId', 'productCode', 'productName', 'quantity', 'price', 'discount',
        'productBatchId', 'conversionValue', 'quantityReturn', 'refId', 'inventoryId', 'discountObject', 'inventoryPlanId',
        'discountObject'
    ];
    protected $casts = [
        'discountObject' => 'array'
    ];

    public function purchaseOrder()
    {
        return $this->belongsTo(PurchaseOrder::class, 'purchaseOrderId', 'id')
            ->with('supplier');
    }

    public function productBatch() {
        return $this->belongsTo(ProductBatch::class, 'productBatchId', 'id');
    }

    /**
     * Trường hợp phiếu nhập
     * @return
     */
    public function inventory() {
        return $this->belongsTo(Inventory::class, 'inventoryId', 'id');
    }

    /**
     * Trường hợp phiếu trả hàng nhập
     * @return
     */
    public function refInventory() {
        return $this->belongsTo(Inventory::class, 'refInventoryId', 'id');
    }
}
