<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $productId
 * @property int $productUnitId
 * @property string $productCode
 * @property int $invoiceId
 * @property string $productName
 * @property int $quantity
 * @property int $discount
 * @property int $quantityDiscount
 * @property double $price
 * @property string $discountType
 * @property int $discountValue
 * @property double $subTotal
 * @property string $serialNumbers
 * @property string $discountExplain
 * @property string $productBatchExpire
 * @property int $returnQuantity
 * @property string $note
 * @property int $usePoint
 * @property int $isPromotion
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class InvoiceDetail extends BaseModel
{
    protected $table = 'invoice_details';
    protected $fillable = [
        'productId',
        'productUnitId',
        'productCode',
        'invoiceId',
        'productName',
        'quantity',
        'price',
        'discountType',
        'discountValue',
        'subTotal',
        'serialNumbers',
        'productBatchExpire',
        'returnQuantity',
        'note',
        'usePoint',
    ];

    public function inventories() {
        return $this->hasMany(Inventory::class, 'refId')
            ->where('refTable', Inventory::REF_CODE_INVOICE_DETAIL);
    }

    public function invoice() {
        return $this->belongsTo(Invoice::class, 'invoiceId');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'productId');
    }
    public function productUnit() {
        return $this->belongsTo(ProductUnit::class, 'productUnitId');
    }

    public static function decodeContentHistory($invoiceId) {
        $details = self::query()
            ->with(['inventories.productBatch'])
            ->where('invoiceId', $invoiceId)
            ->get();

        $result = '';
        foreach ($details as $detail) {
            /**
             * @var self $detail
             */
            $result .= '- ' . $detail->productName . ': ' . $detail->quantity . '*' . number_format($detail->subTotal) . ', bao gồm: <br/>';

            foreach ($detail->inventories as $inventory) {
                /**
                 * @var Inventory $inventory
                 * @var ProductBatch $productBatch
                 */
                $productBatch = $inventory->productBatch;
                if ($productBatch) {
                    $result .= '&thinsp;+ ' . $productBatch->name . ' - ' . date('d/m/Y', strtotime($productBatch->expireDate))
                        . ' - SL: ' . abs($inventory->onHand) . '<br/>';
                }
            }
        }

        return $result;
    }
}
