<?php

namespace App\Models\Casts;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class StringSplitCast implements CastsAttributes
{
    public function get($model, $key, $value, $attributes)
    {
        if (!$value) {
            return [];
        }

        $values = explode(',', $value);
        return $values ?? [];
    }

    /**
     * Prepare the given value for storage.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  array  $value
     * @param  array  $attributes
     * @return string
     */
    public function set($model, $key, $value, $attributes)
    {
        if (is_array($value)) {
            return implode(',', $value);
        }

        return null;
    }
}
