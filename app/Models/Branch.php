<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property int $status
 * @property int $requiredOrderApproval
 * @property int $isDrugStore
 */
class Branch extends BaseModel
{
    protected $table = 'branches';
    const KHO_TONG_ID = 55215;
    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
        'provinceId',
        'districtId',
        'wardId',
        'status',
        'requiredOrderApproval',
        'isDrugStore'
    ];

    public function province() {
        return $this->belongsTo(Province::class, 'provinceId');
    }
    public function district() {
        return $this->belongsTo(District::class, 'districtId');
    }
    public function ward() {
        return $this->belongsTo(Ward::class, 'wardId');
    }

    public static function getAllIDs(): array
    {
        static $all;
        if ($all) {
            return $all;
        }

        $all = Branch::where('status', 1)->get()->pluck('id')->all();
        return $all;
    }

    public static function getUserBranches() {
        $user = auth();

        if (!$user) {
            return [];
        }

        $branches =  Branch::query()
            ->with(['province', 'district', 'ward'])
            ->where('status', 1)
            ->whereIn('id', $user->branchIDs)
            ->get();

        foreach ($branches as $branch) {
            $branch->addressDecode = $branch->address . " - " . ($branch->province->name ?? "") . " - "
                . ($branch->district->name ?? "") . " - " . ($branch->ward->name ?? "");
        }

        return $branches;
    }

    public static function decode($branchId, $showPhone = true) {
        $branch = Branch::query()
            ->with(['province', 'district', 'ward'])
            ->find($branchId);

        if (!$branch) {
            return '';
        }

        $decode = '';
        if ($showPhone) {
            $decode .= $branch->phone . " - ";
        }

        $decode .= $branch->address . ", " . ($branch->province->name ?? "") . ", " . ($branch->district->name ?? "")
            . ", " . ($branch->ward->name ?? "");

        return $decode;
    }

    public static function getNameById($branchId) {
        $branch = Branch::query()
            ->find($branchId);

        if (!$branch) {
            return '';
        }

        return $branch->name;
    }
}