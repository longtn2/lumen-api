<?php

namespace App\Models;

class OmiSyncLink extends BaseModel
{
    public $timestamps = false;
    protected $table = 'omi_sync_link';
    protected $fillable = [
        'id', 'omi_prd_id', 'omi_prd_name', 'sync_prd_id', 'sync_prd_name', 'shop_id', 'sale_channel_id', 'status', 'message', 'createdAt', 'updatedAt', 'deletedAt'
    ];
}
