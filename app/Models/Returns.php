<?php

namespace App\Models;

// className 'Return' is not usable
class Returns extends BaseModel
{
    protected $table = 'returns';

    const STATUS_COMPLETE = 1;
    const STATUS_CANCELED = 2;

    public function details() {
        return $this->hasMany(ReturnDetail::class, 'returnId');
    }

    public static function generateCode()
    {
        /**
         * @var self latestEntry
         */
        $latestEntry = self::query()
            ->orderByDesc('id')
            ->first();

        if (!$latestEntry) {
            return 'TH100000';
        }

        $latestCode = $latestEntry->code;
        if (($index = strrpos($latestCode, ".")) !== false) {
            $latestCode = substr($latestCode, 0, $index);
        }
        $latestCode = substr($latestCode, 2);

        $code = (substr($latestCode, 0, 1) >= 1) ? $latestCode + 1 : '100000';
        $maxLop = 25;
        while (self::where('code', 'TH' . $code)->count()) {
            $code++;

            if ($maxLop <= 0) {
                throw new \Exception('Không thể tạo mã trả hàng!');
            }

            $maxLop--;
        }

        return 'TH' . $code;
    }

    public static function getStatusLabel($status)
    {
        return $status == 2 ? 'Đã hủy' : 'Đã trả';
    }

    public static function getSystemInventoryMap($productIds, $currentBranchID) {
        return Inventory::query()
            ->whereIn('productId', $productIds)
            ->selectRaw('id, productId, branchId, SUM(onHand*conversionValue) as quantity')
            ->where('branchId', $currentBranchID)
            ->where('')
            ->groupBy('productId')
            ->pluck('quantity', 'productId');
    }
    public function receiver() {
        return $this->belongsTo(User::class, 'receivedById');
    }

}
