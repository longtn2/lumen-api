<?php
namespace App\Models;

 use Illuminate\Support\Collection;

 /**
 * @property int       $id
 * @property string    $name
 * @property int       $status
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class Category extends BaseModel
{
    protected $table = 'categories';
    protected $fillable = [
        'name',
        'status',
        'parentId'
    ];

    public function products() {
        return $this->hasMany(Product::class, 'categoryId', 'id');
    }

    public function parent() {
        return $this->belongsTo(Category::class, 'parentId', 'id');
    }
    public function children() {
        return $this->hasMany(Category::class, 'parentId', 'id');
    }

    public static function decodeContentHistory($categoryIds) {

        if (!$categoryIds) {
            $ids = [];
        } else {
            if (is_array($categoryIds)) {
                $ids = $categoryIds;
            } else {
                $ids = explode(',', $categoryIds);
            }

        }

        $details = self::query()
            ->whereIn('id', $ids)
            ->get();

        $result = ' ';
        foreach ($details as $detail) {
            /**
             * @var self $detail
             */
            $result .= $detail->name . ' - ';
        }

        return $result;
    }

    public static function getNameFromId($id) {
        $entry = self::query()->find($id);

        if (!$entry) {
            return null;
        }

        return $entry->name;
    }
}
