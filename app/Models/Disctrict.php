<?php

namespace App\Models;

/**
 * @property int $id
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property int $status
 */
class District extends BaseModel
{
    protected $table = 'local_districts';

    public static function getNameById($id) {
        $entry = self::find($id);

        if (!$entry) {
            return null;
        }

        return $entry->name;
    }
}
