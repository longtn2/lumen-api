<?php

namespace App\Models;

/**
 * @property int       $id
 * @property int       $productId
 * @property int       $branchId
 * @property int       $quantityThreshold
 * @property int       $quantityMin
 * @property int       $quantityMax
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class ProductBranchMeta extends BaseModel
{
    protected $table = 'product_branch_meta';
    protected $fillable = [
        'productId', 'branchId', 'onShowRoomQuantity', 'minimalQuantity', 'maximumQuantity'
    ];
}
