<?php

namespace App\Models;

class ProductCombo extends BaseModel
{
    protected $table = 'product_combos';

    public function productUnit()
    {
        return $this->hasOne(ProductUnit::class, 'id', 'productUnitId');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'productId');
    }

    public static function getComboProduct($parentProductId)
    {
        $products = ProductCombo::query()->where('parentProductId', $parentProductId)
            ->with(['productUnit', 'product'])
            ->get();


        $result = [];
        foreach ($products as $product) {
            $pu = $product->productUnit;
            $p = $product->product;


            $product->capitalPrice = $pu->price;
            $product->code = $p->code;
            $product->name = $p->name;
            $product->amount =  $product->capitalPrice * $product->quantity;
            $result[] = $product;
        }

        return $result;
    }
}
