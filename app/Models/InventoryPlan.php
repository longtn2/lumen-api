<?php

namespace App\Models;

/**
 * @property int       $id
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property int       $status
 * @property string    $createdBy
 * @property string    $code
 * @property int       $totalProduct
 * @property int       $processedProduct
 */
class InventoryPlan extends BaseModel
{
    protected $table = 'inventory_plans';
    const STATUS_NEW = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_FINISHED = 2;
    const STATUS_ERROR = 3;

    public function orderSupplier() {
        return $this->belongsTo(OrderSupplier::class, 'inventoryPlanId', 'id');
    }
}
