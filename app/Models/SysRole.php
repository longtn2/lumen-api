<?php


namespace App\Models;


use App\Models\Casts\StringSplitCast;
use Illuminate\Database\Eloquent\Model;

class SysRole extends BaseModel
{
    protected $table = 'sys_roles';
    protected $autoSchema = false;
    const ROLE_ROOT = 1;
    const ROLE_ADMIN = 2;
    const NORMAL_USER = 2;
    const EDITOR = 3;


    protected $fillable = [
        'displayName', 'name'
    ];

    public function getAllowedRoutes()
    {
        $routes = $this->routes;

        $routes = explode("\n", $routes);
        $routes = array_map('trim', $routes);
        $allowed = [];

        foreach ($routes as $route) {
            if (!$route) {
                continue;
            }

            if ($route[0] !== '#') {
                $allowed[] = $route;
            }
        }
        return $allowed;
    }

}
