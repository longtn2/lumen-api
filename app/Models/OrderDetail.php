<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $productId
 * @property int $productUnitId
 * @property int $orderId
 * @property string $productCode
 * @property string $productName
 * @property int $quantity
 * @property string $price
 * @property string $discountType
 * @property int $discountValue
 * @property string $subTotal
 * @property string $viewDiscount
 * @property string $note
 * @property string $productBatchExpire
 */
class OrderDetail extends BaseModel
{
    protected $table = 'order_details';
    protected $fillable = [
        'id',
        'productId',
        'productUnitId',
        'orderId',
        'productCode',
        'productName',
        'quantity',
        'price',
        'discountType',
        'discountValue',
        'subTotal',
        'viewDiscount',
        'note',
        'productBatchExpire'
    ];

    public function order() {
        return $this->belongsTo(Order::class, 'orderId', 'id');
    }

    public function inventories() {
        return $this->hasMany(Inventory::class, 'refId')
            ->where('refTable', Inventory::REF_CODE_ORDER_DETAIL);
    }

    public function productUnit() {
        return $this->belongsTo(ProductUnit::class, 'productUnitId');
    }

    public static function removeInventory($detailId) {
        Inventory::query()
            ->where('refId', $detailId)
            ->where('refTable', Inventory::REF_CODE_ORDER_DETAIL)
            ->delete();

        return true;
    }

    public static function updateQuantityInventory($detailId, $quantityChange) {
        $inventory = Inventory::query()
            ->where('refId', $detailId)
            ->where('refTable', Inventory::REF_CODE_ORDER_DETAIL)
            ->orderByDesc('id')
            ->first();

        if ($inventory) {
            $newQuantity = abs($inventory->onHand) - $quantityChange;

            if ($newQuantity) {
                $inventory->onHand = -1 * $newQuantity;
                $inventory->save();
            } else {
                $inventory->delete();
            }

            return true;
        }

        return false;
    }

    public static function getOrderDetailsSumByProductMap($productIds, $currentBranchID) {
        return OrderDetail::query()
            ->selectRaw('SUM(ABS(`i`.`onHand` * `i`.`conversionValue`)) as order_details_sum, order_details.productId')
            ->join('orders as o', 'o.id', '=', 'order_details.orderId')
            ->join('inventories as i', 'i.refId', '=', 'order_details.id')
            ->where('i.refTable', Inventory::REF_CODE_ORDER_DETAIL)
            ->whereIn('order_details.productId', $productIds)
            ->where('o.branchId', $currentBranchID)
            ->whereIn('o.status', [Order::STATUS_WAIT, Order::STATUS_CONFIRMED, Order::STATUS_DELIVERY])
            ->groupBy('order_details.productId')
            ->get()
            ->pluck('order_details_sum', 'productId');
    }

    public static function decodeContentHistory($orderId) {
        $details = self::query()
            ->with(['inventories'])
            ->where('orderId', $orderId)
            ->get();

        $result = '';
        foreach ($details as $detail) {
            /**
             * @var self $detail
             */
            $result .= '+ ' . $detail->productCode . ' - ' . $detail->productName . ': ' . $detail->quantity . '*' . number_format($detail->subTotal) . '<br/>';
        }

        return $result;
    }
}
