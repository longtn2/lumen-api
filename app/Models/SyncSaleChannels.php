<?php

namespace App\Models;

class SyncSaleChannels extends BaseModel
{
    //Add thêm nếu có liên kết khác
    const TYPE_SHOPEE = 0;
    const TYPE_LAZADA = 1;

    public $timestamps = false;
    protected $table = 'sync_sale_channels';
    protected $fillable = [
        'sale_channel_id',
        'identity_key',
        'name',
        'branch_id',
        'branch_name',
        'is_active',
        'auto_mapping_product',
        'sync_onhand',
        'dis_sale_products',
        'number_sells',
        'sync_order',
        'confirm_order_return',
        'sync_price',
        'price_list',
        'retailer_id',
        'total_product_connected',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public function getSaleChannel() {
        return $this->belongsTo(SaleChannel::class, 'id');
    }
    public function getProductSync() {
        return $this->hasMany(OmiSyncLink::class, 'shop_id', 'identity_key')
        ->join('products', 'omi_sync_link.omi_prd_id', '=', 'products.id')                
        ->where('omi_sync_link.status', 1)->whereNull('omi_sync_link.deletedAt');
    }
}
