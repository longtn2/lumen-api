<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $orderId
 * @property int $value
 * @property string $name
 * @property string $note
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class OrderStatus extends BaseModel
{
    protected $table = 'orders_status';
    protected $fillable = [
        'id',
        'orderId',
        'value',
        'name',
        'note'
    ];
}
