<?php
namespace App\Models;

 use OmiCare\Utils\DB;

 /**
 * @property int       $id
 * @property string    $code
 * @property string    $name
 * @property string    $codePrefix
 * @property double    $discountValue
 * @property double    $minInvoiceAmount
 * @property int       $allowManyCoupons
 * @property int       $status
 * @property \DateTime $startTime
 * @property \DateTime $endTime
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class Coupon extends BaseModel
{
    protected $table = 'coupons';
    private $applyToData = [];

    protected $fillable = [
        'code',
        'name',
        'discountValue',
        'minInvoiceAmount',
        'allowManyCoupons',
        'status',
        'startTime',
        'endTime',
        'applyForBranch',
        'applyForCustomerGroup',
        'applyForSale',
        'applyForSaleChannel',
        'codePrefix',
    ];

    public function isAvailable(): bool
    {
        $now = time();

        return $this->status == 1
            && $now >= strtotime($this->startTime)
            && $now <= strtotime($this->endTime);
    }

    public function pushApplyTo(string $table, array $tableIDs)
    {
        foreach ($tableIDs as $tableID) {
            $this->applyToData[] = [
                'tableName' => $table,
                'couponId' => $this->id,
                'tableId' => $tableID
            ];
        }
    }

    public function saveApplyTo()
    {
        CouponApply::query()
            ->where('couponId', $this->id)
            ->delete();

        if (!empty ($this->applyToData)) {
            DB::table('coupon_applies')->insert($this->applyToData);
        }
    }

    public function getApplyTO()
    {
        $tableMap = [
            'customers' => 'customerIDs',
            'branches' => 'branchIDs',
            'sale_channels' => 'saleChannelIDs',
            'users' => 'saleIDs',
        ];

        $applyTo = [
            'customerIDs' => [],
            'customerGroupIDs' => [],
            'branchIDs' => [],
            'saleChannelIDs' => [],
            'saleIDs' => [],
        ];

        $applyGroups = CouponApply::query()
            ->where('couponId', $this->id)
            ->get()->groupBy('tableName')->toArray();

        foreach ($applyGroups as $tableName => $group) {
            $tableIDs = collect($group)->pluck('tableId')->toArray();
            $tableEntries = DB::table($tableName)->whereIn('id', $tableIDs)->get();

            if (isset($tableMap[$tableName])) {
                $field = $tableMap[$tableName];
                $applyTo[$field] = $tableEntries->pluck('id')->toArray();
            }
        }

        return $applyTo;
    }
}
