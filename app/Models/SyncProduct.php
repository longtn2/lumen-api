<?php

namespace App\Models;

class SyncProduct extends BaseModel
{
    public $timestamps = false;

    //Add thêm nếu có sàn khác
    const TYPE_SHOPEE = 0;
    const TYPE_LAZADA = 1;
    const LIMIT_SHOPEE = 50;
    const LIMIT_LAZADA = 50;

    protected $table = 'sync_product';
    protected $fillable = [
        'item_id',
        'category_id',
        'item_name',
        'item_sku',
        'create_time',
        'update_time',
        'image',
        'weight',
        'dimension',
        'logistic_info',
        'pre_order',
        'condition',
        'size_chart',
        'item_status',
        'has_model',
        'brand',
        'item_dangerous',
        'description_info',
        'description_type',
        'property',
        'model_id',
        'prd_sync',
        'status',
        'message',
        'linked',
        'stock_info_v2',
        'stock_info',
        'price_info',
        'promotion_id',
        'sync_from',
        'shop_id'
    ];
}
