<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $code
 * @property string $type
 * @property int $priceType
 * @property double $priceValue
 * @property int $status
 * @property int $order
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class Surcharge extends BaseModel
{
    protected $table = 'surcharges';
    protected $fillable = [
        'code',
        'type',
        'priceType',
        'priceValue',
        'status',
        'order',
    ];

    public static $listStatus = [
        [
            'id' => 1,
            'name' => 'Đang thu'
        ],
        [
            'id' => 0,
            'name' => 'Đã ngừng thu'
        ]
    ];

    public static function generalCode()
    {
        $codeHead = 'THK';
        /**
         * @var self latestEntry
         */
        $latestEntry = self::query()
            ->orderByDesc('id')
            ->first();

        if (!$latestEntry) {
            return $codeHead . '000001';
        }

        $latestCode = $latestEntry->code;
        $latestCode = substr($latestCode, strlen($codeHead));

        $code = sprintf("%06d", $latestCode + 1);
        $maxLop = 25;
        while (self::where('code', $codeHead . $code)->count()) {
            $code++;

            if ($maxLop <= 0) {
                throw new \Exception('Không thể tạo mã thu khác!');
            }

            $maxLop--;
        }

        return $codeHead . $code;
    }

    public function getInvoiceOrderAmount($totalOrigin) {
        if ($this->priceType == DISCOUNT_TYPE_PERCENT) {
            $amount = $totalOrigin * $this->priceValue / 100;
        } else {
            $amount = min($totalOrigin, $this->priceValue);
        }

        return $amount;
    }
}
