
<?php

namespace App\Models;

/**
 * @property int $id
 * @property int $deliveryId
 * @property int $value
 * @property string $name
 * @property string $note
 * @property string $updatedBy
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 */
class DeliveryStatus extends BaseModel
{
    protected $table = 'delivery_status';
    protected $fillable = [
        'id',
        'deliveryId',
        'value',
        'name',
        'note',
        'updatedBy'
    ];
}
