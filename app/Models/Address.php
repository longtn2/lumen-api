<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class User extends BaseModel 
{

    protected $table = 'address';

    const TYPE_APARTMENT = 1; //chung cư
    const TYPE_COMPANY = 2; // công ty
    const TYPE_HOME = 3;    //nhà riêng

    protected $fillable = ['customer_id', 'phone', 'addressValue', 'name', 'is_default', 'provinceId',
        'districtId', 'wardId', 'typeAddress', 'apartmentName', 'apartmentBuilding', 'apartmentRoom'];
}
