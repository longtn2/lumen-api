<?php

namespace App\Models;

use App\Models\Casts\StdClassCast;
use App\Services\Promotion\PromotionConditionIf;
use App\Services\Promotion\PromotionConditionThen;
use Illuminate\Database\Eloquent\Collection;

/**
 * @property int       $id
 * @property string    $code
 * @property string    $name
 * @property int       $status
 * @property string    $note
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property int       $usingVoucher
 * @property string    $currentIfId
 * @property string    $currentThenId
 * @property array     $formValues
 * @property \DateTime $startTime
 * @property \DateTime $endTime
 * @property string    $branchIDs
 * @property int       $applyForCustomerGroup
 * @property int       $applyForBranch
 */
class Promotion extends BaseModel
{
    CONST APPLY_FOR_BRANCH_ALL = 0;
    CONST APPLY_FOR_BRANCH_SPECIFIED = 1;

    protected $table = 'promotions';
    protected $casts = [
        'formValues' => 'array'
    ];

    protected $fillable = [
        'code',
        'name',
        'status',
        'note',
        'usingVoucher',
        'currentIfId',
        'currentThenId',
        'formValues',
        'startTime',
        'endTime',
        'applyForBranch',
        'applyForCustomerGroup'
    ];

    public function attachBranchIDs()
    {
        if ($this->applyForBranch != self::APPLY_FOR_BRANCH_ALL) {
            $this->branchIDs = PromotionBranch::where('promotionId', $this->id)->get()->pluck('branchId')->toArray();
        } else {
            $this->branchIDs = Branch::getAllIDs();
        }
    }

    /**
     * @param Promotion[]|Collection $promotions
     */
    public static function attachBranchIDsForMany($promotions)
    {
        $promotionIDs = [];
        $specifiedPromotions = [];
        foreach ($promotions as $promotion) {
            if ($promotion->applyForBranch == self::APPLY_FOR_BRANCH_ALL) {
                $promotion->branchIDs = Branch::getAllIDs();
            } else {
                $promotionIDs[] = $promotion->id;
                $specifiedPromotions[] = $promotion;
            }
        }

        $groups = PromotionBranch::whereIn('promotionId', $promotionIDs)->get()->groupBy('promotionId')->toArray();

        foreach ($specifiedPromotions as $promotion) {
            if (isset( $groups[$promotion->id])) {
                $promotion->branchIDs = collect($groups[$promotion->id])->pluck('branchId')->toArray();
            } else {
                $promotion->branchIDs = [];
            }
        }

        //PromotionBranch::where('promotionId', $this->id)->get()->pluck('branchId')->toArray();
    }

    public static function getCurrentPromotions($branchId = null, $type = 'IfHasProduct')
    {
        $now = date('Y-m-d H:i:s');

        $promotions = Promotion::query()
            ->where('status', 1)
            ->where('currentIfId', $type)
            ->where('startTime', '<=', $now)
            ->where('endTime', '>=', $now)
            ->get();

        Promotion::attachBranchIDsForMany($promotions);
        $currentBranchPromotions = collect([]);
        foreach ($promotions as $promotion) {
            $branchIDs = $promotion->branchIDs;
            if (in_array($branchId, $branchIDs)) {
                $currentBranchPromotions[] = $promotion;
            }
        }

        $tagIDs = [];

        foreach ($currentBranchPromotions as $key => $promotion) {
            $tagDiscountIDs = [];
            $totalQuantityDiscount = 0;

            $formValues = $promotion->formValues;

            foreach ($formValues as $formValue) {
                if (isset($formValue['cIf']['ProductPromotionGroup'])) {
                    foreach ($formValue['cIf']['ProductPromotionGroup'] as $tag) {
                        $tagIDs[] = $tag['id'];
                    }
                }

                if (isset($formValue['cThen']['ProductPromotionGroup'])) {
                    foreach ($formValue['cThen']['ProductPromotionGroup'] as $tag) {
                        $tagDiscountIDs[] = $tag['id'];
                    }

                    $totalQuantityDiscount += ($formValue['cThen']['Quantity'] ?? 0);
                }
            }

            $productUnits = ProductUnitTag::with('productUnit')->whereIn('tagId', $tagIDs)->get()->pluck('productUnit');
            $promotion->appliedProductUnits = $productUnits;

            $discountProductUnits = ProductUnitTag::with('productUnit.product')->whereIn('tagId', $tagDiscountIDs)->get()->pluck('productUnit');
            ProductUnit::batchDecode($discountProductUnits, $branchId);
            $promotion->discountProductUnits = $discountProductUnits;
            $promotion->totalQuantityDiscount = $totalQuantityDiscount;

        }

        return $currentBranchPromotions;

    }


}
