<?php
namespace App\Models;

 use OmiCare\Http\Request;

 /**
 * @property int       $id
 * @property string    $name
 * @property string    $code
 * @property string    $gender
 * @property string    $contactNumber
 * @property string    $retailerId
 * @property string    $branchId
 * @property string    $email
 * @property string    $modifiedDate
 * @property string    $createdDate
 * @property string    $birthDate
 * @property int       $type
 * @property string    $groups
 * @property int       $debt
 * @property int       $rewardPoint
 * @property string    $address
 * @property string    $locationName
 * @property string    $wardName
 * @property string    $organization
 * @property \DateTime $createdAt
 * @property \DateTime $updatedAt
 * @property string    $comments
 * @property string    $taxCode
 */
class Customer extends BaseModel
{
    protected $table = 'customers';
    protected $casts = [
        'avatar' => 'array'
    ];
    protected $fillable = [
    'name',
    'code',
    'gender',
    'contactNumber',
    'retailerId',
    'branchId',
    'email',
    'modifiedDate',
    'createdDate',
    'birthDate',
    'type',
    'groups',
    'debt',
    'rewardPoint',
    'address',
    'locationName',
    'wardName',
    'organization',
    'comments',
    'taxCode',
    'tienSuBenh',
    'tienSuDiUng',
    'soThich',
    'giaDinh',
    'status',
        'avatar',
        'tagId'
];
    public function addresses()
    {
        return $this->hasMany(Address::class, 'customer_id');
    }

    public static function getDefaultAddress($customerId) {
        return Address::query()
            ->where('customer_id', $customerId)
            ->where('is_default', 1)
            ->orderByDesc('id')
            ->first();
    }

    public function saveHistory($action, $options = [])
    {
        /**
         * @var User $user
         */
        $user = auth();
        $ignored = ['updated_at' => true, 'created_by' => true, 'created_at' => true];

        if (!$user) {
            $user = new User();
            $user->id = 0;
            $user->name = 'Bot';
            $user->email = 'omipos-bot@omicare.vn';
        }

        $req = new Request();
        $currentBranchID = $req->header('X-Branch-Id');

        $log = new History();
        $log->refTable = $this->getTable();
        $log->refId = $this->id;
        $log->branchId = $currentBranchID;
        $log->action = $action;
        $log->diff = [];
        $log->createdBy = $user->id;

        switch ($action) {
            case History::ACTION_CREATED:
                $content = 'Thêm mới khách hàng: ' . $this->code . ', tên KH: ' . $this->name . ', Giới tính' . $this->gender == 1 ? 'Name' : 'Nữ'
                    .', SĐT: '. $this->contactNumber. ', Ngày sinh: '. $this->birthDate . ', Email: '. $this->email;

                if ($this->tienSuBenh || $this->tienSuDiUng || $this->soThich || $this->giaDinh) {
                    $content .= ', Tiền sử bệnh: ' . $this->tienSuBenh . ', Tiền sử dị ứng: '
                        . $this->tienSuDiUng. ', Sở Thích: '. $this->soThich . ', Gia đình: '. $this->giaDinh;
                }
                $log->content = $content;
                $log->save();

                break;
            case History::ACTION_UPDATED:
                $origins = $this->getOriginal();
                $changes = $this->getDirty();

                $content = 'Cập nhật thông tin KH: ' . $this->code;

                if (isset($origins['name']) && isset($changes['name']) && $origins['name'] != $changes['name']) {
                    $content .= ', Họ tên: ' . $origins['name'] . ' -> ' . $changes['name'];
                }
                $log->content = $content;

                $diff = [];

                foreach ($changes as $key => $value) {
                    $oldVal = $origins[$key] ?? null;

                    if (!isset($ignored[$key]) && $oldVal != $value) {
                        $diff[$key] = [$oldVal, $value];
                    }
                }

                if (count($diff)) {
                    $log->diff = $diff;
                    $log->save();
                }

                break;
            default:

                break;
        }

        return true;
    }
}
