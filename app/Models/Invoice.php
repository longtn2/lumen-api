<?php

namespace App\Models;

/**
 * @property int $id
 * @property string $uuid
 * @property string $code
 * @property string $purchaseDate
 * @property int $branchId
 * @property string $branchName
 * @property int $soldById
 * @property string $soldByName
 * @property int $customerId
 * @property string $customerCode
 * @property string $customerName
 * @property string $orderCode
 * @property string $orderCrmCode
 * @property double $totalOrigin
 * @property double $total
 * @property double $totalPayment
 * @property string $discountType
 * @property int $discountValue
 * @property double $discount
 * @property string $discountExplain
 * @property int $status
 * @property string $statusValue
 * @property string $description
 * @property string $usingCod
 * @property string $modifiedDate
 * @property string $createdDate
 * @property string $invoiceDetails
 * @property string $payments
 * @property string $invoiceOrderSurcharges
 * @property string $saleChannelId
 * @property string $orderId
 * @property int $createdBy
 * @property boolean $isPack
 * @property array $gifts
 * @property array $promotionInvoiceDetails
 */
class Invoice extends BaseModel
{
    protected $table = 'invoices';
    public $promotionInvoiceDetails = [];
    public $promotionCouponCodes = [];

    protected $fillable = [
        'uuid',
        'code',
        'purchaseDate',
        'branchId',
        'branchName',
        'soldById',
        'soldByName',
        'customerId',
        'customerCode',
        'customerName',
        'orderCode',
        'orderCrmCode',
        'totalOrigin',
        'total',
        'totalPayment',
        'discountType',
        'discountValue',
        'status',
        'statusValue',
        'description',
        'usingCod',
        'invoiceDetails',
        'payments',
        'invoiceOrderSurcharges',
        'saleChannelId',
        'orderId',
        'createdBy',
        'isPack'
    ];

    const STATUS_PROCESS = 3;
    const STATUS_COMPLETE = 1;
    const STATUS_NOT_DELIVER = 5;
    const STATUS_CANCELED = 2;

    public static $listStatus = [
        [
            'id' => self::STATUS_PROCESS,
            'name' => 'Đang xử lý'
        ],
        [
            'id' => self::STATUS_COMPLETE,
            'name' => 'Hoàn thành'
        ],
        [
            'id' => self::STATUS_NOT_DELIVER,
            'name' => 'Không giao được'
        ],
        [
            'id' => self::STATUS_CANCELED,
            'name' => 'Đã hủy'
        ],
    ];

    public static $listStatusPack = [
        [
            'id' => 0,
            'name' => 'Chưa đóng gói'
        ],
        [
            'id' => 1,
            'name' => 'Đã đóng gói'
        ],
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customerId');
    }

    public function returns()
    {
        return $this->hasMany(Returns::class, 'invoiceId');
    }

    public function saleChannel()
    {
        return $this->belongsTo(SaleChannel::class, 'saleChannelId');
    }

    public function userCreated()
    {
        return $this->belongsTo(User::class, 'createdBy');
    }

    public function cashFlows()
    {
        return $this->hasMany(Cashflow::class, 'invoiceId');
    }

    public function invoiceItems()
    {
        return $this->hasMany(InvoiceDetail::class, 'invoiceId');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branchId');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'orderId');
    }

    public function soldBy()
    {
        return $this->belongsTo(User::class, 'soldById');
    }

    public function delivery()
    {
        return $this->hasOne(Delivery::class, 'invoiceId');
    }

    public function invoiceSurcharges() {
        return $this->hasMany(InvoiceSurcharge::class,'invoiceId');
    }

    /**
     * @param $action
     * @param array $options ['isUpdateInvoice', 'isOrderProcessing', 'isCreateNewInvoice', 'invoiceOriginCode']
     * @return bool
     */
    public function saveHistory($action, $options = [])
    {
        /**
         * @var User $user
         */
        $user = auth();
        $ignored = ['updated_at' => true, 'created_by' => true, 'created_at' => true];

        if (!$user) {
            $user = new User();
            $user->id = 0;
            $user->name = 'Bot';
            $user->email = 'omipos-bot@omicare.vn';
        }

        $log = new History();
        $log->refTable = $this->getTable();
        $log->refId = $this->id;
        $log->branchId = $this->branchId;
        $log->action = $action;
        $log->diff = [];
        $log->createdBy = $user->id;

        switch ($action) {
            case History::ACTION_CREATED:
                if (!empty($options['isUpdateInvoice'])) {
                    $content = 'Tạo hóa đơn ' . $this->code . ' được tạo từ cập nhật thông tin hóa đơn: ' . ($options['invoiceOriginCode'] ?? '')
                        .', bao gồm:<br/><br/>' . InvoiceDetail::decodeContentHistory($this->id);
                } else {
                    $content = 'Tạo hóa đơn: ' . $this->code;

                    if (!empty($options['isOrderProcessing'])) {
                        $content .= '( cho đơn đặt hàng: ' . $this->orderCode . ')';
                    }

                    $content .= ', khách hàng: ' . $this->customerName . ', giảm giá: ' . number_format($this->getDiscountPrice()) .
                        ', giá trị: ' . number_format($this->total) . ', thời gian: ' . date('d/m/Y H:i:s', strtotime($this->purchaseDate)) .
                        ', trạng thái: ' . self::getStatusLabel($this->status) . ', Người tạo: ' . User::getNameFromId($this->createdBy) .
                        ', Người bán: ' . User::getNameFromId($this->soldById) . ', bao gồm: <br/><br/>' .
                        InvoiceDetail::decodeContentHistory($this->id);
                }

                $log->content = $content ?? 'Tạo hóa đơn: ' . $this->code;
                $log->save();

                break;
            case History::ACTION_UPDATED:
                $origins = $this->getOriginal();
                $changes = $this->getDirty();

                $content = 'Cập nhật thông tin hóa đơn: ' . $this->code;

                if (isset($origins['customerName']) && isset($changes['customerName']) && $origins['customerName'] != $changes['customerName']) {
                    $content .= ', khách hàng: ' . $origins['customerName'] . ' -> ' . $changes['customerName'];
                }
                if (isset($origins['purchaseDate']) && isset($changes['purchaseDate']) && $origins['purchaseDate'] != $changes['purchaseDate']) {
                    $content .= ', thời gian: ' . date('d/m/Y H:i:s', strtotime($origins['purchaseDate'])) . ' -> ' . date('d/m/Y H:i:s', strtotime($changes['purchaseDate']));
                }
                if (isset($origins['status']) && isset($changes['status']) && $origins['status'] != $changes['status']) {
                    $content .= ', trạng thái: ' . self::getStatusLabel($origins['status']) . ' -> ' . self::getStatusLabel($changes['status']);
                }
                if (isset($origins['soldById']) && isset($changes['soldById']) && $origins['soldById'] != $changes['soldById']) {
                    $content .= ', Người bán: ' . User::getNameFromId($origins['soldById']) . ' -> ' . User::getNameFromId($changes['soldById']);
                }
                if (isset($origins['saleChannelId']) && isset($changes['saleChannelId']) && $origins['saleChannelId'] != $changes['saleChannelId']) {
                    $content .= ', Kênh bán: ' . SaleChannel::getNameFromId($origins['saleChannelId']) . ' -> ' . SaleChannel::getNameFromId($changes['saleChannelId']);
                }

                $content .= ', bao gồm: <br/><br/>' . InvoiceDetail::decodeContentHistory($this->id);
                $log->content = $content;

                $diff = [];

                foreach ($changes as $key => $value) {
                    $oldVal = $origins[$key] ?? null;

                    if (!isset($ignored[$key]) && $oldVal != $value) {
                        $diff[$key] = [$oldVal, $value];
                    }
                }

                if (count($diff)) {
                    $log->diff = $diff;
                    $log->save();
                }

                break;
            case History::ACTION_CANCEL:
                if (!empty($options['isUpdateInvoice'])) {
                    $content = 'Hủy hóa đơn ' . $this->code . ' do việc cập nhật thông tin hóa đơn này, với giá trị: ' .
                        number_format($this->total) . ', bao gồm: <br/><br/>' . InvoiceDetail::decodeContentHistory($this->id);
                } else {
                    $content = 'Hủy hóa đơn ' . $this->code . ', với giá trị: ' . number_format($this->total) .
                        ', bao gồm:<br/><br/>' . InvoiceDetail::decodeContentHistory($this->id);
                }

                $log->content = $content;
                $log->save();

                break;
            case History::ACTION_EXPORT:
                $log->content = 'Xuất file Danh Sách Hóa Đơn';
                $log->save();

                break;
            default:

                break;
        }

        return true;
    }

    public function saveStatus($status, $note = null, $saveInvoice = true)
    {
        $statusLabel = Invoice::getStatusLabel($status);

        $this->status = $status;
        $this->statusValue = $statusLabel;
        if ($saveInvoice) {
            $this->save();
        }

        $orderStatus = new InvoiceStatus();
        $orderStatus->invoiceId = $this->id;
        $orderStatus->value = $status;
        $orderStatus->name = $statusLabel;
        $orderStatus->note = $note;
        $orderStatus->save();

        return true;
    }

    public static function getStatusLabel($status)
    {
        static $listStatusMap;

        if (!$listStatusMap) {
            $listStatusMap = array_column(Invoice::$listStatus, 'name', 'id');
        }

        return $listStatusMap[$status] ?? null;
    }

    public function getDiscountPrice() {
        if ($this->discountType == DISCOUNT_TYPE_PERCENT) {
            $discountPrice = ceil($this->totalOrigin * $this->discountValue / 100);
        } else {
            $discountPrice = $this->discountValue;
        }

        return $discountPrice + (int) $this->discount;
    }

    public function removeInventories()
    {
        $invoiceDetailIds = InvoiceDetail::query()
            ->where('invoiceId', $this->id)
            ->pluck('id');

        Inventory::query()
            ->whereIn('refId', $invoiceDetailIds)
            ->where('refTable', Inventory::REF_CODE_INVOICE_DETAIL)
            ->delete();

        return true;
    }

    public static function generalCode()
    {
        /**
         * @var self latestEntry
         */
        $latestEntry = self::query()
            ->orderByDesc('id')
            ->first();

        if (!$latestEntry) {
            return 'HD100000';
        }

        $latestCode = $latestEntry->code;
        if (($index = strrpos($latestCode, ".")) !== false) {
            $latestCode = substr($latestCode, 0, $index);
        }
        $latestCode = substr($latestCode, 2);

        $code = (substr($latestCode, 0, 1) >= 1) ? $latestCode + 1 : '100000';
        $maxLop = 25;
        while (self::where('code', 'HD' . $code)->count()) {
            $code++;

            if ($maxLop <= 0) {
                throw new \Exception('Không thể tạo mã hóa đơn!');
            }

            $maxLop--;
        }

        return 'HD' . $code;
    }

    public static function getCodeFromId($id) {
        $entry = self::find($id);

        if (!$entry) {
            return null;
        }

        return $entry->code;
    }
}
