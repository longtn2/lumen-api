<?php

namespace App\Models;

class CustomerTag extends BaseModel
{
    protected $table = 'customer_tags';
    protected $fillable = ['tagId', 'customerId'];
}
