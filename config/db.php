<?php

return [
    'default' =>  [
        'host' => env('DB_HOST'),
        'username' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD'),
        'database' => env('DB_NAME'),
        'port' => env('DB_PORT')
    ],
    'omipharma' =>  [
        'host' => env('DB_HOST'),
        'username' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD'),
        'database' => 'omipharma_vn',
        'port' => env('DB_PORT')
    ]
];
