<?php
return [
    'default' => [
        'host' => env('REDIS_HOST', '127.0.0.1'),
        'port' => env('REDIS_PORT', 6379),
        'db' => env('REDIS_DB', 0),
        'prefix' => env('REDIS_PREFIX', 'app.')
    ]
];
