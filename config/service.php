<?php
return [
    'twilio' => [
        'accountSid' => env('TWILIO_ACCOUNT_SID'),
        'authToken' => env('TWILIO_API_AUTH_TOKEN'),
        'apiKeySecret' => env('TWILIO_API_KEY_SECRET'),
        'apiKeySid' => env('TWILIO_API_KEY_SID')
    ],
    'fcm' => [
        'serverKey' => env('FCM_SERVER_KEY')
    ],
    'vmg' => [
        'token' => env('VMG_SMS_TOKEN')
    ],
    'elasticsearch' => [
        'enabled' => env('ELASTIC_SEARCH_ENABLED', false)==1,
        'host' => env('ELASTIC_SEARCH_HOST', 'http://localhost:9200')
    ],
    'crm' => [
        'endpoint' => env("CRM_ENDPOINT"),
        'credential' => env('CRM_CREDENTIAL'),
        'authbasic' => env('CRM_AUTH_BASIC')
    ]
];
