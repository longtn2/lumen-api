<?php

namespace OmiCare;

use App\Exception\PermissionRequiredException;
use App\Utils\XLogger;
use OmiCare\Exception\HttpException;
use OmiCare\Exception\HttpNotFound;
use OmiCare\Exception\ValidationException;
use OmiCare\Http\IResponse;
use OmiCare\Http\Redirect;
use OmiCare\Http\Request;
use OmiCare\Http\Response;
use OmiCare\Utils\Auth;
use OmiCare\Utils\DB;

/**
 * @property PermissionValidation $permissionValidation
 */
class Application
{
    private $uriPrefix;
    private $request;
    private $permissionValidation;

    public function __construct()
    {
        $this->request = new Request();
    }

    public function setPermissionValidation(PermissionValidation $permissionValidation)
    {
        $this->permissionValidation = $permissionValidation;
    }

    /**
     * Distpatch app.
     *
     * @return Redirect|Response
     */
    public function dispatch(): IResponse
    {
        try {
            $this->uriPrefix = config('app')['uri_prefix'];

            $this->register();
            //$this->bootDB();
            return $this->handleRequest();
        } catch (\Throwable $e) {
            return $this->handleException($e, $this->request);
        }
    }

    public function writeLog(Request $req, IResponse $res, \Throwable $e = null)
    {
        $entry = [];
        $user = Auth::user();
        $username = null;

        if ($user) {
            if ($user->email) {
                $username = 'user:'.$user->email;
            } else {
                $username = 'user:'.$user->id;
            }
        }

        $entry['username'] = $username;
        $entry['response'] = '';
        $entry['request_uri'] = $_SERVER['REQUEST_URI'];

        $entry['http_method'] = $_SERVER['REQUEST_METHOD'];

        $entry['request_headers'] = json_encode(getallheaders(), JSON_PRETTY_PRINT);
        $entry['time'] = date('Y-m-d H:i:s');
        $entry['ip'] = $req->ip();
        $entry['event_type'] = 'Api';

        if ($e) {
            $entry['event_type'] = 'ApiError';
        }
        $entry['parameters'] = json_encode($req->toArray(), JSON_PRETTY_PRINT);
        $entry['user_agent'] = @$_SERVER['HTTP_USER_AGENT'];
        $entry['query'] = json_encode(DB::getQueryLogAll(), JSON_PRETTY_PRINT);
        $entry['http_code'] = $res->getStatusCode();
        $entry['response'] = json_encode($res->getData(), JSON_PRETTY_PRINT);

        if ($e) {
            $entry['exception'] = get_class($e);
            $entry['message'] = $e->getMessage();
            $entry['stack_trace'] = $e->getTraceAsString();
        }

        XLogger::push($entry);
    }

    /**
     * All exception goto here.
     *
     * @return IResponse
     */
    protected function handleException(\Throwable $e, Request $request): Response
    {
        $logWrite = false;
        if ($e instanceof \LogicException) {
            $response = new Response([
                'code' => -2,
                'message' => $e->getMessage(),
            ]);
        } elseif ($e instanceof ValidationException) {
            $response = new Response([
                'code' => -1,
                'message' => 'Vui lòng kiểm tra lại dữ liệu',
                'errors' => $e->getValidationErrors()
            ]);
        } elseif ($e instanceof HttpException) {
            $response = new Response([
                'code' => $e->getHttpCode(),
                'message' => $e->getMessage(),
            ], $e->getHttpCode());
        } else if ($e instanceof PermissionRequiredException) {
            $debugInfo  = null;
            if (config('app')['debug']) {
                $debugInfo = PermissionValidation::getDebugInfo();
                $user = \auth();
                $response = new Response([
                    'code' => 401,
                    'message' => $e->getMessage(),
                    'requiredPermissions' => $e->requiredPermissions,
                    'permissionDebugInfo' => [
                        'message' => $debugInfo,
                        'user' => $user ?? null,
                        'roles' => $user ? $user->getUserRoles() : [],
                    ]
                ]);
            } else {
                $response = new Response([
                    'code' => 401,
                    'message' => $e->getMessage(),
                    'requiredPermissions' => 'Only avail if DEBUG enabled',
                ]);
            }

        } else {
            if (config('app')['debug']) {
                $response = new Response([
                    'code' => 500,
                    'message' => $e->getMessage(),
                    'exception' => get_class($e),
                    'trace' => explode("\n", $e->getTraceAsString())
                ], 200);
            } else {
                $response = new Response([
                    'code' => 500,
                    'message' => 'Interval server error',
                    'exception' => get_class($e)
                ], 200);
            }
        }

        $this->writeLog($request, $response, $e);

        if ($logWrite) {
            logfile_write($e);
        }

        return $response;
    }

    protected function getWhiteListClasses(): array
    {
        return [];
    }

    protected function register()
    {
    }

    private static function url2Class($url, $camelCase = false)
    {
        if (strpos($url, '-') === false) {
            return $camelCase ? $url : ucwords($url);
        }

        $b = implode('', array_map('ucwords', explode('-', $url)));

        if ($camelCase) {
            $b[0] = strtolower($b[0]);
        }

        return $b;
    }

    /**
     * Handle request.
     *
     * @throws HttpNotFound
     *
     * @return Response | Redirect
     */
    private function handleRequest(): IResponse
    {
        list($uri) = explode('?', $_SERVER['REQUEST_URI']);

        if ($this->uriPrefix) {
            $uri = substr($uri, strlen($this->uriPrefix), strlen($uri));
        }

        if ($uri === '/') {
            $className = '\\App\Controllers\\Controller';
            $action = 'indexAction';
        } else {
            $parts = explode('/', $uri);

            if (!isset($parts[1])) {
                throw new HttpNotFound('Invalid namespace');
            }

            if (!isset($parts[2])) {
                throw new HttpNotFound('Invalid controller');
            }

            $namespace = ucfirst($parts[1]);
            $className = self::url2Class($parts[2]).'Controller';
            $method = self::url2Class($parts[3] ?? 'index', true);
            $action = $method.'Action';
            $args = [];

            for ($i = 4; $i < count($parts); ++$i) {
                $args[] = urldecode($parts[$i]);
            }
            $this->request->setArgs($args);
            $whiteListClasses = $this->getWhiteListClasses();

            if (!isset($whiteListClasses[$className])) {
                throw new \Exception("{$className} is not in whitelist Class");
            }

            $className = 'App\Controllers\\'.$namespace."\\{$className}";
        }

        if ($this->permissionValidation) {
            if (!$this->permissionValidation->validate($className, $action, $this->request)) {
                throw new PermissionRequiredException(PermissionValidation::getRequiredPermissions(),
                    'Bạn không có quyền truy cập trang này');
            }
        }

        $instance = new $className();

        $ret = $instance->{$action}($this->request);

        if (!($ret instanceof IResponse)) {
            if (is_array($ret)) {
                $permissionDebugInfo  = null;
                if (config('app')['debug']) {
                    $permissionDebugInfo = PermissionValidation::getDebugInfo();
                }
                $ret['permissionDebugInfo'] = $permissionDebugInfo;
            }
            $ret = new Response($ret);
        }

        $this->writeLog($this->request, $ret);

        return $ret;
    }
}
