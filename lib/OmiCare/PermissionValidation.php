<?php

namespace OmiCare;

use OmiCare\Http\Request;

class PermissionValidation
{
    protected static $debugInfo = [];
    protected static $requiredPermissions = [];

    public static function getRequiredPermissions(): array
    {
        return static::$requiredPermissions;
    }

    public function validate(string $class, string $action, Request $request): bool
    {

        return true;
    }

    public static function getDebugInfo(): array
    {
        return self::$debugInfo;
    }

}
