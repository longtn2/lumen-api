<?php

define('ROOT', dirname(dirname(__DIR__)));
define('APP_ENV', env('APP_ENV', 'production'));
define('APP_DEBUG', env('APP_DEBUG', false));
define('APP_URL', env('APP_URL', false));

set_error_handler(function ($errno, $errstr, $errfile, $errline, array $errcontext) {
    // error was suppressed with the @-operator
    if (0 === error_reporting()) {
        return false;
    }

    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
});

function d1()
{
    $args = func_get_args();

    if (count($args) === 1) {
        $v = $args[0];
    } else {
        $v = $args;
    }

    if (php_sapi_name() === 'cli') {
        echo json_encode($v, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

        die;
    }
    echo '<pre>';
    echo json_encode($v, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    echo '</pre>';

    die;
}

// function auth()
// {
//     return \OmiCare\Utils\Auth::user(true, false);
// }

// function appenv($key, $default = null)
// {
//     static $vars;

//     if (!isset($vars)) {
//         $vars = parse_ini_file(ROOT . '/env.ini');
//     }

//     return isset($vars[$key]) ? $vars[$key] : $default;
// }

// function config($name)
// {
//     static $allConfigs;

//     if (!isset($allConfigs[$name])) {
//         $allConfigs[$name] = require_once ROOT . '/configs/' . $name . '.php';
//     }

//     return  $allConfigs[$name];
// }

// function request($key = null, $default = null)
// {
//     static $req;

//     if (!$req) {
//         $req = new \OmiCare\Http\Request();
//     }

//     return $key === null ? $req : $req->get($key, $default);
// }

// function response($data, $status = 200, $headers = []): OmiCare\Http\Response
// {
//     return new \OmiCare\Http\Response($data, $status, $headers);
// }

// function redirect($url, array $query = []): OmiCare\Http\Redirect
// {
//     return new \OmiCare\Http\Redirect($url, $query);
// }

// if (!function_exists('getallheaders')) {
//     function getallheaders()
//     {
//         $headers = [];

//         foreach ($_SERVER as $name => $value) {
//             if (substr($name, 0, 5) == 'HTTP_') {
//                 $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
//             }
//         }

//         return $headers;
//     }
// }

function logfile_write($string, $filename = 'error')
{
    if ($string instanceof \Throwable) {
        $e = $string;
        $string = $e->getMessage() . "\n" . $e->getTraceAsString() . "\n";
    } elseif (!is_string($string)) {
        $string = json_encode($string, JSON_PRETTY_PRINT);
    }

    static $handle;
    $filename = $filename . '-' . date('Y-m-d') . '.log';

    if (!isset($handle)) {
        if (!is_dir(ROOT . '/tmp/logs')) {
            mkdir(ROOT . '/tmp/logs', 0777);
        }
        $handle = fopen(ROOT . '/tmp/logs/' . $filename, 'a+');
    }

    $time = '[' . date('Y-m-d H:i') . "]\n";
    fwrite($handle, $time . $string . PHP_EOL);
}

// function view($path, array $vars = [], $layout = null): OmiCare\Http\Response
// {
//     $__path = $path;
//     $__layout = $layout;
//     extract($vars);
//     ob_start();

//     include ROOT . '/app/Views/' . $__path . '.phtml';
//     $contents = ob_get_clean();
//     $response = new \OmiCare\Http\Response($contents);

//     return $response->withHeaders(['Content-Type' => 'text/html']);
// }
if (!function_exists('h')) {
    function h($a)
    {
        echo htmlentities($a);
    }
}

function n($num)
{
    echo number_format($num);
}

// function url($path)
// {
//     $baseUrl = config('app')['url'] . config('app')['uri_prefix'];

//     return rtrim($baseUrl) . '/' . ltrim($path, '/');
// }

function uuid_v4()
{
    $data = openssl_random_pseudo_bytes(16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}
