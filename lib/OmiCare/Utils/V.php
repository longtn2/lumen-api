<?php

namespace OmiCare\Utils;

use OmiCare\Exception\ValidationException;
use OmiCare\Http\Request;
use Valitron\Validator;

/**
 * Lib xử lí validation, kế thừa từ https://github.com/vlucas/valitron.
 */
class V extends Validator
{
    const required = 'required';
    const unique = 'unique';
    const requiredWith = 'requiredWith';
    const requiredWithout = 'requiredWithout';
    const equals = 'equals';
    const different = 'different';
    const accepted = 'accepted';
    const numeric = 'numeric';
    const integer = 'integer';
    const boolean = 'boolean';
    const array = 'array';
    const length = 'length';
    const lengthBetween = 'lengthBetween';
    const lengthMin = 'lengthMin';
    const lengthMax = 'lengthMax';
    const min = 'min';
    const max = 'max';
    const listContains = 'listContains';
    const in = 'in';
    const notIn = 'notIn';
    const ip = 'ip';
    const ipv4 = 'ipv4';
    const ipv6 = 'ipv6';
    const email = 'email';
    const emailDNS = 'emailDNS';
    const url = 'url';
    const urlActive = 'urlActive';
    const alpha = 'alpha';
    const alphaNum = 'alphaNum';
    const ascii = 'ascii';
    const slug = 'slug';
    const regex = 'regex';
    const date = 'date';
    const dateFormat = 'dateFormat';
    const dateBefore = 'dateBefore';
    const dateAfter = 'dateAfter';
    const contains = 'contains';
    const subset = 'subset';
    const containsUnique = 'containsUnique';
    const creditCard = 'creditCard';
    const instanceOf = 'instanceOf';
    const optional = 'optional';
    const arrayHasKeys = 'arrayHasKeys';
    const vietnameseName = 'vietnameseName';
    const password = 'password';
    const arrayLengthBetween = 'arrayLengthBetween';

    private static $ruleAdded = false;
    protected $stop_on_first_fail = true;

    /**
     * Throw ValidationException nếu validate failed.
     *
     * @throws ValidationException
     */
    public function validOrFail()
    {
        if (!$this->validate()) {
            throw new ValidationException($this);
        }
    }

    /**
     * @param array|Request $data
     *
     * @return static
     */
    public static function make($data)
    {
        if (!self::$ruleAdded) {
            self::addRules();
            self::$ruleAdded = true;
        }

        if ($data instanceof Request) {
            $data = $data->all();
        }

        if (!is_array($data)) {
            throw new \InvalidArgumentException('data must be an array');
        }

        return new static($data, [], 'vi');
    }

    /**
     * Lấy danh sách kí tự tiếng việt.
     *
     * @return array
     */
    private static function getVietnameseCharMap()
    {
        static $map;

        if ($map) {
            return $map;
        }

        $chars = ['à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă',
            'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ', 'è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ',
            'ì', 'í', 'ị', 'ỉ', 'ĩ',
            'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ',
            'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ',
            'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ',
            'đ',
            'À', 'Á', 'Ạ', 'Ả', 'Ã', 'Â', 'Ầ', 'Ấ', 'Ậ', 'Ẩ', 'Ẫ', 'Ă', 'Ằ', 'Ắ', 'Ặ', 'Ẳ', 'Ẵ',
            'È', 'É', 'Ẹ', 'Ẻ', 'Ẽ', 'Ê', 'Ề', 'Ế', 'Ệ', 'Ể', 'Ễ',
            'Ì', 'Í', 'Ị', 'Ỉ', 'Ĩ',
            'Ò', 'Ó', 'Ọ', 'Ỏ', 'Õ', 'Ô', 'Ồ', 'Ố', 'Ộ', 'Ổ', 'Ỗ', 'Ơ', 'Ờ', 'Ớ', 'Ợ', 'Ở', 'Ỡ',
            'Ù', 'Ú', 'Ụ', 'Ủ', 'Ũ', 'Ư', 'Ừ', 'Ứ', 'Ự', 'Ử', 'Ữ',
            'Ỳ', 'Ý', 'Ỵ', 'Ỷ', 'Ỹ',
            'Đ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            ' ', '_', '-'
        ];

        $map = array_flip($chars);

        return $map;
    }

    /**
     * Add custom rules.
     */
    private static function addRules()
    {
        static::addRule(self::password, function ($field, $value) {
            if (mb_strlen($value) < 8) {
                throw new ValidationException('Mật khẩu phải có tối thiểu 8 kí tự', $field);
            }

            $stupidPasswords = [
                '01234567' => true,
                '12345678' => true,
                '123456789' => true,
                '1234567890' => true,
                'iloveyou' => true,
                'password' => true,
            ];

            if (isset($stupidPasswords[$value])) {
                throw new ValidationException("Mật khẩu `{$value}` quá phổ biến. Bạn vui lòng chọn mật khẩu khác", $field);
            }

            return true;
        });

        static::addRule(self::vietnameseName, function ($field, $value, array $params, array $fields) {
            $map = self::getVietnameseCharMap();
            $chrArray = preg_split('//u', $value, -1, PREG_SPLIT_NO_EMPTY);

            foreach ($chrArray as $char) {
                if (!isset($map[$char])) {
                    return false;
                }
            }

            return true;
        }, '{field} ín invalid vietnamese name');

        static::addRule(self::unique, function ($field, $value, array $params, array $fields) {
            if (empty($params[0])) {
                throw new \InvalidArgumentException("Missing table name for rule: unique. Example:  \$v->rule(V::unique, 'email', ['customers', 'id', 3808])");
            }

            $first = $params[0];
            $table = $first[0];

            if (isset($first[1]) && isset($first[2])) {
              /*  if (!isset($first[2])) {
                    throw new \InvalidArgumentException("Missing ignored field name for rule: unique. Example:  \$v->rule(V::unique, 'email', ['customers', 'id', 3808])");
                }*/

                list(, $ignoredField, $ignoredValue) = $first;

                return !boolval(DB::table($table)
                    ->where($ignoredField, '!=', $ignoredValue)
                    ->where($field, $value)
                    ->first());
            }

            return !boolval(DB::table($table)->where($field, $value)->first());
        }, '{field} must be unique');

        static::addRule(V::arrayLengthBetween,  function ($field, $value, array $params, array $fields) {
            if (!is_array($value)) {
                throw new \InvalidArgumentException("V::arrayLengthBetween: value is not an array");
            }

            if (empty($params[0])) {
                throw new \InvalidArgumentException("V::arrayLengthBetween: Missing 1st param");
            }

            if (empty($params[1])) {
                throw new \InvalidArgumentException("V::arrayLengthBetween: Missing 2nd param");
            }

            $count = count($value);
            return $count >= $params[0] && $count <= $params[1];
        },
        '{field} length invalid');
    }
}
