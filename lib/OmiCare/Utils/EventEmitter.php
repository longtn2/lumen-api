<?php

namespace OmiCare\Utils;

class EventEmitter
{
    private $redis;

    public function __construct()
    {
        $config = config('redis')['default'];
        $redis = new \Redis();
        $redis->connect($config['host'], $config['port']);
        $redis->select(1);

        $this->redis = $redis;
    }

    public static function instance()
    {
        static $instance;

        if ($instance) {
            return $instance;
        }

        $instance = new static();

        return $instance;
    }

    public function emit(string $channel, $message)
    {
        $this->redis->publish($channel, $message);
    }

    public function on(array $channels, $callback)
    {
        $this->redis->subscribe($channels, $callback);
    }
}
