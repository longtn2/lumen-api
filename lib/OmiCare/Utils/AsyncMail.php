<?php

namespace OmiCare\Utils;

class AsyncMail
{
    const TYPE_RAW = 'raw';
    const TYPE_TEMPLATE = 'template';

    private $to;
    private $subject;
    private $content;
    private $type = 'raw';
    private $template;
    private $vars = [];
    private $cc;

    public function __construct(string $to, string $subject, string $content = '', array $cc = [])
    {
        $this->to = $to;
        $this->cc = $cc;
        $this->subject = $subject;
        $this->content = $content;
    }

    public function setTemplate(string $template, array $vars = [])
    {
        $this->template = $template;
        $this->vars = $vars;
        $this->type = self::TYPE_TEMPLATE;
    }

    public function setContent($path, array $vars = [])
    {
        $this->type = self::TYPE_RAW;
        $__path = $path;
        extract($vars);
        ob_start();

        include ROOT.'/app/Views/Mails/'.$__path.'.phtml';
        $this->content = ob_get_clean();
    }

    public function send(): int
    {
        if ($this->type === self::TYPE_TEMPLATE) {
            if (empty($this->template)) {
                throw new \InvalidArgumentException('Missing mail template');
            }
        } else {
            if (empty($this->content)) {
                throw new \InvalidArgumentException('Missing mail content');
            }
        }

        $id = DB::table('async_mails')->insertGetId([
            'to' => $this->to,
            'type' => $this->type,
            'subject' => $this->subject,
            'cc' => implode(',', $this->cc),
            'vars' => json_encode($this->vars, JSON_PRETTY_PRINT),
            'template' => $this->template,
            'content' => $this->content,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        EventEmitter::instance()->emit('AsyncMail', 'send');

        return $id;
    }
}
