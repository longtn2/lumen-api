<?php

namespace OmiCare\Utils;

class FileCache
{
    private $dir;

    public function __construct()
    {
        $dir = ROOT.'/tmp/cache';

        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }

        $this->dir = $dir;
    }

    public function set($key, $value)
    {
        $path = $this->dir.'/app_'.md5($key);
        file_put_contents($path, serialize([
            'value' => $value,
            'ttl' => 0
        ]));
    }

    public function setex($key, $ttl, $value)
    {
        $path = $this->dir.'/app_'.md5($key);
        file_put_contents($path, serialize([
            'value' => $value,
            'ttl' => time() + $ttl
        ]));
    }

    public function get($key)
    {
        $path = $this->dir.'/app_'.md5($key);

        if (!is_file($path)) {
            return null;
        }

        $obj = unserialize(file_get_contents($path));

        if ($obj['ttl'] === 0) {
            return $obj['value'];
        }

        if ($obj['ttl'] < time()) {
            @unlink($path);

            return null;
        }

        return $obj['value'];
    }

    public function exists($key)
    {
        $value = $this->get($key);

        return (bool) $value;
    }

    public function ttl($key)
    {
        $path = $this->dir.'/app_'.md5($key);

        if (!is_file($path)) {
            return 0;
        }

        $obj = unserialize(file_get_contents($path));

        return $obj['ttl'] - time();
    }

    public function del($key)
    {
        $path = $this->dir.'/app_'.md5($key);

        if (!is_file($path)) {
            return false;
        }
        @unlink($path);
    }

    public function flushDB()
    {
        $files = glob(ROOT.'/tmp/cache/*');

        foreach ($files as $file) {
            @unlink($file);
        }
    }
}
