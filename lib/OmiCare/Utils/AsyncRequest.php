<?php

namespace OmiCare\Utils;

class 
{
    const STATUS_WAITING = 0;
    const STATUS_SENT = 1;
    const STATUS_ERROR = 2;

    const CONTENT_TYPE_JSON = 'application/json';
    const CONTENT_TYPE_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    const SUPPORTED_CONTENT_TYPES = [
        self::CONTENT_TYPE_JSON => true,
        self::CONTENT_TYPE_WWW_FORM_URLENCODED => true,
    ];

    private $contentType;

    public function __construct($contentType = 'application/json')
    {
        if (!isset(self::SUPPORTED_CONTENT_TYPES[$contentType])) {
            throw new \InvalidArgumentException("Content-Type: {$contentType} is not supported");
        }

        $this->contentType = $contentType;
    }

    public function get($uri, $params = [], array $headers = [])
    {
        $id = DB::table('async_requests')
            ->insertGetId([
                'method' => 'GET',
                'content_type' => $this->contentType,
                'uri' => $uri,
                'status' => self::STATUS_WAITING,
                'params' => json_encode($params, JSON_PRETTY_PRINT),
                'headers' => json_encode($headers, JSON_PRETTY_PRINT),
                'created_at' => new \DateTime()
            ]);

        EventEmitter::instance()->emit('AsyncRequest', 'send');

        return $id;
    }

    public function post($uri, array $params = [], array $headers = [])
    {
        $id = DB::table('async_requests')
            ->insertGetId([
                'method' => 'POST',
                'content_type' => $this->contentType,
                'uri' => $uri,
                'status' => self::STATUS_WAITING,
                'params' => json_encode($params, JSON_PRETTY_PRINT),
                'headers' => json_encode($headers, JSON_PRETTY_PRINT),
                'created_at' => new \DateTime()
            ]);

        EventEmitter::instance()->emit('AsyncRequest', 'send');

        return $id;
    }
}
