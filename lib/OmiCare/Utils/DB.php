<?php

namespace OmiCare\Utils;

use App\Models\OmiAccount\OAInstitution;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Connection;

/**
 * @method static \Illuminate\Database\ConnectionInterface connection(string $name = null)
 * @method static string getDefaultConnection()
 * @method static void setDefaultConnection(string $name)
 * @method static \Illuminate\Database\Query\Builder table(string $table)
 * @method static \Illuminate\Database\Query\Expression raw($value)
 * @method static mixed selectOne(string $query, array $bindings = [])
 * @method static array select(string $query, array $bindings = [])
 * @method static bool insert(string $query, array $bindings = [])
 * @method static int update(string $query, array $bindings = [])
 * @method static int delete(string $query, array $bindings = [])
 * @method static bool statement(string $query, array $bindings = [])
 * @method static int affectingStatement(string $query, array $bindings = [])
 * @method static bool unprepared(string $query)
 * @method static array prepareBindings(array $bindings)
 * @method static mixed transaction(\Closure $callback, int $attempts = 1)
 * @method static void beginTransaction()
 * @method static void commit()
 * @method static void rollBack()
 * @method static int transactionLevel()
 * @method static array pretend(\Closure $callback)
 *
 * @see \Illuminate\Database\DatabaseManager
 * @see \Illuminate\Database\Connection
 */
class DB
{
    private static $db;
    private static $capsule;

    public static function __callStatic($name, $arguments)
    {
        return self::db()->{$name}(...$arguments);
    }

    public static function getConnection($name = null): Connection
    {
        self::db();

        return self::$capsule->getConnection($name);
    }

    public static function boot()
    {
        self::db();
    }

    public static function beginTransactionAll()
    {
        self::beginTransaction();
    }

    public static function commitAll()
    {
        self::commit();
    }

    public static function rollbackAll()
    {
        self::rollBack();
    }

    public static function getQueryLogAll()
    {
        $log1 = self::getConnection('default')->getQueryLog();

        return [
            'mysql' => [
                'count' => count($log1),
                'data' => $log1
            ]
        ];
    }

    private static function db()
    {
        if (self::$db) {
            return self::$db;
        }

        $capsule = new Capsule();

        $dbconfigs = config('db');

        foreach ($dbconfigs as $connectionName => $dbconfig) {

            $capsule->addConnection([
                'driver' => 'mysql',
                'host' => $dbconfig['host'],
                'database' => $dbconfig['database'],
                'username' => $dbconfig['username'],
                'password' => $dbconfig['password'],
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'strict' => false,
                'prefix' => '',
            ], $connectionName);
        }


        $capsule->bootEloquent();

        self::$capsule = $capsule;
        self::$db = $capsule->getConnection();
        self::$db->enableQueryLog();
        return self::$db;
    }
}
