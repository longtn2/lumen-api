<?php

namespace OmiCare\Utils;

class Cache
{
    private static $driver;
    private static $prefix;
    private static $driverName;

    /**
     * @return FileCache|\Redis
     */
    public static function driver()
    {
        if (self::$driver) {
            return self::$driver;
        }

        $driver = config('app')['cache_driver'];

        if ($driver === 'redis') {
            $redis = new \Redis();
            $config = config('redis')['default'];
            $redis->connect($config['host'], $config['port']);
            $redis->select($config['db']);
            self::$prefix = $config['prefix'];
            self::$driver = $redis;
        } elseif ($driver === 'file') {
            self::$prefix = '';
            self::$driver = new FileCache();
        } else {
            throw new \Error('Invalid cache driver');
        }

        return  self::$driver;
    }

    public static function get($key)
    {
        if (!config('app')['cache_enabled']) {
            return null;
        }

        $data = self::driver()->get(self::$prefix.$key);

        if (!$data) {
            return null;
        }

        return unserialize($data);
    }

    public static function set($key, $value, $ttl = 0)
    {
        if ($ttl === 0) {
            self::driver()->set(self::$prefix.$key, serialize($value));
        } else {
            self::driver()->setex(self::$prefix.$key, $ttl, serialize($value));
        }
    }

    public static function exists($key)
    {
        return self::driver()->exists(self::$prefix.$key);
    }

    public static function ttl($key)
    {
        return self::driver()->ttl(self::$prefix.$key);
    }

    public static function clearAll()
    {
        if (config('app')['cache_driver'] === 'redis') {
            $keys = self::driver()->keys(self::$prefix.'*');
            self::driver()->del($keys);
        } else {
            return self::driver()->flushDB();
        }
    }

    public static function remove($key)
    {
        return self::driver()->del(self::$prefix.$key);
    }
}
