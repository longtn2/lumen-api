<?php

namespace OmiCare\Utils;

use App\Models\Customer;
use App\Models\User;
use Firebase\JWT\JWT;
use OmiCare\Exception\Unauthorized;
use Firebase\JWT\Key;

class Auth
{
    /**
     * Throw exception if user does not logged in.
     *
     * @throws Unauthorized
     */
    public static function required()
    {
        if (!self::user()) {
            throw new Unauthorized();
        }
    }

    /**
     * @param mixed $usingCache
     * @param bool  $usingModel
     *
     * @return null|User
     */
    public static function user()
    {
        static $user;

        if ($user !== null) {
            return $user;
        }

        try {
            $token = self::getToken();

            if (!$token) {
                return null;
            }

            $decoded = JWT::decode($token,  new Key(env('JWT_SECRET'), 'HS256'));

            if (isset($decoded->user)) {
                $user = User::find($decoded->user->id);
                return $user;
            }

            $user = false;
            return $user;
        } catch (\Exception $e) {
            logfile_write($e);
        }

        return null;
    }

    public static function getToken()
    {
        static $token;

        if ($token) {
            return $token;
        }

        $req = request();
        $token = $req->get('_token');

        if ($token) {
            return $token;
        }

        $token = $req->header('X-Token');

        if ($token) {
            return $token;
        }

        $authorization = $req->header('Authorization');

        if (strpos($authorization, ',') !== false) {
            $parts = explode(',', $authorization);
            $map = [];

            foreach ($parts as $part) {
                $tmp = explode(' ', trim($part));
                $map[$tmp[0]] = $tmp[1];
            }

            return $map['Bearer'] ?? null;
        }
        $tmp = explode(' ', $authorization);

        if (count($tmp) === 1) {
            return null;
        }

        list($type, $token) = $tmp;

        if ($type !== 'Bearer') {
            return null;
        }

        return $token;
    }
}
