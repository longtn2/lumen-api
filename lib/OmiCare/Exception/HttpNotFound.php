<?php

namespace OmiCare\Exception;

class HttpNotFound extends HttpException
{
    public function __construct($message = "Page not found")
    {
        parent::__construct($message, 404);
    }
}
