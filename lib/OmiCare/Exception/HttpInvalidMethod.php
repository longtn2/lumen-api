<?php

namespace OmiCare\Exception;

use Throwable;

class HttpInvalidMethod extends HttpException
{
    public function __construct($message = "Method not allowed")
    {
        parent::__construct($message, 405);
    }
}
