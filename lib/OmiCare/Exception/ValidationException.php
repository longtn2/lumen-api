<?php

namespace OmiCare\Exception;

use OmiCare\Utils\V;

class ValidationException extends \Exception
{
    private $errors;

    /**
     * ValidationException constructor.
     *
     * @param string|V $v
     * @param string   $field
     */
    public function __construct($v, $field = null)
    {
        $allMessages = [];

        if ($v instanceof V) {
            $this->errors = $v->errors();
        } elseif (is_string($v)) {
            if (empty($field)) {
                throw new \InvalidArgumentException('Field must be string');
            }
            $this->errors = [
                $field => [$v]
            ];
        }

        foreach ($this->errors as $f => $messages) {
            array_push($allMessages, ...$messages);
        }

        $message = implode("\n", $allMessages);

        parent::__construct($message, 0, null);
    }

    public function getValidationErrors()
    {
        return $this->errors;
    }
}
