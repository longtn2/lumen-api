<?php

namespace OmiCare\Exception;

class HttpException extends \Exception
{
    private $httpCode = 503;

    public function __construct($message = "", $httpCode = 503)
    {
        $this->httpCode = $httpCode;
        parent::__construct($message);
    }

    public function getHttpCode()
    {
        return $this->httpCode;
    }
}
