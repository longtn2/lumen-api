<?php

namespace OmiCare\Console;

class ScheduleConfig
{
    private $crontabs = [];

    public function getCronTabs(): array
    {
        return $this->crontabs;
    }

    protected function cron(string $cron, string $command)
    {
        $this->crontabs[] = compact('cron', 'command');
    }

    protected function everyMinute(string $command)
    {
        $this->cron("* * * * *", $command);
    }

    protected function intervalMinutes(int $numberMinute, string $command)
    {
        if ($numberMinute === 1) {
            $this->everyMinute($command);
        } else {
            $this->cron("*/$numberMinute * * * *", $command);
        }
    }

    protected function dailyAt(string $time, string $command)
    {
        list($hour, $minute) = explode(':', $time);

        $this->cron("$minute $hour * * *", $command);
    }

    public function setUp() {

    }
}
