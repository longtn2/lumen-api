<?php

namespace OmiCare\Http;

use OmiCare\Exception\HttpInvalidMethod;
use OmiCare\Exception\Unauthorized;
use OmiCare\Utils\Auth;
use OmiCare\Utils\V;

/**
 * Simple Request Wrapper.
 */
class Request implements \Iterator, \JsonSerializable, \ArrayAccess
{
    private $_data;
    private $_query;
    private $position = 0;
    private $_method;   
    private $_headers;
    private $_args;
    private $_uri;

    public function __construct(array $args = [], array $data = [])
    {
        $this->_args = $args;
        $this->_query = $_GET;
        $this->_method = $_SERVER['REQUEST_METHOD'];
        $this->_uri = $_SERVER['REQUEST_URI'];
        $this->_headers = getallheaders();

        if ($this->_method === 'GET') {
            $this->_data = $_GET;
        } else {
            if (isset($_SERVER['CONTENT_TYPE']) && strtolower($_SERVER['CONTENT_TYPE']) === 'application/json') {
                $inputJSON = file_get_contents('php://input');
                $this->_data = json_decode($inputJSON, true); //convert JSON into array
            } else {
                $this->_data = $_POST;
            }
        }

        if (!empty($data)) {
            $this->_data = array_merge($this->_args, $data);
        }

        if (empty($this->_data)) {
            $this->_data = [];
        }
    }

    public function __get($name)
    {
        return $this->get($name);
    }

    public function __set($name, $value)
    {
        throw new \LogicException('Request is readonly');
    }

    public function __isset($name)
    {
        return isset($this->_data[$name]);
    }

    public function setArgs(array $args = [])
    {
        $this->_args = $args;
    }

    public function uri()
    {
        return $this->_uri;
    }

    /**
     * Gets query string value.
     *
     * @param $key
     * @param $default
     *
     * @return mixed
     */
    public function query($key, $default = null)
    {
        return $this->_query[$key] ?? $default;
    }

    /**
     * @param null $key
     * @param null $default
     *
     * @return array
     */
    public function queries()
    {
        return $this->_query;
    }

    /**
     * @param $index
     * @param null $default
     *
     * @return null|mixed
     */
    public function argument($index, $default = null)
    {
        return $this->_args[$index] ?? $default;
    }

    /**
     * @return array
     */
    public function arguments()
    {
        return $this->_args;
    }

    public function onlyPOST()
    {
        $this->restrictMethods('POST');
    }

    public function onlyGET()
    {
        $this->restrictMethods('GET');
    }

    /**
     * @param $method
     *
     * @throws HttpInvalidMethod
     */
    public function restrictMethods($method)
    {
        if (!$this->isMethod($method)) {
            throw new HttpInvalidMethod('Method not allowed');
        }
    }

    /**
     * Get request headers.
     *
     * @param $key
     * @param null $default
     *
     * @return null|mixed
     */
    public function header($key, $default = null)
    {
        return $this->_headers[$key] ?? $default;
    }

    public function headers()
    {
        return $this->_headers;
    }

    /**
     * @param string $method
     * @return bool
     */
    public function isMethod(string $method): bool
    {
        if (is_string($method)) {
            $method = strtoupper($method);

            if ($method === 'ANY') {
                return true;
            }

            return $this->_method === $method;
        }

        if (is_array($method)) {
            $methods = array_map('strtoupper', $method);

            return in_array($this->_method, $methods);
        }

        return false;
    }

    public function all()
    {
        return $this->_data;
    }

    /**
     * @param null $key
     * @param null $default
     *
     * @return null|mixed
     */
    public function get($key = null, $default = null)
    {
        if ($key === null) {
            return $this->_data;
        }

        return $this->_data[$key] ?? $default;
    }

    /**
     * Gets safe string.
     *
     * @param $key
     * @param int $limit
     *
     * @return false|string
     */
    public function getSafeString($key, $limit = 500)
    {
        $value = $this->get($key);
        $value = trim(strip_tags($value));

        return mb_substr($value, 0, $limit);
    }

    /**
     * @return mixed
     */
    public function method(): string
    {
        return $this->_method;
    }

    public function ip(): string
    {
        static $ip;

        if (isset($ip)) {
            return $ip;
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (!empty($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (!empty($_SERVER['HTTP_FORWARDED'])) {
            $ip = $_SERVER['HTTP_FORWARDED'];
        } elseif (!empty($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else {
            $ip = 'UNKNOWN';
        }

        return $ip;
    }

    public function getInt($key, $default = 0): int
    {
        return intval($this->get($key) ?? $default);
    }

    public function getFloat($key, $default = 0.0): float
    {
        return floatval($this->get($key) ?? $default);
    }

    public function toArray(): array
    {
        return $this->get();
    }

    public function getTrimmed($key, $default = ''): string
    {
        return trim($this->get($key, $default));
    }

    public function getExplodedString($key, $default = ''): array
    {
        $value = trim($this->get($key, $default));

        if (!$value) {
            return [];
        }

        return explode(',', $value);
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->_data[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->_data;
    }

    public function valid()
    {
        return isset($this->_data[$this->position]);
    }

    public function jsonSerialize()
    {
        return $this->_data;
    }

    public function offsetSet($offset, $value)
    {
        throw new \LogicException('Request is readonly');
    }

    public function offsetExists($offset)
    {
        return isset($this->_data[$offset]);
    }

    public function offsetUnset($offset)
    {
        throw new \LogicException('Request is readonly');
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function user()
    {
        return Auth::user();
    }

    public function validator(): V
    {
        return V::make($this);
    }

    public function only(array $keys): array
    {
        $ret = [];
        $params = $this->all();

        foreach ($keys as $key) {
            $ret[$key] = $params[$key] ?? null;
        }

        return $ret;
    }

    public function currentBranchId()
    {
        static $branchId;

        if ($branchId) {
            return $branchId;
        }

        $branchId =  $this->header('X-Branch-Id');

        return (int) $branchId;
    }

    public function getTmpCustomerId(): string
    {
        $tmpCustomerID = $this->header('X-Tmp-Customer-Id');

        if (strlen($tmpCustomerID) > 100) {
            $tmpCustomerID = substr($tmpCustomerID, 0, 100);
        }

        if (empty($tmpCustomerID)) {
            throw new Unauthorized('Missing customerID');
        }

        return $tmpCustomerID;
    }

    public function getPlatform(): string
    {
        if ($this->isIos()) {
            return 'ios';
        }

        if ($this->isAndroid()) {
            return 'android';
        }

        if ($this->isMobile()) {
            return 'mobile';
        }

        return 'desktop';
    }

    public function isMobile()
    {
        return preg_match(
            '/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\\.browser|up\\.link|webos|wos)/i',
            $_SERVER['HTTP_USER_AGENT']
        );
    }

    public function isIos(): bool
    {
        return preg_match('/CFNetwork/', $_SERVER['HTTP_USER_AGENT']) === 1;
    }

    public function isAndroid(): bool
    {
        return preg_match('/okhttp/', $_SERVER['HTTP_USER_AGENT']) === 1;
    }

    public function isOmiCare(): bool
    {
        $isOmiCare = false;

        if (preg_match('/okhttp|CFNetwork/', $_SERVER['HTTP_USER_AGENT'])) {
            $isOmiCare = true;
        }

        return $isOmiCare;
    }
}
