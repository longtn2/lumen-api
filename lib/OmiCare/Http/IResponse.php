<?php

namespace OmiCare\Http;

interface IResponse
{
    public function getData();

    public function getStatusCode();
}
