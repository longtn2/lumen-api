<?php

namespace OmiCare\Http;

/**
 * Simple Response Wrapper.
 */
class Response implements IResponse
{
    private $data;
    private $status;
    private $headers;

    public function __construct($data, $status = 200, $headers = [])
    {
        $this->data = $data;
        $this->headers = $headers ?? [];
        $this->status = $status;
    }

    public function withHeaders(array $headers)
    {
        $this->headers = $headers;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getStatusCode()
    {
        return $this->status;
    }

    public function send()
    {
        if ($this->status !== 200) {
            http_response_code($this->status);
        }

        if (!empty($this->headers)) {
            foreach ($this->headers as $key => $value) {
                header("{$key}:{$value}");
            }
        }

        if (is_scalar($this->data)) {
            echo $this->data;
        } else {
            header('Content-Type:application/json');
            echo json_encode($this->data);
        }
    }
}
