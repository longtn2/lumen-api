<?php

namespace OmiCare\Http;

class Redirect implements IResponse
{
    private $url;

    public function __construct($url, array $queries = [])
    {
        if (strpos($url, '?') === false) {
            if (!empty($queries)) {
                $this->url = $url.'?'.http_build_query($queries);
            } else {
                $this->url = $url;
            }
        } else {
            $this->url = $url.'&'.http_build_query($queries);
        }
    }

    public function getData()
    {
        return [
            'redirected' => $this->url
        ];
    }

    public function getStatusCode()
    {
        return 301;
    }

    public function send()
    {
        header('Location:'.$this->url);
    }
}
