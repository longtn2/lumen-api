<?php

namespace OmiCare;

use App\Console\ConsoleScheduleConfig;
use OmiCare\Utils\DB;
use Cron\CronExpression;

class Command
{
    private $argv;
    private $aliases = [];

    public function __construct($argv, array $aliases = [])
    {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->argv = $argv;
        $this->aliases = $aliases;
    }

    public function handle()
    {
        try {
            DB::boot();

            if (!isset($this->argv[1])) {
                throw new \Exception('Missing command. Example php cmd Hello');
            }

            $command = $this->argv[1];

            if ($command === 'schedule') {
                $this->handleSchedule();
                return;
            }

            if (isset($this->aliases[$command])) {
                $command = $this->aliases[$command];
            }

            $className = '\\App\\Console\\Commands\\'.$command;

            if (!class_exists($className)) {
                throw new \Exception("Class {$className} does not exist");
            }

            $args = [];

            for ($i = 2; $i < count($this->argv); ++$i) {
                $args[] = $this->argv[$i];
            }

            $instance = new $className();
            $instance->handle(...$args);
        } catch (\Exception $e) {
            echo $e->getMessage()."\n";
            echo $e->getTraceAsString()."\n";
        }
    }

    public function handleSchedule()
    {
        $config = new ConsoleScheduleConfig();
        $config->setUp();
        $cronTabs = $config->getCronTabs();
        $today = new \DateTime();


        foreach ($cronTabs as $cronTab) {
            $cron = new CronExpression($cronTab['cron']);
            if ($cron->isDue($today)) {
                $this->exec($cronTab['command']);
            }
        }

    }

    private function exec(string $command)
    {
        logfile_write("Exec $command", 'schedule');

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            echo shell_exec($command);
        } else {
            echo shell_exec("$command >/dev/null 2>/dev/null &");
        }
    }
}
