<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'v1'], function () use ($router) {
    $router->post('/auth/login', 'V1\AuthController@login');
    $router->post('/branch/show', 'V1\BranchesController@show');
    
    //using route with any method
    $router->addRoute(['GET','POST'],'/product/{action}', function($action) {
        return 'V1\ProductsController@'.$action;
    } );
    // $router->addRoute(['GET','POST'],'/product/show', 'V1\ProductsController@show');
    // $router->addRoute(['GET','POST'],'/product/redis-set', 'V1\ProductsController@redisSet');
    // $router->addRoute(['GET','POST'],'/product/redis-get', 'V1\ProductsController@redisGet');
    // $router->addRoute(['GET','POST'],'/product/redis-publish', 'V1\ProductsController@redisPublish');
    // $router->addRoute(['GET','POST'],'/product/redis-sub', 'V1\ProductsController@redisSub');
    // $router->addRoute(['GET','POST'],'/product/redis-pub-sub', 'V1\ProductsController@redisPubSub');

});

// $router->post('/auth/login', 'AuthController@login');
